import { path } from 'ramda'

import popoverConsts from '~/utils/popoverConsts'
import { objectDiff } from '~/utils/objectDiff'

function parseServerError({ e }) {
  const errors = path(['response', 'data'], e)

  const parsedErrors = {}

  for (const item in errors) {
    const isArray = Array.isArray(errors[item])

    parsedErrors[item] = isArray ? errors[item][0] : errors[item]
  }

  return parsedErrors
}

export const state = () => ({
  modalSession: null,
  initModalSession: null,
  modalSessionServerErrors: {},
  event: {},
  eventEditServerErrors: {},
  eventOrganizerPhonesError: false,
  eventOrganizerEmailsError: false,
})

export const getters = {
  MODAL_SESSION: state => state.modalSession,
  INIT_MODAL_SESSION: state => state.initModalSession,
  MODAL_SESSION_SERVER_ERRORS: state => state.modalSessionServerErrors,
  EVENT: state => state.event,
  EVENT_EDIT_SERVER_ERRORS: state => state.eventEditServerErrors,
  EVENT_ORGANIZER_PHONES_ERRORS: state => state.eventOrganizerPhonesError,
  EVENT_ORGANIZER_EMAILS_ERRORS: state => state.eventOrganizerEmailsError,
}

export const actions = {
  setEventOrganizerPhonesError({ commit }, hasError) {
    commit('EVENT_ORGANIZER_PHONES_ERRORS', hasError)
  },
  setEventOrganizerEmailsError({ commit }, hasError) {
    commit('EVENT_ORGANIZER_EMAILS_ERRORS', hasError)
  },
  setSession  ({ commit, dispatch }, { session }) {
    commit('SET_MODAL_SESSION', session)
    commit('SET_INIT_MODAL_SESSION', session)
  },
  openModalSession({ commit, dispatch }, { session }) {
    commit('SET_MODAL_SESSION', session)
    commit('SET_INIT_MODAL_SESSION', session)

    dispatch('popovers/TOGGLE_POPOVER', popoverConsts.editEventPlaceTime, {
      root: true,
    })
  },
  closeModalSession({ commit, dispatch }) {
    commit('SET_MODAL_SESSION', null)
    commit('SET_INIT_MODAL_SESSION', null)

    dispatch('popovers/TOGGLE_POPOVER', '', { root: true })
  },
  updateModalSession({ commit }, { session }) {
    session.results.map(el => {
      if(!el.end_date) {delete el.end_date}
      if(!el.end_time) {delete el.end_time}
      return el
    })
    commit('SET_MODAL_SESSION', session)
  },
  async saveModalSession({ getters, dispatch, commit }) {
    const { MODAL_SESSION, INIT_MODAL_SESSION } = getters

    const hasDiffArray = objectDiff(
      MODAL_SESSION,
      INIT_MODAL_SESSION,
      (path, key) => {
        if (path.join('.') === 'city') {
          return key !== 'id'
        } else if (path.join('.') === 'location') {
          return key !== 'id'
        }

        return false
      },
    )

    if (hasDiffArray) {
      const postData = hasDiffArray.reduce(
        (result, diff) => {
          const { path, lhs } = diff

          if (path[0] === 'results') {
            // sessions
            const resultIdx = path[1]
            const key = path[2]
            const id = MODAL_SESSION.results[resultIdx].id

            result.sessions[id] = {
              ...result.sessions[id],
              [key]: lhs,
            }
          } else if (path.join('.') === 'city.id') {
            // location city
            const locationId = INIT_MODAL_SESSION.location.id

            result.locations[locationId] = {
              ...result.locations[locationId],
              city: lhs,
            }
          } else if (path.join('.') === 'location.id') {
            // location main (title, address, geometry)
            const locationId = INIT_MODAL_SESSION.location.id

            const { title, address, geometry } = MODAL_SESSION.location

            result.locations[locationId] = {
              ...result.locations[locationId],
              title,
              address,
              geometry,
            }
          } else if (path.join('.') === 'location') {
            // location reset
            const locationId = INIT_MODAL_SESSION.location.id

            result.locations[locationId] = {
              ...result.locations[locationId],
              title: '',
              address: null,
              geometry: null,
            }
          } else if (path.join('.') === 'location') {
            // city reset
            const locationId = INIT_MODAL_SESSION.location.id

            result.locations[locationId] = {
              ...result.locations[locationId],
              city: null,
            }
          }

          return result
        },
        {
          sessions: {},
          locations: {},
        },
      )

      const hasUpdatedSessions = Object.keys(postData.sessions).length
      const hasUpdatedLocations = Object.keys(postData.locations).length
      let isCatch = false
      try {
        if (hasUpdatedSessions) {
          await dispatch('updateSessions', { sessions: postData.sessions })
        }

        if (hasUpdatedLocations) {
          await dispatch('updateLocations', { locations: postData.locations })
        }
      }
      catch (e) {
        isCatch = true
        commit('SET_MODAL_SESSION_ERRORS', parseServerError({ e }))
        throw e
      }
      finally {
        if(!isCatch) {
          commit('SET_INIT_MODAL_SESSION', MODAL_SESSION)
        }
      }
    }
  },
  updateSessions(_, { sessions }) {
    return Promise.all(
      Object.keys(sessions).map(sessionId => {
        return this.$axios.patch(
          `/events/sessions/${sessionId}/`,
          sessions[sessionId],
        )
      }),
    )
  },
  updateLocations(_, { locations }) {
    return Promise.all(
      Object.keys(locations).map(locationId => {
        return this.$axios.patch(
          `/locations/${locationId}/`,
          locations[locationId],
        )
      }),
    )
  },
  restoreModalSessionServerErrorrs({ commit }) {
    commit('SET_MODAL_SESSION_ERRORS', {})
  },
  async loadEventEdit({ commit, dispatch }, { id }) {
    try {
      const event = await dispatch(
        'event/FETCH',
        {
          id,
          params: {
            expand: 'type,tags,genres,genres.parent,category,cover',
          },
        },
        { root: true },
      )
      commit('SET_EVENT', event)

      await dispatch(
        'events/sessions/FETCH',
        {
          event: id,
          expand: 'location,location.city',
          page_size: 100,
        },
        { root: true },
      )

      if (!path(['permissions', 'can_edit'], event)) {
        return this.app.router.push({
          name: 'events-id',
          params: { access: false },
        })
      }
    } catch (e) {
      console.log(e)
      this.app.router.push('/')
    }
  },
  async eventEdit({ commit, dispatch }, payload) {
    try {
      const res = await this.$axios({
        method: 'put',
        url: `/events/${payload.id}/`,
        data: payload.data,
      })
      return res.data
    } catch (e) {
      commit('EVENT_EDIT_SERVER_ERRORS',  parseServerError({ e }))
      throw e
    }
  },
}

export const mutations = {
  SET_MODAL_SESSION(state, v) {
    state.modalSession = v
  },
  SET_INIT_MODAL_SESSION(state, v) {
    state.initModalSession = v
  },
  SET_MODAL_SESSION_ERRORS(state, v) {
    state.modalSessionServerErrors = v
  },
  SET_EVENT(state, v) {
    state.event = v
  },
  EVENT_EDIT_SERVER_ERRORS(state, v) {
    state.eventEditServerErrors = v
  },
  EVENT_ORGANIZER_PHONES_ERRORS(state, v) {
    state.eventOrganizerPhonesError = v
  },
  EVENT_ORGANIZER_EMAILS_ERRORS(state, v) {
    state.eventOrganizerEmailsError = v
  },
}

export const namespaced = true
