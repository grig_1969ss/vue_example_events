export const state = () => ({
  results: [],
})

export const getters = {
  RESULTS: state => state.results,
}

export const actions = {
  async FETCH({ commit, getters }, code) {
    const { data } = await this.$axios.get(`/coupons/${code}/`)
    commit('SET', data)
    return data
  },
}

export const mutations = {
  SET(state, data) {
    Object.assign(state, data)
  },
}

export const namespaced = true
