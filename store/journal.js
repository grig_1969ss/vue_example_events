import { journals } from '~/api'
import { prop, path, groupBy } from 'ramda'
import { getLatestModified } from '~/utils'
import { mapDatesData, deSerializeObjectDates } from '~/utils/event'

export const state = () => ({
  journalsList: null,
  currentJournal: null,
  journalEvents: [],
  similarJournals: null,
})

export const getters = {
  IS_OWN: (state, getters, rootGetters) => {
    if (state.currentJournal === null) return null
    return (
      path(['currentJournal', 'is_own'], state) ||
      prop('id', rootGetters['user/GET_USER']) ===
        path(['created_by', 'id'], getters.CURRENT_JOURNAL)
    )
  },
  JOURNALS_LIST: state => path(['journalsList', 'results'], state),
  JOURNALS_COUNT: state => path(['journalsList', 'count'], state) || 0,
  JOURNALS_PAGE_COUNT: state => path(['journalsList', 'num_pages'], state) || 1,
  CURRENT_JOURNAL: state => state.currentJournal,
  modified: state => path(['modified'], state.currentJournal),
  journalsModified: state =>
    getLatestModified(path(['journalsList', 'results'], state)),
  GET_JOURNAL_FIELD: state => key => {
    return state.currentJournal && state.currentJournal[key]
      ? state.currentJournal[key]
      : null
  },
  JOURNAL_GROUP_EVENTS: (_, getters) =>
    Object.values(groupBy(item => item.event.id, getters.JOURNAL_EVENTS)),
  GET_CREATOR_FULL_NAME: (state, getters) =>
    path(['currentJournal', 'created_by', 'name'], state) || '',
  /* events */
  JOURNAL_EVENTS_DATA: state => path(['journalEvents'], state),
  JOURNAL_EVENTS: state => {
    const result = path(['journalEvents', 'results'], state) || []
    return result.map(session => {
      let event = session.event
      if (typeof(event) === 'object' && event !== null){
        event = deSerializeObjectDates(event)
      }
      return deSerializeObjectDates(Object.assign(session, {event: event}))
    })
  },
  JOURNAL_EVENTS_COUNT: state =>
    path(['journalEvents', 'count'], state) ||
    path(['currentJournal', 'events', 'count'], state) ||
    0,
  HAS_JOURNAL_EVENTS: state =>
    path(['journalEvents', 'results', 'length'], state),
  HAS_MORE_JOURNAL_EVENTS: state => path(['journalEvents', 'next'], state),
  /* similar journals */
  SIMILAR_JOURNALS: state => path(['similarJournals', 'results'], state) || [],
  HAS_SIMILAR_JOURNALS: state =>
    path(['similarJournals', 'results', 'length'], state),
}

export const actions = {
  async CREATE_JOURNAL(_, p) {
    const res = await journals(this.$axios).post({ body: p })
    return res.data
  },
  async REMOVE_JOURNAL({ getters }, id) {
    const jid = id || getters.CURRENT_JOURNAL.id
    await journals(this.$axios).delete({ url: ':jid/', urlParams: { jid } })
  },
  async ADD_TO_JOURNAL({ getters, dispatch }, { id, type, params }) {
    const jid = id || getters.CURRENT_JOURNAL.id
    const { data } = await journals(this.$axios).post({
      url: ':jid/:type/add/',
      urlParams: { jid, type },
      body: params,
    })

    dispatch('notifications/success', data.detail, { root: true })
    return data
  },
  async REMOVE_FROM_JOURNAL({ dispatch, getters }, { type, params }) {
    const jid = getters.CURRENT_JOURNAL.id
    const { data } = await journals(this.$axios).post({
      url: ':jid/:type/remove/',
      urlParams: { jid, type },
      body: params,
    })

    dispatch('notifications/success', data.detail, { root: true })
    return data
  },
  async UPDATE_JOURNAL({ getters, commit }, params) {
    const jid = getters.CURRENT_JOURNAL.id
    const res = await journals(this.$axios).patch({
      url: ':jid/',
      urlParams: { jid },
      body: params,
    })
    commit('SET_CURRENT_JOURNAL', res.data)
    return res.data
  },
  async FETCH_JOURNALS({ commit }, params) {
    try {
      const answer = await this.$axios.get('/journals/', { params })
      const list = answer.data ? answer.data : null
      list && commit('SET_JOURNALS_LIST', list)
    } catch (err) {
      console.log(err)
      throw err
    }
  },
  CLEAR_JOURNALS_LIST({ commit }) {
    commit('SET_JOURNALS_LIST', [])
  },
  async GET_JOURNAL({ commit }, id) {
    try {
      const { data } = await this.$axios.get(`/journals/${id}/`, {
        params: {
          expand: 'cover,created_by,created_by.avatar',
        },
      })

      commit('SET_CURRENT_JOURNAL', data)

      return data
    } catch (err) {
      console.log('ACTION ERR, cant get journal', err)
      throw err
    }
  },
  async GET_JOURNAL_COMMENTS({ commit }, id) {
    try {
      const { data } = await this.$axios.get(`/journals/${id}/comments/`, {
        params: {
          comments_expand: 'created_by,created_by.avatar',
        },
      })
      commit('comments/SET', data, { root: true })
      return data
    } catch (err) {
      console.log(err)
      throw err
    }
  },
  async GET_JOURNAL_EVENTS({ commit, getters }, params = { page: 1 }) {
    const id = prop('id', getters.CURRENT_JOURNAL) || params.id

    try {
      const { data } = await this.$axios.get('/events/sessions/', {
        params: {
          journal: id,
          page: params.page,
          expand: 'event.cover',
          page_size: 50,
          ...params,
        },
      })

      commit('SET_JOURNAL_EVENTS', data)
    } catch (err) {
      console.log(err)
      throw err
    }
  },
  async GET_MORE_JOURNAL_EVENTS({ commit, getters }) {
    const { next } = getters.JOURNAL_EVENTS_DATA

    const { data } = await this.$axios.get(next)

    commit('SET_JOURNAL_EVENTS', {
      ...data,
      results: [...getters.JOURNAL_EVENTS, ...data.results],
    })
  },
  async SEND_COMMENT_JOURNAL({ dispatch, getters }, payload) {
    const check = await dispatch('user/CHECK_PERSONAL_DATA_AGREEMENT', null, {
      root: true,
    })
    if (!check) return

    const id = getters.CURRENT_JOURNAL.id
    const { data } = await this.$axios.post(`/journals/${id}/comment/`, payload)

    const comment = await dispatch(
      'comments/FETCH_BY_ID',
      { id: data.id },
      {
        root: true,
      },
    )
    await dispatch('comments/ADD', comment, { root: true })
  },
  async GET_SIMILAR_JOURNALS({ commit }, { id }) {
    const { data } = await this.$axios.get(`/journals/`, {
      params: {
        similar: id,
        expand: 'cover,created_by,created_by.avatar',
      },
    })

    commit('SET_SIMILAR_JOURNALS', data)

    return data
  },
}

export const mutations = {
  SAVE_JOURNAL_SEARCHED_EVENTS(state, events) {
    state.searchedEvents = events
  },
  SET_JOURNALS_LIST(state, list) {
    state.journalsList = list
  },
  SET_CURRENT_JOURNAL(state, journal) {
    state.currentJournal = journal
  },
  SET_JOURNAL_EVENTS(state, data) {
    const results = data.results || []
    results.map(session => {
      if (typeof(session.event) === 'object' && session.event !== null){
        session.event = mapDatesData(session.event)
      }
      return mapDatesData(session)
    })
    data.results = results
    state.journalEvents = data
  },
  SET_SIMILAR_JOURNALS(state, v) {
    state.similarJournals = v
  },
}

export const namespaced = true
