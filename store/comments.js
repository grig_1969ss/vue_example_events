export const state = () => ({
  count: 0,
  next: null,
  num_pages: 0,
  page_number: 1,
  page_size: 4,
  previous: null,
  results: [],
})

export const getters = {
  RESULTS: state => state.results,
  COUNT: state => state.count,
  IS_MORE: state => Boolean(state.next),
}

export const actions = {
  async FETCH_BY_ID(_, { id }) {
    const { data } = await this.$axios.get(`/comments/${id}/`, {
      params: {
        expand: 'created_by,created_by.avatar',
      },
    })

    return data
  },
  async FETCH({ commit, getters }, params) {
    const { data } = await this.$axios.get('/comments/', { params })
    commit('SET', data)
    return data
  },
  async FETCH_MORE({ commit, getters }, params) {
    const url = params.next ? params.next : '/comments/'

    const { data } = await this.$axios.get(url, {
      params: {
        expand: 'created_by,created_by.avatar',
        ...params,
      },
    })

    commit('SET', { results: getters.RESULTS.concat(data.results) })
    return data
  },
  async FETCH_NEXT({ commit, getters, state }, params) {
    const { data } = await this.$axios.get(state.next, { params })
    data.results = getters.RESULTS.concat(data.results)
    commit('SET', data)
  },
  ADD({ commit, getters }, data) {
    commit('SET', {
      results: getters.RESULTS.concat(data),
      count: data.parent ? getters.COUNT : getters.COUNT + 1,
    })
  },
  SET({ commit }, data) {
    commit('SET', data)
  },
}

export const mutations = {
  SET(state, data) {
    // data.results = uniqBy(item => item.id, data.results) // убрать, когда с апи перестанут приходить дубли
    Object.assign(state, data)
  },
}

export const namespaced = true
