export const state = () => ({
  loading: false,
  count: 0,
  next: null,
  num_pages: 0,
  page_number: 1,
  page_size: 4,
  previous: null,
  results: [],
})

export const getters = {
  results: state => state.results,
  moreResult: state => Boolean(state.next),
}

export const actions = {
  async FETCH({ commit, rootState }, params) {
    commit('CLEAR_RESULT')
    const expandParams = { expand: 'region,country' }
    const { data } = await this.$axios.get('/geo/cities/', {
      params: { ...expandParams, ...params },
      headers: { 'accept-language': rootState.lang.locale },
    })
    commit('SET', data)
    return data
  },
  async FETCH_MORE({ commit, getters, state }) {
    const { data } = await this.$axios.get(state.next)
    data.results = state.results.concat(data.results)
    commit('SET', data)
  },
}

export const mutations = {
  SET(state, data) {
    state = Object.assign(state, data)
  },
  CLEAR_RESULT(state) {
    state = {
      loading: false,
      count: 0,
      next: null,
      num_pages: 0,
      page_number: 1,
      page_size: 4,
      previous: null,
      results: [],
    }
  },
}

export const namespaced = true
