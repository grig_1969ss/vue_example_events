import { prop, isNil, isEmpty } from 'ramda'
import { saveAs } from 'file-saver'
import bannerConsts from '~/utils/bannerConsts'
import cookieConsts from '~/utils/cookieConsts'

const resetStore = {
  profile: null,
  organizer: null,
  counters: {
    events: 0,
    journals: 0,
    tickets: 0,
    marketplace_orders: 0,
  },
}

export const state = () => ({
  user: null,
  ...resetStore
})

export const getters = {
  WITH_RIGHTS: state =>
    prop('is_superuser', state.user) || prop('is_moderator', state.user),
  HAS_USER: state => !!state.user,
  GET_USER: state => state.user,
  PROFILE: state => state.profile,
  ORGANIZER: state => state.organizer,
  ORGANIZER_AS_PROFILE: (state, getters) => {
    const { ORGANIZER } = getters
    if (!ORGANIZER) return ORGANIZER
    const user = prop('user', ORGANIZER)

    if ( !isNil(user) && !isEmpty(user) ) {
      return user
    }

    const {
        title: name,
        logo: avatar,
        emails: additional_emails,
        ...organizer
      } = ORGANIZER
      return {
        name,
        avatar,
        additional_emails,
        is_organizer: true,
        is_customer: false,
        is_supplier: false,
        url_vk: null,
        url_fb: null,
        url_instagram: null,
        ...organizer
      }
  },
  IS_OWN_PROFILE: (state, getters) => {
    const { GET_USER, PROFILE } = getters

    const userId = prop('id', GET_USER)
    const profileId = prop('id', PROFILE)

    return !!(userId && profileId && userId === profileId)
  },

  COUNTERS: state => state.counters,

  PERSONAL_DATA_AGREEMENT: state => prop('personal_data_agreement', state.user),
}

const root = { root: true }

export const actions = {
  RESET_STORE({ commit }) {
    commit('RESET_STORE')
  },

  async GET_USER_DATA({ commit, getters }, { silent = false, ...params } = {}) {
    const { data } = await this.$axios.get('/auth/profile/', {
      params: { expand: 'avatar,city', ...params },
      silent,
    })
    commit('SET_USER_DATA', data)
    return getters.GET_USER
  },

  async GET_PROFILE({ commit, getters }, { id, ...params } = {}) {
    const { data } = await this.$axios.get(`/users/${id}/`, {
      params: { expand: 'avatar', ...params },
    })
    commit('SET_PROFILE', data)
    return getters.PROFILE
  },

  async GET_ORGANIZER({ commit, getters }, { id, ...params } = {}) {
    const { data } = await this.$axios.get(`/organizers/${id}/`, {
      params: { expand: 'logo', ...params },
    })
    commit('SET_ORGANIZER', data)

    return getters.ORGANIZER
  },

  async UPDATE_USER({ commit, getters }, payload) {
    try {
      const { data } = await this.$axios.patch('/auth/profile/', payload, {
        params: {expand: 'avatar,city'}
      })
      commit('SET_USER_DATA', data)
      return data
    } catch (exception) {
      const errors =
        exception.response && exception.response.data
          ? exception.response.data
          : null
      throw errors
    }
  },

  async PARTIAL_UPDATE_USER({ commit, getters, dispatch }, { key, value }) {
    try {
      const payload = { [key]: value }
      await this.$axios.patch('/auth/profile/', payload)
      dispatch('GET_USER_DATA')
    } catch (exception) {
      const errors =
        exception.response && exception.response.data
          ? exception.response.data
          : null
      throw errors
    }
  },

  async REMOVE_USER({ commit, dispatch }) {
    await this.$axios.delete('/auth/profile/')
    commit('SET_USER_DATA', null)
    await dispatch('auth/CLEAR_USER_DATA', null, root)
  },

  async SET_PERSONAL_DATA_AGREEMENT({ commit, dispatch }, v) {
    commit('SET_PERSONAL_DATA_AGREEMENT', v)

    try {
      await dispatch('UPDATE_USER', {
        personal_data_agreement: v,
      })
    } catch (e) {
      commit('SET_PERSONAL_DATA_AGREEMENT', !v)
      throw e
    }
  },

  async DOWNLOAD_PERSONAL_DATA(_, format) {
    const { data } = await this.$axios.post('/auth/profile/full/', {
      method: format,
    })

    const fileName = `personal-data.${format}`
    const blob = new Blob([data], { type: `text/${format}` })

    saveAs(blob, fileName)
  },

  async CHECK_PERSONAL_DATA_AGREEMENT({ getters, dispatch }) {
    const { PERSONAL_DATA_AGREEMENT } = getters

    const isBoolean = typeof PERSONAL_DATA_AGREEMENT === 'boolean'

    if (!(isBoolean && PERSONAL_DATA_AGREEMENT)) {
      await dispatch('popovers/TOGGLE_BANNER', '', root)
      await dispatch(
        'popovers/TOGGLE_BANNER',
        bannerConsts.agreementBanner,
        root,
      )
      return false
    }

    return true
  },

  async CHECK_COOKIE_IS_ACCEPTED({ dispatch }) {
    const isCookieAccepted = this.$cookies.get(cookieConsts.isCookieAccepted)
    if (isCookieAccepted) return true

    await dispatch('popovers/TOGGLE_BANNER', '', root)
    await dispatch('popovers/TOGGLE_BANNER', bannerConsts.cookieBanner, root)
    return false
  },

  REQUEST(ctx, data = {}) {
    const id = ctx.getters.ORGANIZER.id || 0
    const from_url = window.location
      ? window.location.href
      : process.env.domainUrl + this.$router.currentRoute.fullPath

    return this.$axios.$post(`organizers/${id}/request/?from_url=${from_url}`, {
      ...data,
      from_url,
    })
  },

  SEND_ORGANIZER_REQUEST(ctx, { path, id, ...payload }) {
    const from_url = window.location
      ? window.location.href
      : process.env.domainUrl + this.$router.currentRoute.fullPath

    return this.$axios.post(
      `organizers/${id}/request/?from_url=${from_url}`,
      payload,
    )
  },
}

export const mutations = {
  RESET_STORE (state){
    state = Object.assign(state, resetStore)
  },
  SET_USER_DATA(state, userData) {
    state.user = userData
  },
  SET_PROFILE(state, profile) {
    state.profile = profile
  },
  SET_ORGANIZER(state, organizer) {
    state.organizer = organizer
  },
  SET_PERSONAL_DATA_AGREEMENT(state, v) {
    state.user.personal_data_agreement = v
  },
  SET_COUNTERS(state, counters = {}) {
    const mergedCounters = { ...state.counters, ...counters }

    Object.keys(mergedCounters).forEach(key => {
      const val = mergedCounters[key]
      const isPositiveNumber = typeof val === 'number' && !isNaN(val) && val > 0

      if (!isPositiveNumber) mergedCounters[key] = 0
    })

    state.counters = mergedCounters
  },
}

export const namespaced = true
