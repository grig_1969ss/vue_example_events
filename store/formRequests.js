export const state = () => ({})

export const getters = {}

export const actions = {
  async MAIL_PICKING(
    { commit, dispatch, getters },
    { message, email } = {},
  ) {
    const from_url = window.location
      ? window.location.href
      : process.env.domainUrl + this.$router.currentRoute.fullPath
    try {
      await this.$axios.post('/requests/', {
        action: 11,
        message,
        email,
        from_url
      })
    } catch (exception) {
      throw exception.response && exception.response.data
        ? exception.response.data
        : null
    }
  },
  async ORDER_CALL(
    { commit, dispatch, getters },
    { name, phone, email } = {},
  ) {
    const from_url = window.location
      ? window.location.href
      : process.env.domainUrl + this.$router.currentRoute.fullPath

    try {
      await this.$axios.post('/requests/', {
        action: 11,
        name,
        phone,
        email,
        from_url
      })
    } catch (exception) {
      throw exception.response && exception.response.data
        ? exception.response.data
        : null
    }
  },
}

export const mutations = {}
