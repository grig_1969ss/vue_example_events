import { path, prop } from 'ramda'

export const state = () => ({
  loading: false,
  post: null,
})

export const getters = {
  LOADING: state => state.loading,
  modified: state => path(['modified'], state.post),
  POST: state => state.post,
  POST_URL: state =>
    [prop('id', state.post), prop('slug', state.post)].join('-'),
}

export const actions = {
  async FETCH({ commit, getters }, id) {
    if (getters.LOADING) return
    commit('LOADING', true)
    try {
      const { data } = await this.$axios.get(`/posts/${id}/`)
      commit('SET', data)
      commit('comments/SET', data.comments, { root: true })
    } finally {
      commit('LOADING', false)
    }
  },
  async CREATE({ commit, getters }, payload) {
    if (getters.LOADING) return
    commit('LOADING', true)
    try {
      const { data } = await this.$axios.post('/posts/', payload)
      commit('SET', data)
      return data
    } finally {
      commit('LOADING', false)
    }
  },
  async DELETE({ commit, getters }, id) {
    if (getters.LOADING) return
    commit('LOADING', true)
    try {
      await this.$axios.delete(`/posts/${id}/`)
    } finally {
      commit('LOADING', false)
    }
  },
  async UPDATE({ commit, getters }, { id, payload }) {
    if (getters.LOADING) return
    commit('LOADING', true)
    try {
      const { data } = await this.$axios.put(`/posts/${id}/`, payload)
      commit('SET', data)
      return data
    } finally {
      commit('LOADING', false)
    }
  },
  async ADD_EVENTS(_, { id, events }) {
    return this.$axios.post(`/posts/${id}/events/add/`, { events: events })
  },
  async REMOVE_EVENTS(_, { id, events }) {
    try {
      await this.$axios.post(`/posts/${id}/events/remove/`, {
        events: events,
      })
    } catch (e) {
      console.log(e)
    }
  },
  async ADD_PLACES(_, { id, institutions, connect_events = false }) {
    try {
      return await this.$axios.post(`/posts/${id}/institutions/add/`, {
        institutions,
        connect_events,
      })
    } catch (e) {
      console.log(e)
    }
  },
  async REMOVE_PLACES({ commit, getters }, { id, institutions }) {
    try {
      await this.$axios.post(`/posts/${id}/institutions/remove/`, {
        institutions: institutions,
      })
    } catch (e) {
      console.log(e)
    }
  },
  async SEND_COMMENT({ dispatch, getters }, payload) {
    const check = await dispatch('user/CHECK_PERSONAL_DATA_AGREEMENT', null, {
      root: true,
    })
    if (!check) return

    const id = getters.POST.id
    const { data } = await this.$axios.post(`/posts/${id}/comment/`, payload)
    dispatch('comments/ADD', data, { root: true })
  },
  RESET_POST({ commit }) {
    commit('SET', null)
  },
}

export const mutations = {
  LOADING(state, value) {
    state.loading = value
  },
  SET(state, data) {
    state.post = data
  },
}

export const namespaced = true
