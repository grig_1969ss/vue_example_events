import { parse, format } from 'date-fns'
import { dissoc } from 'ramda'

import { preFormatEvent } from '~/utils/eventVersionB'

// function getTimeZoneName(timezone) {
//     return typeof timezone === 'string' ? timezone : timezone.name
// }

const event = () => ({
    title: '',
    links: [],
    period: [null],
    isOnline: false,
    location: null,
    address: '',
    cover: null,
    tags: [],
    emails: '',
    promoPlan: null,
    created: true
})

export const state = () => ({
    event: event(),
    serverErrors: {},
    croppedCoverImage: null,
})

export const getters = {
    event: state => state.event,
    serverErrors: state => state.serverErrors,
    croppedCoverImage: state => state.croppedCoverImage,
}

export const actions = {
    SET_FIELD({ commit }, payload) {
        commit('SAVE_EVENT_FIELD', payload)

        if (payload.validations) {
            payload.validations.map(v => {
                commit('RESTORE_SERVER_ERROR_BY_KEY', v)
            })
        } else {
            commit('RESTORE_SERVER_ERROR_BY_KEY', payload.key)
        }
    },
    async CREATE_EVENT({ state, commit }) {
        // const { event } = state
        const event = this.$cookies.get('eventForCreateTestBC')
        try {
            const start = event.period[0]
                ? parse(event.period[0], `yyyy-MM-dd'T'HH:mm`, new Date())
                : null

            const sessions = [
                {
                    start_date: start ? format(start, 'yyyy-MM-dd') : null,
                },
            ]

            const postData = preFormatEvent({
                event,
                sessions,
                isCreate: true,
            })

            const { data } = await this.$axios.post('/events/', postData)

            commit('RESTORE_EVENT')

            return data
        } catch (e) {
            console.log(e)

            const responseError = e.response.data

            const parsedErrors = {}

            for (const item in responseError) {
                const isArray = Array.isArray(responseError[item])

                parsedErrors[item] = isArray
                    ? responseError[item][0]
                    : responseError[item]
            }

            commit('SET_SERVER_ERRORS', parsedErrors)
            throw e
        }
    },
    SET_CROPPED_COVER_IMAGE({ commit }, v) {
        commit('SET_CROPPED_COVER_IMAGE', v)
    },
}

export const mutations = {
    SAVE_EVENT_FIELD(state, { key, value }) {
        state.event[key] = value
    },
    SET_SERVER_ERRORS(state, v) {
        state.serverErrors = v
    },
    SET_CROPPED_COVER_IMAGE(state, v) {
        state.croppedCoverImage = v
    },
    RESTORE_SERVER_ERROR_BY_KEY(state, v) {
        state.serverErrors = dissoc(v, state.serverErrors)
    },
    RESTORE_EVENT(state) {
        state.event = event()
    },
}
