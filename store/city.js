import { prop, path, type } from 'ramda'
import cityByIP from '~/utils/geoLite2'

const NEW_YORK_ID = 5128581
const USA_ID = 6252001
const RUSSIA_ID = 2017370
const countryIdForHide = [
  587116, // Азербайджан
  174982, // Армения
  630336, // Белорусия
  1522867, // Казахстан
  1527747, // Киргизия
  617790, // Молдавия
  2017370, // Россия
  1220409, // Таджикистан
  1512440, // Узбекистан
  1218197, // Туркмения
  690791, // Украина
  614540, // Грузия
]

export const state = () => ({
  city: {
    id: 0,
  },
  fromCountryId: null,
  geo: null,
  geoCity: null,
  userCity: null,
  userAcceptCity: false,
  hasGeoLoaded: false,
  geoLoader: false,
  isGeoProcessed: false,
  cityChangedInPopover: false,
  cityChangedFromTitle: false,
  finishCityChange: false,
})

const whichCity = state => {
  if (state.hasGeoLoaded
    && !state.cityChangedFromTitle
    && !state.cityChangedInPopover) return state.geoCity

  return state.city
}

const whichUserCity = state => {
  if (state.userCity === null
    || (state.cityChangedInPopover && state.finishCityChange)) return state.city

  return state.userCity
}

export const getters = {
  GEO_LOADER: state => state.geoLoader,
  GET_CITY: state => whichCity(state),
  GET_GEO_CITY: state => state.geoCity,
  GET_CITY_ID: state => prop('id', whichCity(state)),
  GEO_HAS_LOADED: state => state.hasGeoLoaded,
  IS_DIFFERENT_CITY: state =>
    prop('id', state.geoCity) !== prop('id', state.city),
  IS_ACCEPTED_GEO_CITY: state => state.userAcceptCity,
  GET_CITY_URL_ID: state => prop('url_id', whichCity(state)),
  GET_CITY_DECL: state => locale => {
    const loc = locale === 'ru' ? 'ru_ru' : 'en_us'
    return (
      path(['translations', loc, 'name_declination'], whichCity(state)) ||
      path(['translations', loc, 'name'], whichCity(state)) ||
      prop('name_declination', whichCity(state))
    )
  },
  GET_CITY_NAME: state => locale => {
    const loc = locale === 'ru' ? 'ru_ru' : 'en_us'
    return (
      path(['translations', loc, 'name'], whichCity(state)) ||
      prop('name', whichCity(state))
    )
  },
  COUNTRY_ID: state => state.fromCountryId,
  // GET_CITY_DESCRIPTION: state => locale => {
  //   const loc = locale === 'ru' ? 'ru_ru' : 'en_us'

  //   return (
  //     path(['translations', loc, 'content'], whichCity(state)) ||
  //     prop('description', whichCity(state))
  //   )
  // },
  SHOW_RUSSIAN_LANG: state => countryIdForHide.includes(state.fromCountryId),
  IS_GEO_PROCESSED: state => state.isGeoProcessed,
  CITY_CHANGED_IN_POPOVER: state => state.cityChangedInPopover,
  CITY_CHANGED_FROM_TITLE: state => state.cityChangedFromTitle,
  GET_USER_CITY: state => whichUserCity(state),
}

export const actions = {
  async FETCH_CITY(_, { id }) {
    try {
      const { data } = await this.$axios.get(`/geo/cities/${id}/`)
      return data
    } catch (e) {
      console.log(e)
    }
  },
  async GET_CITY({ state, commit, getters, dispatch }, id) {
    const countryId = path(['country'], getters.GET_CITY)
    try {
      const { data } = await this.$axios.get(`/geo/cities/${id}/`, {
        params: { expand: 'cover' },
      })
      commit('SET_CITY', data)
      if (state.cityChangedInPopover) {
        commit('SET_USER_CITY', data)
        commit('SET_FINISH_CITY_CHANGE', true)
      }
      const countryNewId = data.country
      if (type(countryId) !== 'Undefined' && countryId !== countryNewId) {
        await dispatch('country/FETCH', countryNewId, { root: true })
      }
      return data
    } catch (err) {
      return new Error(err)
    }
  },
  async GET_USER_CITY({ commit }, id) {
    try {
      const { data } = await this.$axios.get(`/geo/cities/${id}/`, {
        params: { expand: 'cover' },
      })

      commit('SET_USER_CITY', data)
    } catch (err) {
      return new Error(err)
    }
  },
  async GEO_CITY({ commit, state }, params) {
    try {
      const res = await this.$axios.get('/geo/cities/get-city/', { params })
      commit('SET_GEO_CITY', res.data)
      commit('SET_GEO_LOADER', false)
      return res.data
    } catch (err) {
      console.error(err)
      throw err
    }
  },
  SAVE_GEO({ commit }, geo) {
    commit('SET_GEO', geo)
  },
  async ACCEPT_CITY({ commit, state, dispatch }, date) {
    const cityId = state.hasGeoLoaded && !state.cityChangedInPopover
      ? state.geoCity.id
      : state.city.id

    if (state.hasGeoLoaded) {
      this.$cookies.setIfAccepted('geoCityId', cityId, {
        maxAge: 60 * 60 * 24 * 30,
      })
    }

    commit('CHANGE_CITY', true)
    this.$cookies.set('userSelectedCity', cityId, { maxAge: 1.21e6, path: '/' })
    this.$cookies.set('citySelectionDate', date, { maxAge: 1.21e6, path: '/' })
    commit('FINISH_CITY_DETECTING', false)
  },
  async GET_CITY_BY_IP({ commit, getters, dispatch }, ip) {
    try {
      const { city, country } = await cityByIP(ip)
      await dispatch('country/FETCH', country.geonameId || USA_ID, {
        root: true,
      })
      const defaultCity = prop('0', getters['country/TOP_CITIES'])
      await dispatch('GET_CITY', city.geonameId || defaultCity.id)
    } catch (e) {
      await dispatch('GET_CITY', NEW_YORK_ID)
      await dispatch('country/FETCH', USA_ID, { root: true })
    }
  },
  async INITIAL_CITY({ commit, getters, dispatch }, { ip, id }) {
    if (Number(id)) {
      await dispatch('GET_CITY', id)
      const countryId = path(['country'], getters.GET_CITY)
      await dispatch('country/FETCH', countryId, { root: true })
    } else {
      await dispatch('GET_CITY_BY_IP', ip)
    }
    return getters.GET_CITY_ID
  },
  async GET_ORIGIN_COUNTRY({ commit, getters, state }, { ip, id }) {
    if (Number(id)) {
      commit('SET_ORIGIN_COUNTRY', ip === '127.0.0.1' ? RUSSIA_ID : id)
    } else {
      try {
        const { country } = await cityByIP(ip)
        commit('SET_ORIGIN_COUNTRY', country.geonameId)
      } catch (e) {
        commit('SET_ORIGIN_COUNTRY', ip === '127.0.0.1' ? RUSSIA_ID : USA_ID)
      }
    }
  },
}

export const mutations = {
  SET_GEO_LOADER(state, p) {
    state.geoLoader = p
  },
  SET_CITY(state, city) {
    state.city = city
    this.$cookies.setIfAccepted('cityId', city.id, {
      maxAge: 60 * 60 * 24 * 30,
      path: '/',
    })
  },
  SET_GEO_CITY(state, city) {
    state.geoCity = city
    state.hasGeoLoaded = true
  },
  SET_ORIGIN_COUNTRY(state, country) {
    state.fromCountryId = Number(country)
    this.$cookies.setIfAccepted('fromCountryId', country, {
      maxAge: 60 * 60 * 24 * 30,
      path: '/',
    })
  },
  SET_GEO(state, geo) {
    state.geo = geo
  },
  CHANGE_CITY(state, p) {
    state.userAcceptCity = p
  },
  FINISH_CITY_DETECTING(state, p) {
    state.isGeoProcessed = p
  },
  CHANGE_CITY_IN_POPOVER(state, p) {
    state.cityChangedInPopover = p
  },
  CHANGE_CITY_FROM_TITLE(state, p) {
    state.cityChangedFromTitle = p
  },
  SET_USER_CITY(state, p) {
    state.userCity = p
  },
  SET_FINISH_CITY_CHANGE(state, p) {
    state.finishCityChange = p
  },
}

export const namespaced = true
