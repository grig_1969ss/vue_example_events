export const state = () => ({
  next: null,
  count: 0,
  results: [],
})

export const getters = {
  users: state => state.results,
  count: state => state.count,
  more_users: state => Boolean(state.next),
}

export const actions = {
  async FETCH({ commit, getters }, params) {
    commit('set', {
      count: 0,
      next: null,
      previous: null,
      results: [],
    })
    const { data } = await this.$axios.get('/users/', { params })
    commit('set', data)
    return data
  },
  async FETCH_MORE({ commit, getters, state }, payload) {
    const url = state.next
    const { data } = await this.$axios.get(url)
    data.results = state.results.concat(data.results)
    commit('set', data)
  },
}

export const mutations = {
  set(state, data) {
    Object.assign(state, data)
  },
}

export const namespaced = true
