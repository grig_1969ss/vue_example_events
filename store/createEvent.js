import { parse, format } from 'date-fns'
import { dissoc } from 'ramda'

import { preFormatEvent } from '~/utils/event'

function getTimeZoneName(timezone) {
  return typeof timezone === 'string' ? timezone : timezone.name
}

const event = () => ({
  title: '',
  description: '',
  links: [],
  phones: [],
  emails: [],
  period: [null, null],
  isOnline: false,
  timezone: null,
  city: null,
  location: null,
  language: null,
  gender: 2,
  age: [20, 80],
  cover: null,
  tags: [],
  newTags: [],
  genres: [],
  category: null,
  eventType: null,
  promoPlan: null,
  promoStartDate: '',
  promoLanguages: [],
  promoEmail: '',
  promoCoupon: '',
  promoCustomerSite: '',
})

export const state = () => ({
  event: event(),
  serverErrors: {},
  croppedCoverImage: null,
})

export const getters = {
  event: state => state.event,
  serverErrors: state => state.serverErrors,
  croppedCoverImage: state => state.croppedCoverImage,
}

export const actions = {
  SET_FIELD({ commit }, payload) {
    commit('SAVE_EVENT_FIELD', payload)

    if (payload.validations) {
      payload.validations.map(v => {
        commit('RESTORE_SERVER_ERROR_BY_KEY', v)
      })
    } else {
      commit('RESTORE_SERVER_ERROR_BY_KEY', payload.key)
    }
  },
  async CREATE_EVENT({ state, commit }) {
    const { event } = state
    try {
      const start = event.period[0]
        ? parse(event.period[0], `yyyy-MM-dd'T'HH:mm`, new Date())
        : null
      const end = event.period[1]
        ? parse(event.period[1], `yyyy-MM-dd'T'HH:mm`, new Date())
        : null

      const sessions = [
        {
          start_date: start ? format(start, 'yyyy-MM-dd') : null,
          start_time: start ? format(start, 'HH:mm') : null,
          end_date: end ? format(end, 'yyyy-MM-dd') : null,
          end_time: end ? format(end, 'HH:mm') : null,
          timezone: getTimeZoneName(event.timezone),
          weekdays: [1, 2, 3, 4, 5, 6, 7],
        },
      ]

      const postData = preFormatEvent({
        event,
        sessions,
        isCreate: true,
      })

      const { data } = await this.$axios.post('/events/', postData)

      commit('RESTORE_EVENT')

      return data
    } catch (e) {
      console.log(e)

      const responseError = e.response.data

      const parsedErrors = {}

      for (const item in responseError) {
        const isArray = Array.isArray(responseError[item])

        parsedErrors[item] = isArray
          ? responseError[item][0]
          : responseError[item]
      }

      commit('SET_SERVER_ERRORS', parsedErrors)
      throw e
    }
  },
  SET_CROPPED_COVER_IMAGE({ commit }, v) {
    commit('SET_CROPPED_COVER_IMAGE', v)
  },
}

export const mutations = {
  SAVE_EVENT_FIELD(state, { key, value }) {
    state.event[key] = value
  },
  SET_SERVER_ERRORS(state, v) {
    state.serverErrors = v
  },
  SET_CROPPED_COVER_IMAGE(state, v) {
    state.croppedCoverImage = v
  },
  RESTORE_SERVER_ERROR_BY_KEY(state, v) {
    state.serverErrors = dissoc(v, state.serverErrors)
  },
  RESTORE_EVENT(state) {
    state.event = event()
  },
}
