import {
  trackUserRegistration,
  trackUserFastRegistration,
} from '~/utils/analytics'

export const state = () => ({
  token: null,
  timerId: null,
  timerInterval: 1500000, // 25 minutes
  isSimple: false,
  newRegistrationData: {},
  newRegistrationErrors: {},
})

export const getters = {
  IS_AUTH: state => Boolean(state.token),
  TOKEN: state => state.token,
  IS_TIMER_SET: state => Boolean(state.timerId),
  TIMER_ID: state => state.timerId,
  TIMER_INTERVAL: state => state.timerInterval,
  NEW_REGISTRATION_DATA: state => state.newRegistrationData,
  NEW_REGISTRATION_ERRORS: state => state.newRegistrationErrors,
}

const root = { root: true }

export const actions = {
  NEW_REGISTRATION_DATA({ commit, state }, payload){
    const data = payload
    commit('SET_NEW_REGISTRATION_DATA', {...state.newRegistrationData, ...data})
  },
  async NEW_REGISTRATION({ commit, dispatch, getters }) {
     await dispatch('REGISTRATION', getters.NEW_REGISTRATION_DATA)
  },
  async LOG_IN({ commit, dispatch }, payload) {
    try {
      const { data } = await this.$axios.post('/auth/login/', payload)
      await dispatch('SET_UP_TOKEN', data.token)
      dispatch('START_REFRESHER')
    } catch (exception) {
      throw exception.response && exception.response.data
        ? exception.response.data
        : null
    }
  },

  async SOCIAL_LOG_IN({ dispatch }, payload) {
    const { data } = await this.$axios.post('/oauth/login/social/jwt/', payload)
    await dispatch('SET_UP_TOKEN', data.token)
    dispatch('START_REFRESHER')
  },

  async SET_UP_TOKEN({ commit, dispatch }, token) {
    try {
      commit('SET_TOKEN', token)
      dispatch('SET_AUTH_COOKIE', { token })
      this.$axios.setAuthHeader(token)
    } catch (err) {
      console.log('invalid token, clear data...')
      console.log(err)
      dispatch('CLEAR_USER_DATA')
    }
  },

  SET_AUTH_COOKIE(_, { token, age }) {
    if (token) {
      this.$cookies.set(
        'auth',
        { token },
        {
          maxAge: age || 36000,
          path: '/',
        },
      )
    } else {
      this.$cookies.remove('auth')
    }
  },

  async CLEAR_USER_DATA({ commit, dispatch }) {
    commit('SET_TOKEN', null)
    await dispatch('SET_AUTH_COOKIE', { token: null })
    this.$axios.setAuthHeader(null)
    dispatch('CLEAR_REFRESH_TIMER')
    commit('user/SET_USER_DATA', null, root)
    commit('popovers/TOGGLE_BANNER', null, root)
  },

  async REFRESH({ dispatch, getters }) {
    const { data } = await this.$axios
      .post('/auth/token/refresh/', {
        token: getters.TOKEN,
      })
      .catch(err => {
        console.log(err)
      })
    await dispatch('SET_UP_TOKEN', data.token)
    dispatch('START_REFRESHER')
  },

  VERIFY_TOKEN({ getters }) {
    const { TOKEN }  = getters
    return this.$axios.post('/auth/token/verify/', {
      token: TOKEN,
    })
  },

  async LOG_OUT({ dispatch, commit }) {
    commit('SET_SIMPLE', false)
    try {
      await this.$axios.post('/auth/logout/')
    } catch (err) {
      throw err
    } finally {
      dispatch('CLEAR_USER_DATA')
    }
  },

  async REGISTRATION({ commit, dispatch, getters }, payload) {
    try {
      const { data } = await this.$axios.post(
        '/auth/signup/',
        { ...payload },
        { params: { expand: 'avatar,city' } }
      )
      await dispatch('SET_UP_TOKEN', data.token)
      dispatch('START_REFRESHER')
      commit('user/SET_USER_DATA', data.user, root)
      trackUserRegistration(this)
    } catch (exception) {
      commit('SET_NEW_REGISTRATION_ERRORS', exception.response.data)
      throw exception.response && exception.response.data
        ? exception.response.data
        : null
    }
  },

  async START_REFRESHER({ commit, dispatch, getters }) {
    // commit('SET_SIMPLE', false)
    const refresher = async () => {
      try {
        await dispatch('REFRESH')
      } catch (e) {
        dispatch('CLEAR_USER_DATA')
      }
    }
    if (getters.TIMER_ID) {
      dispatch('CLEAR_REFRESH_TIMER')
    }
    const newTimerId = setInterval(refresher, getters.TIMER_INTERVAL)
    commit('SET_TIMER_ID', newTimerId)
  },

  CLEAR_REFRESH_TIMER({ commit, dispatch, getters }) {
    if (getters.IS_TIMER_SET) {
      clearInterval(getters.TIMER_ID)
      commit('SET_TIMER_ID', null)
    }
  },

  async RESET_PASSWORD(context, payload) {
    try {
      await this.$axios.post('/auth/password/reset/', payload)
    } catch (exception) {
      throw exception.response && exception.response.data
        ? exception.response.data
        : null
    }
  },

  async CONFIRM_PASSWORD({ dispatch }, payload) {
    try {
      const { data } = await this.$axios.post(
        '/auth/password/reset/confirm/',
        payload,
      )
      await dispatch('SET_UP_TOKEN', data.token)
      dispatch('START_REFRESHER')
    } catch (exception) {
      throw exception.response && exception.response.data
        ? exception.response.data
        : null
    }
  },

  async CHANGE_PASSWORD(context, payload) {
    await this.$axios.post('/auth/password/change/', payload)
  },

  SET_TEMP_TOKEN({ commit, dispatch }, token) {
    commit('SET_TOKEN', token)
    commit('SET_SIMPLE', true)
    dispatch('SET_AUTH_COOKIE', { token, age: 3600 })
    this.$axios.setAuthHeader(token)
  },

  async SIMPLE_REGISTRATION(
    { commit, dispatch, getters },
    { is_organizer = true, is_company = false, ...payload } = {},
  ) {
    try {
      const { data } = await this.$axios.post(
        '/auth/signup/',
        {
          ...payload,
          is_organizer,
          is_company,
          simple: true,
          personal_data_agreement: true,
        },
        { params: { expand: 'avatar,city' } }
      )
      await dispatch('SET_TEMP_TOKEN', data.token)
      commit('user/SET_USER_DATA', data.user, root)
      trackUserFastRegistration(this)
    } catch (exception) {
      throw exception.response && exception.response.data
        ? exception.response.data
        : null
    }
  },
  async COMPLETE_SIMPLE_REGISTRATION({ dispatch, commit }, payload) {
    try {
      const res = await this.$axios.post('/auth/password/set/', payload)
      commit('SET_SIMPLE', false)
      await dispatch('SET_UP_TOKEN', res.data.token)
      dispatch('START_REFRESHER')
      return res.data
    } catch (err) {
      throw err.response && err.response.data ? err.response.data : null
    }
  },
}

export const mutations = {
  SET_TOKEN(state, token) {
    state.token = token
  },
  SET_SIMPLE(state, t) {
    state.isSimple = t
    if (!this.$cookies.get('simple')) {
      this.$cookies.setIfAccepted('simple', t, {
        maxAge: 3600,
        path: '/',
      })
    }
  },
  SET_TIMER_ID(state, id) {
    state.timerId = id
  },
  SET_NEW_REGISTRATION_DATA(state, v) {
    state.newRegistrationData = v
  },
  SET_NEW_REGISTRATION_ERRORS(state, v) {
    state.newRegistrationErrors = v
  },
}

export const namespaced = true
