import { prop, isNil, path } from 'ramda'

const withToken = s =>
  path(['auth', 'token'], s)
    ? { Authorization: `JWT ${path(['auth', 'token'], s)}` }
    : {}

const initList = {
  count: 0,
  next: null,
  previous: null,
  num_pages: 0,
  page_number: 1,
  results: [],
}

const initState = {
  supplier: {},
  customer: {},
  orders: {
    ...initList
  },
  orders_initialized: false,
  orders_loading: false,
  orders_filters: [],

  replies: {
    ...initList
  },
  replies_initialized: false,
  replies_init_count: 0,
  replies_loading: false,
  replies_filters: [],
}

export const state = () => ({
  ...initState
})

export const getters = {
  SUPPLIER: state => state.supplier,
  CUSTOMER: state => state.customer,

  ORDERS: state => prop('results', state.orders) || [],
  ORDERS_COUNT: state => prop('count', state.orders) || 0,
  ORDERS_HAS_MORE: state => Boolean(prop('next', state.orders)),
  ORDERS_PAGE: state => prop('page_number', state.orders),
  ORDERS_INITIALIZED: state => state.orders_initialized,
  ORDERS_LOADING: state => state.orders_loading,
  ORDERS_FILTERS: state => state.orders_filters,

  REPLIES: state => prop('results', state.replies) || [],
  REPLIES_COUNT: state => prop('count', state.replies) || 0,
  REPLIES_HAS_MORE: state => Boolean(prop('next', state.replies)),
  REPLIES_PAGE: state => prop('page_number', state.replies),
  REPLIES_INITIALIZED: state => state.replies_initialized,
  REPLIES_LOADING: state => state.replies_loading,
  REPLIES_FILTERS: state => state.replies_filters,
}

export const actions = {
  RESET_STORE({ commit }){
    commit('RESET_STORE')
  },

  RESET_ORDERS({ commit }){
    commit('SET_ORDERS', { ...initList })
  },

  RESET_REPLIES({ commit }){
    commit('SET_REPLIES', { ...initList })
  },

  async FETCH_USER({ commit, getters }, { user_type, id, ...params }) {
    if (!['customers', 'suppliers'].includes(user_type)){
      throw new Error(`${user_type} is wrong user_type value`)
    }
    const { data } = await this.$axios.get(`/marketplace/${user_type}/${id}/`, { params })
    commit('SET_USER', {user_type, data})
    return data
  },

  async FETCH_ORDERS_NEXT({ commit, dispatch, getters }, { ...params }) {
    if ( !getters.ORDERS_HAS_MORE){
      return { ...initList }
    }
    return dispatch('FETCH_ORDERS', { ...params, page: getters.ORDERS_PAGE + 1 })
  },

  async FETCH_ORDERS({ commit, getters, rootGetters }, { ...params }) {
    if (getters.ORDERS_LOADING) return
    try {
      commit('SET_ORDERS_LOADING', true)
      const { data } = await this.$axios.get(`/marketplace/orders/`, {
        params: {expand: 'city,customer,chosen_supplier,type', ...params}
      })
      const copy = Object.assign({}, data);
      copy.results = getters.ORDERS.concat(copy.results)
      commit('SET_ORDERS', copy)

      const COUNTERS = rootGetters['user/COUNTERS']
      if ( COUNTERS.marketplace_orders < getters.ORDERS_COUNT ){
        commit('user/SET_COUNTERS', {marketplace_orders: getters.ORDERS_COUNT}, { root: true })
      }
      if (!getters.ORDERS_INITIALIZED){
        commit('SET_ORDERS_INITIALIZED', true )
      }
      return data
    } catch (err) {
      throw err
    } finally {
      commit('SET_ORDERS_LOADING', false)
    }
  },

  async FETCH_REPLIES_NEXT({ commit, dispatch, getters }, { ...params }) {
    if ( !getters.REPLIES_HAS_MORE){
      return { ...initList }
    }
    return dispatch('FETCH_REPLIES', { ...params, page: getters.REPLIES_PAGE + 1 })
  },

  async FETCH_REPLIES({ commit, getters }, { ...params }) {
    if (getters.REPLIES_LOADING) return
    try {
      commit('SET_REPLIES_LOADING', true)
      const { data } = await this.$axios.get(`/marketplace/replies/`, {
        params: {
          expand: 'order.city,order.customer,order.chosen_supplier,order.type',
          ...params
        }
      })
      const copy = Object.assign({}, data);
      copy.results = getters.REPLIES.concat(copy.results)
      commit('SET_REPLIES', copy)
      if (!getters.REPLIES_INITIALIZED){
        commit('SET_REPLIES_INITIALIZED', {
          value:true, count:getters.ORDERS_COUNT
        })
      }
      return data
    } catch (err) {
      throw err
    } finally {
      commit('SET_REPLIES_LOADING', false)
    }
  },
  async UPDATE_SUPPLIER({ getters, dispatch, rootState }, {id , payload}) {
    try {
      const res = await this.$axios({
        method: 'patch',
        url: `/marketplace/suppliers/${id}/`,
        data: { ...payload },
        headers: withToken(rootState),
      })
      return res.data

    } catch (exception) {
      const errors =
        exception.response && exception.response.data
          ? exception.response.data
          : null
      throw errors
    }
  },

  async UPDATE_CUSTOMER({ getters, dispatch, rootState }, {id, payload}) {
    try {
      const res = await this.$axios({
        method: 'patch',
        url: `/marketplace/customers/${id}/`,
        data: payload,
        headers: withToken(rootState),
      })
      return res.data

    } catch (exception) {
      const errors =
        exception.response && exception.response.data
          ? exception.response.data
          : null
      throw errors
    }
  },
}

export const mutations = {
  SET_USER(state, {user_type, data}) {
    if ( user_type === 'customers' ){
      state.customer = data
    } else if ( user_type === 'suppliers' ) {
      state.supplier = data
    }
  },
  SET_ORDERS(state, data) {
    state.orders = data
  },
  SET_ORDERS_FILTERS(state, data){
    state.orders_filters = data
  },
  SET_ORDERS_LOADING(state, value){
    state.orders_loading = value
  },
  SET_ORDERS_INITIALIZED(state, value){
    state.orders_initialized = value
  },

  SET_REPLIES(state, data) {
    state.replies = data
  },
  SET_REPLIES_LOADING(state, value){
    state.replies_loading = value
  },
  SET_REPLIES_FILTERS(state, data){
    state.replies_filters = data
  },
  SET_REPLIES_INITIALIZED(state, { value, count }){
    state.replies_initialized = value
    if (!isNil(count)){
      state.replies_init_count = count
    }
  },
  RESET_STORE(state){
    state = Object.assign(state, initState)
  }

}

export const namespaced = true
