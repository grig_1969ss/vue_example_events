import { isEmpty, path, prop, is, isNil } from 'ramda'

function getPostData(reply) {
  return {
    message: reply.message,
    order: reply.order,
    proposed_price: reply.proposed_price
      ? {'amount': reply.proposed_price, 'currency': 'RUB'}
      : null,
  }
}

function updateServerError(newData, oldData, serverErrors){
  const newServerErrors = {}
  for (const [key, value] of Object.entries(newData)) {
    if (oldData[key] !== value) {
      if (serverErrors[key]) {
        newServerErrors[key] = ''
      }
    }
  }
  return newServerErrors
}

function parseServerErrors(exception){
  const responseError = path(['response', 'data'], exception)
  if ( !responseError ) {
    console.error(exception)
    throw exception
  }

  const parsedErrors = {}
  for (const item in responseError) {
    const isArray = Array.isArray(responseError[item])

    parsedErrors[item] = isArray
      ? responseError[item][0]
      : responseError[item]
  }

  return parsedErrors
}

export const state = () => ({
  selectedReply: null,
  loading: false,
  newReply: {},
  serverErrors: {},
  replyCreated: false,
  deletedReply: null,
  editReply: {},
  comments: {},
  commentsLoading: false
})

export const getters = {
  LOADING: state => state.loading,
  NEW_REPLY: state => state.newReply,
  SERVER_ERRORS: state => state.serverErrors,
  REPLY_CREATED: state => state.replyCreated,
  DELETED_REPLY: state => state.deletedReply,
  SELECTED_REPLY: state => state.selectedReply,
  EDIT_REPLY: state => state.editReply,
  COMMENTS: state => state.comments.results || [],
  COMMENTS_COUNT: state => state.comments.count || 0,
  COMMENTS_HAS_MORE: state => !!state.comments.next,
  COMMENTS_NEXT: state => state.comments.next,
  COMMENTS_LOADING: state => state.commentsLoading

}

export const actions = {
  RESET_COMMENTS({ commit }){
     commit('SET_COMMENTS', {})
  },
  async FETCH_COMMENTS({ commit, getters }, { id }) {
    if ( getters.COMMENTS_LOADING ) return
    commit('SET_COMMENTS_LOADING', true)
    const { data } = await this.$axios.get(`/marketplace/replies/${id}/comments/`, {
      params: { comments_expand: 'created_by.avatar' },
    })
    commit('SET_COMMENTS_LOADING', false)
    commit('SET_COMMENTS', data)
  },

  async FETCH_COMMENTS_NEXT({ commit, getters, state }, params) {
    const { COMMENTS_NEXT, COMMENTS, COMMENTS_LOADING } = getters
    if ( COMMENTS_LOADING ) return
    if ( !COMMENTS_NEXT ) return

    commit('SET_COMMENTS_LOADING', true)
    const { data } = await this.$axios.get(COMMENTS_NEXT, { params })
    data.results = COMMENTS.concat(data.results)

    commit('SET_COMMENTS_LOADING', false)
    commit('SET_COMMENTS', data)
  },
  async FETCH_COMMENTS_MORE({ commit, getters }, params) {
    const { COMMENTS, COMMENTS_LOADING } = getters
    if ( COMMENTS_LOADING ) return
    commit('SET_COMMENTS_LOADING', true)
    const url = params.next ? params.next : '/comments/'

    const { data } = await this.$axios.get(url, {
      params: {
        expand: 'created_by.avatar',
        ...params,
      },
    })
    data.results = COMMENTS.concat(data.results)

    commit('SET_COMMENTS_LOADING', false)
    commit('SET_COMMENTS', data)
    return data
  },

  ADD_COMMENT({ commit, getters }, data) {
    const { COMMENTS, COMMENTS_COUNT } = getters
    commit('SET_COMMENTS', {
      ...COMMENTS,
      results: COMMENTS.concat(data),
      count: data.parent
        ? COMMENTS_COUNT
        : COMMENTS_COUNT + 1,
    })
  },
  async SEND_COMMENT({ commit, dispatch, getters, rootGetters },
                     { id, payload }) {
    if ( getters.COMMENTS_LOADING ) return

    const check = await dispatch('user/CHECK_PERSONAL_DATA_AGREEMENT', null, {
      root: true,
    })
    if (!check) return
    commit('SET_COMMENTS_LOADING', true)

    try {
      const { data } = await this.$axios({
        method: 'post',
        url: `/marketplace/replies/${id}/comment/`,
        params: { comments_expand: 'created_by.avatar' },
        data: payload,
      })
      dispatch(
        'ADD_COMMENT',
        { ...data, created_by: rootGetters['user/GET_USER'] },
      )
    } catch (e) {
      commit('SET_COMMENTS_LOADING', false)
      console.error(e)
    } finally {
      commit('SET_COMMENTS_LOADING', false)
    }
  },

  UPDATE_NEW_REPLY({ commit, getters }, payload) {
    commit('SET_REPLY_CREATED', false)
    const { NEW_REPLY, SERVER_ERRORS } = getters
    const newServerErrors = updateServerError(payload, NEW_REPLY, SERVER_ERRORS)
    const updated = Object.assign(NEW_REPLY, payload)
    commit('SET_NEW_REPLY', updated)
    if (!isEmpty(newServerErrors)){
      commit('SET_SERVER_ERRORS', newServerErrors)
    }
  },

  UPDATE_EDIT_REPLY({ commit, getters }, payload) {
    const { EDIT_REPLY, SERVER_ERRORS } = getters
    const newServerErrors = updateServerError(
      payload, EDIT_REPLY, SERVER_ERRORS
    )

    const updated = Object.assign(EDIT_REPLY, payload)
    commit('SET_EDIT_REPLY', updated)
    if (!isEmpty(newServerErrors)){
      commit('SET_SERVER_ERRORS', newServerErrors)
    }
  },

  async CREATE_REPLY({ commit, getters }) {
    if ( getters.LOADING )
      return
    const { NEW_REPLY } = getters
    try {
      commit('SET_LOADING', true)
      const postData = getPostData(NEW_REPLY)
      const { data } = await this.$axios.post(
        '/marketplace/replies/', postData
      )

      commit('SET_NEW_REPLY', {})
      commit('SET_SERVER_ERRORS', {})
      commit('SET_REPLY_CREATED', true)

      return data
    } catch (e) {
      commit('SET_LOADING', false)
      const parsedErrors = parseServerErrors(e)
      commit('SET_SERVER_ERRORS', parsedErrors)
    } finally {
      commit('SET_LOADING', false)
    }
  },

  RESET_SERVER_ERRORS( {commit} ){
    commit('SET_SERVER_ERRORS', {})
  },

  async SELECT_REPLY({ commit, dispatch, getters, rootGetters }, { ...reply }) {
    const { LOADING } = getters
    if ( LOADING )
      return
    commit('SET_SELECTED_REPLY', null)
    commit('SET_LOADING', true)
    try {
      await this.$axios.post(
        `/marketplace/replies/${reply.id}/accept/`
      )
      commit('SET_SELECTED_REPLY', reply.id)
      const orderId = path(['order', 'id'], reply) || prop('order', reply)
      if ( orderId )
        dispatch(
          'marketplace/order/FETCH_ORDER',
          { id: orderId },
          { root: true }
        )
      return true
    } catch (e) {
      commit('SET_LOADING', false)
      throw e
    } finally {
      commit('SET_LOADING', false)
    }
  },
  async UPDATE_REPLY({ commit, getters }, { id }) {
    if ( getters.LOADING )
      return
    const { EDIT_REPLY } = getters
    try {
      commit('SET_LOADING', true)
      const postData = getPostData(EDIT_REPLY)
      const config = {
        params: {
          expand: 'created_by,created_by.avatar'
        }
      }
      const { data } = await this.$axios.patch(
        `/marketplace/replies/${id}/`, postData, config
      )
      commit('SET_EDIT_REPLY', data)
      commit('SET_SERVER_ERRORS', {})

      return data
    } catch (e) {
      commit('SET_LOADING', false)
      const parsedErrors = parseServerErrors(e)
      commit('SET_SERVER_ERRORS', parsedErrors)
    } finally {
      commit('SET_LOADING', false)
    }
  },
  async DELETE_REPLY({ commit, getters }, { id }) {
    try {
      await this.$axios.delete(`/marketplace/replies/${id}/`)
      commit('SET_DELETED_REPLY', id)
      return true
    } catch (err) {
      console.log('error during removal')
      throw err
    }
  },
}

export const mutations = {
  SET_LOADING(state, value) {
    state.loading = value
  },
  SET_SELECTED_REPLY(state, value) {
    state.selectedReply = value
  },
  SET_REPLY_CREATED(state, value){
    state.replyCreated = value
  },
  SET_NEW_REPLY(state, data) {
    state.newReply = Object.assign({}, data)
  },
  SET_SERVER_ERRORS(state, data) {
    state.serverErrors = Object.assign({}, data)
  },
  SET_DELETED_REPLY(state, value){
    state.deletedReply = value
  },
  SET_EDIT_REPLY(state, data){
    if ( data.hasOwnProperty('proposed_price') ){
      let proposed_price = prop('proposed_price', data)
      data.proposed_price = isNil(proposed_price) || isEmpty(proposed_price)
        ? null
        : is(Object, proposed_price)
          ? proposed_price.amount
          : proposed_price
    }

    state.editReply = Object.assign(
      {},
      data
    )
  },
  SET_COMMENTS(state, data) {
    state.comments = Object.assign({}, data)
  },
  SET_COMMENTS_LOADING(state, value){
    state.commentsLoading = value
  }
}

export const namespaced = true