import { prop } from "ramda";

const initList = {
  count: 0,
  next: null,
  previous: null,
  num_pages: 0,
  page_number: 1,
  results: [],
}

const initState = {
  order_types: {
    ...initList
  },
  all_order_types: [],
  order_types_initialized: false,
  order_types_loading: false,

  orders: {
    ...initList
  },
  orders_initialized: false,
  orders_loading: false,
  orders_filters: [],

  replies: {
    ...initList
  },
  replies_loading: false,

}

export const state = () => ({
  ...initState
})

export const getters = {
  ORDER_TYPES: state => prop('results', state.order_types) || [],
  ORDER_TYPES_COUNT: state => prop('count', state.order_types) || 0,
  ORDER_TYPES_HAS_MORE: state => Boolean(prop('next', state.order_types)),
  ORDER_TYPES_PAGE: state => prop('page_number', state.order_types),
  ORDER_TYPES_INITIALIZED: state => state.order_types_initialized,
  ORDER_TYPES_LOADING: state => state.order_types_loading,
  ALL_ORDER_TYPES: state => state.all_order_types || [],

  ORDERS: state => prop('results', state.orders) || [],
  ORDERS_COUNT: state => prop('count', state.orders) || 0,
  ORDERS_HAS_MORE: state => Boolean(prop('next', state.orders)),
  ORDERS_PAGE: state => prop('page_number', state.orders),
  ORDERS_INITIALIZED: state => state.orders_initialized,
  ORDERS_LOADING: state => state.orders_loading,
  ORDERS_FILTERS: state => state.orders_filters,

  REPLIES: state => prop('results', state.replies) || [],
  REPLIES_COUNT: state => prop('count', state.replies) || 0,
  REPLIES_HAS_MORE: state => Boolean(prop('next', state.replies)),
  REPLIES_PAGE: state => prop('page_number', state.replies),
  REPLIES_LOADING: state => state.replies_loading,
}

export const actions = {

  RESET_STORE({ commit }){
    commit('RESET_STORE')
  },

  RESET_ORDERS({ commit }){
    commit('SET_ORDERS', { ...initList })
    commit('SET_ORDERS_LOADING', false)
  },

  RESET_REPLIES({ commit }){
    commit('SET_REPLIES', { ...initList })
    commit('SET_REPLIES_LOADING', false)
  },

  async FETCH_ALL_ORDER_TYPES({ commit, dispatch, getters }){
    await dispatch('FETCH_ORDER_TYPES')
    while (getters.ORDER_TYPES_HAS_MORE) {
      await dispatch('FETCH_ORDER_TYPES_NEXT')
    }
    commit('SET_ORDER_ALL_TYPES', getters.ORDER_TYPES)
    commit('SET_ORDER_TYPES_INITIALIZED', true )
  },

  async FETCH_ORDER_TYPES_NEXT({ commit, dispatch, getters }, { ...params }) {
    if ( !getters.ORDER_TYPES_HAS_MORE){
      return {...initList}
    }
    return dispatch('FETCH_ORDER_TYPES', {...params, page: getters.ORDER_TYPES_PAGE + 1 })
  },

  async FETCH_ORDER_TYPES({ commit, getters, rootGetters }, { ...params }) {
    if (getters.ORDER_TYPES_LOADING) return
    try {
      commit('SET_ORDER_TYPES_LOADING', true)
      const { data } = await this.$axios.get(`/marketplace/orders/types/`, {
        params: {expand: 'form', ...params}
      })
      const copy = Object.assign({}, data);
      copy.results = getters.ORDER_TYPES.concat(copy.results)
      commit('SET_ORDER_TYPES', copy)

      return data
    } catch (err) {
      throw err
    } finally {
      commit('SET_ORDER_TYPES_LOADING', false)
    }
  },

  async FETCH_ORDERS_NEXT({ commit, dispatch, getters }, { ...params }) {
    if ( !getters.ORDERS_HAS_MORE){
      return {...initList}
    }
    return dispatch('FETCH_ORDERS', { ...params, page: getters.ORDERS_PAGE + 1 })
  },

  async FETCH_ORDERS({ commit, getters, rootGetters }, { ...params }) {
    if (getters.ORDERS_LOADING) return
    try {
      commit('SET_ORDERS_LOADING', true)
      const { data } = await this.$axios.get(`/marketplace/orders/`, {
        params: {expand: 'city,customer,chosen_supplier,type', ...params}
      })
      const copy = Object.assign({}, data);
      copy.results = getters.ORDERS.concat(copy.results)
      commit('SET_ORDERS', copy)
      if (!getters.ORDERS_INITIALIZED){
        commit('SET_ORDERS_INITIALIZED', true )
      }
      return data
    } catch (err) {
      throw err
    } finally {
      commit('SET_ORDERS_LOADING', false)
    }
  },

  async FETCH_REPLIES_NEXT({ commit, dispatch, getters }, { ...params }) {
    if ( !getters.REPLIES_HAS_MORE) {
      return {...initList}
    }
    return dispatch('REPLIES_ORDERS', { ...params, page: getters.ORDERS_PAGE + 1 })
  },

  async FETCH_REPLIES({ commit, getters, rootGetters }, { ...params }) {
    if (getters.REPLIES_LOADING) return
    try {
      commit('SET_REPLIES_LOADING', true)
      const { data } = await this.$axios.get(`/marketplace/replies/`, {
        params: { expand: 'created_by,created_by.avatar', ...params }
      })
      const copy = Object.assign({}, data);
      copy.results = getters.REPLIES.concat(copy.results)
      commit('SET_REPLIES', copy)
      return data
    } catch (err) {
      throw err
    } finally {
      commit('SET_REPLIES_LOADING', false)
    }
  },

}

export const mutations = {
  RESET_STORE(state){
    state = Object.assign(state, initState)
  },
  SET_ORDER_TYPES(state, data) {
    state.order_types = data
  },
  SET_ORDER_ALL_TYPES(state, data) {
    state.all_order_types = data
  },
  SET_ORDER_TYPES_LOADING(state, value){
    state.order_types_loading = value
  },
  SET_ORDER_TYPES_INITIALIZED(state, value){
    state.order_types_initialized = value
  },
  SET_ORDERS(state, data) {
    state.orders = data
  },
  SET_ORDERS_FILTERS(state, data){
    state.orders_filters = data
  },
  SET_ORDERS_LOADING(state, value){
    state.orders_loading = value
  },
  SET_ORDERS_INITIALIZED(state, value){
    state.orders_initialized = value
  },
  SET_REPLIES(state, data) {
    state.replies = data
  },
  SET_REPLIES_LOADING(state, value){
    state.replies_loading = value
  },
}

export const namespaced = true