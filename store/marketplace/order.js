import { isEmpty, path, is, isNil } from 'ramda'
import { format } from 'date-fns'

function getPostData(order) {
  const postData = {
    title: order.title,
    city: path(['city', 'id'], order),
    type: order.type,
    price_type: order.price_type,
    price: order.price
      ? {'amount': order.price, 'currency': 'RUB'}
      : null,
    price_interval_from: order.price_interval_from
      ? {'amount': order.price_interval_from, 'currency': 'RUB'}
      : null,
    price_interval_to: order.price_interval_to
      ? {'amount': order.price_interval_to, 'currency': 'RUB'}
      : null,
    description: order.description,
    data: {}
  }
  const defaultFields = Object.keys(postData)
  for (const [key, val] of Object.entries(order)){
    if (defaultFields.includes(key)) continue
    if (is(Date, val)) {
      postData.data[key] = val ? format(val, 'yyyy-MM-dd'): null
    } else if (is(Object, val)) {
      if ( val.value ){
        postData.data[key] = val.value
      } else {
        postData.data[key] = {}
        for (const [_k, _v] of Object.entries(val)) {
          if (is(Date, _v)) {
            postData.data[key][_k] = _v ? format(_v, 'yyyy-MM-dd'): null
          } else {
            postData.data[key][_k] = _v
          }
        }
      }
    } else {
      postData.data[key] = val
    }
  }
  return postData
}

function updateServerError(newData, oldData, serverErrors){
  const newServerErrors = {}
  for (const [key, value] of Object.entries(newData)) {
    if (oldData[key] !== value) {
      if (serverErrors[key]) {
        newServerErrors[key] = ''
        if (key === 'price_type') {
          const price_fields = [
            'price', 'price_interval_from', 'price_interval_to'
          ]
          price_fields.forEach(item => {
            newServerErrors[key] = ''
          })
        }
      }
    }
  }
  return newServerErrors
}

function parseServerErrors(exception){
  const responseError = exception.response.data

  const parsedErrors = {}
  for (const item in responseError) {
    const isArray = Array.isArray(responseError[item])

    parsedErrors[item] = isArray
      ? responseError[item][0]
      : responseError[item]
  }

  return parsedErrors
}

export const state = () => ({
  newOrder: {},
  editOrder: {},
  orderCreated: false,
  serverErrors: {},
  order: null,
  loading: false,
  deletedOrder: null,
  files: null,
  filesErrors: {},
  newFiles: []
})

export const getters = {
  NEW_ORDER: state => state.newOrder,
  EDIT_ORDER: state => state.editOrder,
  SERVER_ERRORS: state => state.serverErrors,
  ORDER_CREATED: state => state.orderCreated,
  ORDER: state => state.order,
  LOADING: state => state.loading,
  DELETED_ORDER: state => state.deletedOrder,
  FILES: state => state.files,
  FILES_ERRORS: state => state.filesErrors,
  NEW_FILES: state => state.newFiles,
}

export const actions = {
  SET_NEW_ORDER({ commit, getters }, payload) {
    commit('SET_ORDER_CREATED', false)
    const { NEW_ORDER, SERVER_ERRORS } = getters
    const newServerErrors = updateServerError(payload, NEW_ORDER, SERVER_ERRORS)
    commit('SET_NEW_ORDER', payload)
    if (!isEmpty(newServerErrors)){
      commit('SET_SERVER_ERRORS', newServerErrors)
    }
  },

  SET_EDIT_ORDER({ commit, getters }, payload) {
    const { EDIT_ORDER, SERVER_ERRORS } = getters
    const newServerErrors = updateServerError(
      payload, EDIT_ORDER, SERVER_ERRORS
    )
    commit('SET_EDIT_ORDER', payload)
    if (!isEmpty(newServerErrors)){
      commit('SET_SERVER_ERRORS', newServerErrors)
    }
  },
  async ADD_FILES({ commit, getters, state }, payload) {
    try {
      const { FILES } = getters
      const results =[]
      FILES.map(file => {
        const formData = new FormData()
        formData.append('order', payload)
        formData.append('file', file)
        results.push( this.$axios.post('marketplace/orders/files/', formData))
      })
      return await Promise.all(results)
        .then(e => {
          state.newFiles = []
          e.map(item => {
            commit('SET_NEW_FILE', [item.data])
          })
        })
    } catch (e) {
      const parsedErrors = parseServerErrors(e)
      commit('SET_FILES_ERRORS', parsedErrors)
    }
  },
  async DELETE_FILE({ commit, getters }, payload) {
    try {
      const deleted  = await this.$axios.delete(`/marketplace/orders/files/${payload}/`)
      return deleted
    } catch (e) {
      console.log(e)
    }
  },
  async CREATE_ORDER({ commit, getters }) {
    if ( getters.LOADING )
      return
    const { NEW_ORDER } = getters
    try {
      commit('SET_LOADING', true)
      const postData = getPostData(NEW_ORDER)
      const { data } = await this.$axios.post(
        '/marketplace/orders/', postData
      )

      commit('SET_NEW_ORDER', {})
      commit('SET_SERVER_ERRORS', {})
      commit('SET_ORDER_CREATED', true)

      return data
    } catch (e) {
      commit('SET_LOADING', false)
      const parsedErrors = parseServerErrors(e)
      commit('SET_SERVER_ERRORS', parsedErrors)
    } finally {
      commit('SET_LOADING', false)
    }
  },

  async UPDATE_ORDER({ commit, getters }, { id }) {
    commit('SET_FILES_ERRORS', {})
    if ( getters.LOADING )
      return
    const { EDIT_ORDER, ORDER } = getters
    try {
      commit('SET_LOADING', true)
      const changed = {}
      for (const [key, val] of Object.entries(EDIT_ORDER)) {
        // TODO: вот тут переделать
        if ( val !== ORDER[key]) {
          changed[key] = val
        }
      }
      if ( isNil(changed) ){
        return
      }
      const postData = getPostData(changed)
      const config = {
        params: {
          expand: ['form','type','created_by','created_by.avatar','city'].join()
        }
      }
      const { data } = await this.$axios.patch(
        `/marketplace/orders/${id}/`, postData, config
      )

      commit('SET_EDIT_ORDER', data)
      commit('SET_ORDER', data)
      commit('SET_SERVER_ERRORS', {})

      return data
    } catch (e) {
      commit('SET_LOADING', false)
      const parsedErrors = parseServerErrors(e)
      commit('SET_SERVER_ERRORS', parsedErrors)
    } finally {
      commit('SET_LOADING', false)
    }
  },

  RESET_SERVER_ERRORS( {commit} ){
    commit('SET_SERVER_ERRORS', {})
  },

  async FETCH_ORDER({ commit, getters }, { id }) {
    if ( getters.LOADING )
      return
    try {
      commit('SET_LOADING', true)
      const config = {
        params: {
          expand: ['form','type','created_by','created_by.avatar','city'].join()
        }
      }
      const { data } = await this.$axios.get(
        `/marketplace/orders/${id}/`, config
      )
      commit('SET_ORDER', data)
      return data
    } catch (e) {
      console.error(e)
      commit('SET_LOADING', false)
    } finally {
      commit('SET_LOADING', false)
    }
  },
  async DELETE_ORDER({ commit, getters }, { id }) {
    try {
      await this.$axios.delete(`/marketplace/orders/${id}/`)
      commit('SET_DELETED_ORDER', id)
      if ( getters.ORDER.id === id ) {
        commit('SET_ORDER', null)
      }
      return true
    } catch (err) {
      console.log('error during removal')
      throw err
    }
  },
}

export const mutations = {
  SET_NEW_ORDER(state, data) {
    state.newOrder = Object.assign({}, data)
  },
  SET_EDIT_ORDER(state, data){
    state.editOrder = Object.assign({}, data)
  },
  SET_ORDER_CREATED(state, value) {
    state.orderCreated = value
  },
  SET_SERVER_ERRORS(state, data) {
    state.serverErrors = Object.assign({}, data)
  },
  SET_ORDER(state, data){
    state.order = Object.assign({}, data)
  },
  SET_LOADING(state, value){
    state.loading = value
  },
  SET_DELETED_ORDER(state, value){
    state.deletedOrder = value
  },
  SET_FILES(state, value){
    state.files = value
  },
  SET_FILES_ERRORS(state, value){
    state.filesErrors = value
  },
  SET_NEW_FILE(state, value){
    if(value.length > 0) {
      state.newFiles = [...state.newFiles, ...value]
    } else {
      state.newFiles = value
    }
  },
}

export const namespaced = true