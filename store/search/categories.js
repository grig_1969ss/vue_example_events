import { path, uniqBy } from 'ramda'

export const state = () => ({
  data: null,
})

export const getters = {
  results: state => state.data || [],
}

export const actions = {
  async fetch({ commit }, params) {
    commit('save', null)
    try {
      const results = await Promise.all([
        this.$axios.get('/categories/', { params }).catch(e => {
          console.log(e.message)
          return { data: { results: [] } }
        }),
        this.$axios.get('/genres/', { only_parents: true, params }).catch(e => {
          console.log(e.message)
          return { data: { results: [] } }
        }),
        this.$axios.get('/event_types/', { params }).catch(e => {
          console.log(e.message)
          return { data: { results: [] } }
        }),
      ])

      const result = uniqBy(item => item.slug, [
        ...(path(['data', 'results'], results[0]) || []),
        ...(path(['data', 'results'], results[1]) || []),
        ...(path(['data', 'results'], results[2]) || []),
      ])
        .sort((a, b) => (a.total_events > b.total_events ? -1 : 1))
        .slice(0, 15)
      commit('save', result)
    } catch (err) {
      console.log(err)
    }
  },
}

export const mutations = {
  save(state, data) {
    state.data = data
  },
}

export const namespaced = true
