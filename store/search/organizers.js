import { prop } from 'ramda'
import { getLatestModified } from '~/utils'

export const state = () => ({
  data: null,
})

export const getters = {
  results: state => prop('results', state.data) || [],
  count: state => prop('count', state.data) || 0,
  next: state => prop('next', state.data),
  modified: (state, getters) => getLatestModified(getters.results),
}

export const actions = {
  async fetch({ commit }, params) {
    const expandParams = {
      expand: ['logo'].join(),
    }
    commit('save', null)
    try {
      const { data } = await this.$axios.get('/organizers/', {
        params: { ...expandParams, ...params },
      })
      commit('save', data)
    } catch (err) {
      console.log(err)
    }
  },
  async fetchMore({ commit, getters }) {
    try {
      const { data } = await this.$axios.get(getters.next)
      data.results = [...getters.results, ...data.results]
      commit('save', data)
    } catch (err) {
      console.log(err)
    }
  },
}

export const mutations = {
  save(state, data) {
    state.data = data
  },
}

export const namespaced = true
