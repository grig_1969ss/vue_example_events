import { complement, isEmpty, is, prop } from 'ramda'

export const state = () => ({
  city: {
    id: null,
  },
  searchText: '',
  searchDates: [],
  searchCategory: '',
  searchPeriod: '',
})

export const getters = {
  CITY: state => state.city,
  SEARCH_TEXT: state => state.searchText || '',
  SEARCH_DATES: state => state.searchDates,
  SEARCH_CATEGORY: state => state.searchCategory,
  SEARCH_PERIOD: state => state.searchPeriod,
}

const root = { root: true }
const isNotEmpty = complement(isEmpty)
const isExist = o => is(Object, o) && isNotEmpty(o)
const cityFromCities = (id, cities) => cities.find(el => el.id === id)

export const actions = {
  async INIT_CITY({ commit, dispatch, rootGetters, getters }, { id, city }) {
    const idFirstCity = id && String(id).split(',')[0]
    if (isExist(city)) {
      commit('SAVE_SEARCH_CITY', city)
      return Promise.resolve(city)
    }

    if (!idFirstCity) return
    if (Number(idFirstCity) === getters.CITY.id) return Promise.resolve()

    const cit = cityFromCities(idFirstCity, rootGetters['cities/results'])
    if (cit) {
      commit('SAVE_SEARCH_CITY', cit)
      return Promise.resolve(cit)
    }
    if (rootGetters['city/GET_CITY'].id === idFirstCity) {
      commit('SAVE_SEARCH_CITY', rootGetters['city/GET_CITY'])
      return Promise.resolve(rootGetters['city/GET_CITY'])
    }
    const { data } = await this.$axios.get(`/geo/cities/${idFirstCity}/`)
    commit('SAVE_SEARCH_CITY', data)
    return data
  },
  CHANGE_CITY({ commit }, { city }) {
    if (isExist(city)) {
      commit('SAVE_SEARCH_CITY', city)
    }
  },
  CHANGE_SEARCH({ commit }, { text }) {
    commit('SAVE_SEARCH_TEXT', text)
  },
  CHANGE_DATES({ commit }, { dates }) {
    if (!dates) return

    if (typeof dates === 'string') {
      const d = dates
        .split(',')
        .map(date =>
          date.length ? this.$moment(date, 'DD.MM.YYYY') : undefined,
        )
      commit('SAVE_SEARCH_DATES', d)
    } else {
      commit('SAVE_SEARCH_DATES', dates)
    }
  },
  CHANGE_CATEGORY({ commit }, { category }) {
    if (typeof category !== 'string') return
    commit('SAVE_SEARCH_CATEGORY', category)
  },
  CHANGE_PERIOD({ commit }, { period }) {
    commit('SAVE_SEARCH_PERIOD', period)
  },
  async DO_SEARCH({ dispatch }, { query }) {
    try {
      const geoname_id = prop('geoname_ids', query)
      await Promise.all([
        dispatch('search/sessions/fetch', query, root),
        dispatch('search/journals/fetch', query, root),
        dispatch('search/organizers/fetch', query, root),
        dispatch('search/categories/fetch', { geoname_id }, root),
      ])
    } catch (err) {
      console.log(err)
    }
  },
  CLEAR({ commit }) {
    commit('CLEAR_PARAMS')
  },
}

export const mutations = {
  SAVE_SEARCH_CITY(state, c) {
    state.city = c
  },
  SAVE_SEARCH_TEXT(state, t) {
    state.searchText = t
  },
  SAVE_SEARCH_DATES(state, d) {
    state.searchDates = d
  },
  SAVE_SEARCH_CATEGORY(state, c) {
    state.searchCategory = c
  },
  SAVE_SEARCH_PERIOD(state, c) {
    state.searchPeriod = c
  },
  CLEAR_PARAMS(state) {
    state.searchText = ''
    state.searchCategory = ''
    state.searchDates = []
  },
}

export const namespaced = true
