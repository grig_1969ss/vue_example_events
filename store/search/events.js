import { unionWith, eqBy, prop } from 'ramda'
import { getLatestModified } from '~/utils'
import { mapDatesData, deSerializeObjectDates } from '~/utils/event'

export const state = () => ({
  seo: null,
  loading: false,
  count: 0,
  next: null,
  num_pages: 0,
  page_number: 1,
  page_size: 4,
  previous: null,
  results: [],
})

export const getters = {
  EVENTS: state => {
    return state.results.map(event => deSerializeObjectDates(event))
  },
  COUNT: state => state.count,
  PAGE_COUNT: state => state.num_pages,
  HAS_EVENTS: state => state.count > 0,
  LOADING: state => state.loading,
  modified: state => getLatestModified(state.results),
  MORE_EVENTS: state => Boolean(state.next),
  LEFT_TO_GET: state => state.count - state.results.length,
  seo: state => state.seo || {},
  isClosest: (state, getters) => getters.seo.total < 5 && getters.seo.closest,
}

export const actions = {
  async FETCH({ commit, getters }, params) {
    commit('RESET')
    if (getters.LOADING) return
    try {
      commit('LOADING', true)
      const innerParams = { expand: ['cover', 'organizer'].join() }
      const { data } = await this.$axios.get('/events/', {
        params: { ...innerParams, ...params },
      })

      commit('SET', data)
      return data
    } finally {
      commit('LOADING', false)
    }
  },

  async FETCH_MORE({ commit, getters, state }, params) {
    if (getters.LOADING) return
    try {
      commit('LOADING', true)

      const { data } = await this.$axios.get(state.next, { params })
      data.results = getters.EVENTS.concat(data.results)

      commit('SET', data)
    } finally {
      commit('LOADING', false)
    }
  },

  async FETCH_UNIQUE({ getters, commit }, params) {
    if (getters.LOADING) return

    commit('LOADING', true)

    try {
      const { data } = await this.$axios.get('/events/', { params })

      data.results = unionWith(eqBy(prop('id')), getters.EVENTS, data.results)

      commit('SET', data)

      return data
    } catch (e) {
      console.log(e)
    } finally {
      commit('LOADING', false)
    }
  },

  async FETCH_SEO({ commit, dispatch, getters }, { id }) {
    try {
      const { data } = await this.$axios.get(`/counters/events/${id}`, {
        params: { expand: 'closest.city,city' },
      })
      commit('saveSeo', data)
      let query = {}
      if (getters.isClosest) {
        query = data.closest
      } else {
        query = data
      }
      const { payment, period, keywords, geoname_ids } = query
      await dispatch('FETCH', {
        payment,
        ...(period ? { period } : { actual: true }),
        keywords,
        geoname_ids,
      })
    } catch (err) {}
  },
}

export const mutations = {
  LOADING(state, value) {
    state.loading = value
  },
  SET(state, data) {
    if(data){
      const results = prop('results', data) || []
      results.map(event => mapDatesData(event))
      data.results = results
    }
    Object.assign(state, data)
  },
  RESET(state) {
    Object.assign(state, {
      count: 0,
      next: null,
      previous: null,
      results: [],
    })
  },
  saveSeo(state, data) {
    state.seo = data
  },
}

export const namespaced = true
