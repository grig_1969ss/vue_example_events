import { prop } from 'ramda'
import { getLatestModified } from '~/utils'
import { mapDatesData, deSerializeObjectDates } from '~/utils/event'

export const state = () => ({
  data: null,
  seo: null,
})

export const getters = {
  results: state => {
    const results = prop('results', state.data) || []
    return results.map(session => {
      let event = session.event
      if (typeof(event) === 'object' && event !== null){
        event = deSerializeObjectDates(event)
      }
      return deSerializeObjectDates(Object.assign(session, {event: event}))
    })
  },
  seo: state => state.seo || {},
  isClosest: (state, getters) => getters.seo.total < 5 && getters.seo.closest,
  count: state => prop('count', state.data) || 0,
  next: state => prop('next', state.data),
  modified: (state, getters) => getLatestModified(getters.results),
}

export const actions = {
  async fetch({ commit }, params) {
    const expandParams = {
      expand: [
        'event',
        'event.category',
        'event.cover',
        'event.type',
        'location',
      ].join(),
    }
    commit('save', null)
    try {
      const { data } = await this.$axios.get('/events/sessions/', {
        params: { ...expandParams, ...params },
      })
      commit('save', data)
    } catch (err) {
      console.log(err)
    }
  },
  async fetchMore({ commit, getters }) {
    try {
      const { data } = await this.$axios.get(getters.next)
      data.results = [...getters.results, ...data.results]
      commit('save', data)
    } catch (err) {
      console.log(err)
    }
  },
  async fetchSeo({ commit, dispatch, getters }, { id }) {
    try {
      const { data } = await this.$axios.get(`/counters/events/${id}`, {
        params: { expand: 'closest.city,city' },
      })
      commit('saveSeo', data)
      let query = {}
      if (getters.isClosest) {
        query = data.closest
      } else {
        query = data
      }
      const { payment, period, keywords, geoname_ids } = query
      await dispatch('fetch', {
        payment,
        ...(period ? { period } : { actual: true }),
        keywords,
        geoname_ids,
      })
    } catch (err) {}
  },
}

export const mutations = {
  save(state, data) {
    if(data){
      const results = prop('results', data) || []
      results.map(session => {
        if (typeof(session.event) === 'object' && session.event !== null){
          session.event = mapDatesData(session.event)
        }
        return mapDatesData(session)
      })
      data.results = results
    }
    state.data = data
  },
  saveSeo(state, data) {
    state.seo = data
  },
}

export const namespaced = true
