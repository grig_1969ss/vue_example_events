import { getLatestModified } from '~/utils'

export const state = () => ({
  loading: false,
  count: 0,
  next: null,
  num_pages: 0,
  page_number: 1,
  page_size: 10,
  previous: null,
  results: [],
})

export const getters = {
  organizers: state => state.results,
  count: state => state.count,
  more: state => Boolean(state.next),
  loading: state => state.loading,
  modified: (state, getters) => getLatestModified(getters.organizers),
}

export const actions = {
  async FETCH({ commit, getters }, params) {
    // commit('RESET')
    if (getters.LOADING) return
    try {
      commit('LOADING', true)
      const { data } = await this.$axios.get('/organizers/', {
        params,
      })

      commit('SET', data)
      return getters.organizers
    } catch (err) {
      console.error(err)
    } finally {
      commit('LOADING', false)
    }
  },
  async FETCH_MORE({ commit, getters, state }, params) {
    if (getters.LOADING) return
    try {
      commit('LOADING', true)

      const { data } = await this.$axios.get(state.next, { params })
      data.results = [...state.results, ...data.results]

      commit('SET', data)
      return getters.organizers
    } finally {
      commit('LOADING', false)
    }
  },
}

export const mutations = {
  LOADING(state, value) {
    state.loading = value
  },
  SET(state, data) {
    Object.assign(state, data)
  },
  RESET(s) {
    Object.assign(s, state())
  },
}

export const namespaced = true
