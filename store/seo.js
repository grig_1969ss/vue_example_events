import { groupBy, sortBy, path } from 'ramda'

export const state = () => ({
  results: null,
  geoId: null,
})

export const getters = {
  results: state => state.results || [],
  periods: (state, getters) =>
    sortBy(path(['period_display']), getters.results.filter(v => v.period)),
  payments: (state, getters) =>
    sortBy(path(['payment_display']), getters.results.filter(v => v.payment)),
  categories: (state, getters) =>
    sortBy(
      path(['category', 'title']),
      getters.results.filter(v => v.category),
    ),
  types: (state, getters) =>
    sortBy(path(['type', 'title']), getters.results.filter(v => v.type)),
  genresGroup: (state, getters) => {
    // берем только жанры
    const genres = getters.results.filter(v => v.genre)
    // группируем по родителю
    const groupedGenres = groupBy(v => v.genre.parent, genres)
    const namedGenresGroup = { unnamed: { results: [] } }
    for (const prop in groupedGenres) {
      // ищем родителя в списке
      const parent = genres.find(value => String(value.genre.id) === prop)
      if (parent) {
        // сохраняем родителя с семейством
        namedGenresGroup[prop] = { ...parent, results: groupedGenres[prop] }
      } else {
        // если родителя в списке нет, то отправляем всех в детдом
        namedGenresGroup.unnamed.results = sortBy(path(['genre', 'title']), [
          ...namedGenresGroup.unnamed.results,
          ...groupedGenres[prop],
        ])
      }
    }
    return namedGenresGroup
  },
}

export const actions = {
  async FETCH({ commit, state }, params) {
    try {
      const geoId = params.geoname_id
      if (geoId === state.geoId) return
      const innerParams = { expand: ['category', 'type', 'genre'].join() }
      const { data } = await this.$axios.get('/counters/events/', {
        params: { ...innerParams, ...params },
      })
      commit('save', data)
      commit('setGeoId', geoId)
    } catch (e) {
      commit('save', null)
      commit('setGeoId', null)
    }
  },
}

export const mutations = {
  save(state, { results }) {
    state.results = results
  },
  setGeoId(state, geoId) {
    state.geoId = geoId
  },
}

export const namespaced = true
