import { isNil, path, prop, map, filter } from 'ramda'

const preStatuses = {
  promo: 'promo',
}

const makePlaces = allPlaces => {
  // let allPlaces = response.data.results
  let locations = {}
  locations.results = []
  for (let institution of allPlaces) {
    for (let el of institution.locations) {
      el.cover = institution.cover
      el.logo = institution.logo
      if (institution.phones && institution.phones[0]) {
        el.phone = institution.phones[0]
      }
      if (institution.links && institution.links[0]) {
        el.link = institution[0]
      }
      el.email = institution.email
      locations.results.push(el)
    }
  }

  locations.results.forEach(el => (el.checked = false))
  return locations.results
}

const firstStepValidation = () => ({
  title: {
    error: false,
    errorMessage: '',
  },
  cover: {
    error: false,
    errorMessage: '',
  },
  images: {
    error: false,
    errorMessage: '',
  },
  description: {
    error: false,
    errorMessage: '',
  },
  categories: {
    error: false,
    errorMessage: '',
  },
  links: {
    error: false,
    errorMessage: '',
  },
  start_dt: {
    key: 'start_dt',
    error: false,
    errorMessage: '',
  },
  end_dt: {
    error: false,
    errorMessage: '',
  },
  promo_gender: {
    error: false,
    errorMessage: '',
  },
  promo_age_from: {
    error: false,
    errorMessage: '',
  },
  promo_age_to: {
    error: false,
    errorMessage: '',
  },
  promo_tags: {
    error: false,
    errorMessage: '',
  },
  non_field_errors: {
    error: false,
    errorMessage: '',
  },
})

const secondStepValidation = () => ({
  non_field_errors: {
    error: false,
    errorMessage: '',
  },
  organizers: {
    error: false,
    errorMessage: '',
  },
  location_address: {
    error: false,
    errorMessage: '',
  },
  location_city: {
    error: false,
    errorMessage: '',
  },
  location_title: {
    error: false,
    errorMessage: '',
  },
  location: {
    error: false,
    errorMessage: '',
  },
  location_phones: {
    error: false,
    errorMessage: '',
  },
  location_email: {
    error: false,
    errorMessage: '',
  },
  location_website: {
    error: false,
    errorMessage: '',
  },
})

export const state = () => ({
  newEvent: {
    cover: null,
    promo: {},
    title: '',
    location_city: null,
    place: {},
    location_address: '',
    location_phones: '',
    location_website: '',
    location_email: '',
    organizers: [],
    agreeCheckbox: false,
    // placeSuggestion: {},
    tags: [],
    gender: 2,
    offer: {},
    startDate: '',
    promoOptions: [],
    interests: [],
    period: [null, null],
    description: '',
    categories: [],
    checkedCategories: [],
    links: [],
    cities: [],
    allPlaces: [],
    placename: '',
    age: [],
    images: [],
    localimage: [],
    localCategories: [],
    findedOrginizers: null,
    secondStepValidation: secondStepValidation(),
    firstStepValidation: firstStepValidation(),
    searchPlaceNext: null,
    validatedStep: 0,
    preStatus: null,
  },
})

export const getters = {
  NEW_EVENT: state => state.newEvent,
  NEW_EVENT_COVER: state => state.newEvent.cover,
  NEW_EVENT_LOCAL_CATEGORIES: state => state.newEvent.localCategories,
  NEW_EVENT_PLACE_PHONE: state => prop('phone', state.newEvent.place),
  NEW_EVENT_PLACE_EMAIL: state => prop('email', state.newEvent.place),
  NEW_EVENT_PLACE_LINK: state => prop('link', state.newEvent.place),
  NEW_EVENT_PLACE_ADDRESS: state => state.newEvent.place.address,
  NEW_EVENT_PLACE_PLACENAME: state => state.newEvent.place.title,
  NEW_EVENT_NAME: state => state.newEvent.title,
  NEW_EVENT_TAGS: state => state.newEvent.tags,
  NEW_EVENT_GENDER: state => state.newEvent.gender,
  NEW_EVENT_OFFER: state => state.newEvent.offer,
  NEW_EVENT_START_DATE: state => state.newEvent.startDate,
  NEW_EVENT_PROMO_OPTIONS: state => state.newEvent.promoOptions,
  NEW_EVENT_CITY: state => state.newEvent.location_city,
  NEW_EVENT_PLACE: state => state.newEvent.place,
  NEW_EVENT_ADDRESS: state => state.newEvent.location_address,
  NEW_EVENT_PHONE: state => state.newEvent.location_phones,
  NEW_EVENT_SITE: state => state.newEvent.location_website,
  NEW_EVENT_MAIL: state => state.newEvent.location_email,
  NEW_EVENT_ORGANIZERS_LIST: state => state.newEvent.organizers,
  NEW_EVENT_AGREE_STEP_2: state => state.newEvent.agreeCheckbox,
  NEW_EVENT_INTERESTS: state => state.newEvent.interests,
  NEW_EVENT_PERIOD: state => state.newEvent.period,
  NEW_EVENT_DESCRIPTION: state => state.newEvent.description,
  NEW_EVENT_ALL_CATEGORIES: state => state.newEvent.categories,
  NEW_EVENT_CHECKED_CATEGORIES: state => state.newEvent.checkedCategories,
  NEW_EVENT_LINKS: state => state.newEvent.links,
  NEW_EVENT_CITIES: state => state.newEvent.cities,
  NEW_EVENT_PLACES: state => state.newEvent.allPlaces,
  NEW_EVENT_PLACENAME: state => state.newEvent.placename,
  NEW_EVENT_AGE: state => state.newEvent.age,
  NEW_EVENT_IMAGES: state => state.newEvent.images,
  NEW_EVENT_LOCAL_IMAGE: state => state.newEvent.localimage,
  NEW_EVENT_FINDED_ORIGNIZERS: state =>
    prop('results', state.newEvent.findedOrginizers),
  NEW_EVENT_FINDED_ORIGNIZERS_MORE: state =>
    Boolean(prop('next', state.newEvent.findedOrginizers)),
  NEW_EVENT_FIRST_STEP_VALIDATION: state => state.newEvent.firstStepValidation,
  NEW_EVENT_SECOND_STEP_VALIDATION: state =>
    state.newEvent.secondStepValidation,
  MORE_PLACES_TO_SEARCH: state => Boolean(state.newEvent.searchPlaceNext),
  VALIDATED_STEP: state => state.newEvent.validatedStep,
  PRE_STATUS: state => state.newEvent.preStatus,
  PRE_STATUSES: () => preStatuses,
  PRE_STATUS_IS_PROMO: (_, getters) =>
    getters.PRE_STATUS === getters.PRE_STATUSES.promo,
}

export const actions = {
  /**
   * payload.key - property of state to change
   * payload.value - new value
   */
  SET_FIELD({ commit }, payload) {
    commit('SAVE_EVENT_FIELD', payload)
    if (payload.validations) {
      payload.validations.map(v => {
        commit('RESTORE_FIRST_STEP_VALIDATION_BY_KEY', v)
      })
    } else {
      commit('RESTORE_FIRST_STEP_VALIDATION_BY_KEY', payload.key)
    }
  },
  CREATE_EVENT_CATEGORY({ commit, state }, category) {
    let isNotNil = val => val !== null
    commit(
      'CREATE_EVENT_CATEGORY',
      filter(
        isNotNil,
        map(val => val.category, state.newEvent.localCategories),
      ),
    )
    commit('RESTORE_FIRST_STEP_VALIDATION_BY_KEY', 'categories')
  },
  CREATE_EVENT_GENDER({ commit }, gender) {
    commit('CREATE_EVENT_GENDER', gender)
    commit('RESTORE_FIRST_STEP_VALIDATION_BY_KEY', 'promo_gender')
  },
  AGREE_STEP_2({ commit }, checked) {
    commit('AGREE_STEP_2', checked)
  },
  CREATE_EVENT_OFFER({ commit }, offer) {
    commit('CREATE_EVENT_OFFER', offer)
  },
  CREATE_EVENT_START_DATE({ commit }, date) {
    commit('CREATE_EVENT_START_DATE', date)
  },
  CREATE_EVENT_PROMO_OPTIONS({ commit }, options) {
    commit('CREATE_EVENT_PROMO_OPTIONS', options)
  },
  CREATE_EVENT_INTERESTS({ commit }, interests) {
    commit('CREATE_EVENT_INTERESTS', interests)
  },
  async GET_ALL_CATEGORIES({ commit, getters }, params) {
    try {
      const response = await this.$axios.get('/categories/', { params })
      commit('GET_ALL_CATEGORIES', [
        ...getters.NEW_EVENT_ALL_CATEGORIES,
        ...response.data.results,
      ])

      return response
    } catch (e) {
      console.log(e)
    }
  },
  RESTORE_CATEGORIES({ commit }) {
    commit('GET_ALL_CATEGORIES', [])
  },
  async SEARCH_CITY({ commit, rootState }, city) {
    if (city.length < 3) return
    let data = {
      search: city,
      page_size: 5,
    }
    const response = await this.$axios.get('/cities/', {
      params: data,
      headers: { 'accept-language': rootState.lang.locale },
    })
    response.data && commit('SEARCH_CITY', response.data)
    return response
  },

  async SEARCH_PLACE({ commit, state }, { value }) {
    const place = value
    if (place.length === 0) {
      commit('SEARCH_PLACE', [])
      commit('SAVE_EVENT_FIELD', { value: {}, key: 'place' })
      commit('SAVE_EVENT_FIELD', { value: '', key: 'placename' })
      commit('SAVE_EVENT_FIELD', { value: '', key: 'location_address' })
      return
    }
    let data = {
      search: place,
      city: path(['location_city', 'id'], state.newEvent),
      page_size: 10,
    }
    commit('SAVE_EVENT_FIELD', { value: place, key: 'placename' })
    if (place.length >= 3 && path(['location_city', 'id'], state.newEvent)) {
      // commit('SAVE_EVENT_FIELD', { value: {}, key: 'place' })
      const response = await this.$axios.get('/places/institutions/', {
        params: data,
      })

      response.data && commit('SEARCH_PLACE', makePlaces(response.data.results))
      commit('SAVE_EVENT_FIELD', {
        value: response.data.next,
        key: 'searchPlaceNext',
      })
      return response
    }
  },
  async SEARCH_MORE_PLACES({ commit, state }, payload) {
    let next = state.newEvent.searchPlaceNext
    if (!next) return
    try {
      const res = await this.$axios.get(state.newEvent.searchPlaceNext)
      commit('SAVE_EVENT_FIELD', {
        value: res.data.next,
        key: 'searchPlaceNext',
      })
      commit('ADD_MORE_PLACES', makePlaces(res.data.results))
    } catch (err) {
      console.log(err)
    }
  },
  async SEARCH_ORGANIZERS({ commit, state }, name) {
    let data = {
      search: name,
    }
    try {
      commit('SEARCH_ORGANIZERS', null)
      const response = await this.$axios.get('/users/', { params: data })
      response.data && commit('SEARCH_ORGANIZERS', response.data)
    } catch (err) {
      throw console.log('fail', err)
    }
  },
  async SEARCH_ORGANIZERS_MORE({ commit, state }) {
    const { data } = await this.$axios.get(state.newEvent.findedOrginizers.next)
    data.results = [...state.newEvent.findedOrginizers.results, ...data.results]
    commit('SEARCH_ORGANIZERS', data)
  },
  async CREATE_FIRST_STEP({ state, commit }) {
    let data = {
      title: state.newEvent.title,
      images: state.newEvent.images.map(v => v.id),
      description: state.newEvent.description,
      categories: state.newEvent.checkedCategories.map(el => el.id),
      links: state.newEvent.links,
      start_dt: state.newEvent.period[0],
      end_dt: state.newEvent.period[1],
      promo_age_from: state.newEvent.age[0],
      promo_age_to: state.newEvent.age[1],
      promo_tags: state.newEvent.tags,
      promo_gender: !isNil(state.newEvent.gender) ? state.newEvent.gender : 2,
    }

    commit('RESTORE_FIRST_STEP_VALIDATION')

    try {
      const response = await this.$axios.post('/events/first-step/', data)
      commit('SET_VALIDATED_STEP', 1)

      return response
    } catch (err) {
      let responseError = err.response.data

      for (const item in responseError) {
        if (!state.newEvent.firstStepValidation.hasOwnProperty(item)) return

        const isArray = Array.isArray(responseError[item])

        state.newEvent.firstStepValidation[item].error = true
        state.newEvent.firstStepValidation[item].errorMessage = isArray
          ? responseError[item][0]
          : responseError[item]
      }
      console.log('Failed to validate first step')
      throw err
    }
  },
  async CHECK_LOCATION_AND_VALIDATE({ state, commit, dispatch }) {
    let data = {
      organizers: state.newEvent.organizers.map(el => el.id),
      location_address: state.newEvent.location_address,
      location_city: path(['location_city', 'id'], state.newEvent),
      location_title: state.newEvent.placename,
      location: state.newEvent.place.id,
      location_email: state.newEvent.location_email,
      location_website: state.newEvent.location_website,
    }
    if (state.newEvent.location_phones) {
      data.location_phones = [state.newEvent.location_phones]
    }

    commit('RESTORE_SECOND_STEP_VALIDATION')

    try {
      await this.$axios.post('/events/second-step/', data)
      commit('SET_VALIDATED_STEP', 2)
    } catch (err) {
      let responseError = err.response.data

      for (let item in responseError) {
        if (!state.newEvent.secondStepValidation.hasOwnProperty(item)) return

        const isArray = Array.isArray(responseError[item])

        state.newEvent.secondStepValidation[item].error = true
        state.newEvent.secondStepValidation[item].errorMessage = isArray
          ? responseError[item][0]
          : responseError[item]
      }
      console.log('create event: failed to validate second step', err)
      throw err
    }
  },

  SET_PROMO_DATA({ commit }, data) {
    commit('SET_PROMO_DATA', data)
  },

  async CREATE_EVENT({ state, commit }) {
    let data = {
      title: state.newEvent.title,
      cover: state.newEvent.cover.id,
      images: state.newEvent.images.map(v => v.id),
      description: state.newEvent.description,
      categories: state.newEvent.checkedCategories.map(el => el.id),
      links: state.newEvent.links,
      start_dt: state.newEvent.period[0],
      end_dt: state.newEvent.period[1],
      location: state.newEvent.place.id,
      organizers: state.newEvent.organizers.map(el => el.id),
      location_address: state.newEvent.location_address,
      location_city: state.newEvent.location_city.id,
      location_title: state.newEvent.placename,
      promo_age_from: state.newEvent.age[0],
      promo_age_to: state.newEvent.age[1],
      promo_gender: state.newEvent.gender,
      promo_tags: state.newEvent.tags,
      location_email: state.newEvent.location_email,
      coupon: state.newEvent.coupon,
      promo_plan: state.newEvent.promo_plan,
      promo_languanges: state.newEvent.promo_languanges,
      extra: state.newEvent.promoOptions,
      location_website: state.newEvent.location_website,
    }
    if (state.newEvent.location_phones) {
      data.location_phones = [state.newEvent.location_phones]
    }
    if (state.newEvent.startDate) {
      data.promo_start_datetime = state.newEvent.startDate
    }
    if (state.newEvent.promo_site) {
      data.promo_site = state.newEvent.promo_site
    }
    try {
      const response = await this.$axios.post('/events/', data)
      commit('CLEAR')
      if (response.data) {
        return response.data
      }
    } catch (err) {
      console.log('create event: error while creation event', err)
      throw err
    }
  },
}

export const mutations = {
  SAVE_EVENT_FIELD(state, { key, value }) {
    state.newEvent[key] = value
  },
  CREATE_EVENT_CATEGORY(state, categories) {
    state.newEvent.checkedCategories = categories
    // state.newEvent.checkedCategories = [...state.newEvent.localCategories]
  },
  CREATE_EVENT_GENDER(state, gender) {
    state.newEvent.gender = gender
  },
  SET_PROMO_DATA(state, data) {
    Object.assign(state.newEvent, data)
  },
  RESTORE_FIRST_STEP_VALIDATION_BY_KEY(state, key) {
    state.newEvent.firstStepValidation[key] = firstStepValidation()[key]
  },
  RESTORE_FIRST_STEP_VALIDATION(state) {
    state.newEvent.firstStepValidation = firstStepValidation()
  },
  RESTORE_SECOND_STEP_VALIDATION(state) {
    state.newEvent.secondStepValidation = secondStepValidation()
  },
  RESTORE_SECOND_STEP_VALIDATION_BY_KEY(state, key) {
    state.newEvent.secondStepValidation[key] = secondStepValidation()[key]
  },
  CREATE_EVENT_OFFER(state, offer) {
    state.newEvent.offer = offer
  },
  CREATE_EVENT_START_DATE(state, date) {
    state.newEvent.startDate = date
  },
  CREATE_EVENT_PROMO_OPTIONS(state, options) {
    state.newEvent.promoOptions = options
  },
  AGREE_STEP_2(state, checked) {
    state.newEvent.agreeCheckbox = checked
  },
  CREATE_EVENT_INTERESTS(state, interests) {
    state.newEvent.interests = interests
  },
  GET_ALL_CATEGORIES(state, categories) {
    state.newEvent.categories = categories
  },
  SEARCH_CITY(state, cities) {
    state.newEvent.cities = cities
  },
  SEARCH_PLACE(state, places) {
    state.newEvent.allPlaces = [...places]
  },
  ADD_MORE_PLACES(state, places) {
    state.newEvent.allPlaces = [...state.newEvent.allPlaces, ...places]
  },
  CREATE_LOCATION(state, location) {
    location.results.forEach(el => {
      el.checked = false
    })
    state.newEvent.location = location
  },
  SEARCH_ORGANIZERS(state, list) {
    state.newEvent.findedOrginizers = list
  },
  SET_VALIDATED_STEP(state, v) {
    if (v < state.newEvent.validatedStep) return

    state.newEvent.validatedStep = v
  },
  CLEAR(state) {
    state.newEvent = {
      cover: null,
      promo: {},
      title: '',
      location_city: null,
      place: {},
      location_address: '',
      location_phones: '',
      location_website: '',
      location_email: '',
      organizers: [],
      agreeCheckbox: false,
      // placeSuggestion: {},
      tags: [],
      gender: 2,
      offer: {},
      startDate: '',
      promoOptions: [],
      interests: [],
      period: [null, null],
      description: '',
      categories: [],
      checkedCategories: [],
      links: [],
      cities: [],
      allPlaces: [],
      placename: '',
      age: [],
      images: [],
      localimage: [],
      localCategories: [],
      findedOrginizers: null,
      secondStepValidation: secondStepValidation(),
      firstStepValidation: firstStepValidation(),
      searchPlaceNext: null,
      validatedStep: 0,
      preStatus: null,
    }
  },
}

export const namespaced = true
