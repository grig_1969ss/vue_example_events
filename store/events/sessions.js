import { groupBy } from 'ramda'
import { getLatestModified } from '~/utils'
import { mapDatesData, deSerializeObjectDates} from '~/utils/event'
import {getEventsData} from "../../utils/event";

export const state = () => ({
  count: 0,
  results: [],
  events: [],
  num_pages: 0,
  next: null
})

export const getters = {
  EVENTS_DATA: state => state.events,
  EVENTS: state => {
    const sessions = [...state.events] || []
    return  getEventsData(sessions)
  },
  GET_SIMILAR_EVENTS: state => {
    const sessions = state.results || []
    return sessions.map(session => {
      let event = session.event
      if (typeof(event) === 'object' && event !== null){
        event = deSerializeObjectDates(event)
      }
      return deSerializeObjectDates(Object.assign(session, {event: event}))
    })
  },
  COUNT: state => state.count,
  PAGE_COUNT: state => state.num_pages,
  HAS_EVENTS: state => state.count > 0,
  GROUP_EVENTS: (state, getters) => Object.values(groupBy(item => {
    if (typeof(item.event) === 'object' && item.event !== null) {
      return item.event.id
    }
    return item.event
  }, getters.GET_SIMILAR_EVENTS)),
  modified: state => getLatestModified(state.results),
  MORE: state => Boolean(state.next),
  LEFT_TO_GET: state => state.count - state.results.length,
}

export const actions = {
  async FETCH({ commit }, params) {
    try {
      const { data } = await this.$axios.get('/events/sessions/', { params })
      if(params.event) {
        commit('SET_EVENTS', data)
      } else {
        commit('SET_SIMILAR_EVENTS', data)
      }
      return data
    } catch (err) {}
  },
}

export const mutations = {
  SET_EVENTS(state, data) {
    const results = data.results || []
    results.map(session => {
      if (typeof(session.event) === 'object' && session.event !== null){
        session.event = mapDatesData(session.event)
      }
      return mapDatesData(session)
    })
    Object.assign(state.events, results)
  },
  SET_SIMILAR_EVENTS(state, data) {
    const results = data.results || []
    results.map(session => {
      if (typeof(session.event) === 'object' && session.event !== null){
        session.event = mapDatesData(session.event)
      }
      return mapDatesData(session)
    })
    Object.assign(data.results, results)
    Object.assign(state, data)
  },
}

export const namespaced = true
