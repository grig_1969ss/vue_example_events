import {unionWith, eqBy, prop} from 'ramda'
import { getLatestModified } from '~/utils'
import { mapDatesData, deSerializeObjectDates} from '~/utils/event'

const initState = {
  loading: false,
  count: 0,
  next: null,
  num_pages: 0,
  page_number: 1,
  page_size: 4,
  previous: null,
  results: [],
  organizer: null,
}

export const state = () => ({
  ...initState
})

export const getters = {
  EVENTS: state => {
    return state.results.map(event => deSerializeObjectDates(event))
  },
  COUNT: state => state.count,
  PAGE_COUNT: state => state.num_pages,
  HAS_EVENTS: state => state.count > 0,
  LOADING: state => state.loading,
  modified: state => getLatestModified(state.results),
  MORE_EVENTS: state => Boolean(state.next),
  LEFT_TO_GET: state => state.count - state.results.length,
}

export const actions = {
  RESET_STORE({ commit }){
    commit('RESET_STORE')
  },

  async FETCH({ commit, getters }, params) {
    commit('RESET')
    if (getters.LOADING) return
    try {
      commit('LOADING', true)
      const { data } = await this.$axios.get('/events/', {
        params,
      })
      commit('SET', data)
      return data
    } catch (err) {
      throw err
    } finally {
      commit('LOADING', false)
    }
  },
  async FETCH_MORE({ commit, getters, state }, params) {
    if (getters.LOADING) return
    try {
      commit('LOADING', true)

      const { data } = await this.$axios.get(state.next, { params })
      data.results = getters.EVENTS.concat(data.results)

      commit('SET', data)
    } finally {
      commit('LOADING', false)
    }
  },
  async FETCH_UNIQUE({ getters, commit }, params) {
    if (getters.LOADING) return

    commit('LOADING', true)

    try {
      const { data } = await this.$axios.get('/events/', { params })

      data.results = unionWith(eqBy(prop('id')), getters.EVENTS, data.results)

      commit('SET', data)

      return data
    } catch (e) {
      console.log(e)
    } finally {
      commit('LOADING', false)
    }
  },
}

export const mutations = {
  RESET_STORE(state){
    state = Object.assign(state, initState)
  },
  LOADING(state, value) {
    state.loading = value
  },
  SET(state, data) {
    const results = data.results || []
    results.map(event => mapDatesData(event))
    data.results = results
    Object.assign(state, data)
  },
  RESET(state) {
    Object.assign(state, {
      count: 0,
      next: null,
      previous: null,
      results: [],
    })
  },
}

export const namespaced = true
