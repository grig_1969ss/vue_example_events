import get from 'lodash/get';
import {parseISO} from "date-fns";
import {omit, path, dissoc, prop, forEachObjIndexed, isEmpty} from 'ramda'

import {getTicketsSchemes, getPaymentCurrencies} from '~/api'
import {makeDateTimeString, formatStrings} from '~/utils/formatDate'
import {objectDiff} from '~/utils/objectDiff'
import { mapDatesData, deSerializeObjectDates } from '~/utils/event'


const getRandomKey = () => `f${(~~(Math.random() * 1e8)).toString(16)}`
export const CLIENT_SCHEME_PREFIX = 'cs_'

const getEmptyScheme = ({currency = {symbol: ''}}) => ({
  id: `${CLIENT_SCHEME_PREFIX}${getRandomKey()}`,
  title: '',
  tickets_total: '',
  tickets_price: {
    amount: '',
    currency,
  },
})

function hasFilledSchemeFields(scheme) {
  const {
    title,
    tickets_total,
    tickets_price: {amont: ticketsPriceAmount},
  } = scheme

  return [title, tickets_total, ticketsPriceAmount].some(Boolean)
}

function generateSessionsWithSchemes({sessions, schemesBySession}) {
  return sessions.reduce((result, session) => {
    const schemes = schemesBySession[session.id] || {}
    const sessionWithScheme = {
      id: session.id,
      start_dt: session.start_dt,
      isActual: !session.isFinished,
      startDtString: makeDateTimeString({
        date: session.start_date,
        time: session.start_time,
        formatString: formatStrings.ticketFormat,
        dateFormatString: formatStrings.fullDate,
        timeFormatString: formatStrings.fullTime,
      }),
      schemes,
    }
    result[session.id] = sessionWithScheme
    return result
  }, {})
}

function getSchemesBySession({schemes}) {
  return schemes.results.reduce((result, scheme) => {
    const sessionId = scheme.session
    const hasSession = Object.prototype.hasOwnProperty.call(result, sessionId)

    if (hasSession) {
      result[sessionId][scheme.id] = scheme
    } else {
      result[sessionId] = {}
      result[sessionId][scheme.id] = scheme
    }

    return result
  }, {})
}

export const state = () => ({
  schemes: null,
  sessionsWithSchemes: null,
  initSessionsWithSchemes: null,
  only_free_registration: false,
  schemeErrors: {},
  currencies: null,
  initCurrency: null,
  currentEvent: null,
  phoneValid: false,
  eventErrors: null,
  loading: false,
  onlyFreeTicketsForm: false
})

export const getters = {
  USER_ID: (state, getters, rootState) => get(rootState.user, 'user.id'),
  SCHEMES: state => state.schemes,
  SESSIONS_WITH_SCHEMES: state => state.sessionsWithSchemes,
  INIT_SESSIONS_WITH_SCHEMES: state => state.initSessionsWithSchemes,
  ONLY_FREE_TICKETS_FORM: state => state.onlyFreeTicketsForm,
  HAS_INIT_SCHEMES: state => {
    if (!state.initSessionsWithSchemes) return false

    return Object.keys(state.initSessionsWithSchemes).some(sessionId => {
      const {schemes} = state.initSessionsWithSchemes[sessionId]

      return Boolean(Object.keys(schemes).length)
    })
  },
  IS_FREE_REGISTRATION: state => {
    const is_paid = Object.values(state.sessionsWithSchemes).some(schemes => {
      return Object.values(schemes['schemes']).some(scheme => {
        return scheme.tickets_price.amount ? parseFloat(scheme.tickets_price.amount) > 0.0: false
      })
    })
    return !is_paid
  },
  CURRENCIES: state => state.currencies,
  SCHEME_ERROR_BY_ID: state => id => state.schemeErrors[id],
  INIT_CURRENCY: state => state.initCurrency,
  LOADING: state => state.loading,
  CURRENT_EVENT: state => state.currentEvent,
  PHONE_VALID: state => state.phoneValid,
  EVENT_ERRORS: state => state.eventErrors,
}

export const actions = {
  setOnlyFreeTicketsForm({commit}, onlyFree = true){
    commit('ONLY_FREE_TICKETS_FORM', onlyFree)
  },
  setEventErrors({commit}, errors) {
    commit('SET_EVENT_ERRORS', errors)
  },
  phoneValid({commit}, isValid= true) {
    commit('SET_PHONE_VALID', isValid)
  },

  async loadTicketsEdit({dispatch, commit}, {id}) {
    commit('LOADING', true)
    try {
      let sessions;
      await dispatch(
        'events/sessions/FETCH',
        {event: id, page_size: 100},
        {root: true},
      ).then(
        data => {
          sessions = data.results
        }
      )
      const {data: schemes} = await getTicketsSchemes(this.$axios, {
        params: {
          event: id,
          page_size: 100,
        },
      })
      commit('SET_SCHEMES', schemes)

      const {data: currencies} = await getPaymentCurrencies(this.$axios)
      commit('SET_CURRENCIES', currencies)

      // TODO: logic for permissions
      // if (!path(['permissions', 'can_edit'], event)) {
      //   return this.app.router.push({
      //     name: 'events-id',
      //     params: { access: false },
      //   })
      // }
      dispatch('generateSessionsWithSchemes', sessions)
    } catch (e) {
      console.log(e)
      commit('LOADING', false)
      this.app.router.push('/')
    } finally {
      commit('LOADING', false)
    }
  },
  generateSessionsWithSchemes({commit, getters, dispatch, rootGetters}, params) {
    const sessions = params
    const schemes = getters.SCHEMES
    const schemesBySession = getSchemesBySession({schemes})

    const sessionsWithSchemes = generateSessionsWithSchemes({
      sessions,
      schemesBySession,
    })

    commit(
      'SET_INIT_SESSION_WITH_SCHEMES',
      JSON.parse(JSON.stringify(sessionsWithSchemes)),
    )
    commit('SET_SESSIONS_WITH_SCHEMES', sessionsWithSchemes)

    dispatch('setInitCurrency')

    const IS_FREE_REGISTRATION = getters['IS_FREE_REGISTRATION']
    const HAS_INIT_SCHEMES = getters['HAS_INIT_SCHEMES']

    if (!HAS_INIT_SCHEMES || !IS_FREE_REGISTRATION){
      Object.keys(sessionsWithSchemes).forEach(id => {
        dispatch('addSchemeBySession', {id})
      })
    }
  },
  setInitCurrency({commit, getters}, currency) {
    const initCurrency =
      currency ||
      path(['results', '0', 'tickets_price', 'currency'], getters.SCHEMES) ||
      getters.CURRENCIES[0]

    commit('SET_INIT_CURRENCY', initCurrency)
  },
  addSchemeBySession({getters, commit}, {id: sessionId}) {
    const sessions = getters.SESSIONS_WITH_SCHEMES

    const emptyScheme = getEmptyScheme({currency: getters.INIT_CURRENCY})

    sessions[sessionId].schemes = {
      ...sessions[sessionId].schemes,
      [emptyScheme.id]: emptyScheme,
    }

    commit('SET_SESSIONS_WITH_SCHEMES', sessions)
  },
  removeSchemeBySession({getters, commit}, {sessionId, schemeId}) {
    const sessions = getters.SESSIONS_WITH_SCHEMES
    const sessionSchemes = omit([schemeId], sessions[sessionId].schemes)

    sessions[sessionId].schemes = sessionSchemes

    commit('SET_SESSIONS_WITH_SCHEMES', sessions)
  },
  updateSessionScheme({getters, commit}, {session, scheme, key, value}) {
    const {id: sessionId} = session
    const {id: schemeId} = scheme

    const sessions = getters.SESSIONS_WITH_SCHEMES
    const schemes = sessions[sessionId].schemes

    const updatedScheme = {...schemes[schemeId], [key]: value}

    sessions[sessionId].schemes[schemeId] = updatedScheme

    commit('SET_SESSIONS_WITH_SCHEMES', sessions)
    commit('RESET_SCHEME_ERRORS_BY_SCHEME', schemeId)
  },
  updateSchemesCurrency({dispatch, getters}, {currency}) {
    forEachObjIndexed(session => {
      forEachObjIndexed(scheme => {
        dispatch('updateSessionScheme', {
          session,
          scheme,
          key: 'tickets_price',
          value: {
            ...scheme.tickets_price,
            currency,
          },
        })
      }, session.schemes)
    }, getters.SESSIONS_WITH_SCHEMES)

    dispatch('setInitCurrency', currency)
  },

  async saveTickets({getters, commit, dispatch}, {id}) {
    const {USER_ID} = getters
    const {ONLY_FREE_TICKETS_FORM, SESSIONS_WITH_SCHEMES, INIT_SESSIONS_WITH_SCHEMES} = getters
    if (ONLY_FREE_TICKETS_FORM){
      forEachObjIndexed(session => {
        forEachObjIndexed(scheme => {
          if (scheme.tickets_price && scheme.tickets_price.amount){
            scheme.tickets_price.amount = 0.0
          }
        }, session.schemes)
      }, SESSIONS_WITH_SCHEMES)
    }
    const diffArray = objectDiff(
      INIT_SESSIONS_WITH_SCHEMES,
      SESSIONS_WITH_SCHEMES,
      (path, key) => {
        if (path[path.length - 1] === 'currency') {
          return key !== 'code'
        }
      },
    )
    let postData = {
      update: {},
      create: {},
    }
    if (diffArray && !isEmpty(diffArray)) {
      postData = diffArray.reduce(
        (result, diff) => {
          const [sessionId, sessionKey] = diff.path

          if (sessionKey === 'schemes') {
            if (diff.kind === 'N') {
              if (hasFilledSchemeFields(diff.rhs)) {
                const newScheme = diff.rhs

                result.create[newScheme.id] = {
                  session: sessionId,
                  ...newScheme,
                }
              }
            } else if (diff.kind === 'E') {
              const schemeId = diff.path[2]
              const key = diff.path[3]

              if (key === 'tickets_price') {
                const key = diff.path[4]
                const schemePath = diff.path.slice(0, 4)
                const {amount, currency} = path(
                  schemePath,
                  INIT_SESSIONS_WITH_SCHEMES,
                )

                result.update[schemeId] = {
                  ...result.update[schemeId],
                  tickets_price: {
                    amount: amount ? amount: 0.0,
                    currency: currency.code,
                    ...prop('tickets_price', result.update[schemeId]),
                    [key]: diff.rhs,
                  },
                }
              } else {
                result.update[schemeId] = {
                  ...result.update[schemeId],
                  [key]: diff.rhs,
                }
              }
            }
          }

          return result
        },
        {
          update: {},
          create: {},
        },
      )
    }

    const hasCreatedSchemes = Object.keys(postData.create).length
    const hasUpdatedSchemes = Object.keys(postData.update).length

    const {CURRENT_EVENT} = getters
    this.$axios.patch(`events/${id}/`, CURRENT_EVENT)
      .then(async () => {
        if(hasCreatedSchemes) {
          await dispatch('createSchemes', {schemes: postData.create})
        }
        if(hasUpdatedSchemes) {
          await dispatch('updateSchemes', {schemes: postData.update})
        }
        this.$router.push({
          name: 'profile-id-events',
          params: {
            id: USER_ID
          },
        })

      })
      .catch(reject => {
        const data = {...get(reject, 'response.data', {})}
        const errorsObj = {}
        Object.keys(data).map((index) => (errorsObj[index] = get(data, `${index}.0`)))
        commit('SET_EVENT_ERRORS', errorsObj)
      })


  },
  async createSchemes({commit, getters}, {schemes}) {
    const postData = Object.keys(schemes).map(key => {
      const {id, session, title, tickets_total, tickets_price} = schemes[key]

      return {
        id,
        session,
        title,
        tickets_total,
        tickets_price: {
          amount: tickets_price.amount ? tickets_price.amount: 0.0,
          currency: tickets_price.currency ? tickets_price.currency.code: '',
        },
      }
    })
    let errorsObj = {}
    commit('SET_SHEME_ERRORS', errorsObj)

    try {
      await this.$axios.post('/tickets/schemes/', postData)
    } catch (e) {
      const errors = path(['response', 'data'], e)

      if (errors) {
        errorsObj = errors.reduce((result, error, idx) => {
          const {id: schemeId} = postData[idx]
          result[schemeId] = error
          return result
        }, {})
        commit('SET_SHEME_ERRORS', errorsObj)
      }

      throw e
    }
  },
  updateSchemes(_, { schemes }) {
    // TODO: parse errors
    return Promise.all(
      Object.keys(schemes).map(schemeId => {
        return this.$axios.patch(
          `/tickets/schemes/${schemeId}/`,
          schemes[schemeId],
        )
      }),
    )
  },
  setCurrentEvent({ getters, commit }, { data }) {
    commit('SET_CURRENT_EVENT', data)
  },
}

export const mutations = {
  ONLY_FREE_TICKETS_FORM(state, value){
    state.onlyFreeTicketsForm = value
  },
  LOADING(state, value) {
    state.loading = value
  },
  SET_SCHEMES(state, v) {
    state.schemes = v
  },
  SET_SESSIONS_WITH_SCHEMES(state, v) {
    state.sessionsWithSchemes = v
  },
  SET_INIT_SESSION_WITH_SCHEMES(state, v) {
    state.initSessionsWithSchemes = v
  },
  SET_CURRENCIES(state, v) {
    state.currencies = v
  },
  SET_SHEME_ERRORS(state, v) {
    state.schemeErrors = v
  },
  RESET_SCHEME_ERRORS_BY_SCHEME(state, v) {
    state.schemeErrors = dissoc(v, state.schemeErrors)
  },
  SET_INIT_CURRENCY(state, v) {
    state.initCurrency = v
  },
  SET_CURRENT_EVENT(state, v) {
    state.currentEvent = v
  },
  SET_PHONE_VALID(state, v) {
    state.phoneValid = v
  },
  SET_EVENT_ERRORS(state, v) {
    state.eventErrors = v
  },

}

export const namespaced = true
