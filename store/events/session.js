import { path } from 'ramda'
import { mapDatesData, deSerializeObjectDates } from '~/utils/event'

const withToken = s =>
  path(['auth', 'token'], s)
    ? { Authorization: `JWT ${path(['auth', 'token'], s)}` }
    : {}

export const state = () => ({
  data: {},
})

export const getters = {
  data: state => {
    let event = state.data.event
    if (typeof(event) === 'object' && event !== null){
      event = deSerializeObjectDates(event)
    }
    return deSerializeObjectDates(Object.assign(state.data, {event: event}))
  },
  isUserVisitor: state => path(['event', 'is_user_visitor'], state.data),
  visitorsCount: state => path(['event', 'visitors_count'], state.data),
  modified: state => path(['event', 'modified'], state.data),
  ticketsProvider: state => path(['event', 'tickets', 'provider'], state.data),
  urlId: state => path(['url_id'], state.data),
  SEATS_SCHEME: state => path(['event', 'tickets', 'data', 'seats_scheme'], state.data),
}

export const actions = {
  async FETCH({ commit }, id) {
    try {
      const { data } = await this.$axios.get(`/events/sessions/${id}/`, {
        params: {
          expand: [
            'location',
            'location.city',
            'event',
            'event.poster_800x1080',
            'event.poster_1080x1080',
            'event.poster_1200x630',
            'event.cover',
            'event.images',
            'event.type',
            'event.category',
            'event.tags',
            'event.genres',
            'event.organizer',
            'event.organizer.logo',
          ].join(),
        },
      })
      commit('SET', data)
      return data
    } catch (err) {
      console.log(err)
      throw err
    }
  },
  async FETCH_TICKETS({ commit }, id) {
    try {
      const { data } = await this.$axios.get(`/events/sessions/${id}/`, {
        params: {
          expand: ['location', 'event.cover', 'event.images'].join(),
        },
      })
      commit('SET', data)
      return data
    } catch (err) {
      console.warn(err)
    }
  },
  async FETCH_COMMENTS({ dispatch }, id) {
    const { data } = await this.$axios.get(`/events/${id}/comments/`, {
      params: { comments_expand: 'created_by.avatar' },
    })
    dispatch('comments/SET', data, { root: true })
  },
  async SEND_COMMENT({ dispatch, getters, rootGetters, rootState }, payload) {
    const check = await dispatch('user/CHECK_PERSONAL_DATA_AGREEMENT', null, {
      root: true,
    })
    if (!check) return

    const id = getters.data.event.id
    try {
      const { data } = await this.$axios({
        method: 'post',
        url: `/events/${id}/comment/`,
        params: { comments_expand: 'created_by.avatar' },
        data: payload,
        headers: withToken(rootState),
      })
      dispatch(
        'comments/ADD',
        { ...data, created_by: rootGetters['user/GET_USER'] },
        { root: true },
      )
    } catch (e) {
      console.log(e)
    }
  },
  async GO_EVENT({ commit, getters, dispatch }) {
    const id = path(['event', 'id'], getters.data)
    const isUserVisitor = getters.isUserVisitor
    const url = isUserVisitor
      ? `/events/${id}/attend/decline/`
      : `/events/${id}/attend/`
    try {
      const { data } = await this.$axios.post(url)
      dispatch('notifications/success', data.message, { root: true })
    } catch (e) {}
    const visitors_count = getters.visitorsCount + 1 - 2 * isUserVisitor
    commit('UPDATE_EVENT', {
      is_user_visitor: !isUserVisitor,
      visitors_count,
    })
  },
  async REQUEST({ commit, getters }, payload) {
    const id = payload.id || path(['event', 'id'], getters.data)
    await this.$axios.post(`/events/${id}/request/`, payload)
  },
}

export const mutations = {
  SET(state, data) {
    if (typeof(data.event) === 'object' && data.event !== null){
      data.event = mapDatesData(data.event)
    }
    state.data = mapDatesData(data)
  },
  UPDATE_EVENT(state, data) {
    Object.assign(state.data.event, data)
  },
}

export const namespaced = true
