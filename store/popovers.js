import bannerConsts from '~/utils/bannerConsts'
import popovers from '~/utils/popoverConsts'
import { path } from 'ramda'

export const state = () => ({
  openedPopover: '',
  showPreLoader: false,
  popoverShareLink: '',
  popoverVideoId: '',
  /* custom data for popovers */
  popoverData: null,
  /* banners don't block scroll */
  openedBanner: '',
})

export const getters = {
  OPENED_POPOVER: state => state.openedPopover,
  OPENED_BANNER: state => state.openedBanner,
  SHOW_PRE_LOADER: state => state.showPreLoader,
  SHARE_LINK: state => state.popoverShareLink,
  VIDEO_ID: state => state.popoverVideoId,
  POPOVER_DATA: state => state.popoverData,
}

export const actions = {
  TOGGLE_POPOVER({ commit, state }, name) {
    if (state.openedPopover === name) commit('SET_TOGGLE_POPOVER', '')
    else commit('SET_TOGGLE_POPOVER', name || '')
  },
  OPEN_POPOVER_WITH_DATA({ commit }, { name, data }) {
    commit('SET_TOGGLE_POPOVER', name || '')
    commit('SET_POPOVER_DATA', data)
  },
  CLEAR_POPOVER_DATA({ commit }) {
    commit('SET_POPOVER_DATA', null)
  },
  TOGGLE_BANNER({ commit, state }, name) {
    state.openedBanner === name
      ? commit('TOGGLE_BANNER', '')
      : commit('TOGGLE_BANNER', name || '')
  },
  SWITCH_PRE_LOADER({ commit }, flag) {
    commit('SWITCH_PRE_LOADER', flag)
  },
  SET_POPOVER_SHARE_LINK({ commit }, v) {
    commit('SET_POPOVER_SHARE_LINK', v)
  },
  SET_POPOVER_VIDEO_ID({ commit }, v) {
    commit('SET_POPOVER_VIDEO_ID', v)
  },
  SET_BANNER_FROM_QUERY({ commit }) {
    const query = path(['currentRoute', 'query'], this.$router)
    if (!query) return

    const { banner, ...restQuery } = query

    const hasBanner = Object.keys(bannerConsts).some(
      v => bannerConsts[v] === banner,
    )

    if (hasBanner) {
      commit('TOGGLE_BANNER', '')
      commit('TOGGLE_BANNER', banner)
      this.$router.replace({
        query: restQuery,
      })
    }
  },
  MAIL_POPOVER({ dispatch, rootGetters }) {
    if (rootGetters['auth/IS_AUTH']) return
    const opened = this.$cookies.get('mailOpened')
    if (!opened) {
      this.$cookies.setIfAccepted('mailOpened', true, {
        path: '/',
        maxAge: 60 * 30,
      })
      dispatch('TOGGLE_POPOVER', popovers.simpleReg)
    }
  },
}

export const mutations = {
  SET_TOGGLE_POPOVER(state, name) {
    state.openedPopover = name
  },
  SET_POPOVER_DATA(state, data) {
    state.popoverData = data
  },
  TOGGLE_BANNER(state, name) {
    state.openedBanner = name
  },
  SWITCH_PRE_LOADER(state, flag) {
    state.showPreLoader = flag
  },
  SET_POPOVER_SHARE_LINK(state, v) {
    state.popoverShareLink = v
  },
  SET_POPOVER_VIDEO_ID(state, v) {
    state.popoverVideoId = v
  },
}

export const namespaced = true
