import { prop, path } from 'ramda'

export const state = () => ({
  project: null,
})

const projectsData = {
  3: {
    contacts: {
      phones: ['+7 966 097 38 98'],
      email: 'katena50587@mail.ru',
      site: 'http://mihailshats.ru',
      skype: null,
    },
  },
  4: {
    contacts: {
      email: 'zagranicy@gmail.com',
      skype: 'oversea17',
      site: null,
      phones: ['+375 29 628 41 14 BY', '+370 64 88 33 57 LT'],
    },
  },
}

const DEFAULT_COLOR = '#223a76'

export const getters = {
  GET_SOCIAL: state => {
    const links = prop('links', state.project)

    return (
      links &&
      links.reduce((res, l) => {
        if (l.type !== 'other') {
          res[l.type] = l.link
        }

        return res
      }, {})
    )
  },
  GET_VERSION: state => prop('version', state.project),
  GET_DESCRIPTION: state => prop('description', state.project),
  GET_DESCRIPTION_TEXT: state => prop('description_text', state.project),
  GET_PROJECT: state => state.project,
  GET_TITLE: state => prop('title', state.project),
  GET_PROJECT_ID: state => prop('id', state.project),
  GET_URL_ID: state => prop('url_id', state.project),
  GET_COVER: state => prop('cover', state.project),
  GET_LOGO: state => prop('logo', state.project),
  GET_LOCATION: state => path(['locations', 'results', '0'], state.project),
  GET_INSTITUTION: state =>
    path(['institutions', 'results', '0'], state.project),
  EMAILS: state =>
    path(['institutions', 'results', '0', 'emails'], state.project) || [],
  GET_USER: (state, { GET_PROJECT_ID }) => {
    const users = path(['users', 'results'], state.project)

    if (users && users.length) {
      const user = users.find(user => user.use_as_contact)

      if (!user) return null

      return {
        ...user,
        ...path([GET_PROJECT_ID, 'contacts'], projectsData),
      }
    } else {
      return null
    }
  },
  GET_LINKS: state => {
    const links = prop('links', state.project)

    return links && links.filter(l => l.type === 'other').map(l => l.link)
  },
  GET_CATEGORIES: state => prop('categories', state.project) || [],
  GET_SPECIAL_COLOR: state =>
    prop('main_color', state.project) || DEFAULT_COLOR,
  HAS_SPECIAL_EVENTS: state =>
    Boolean(path(['events', 'results', '0'], state.project)),
}

export const actions = {
  async FETCH_PROJECT({ commit }, id) {
    const { data } = await this.$axios.get(`/projects/${id}/`)
    commit('SET_PROJECT', data)

    commit('events/SET', data.events, { root: true })
    commit('videos/SET', data.videos, { root: true })
    commit('images/SET', data.images, { root: true })
    commit('users/set', data.users, { root: true })
    commit('comments/SET', data.comments, { root: true })

    if (path(['institutions', 'results', '0'], data)) {
      commit('place/SAVE_PLACE', data.institutions.results[0], {
        root: true,
      })
    }

    return data
  },
  async SEND_COMMENT({ dispatch, getters }, payload) {
    const check = await dispatch('user/CHECK_PERSONAL_DATA_AGREEMENT', null, {
      root: true,
    })
    if (!check) return

    const id = getters.GET_PROJECT_ID
    const { data } = await this.$axios.post(`/projects/${id}/comment/`, payload)
    dispatch('comments/ADD', data, { root: true })
  },
  async SEND_FORM({ dispatch }, payload) {
    // const check = await dispatch('user/CHECK_PERSONAL_DATA_AGREEMENT', null, {
    //   root: true
    // })
    // if (!check) return
    await this.$axios.post(`/projects/registrations/`, payload)
  },
}

export const mutations = {
  SET_PROJECT(state, v) {
    state.project = v
  },
}

export const namespaced = true
