import moment from 'moment'
import { mergeDeepRight, uniq, path } from 'ramda'

const purchase = () => ({
  selectedSessionId: null,
  user: {},
  tickets: [],
})

const initState = () => ({
  schemes: {
    loading: false,
    count: 0,
    next: null,
    num_pages: null,
    page_number: null,
    page_size: null,
    previous: null,
    results: [],
  },
  purchase: purchase(),
  tickets: {
    loading: false,
    count: 0,
    next: null,
    num_pages: null,
    page_number: null,
    page_size: null,
    previous: null,
    results: [],
  },
  ticketForm: {},
  showBuyModal: false
})

export const state = () => ({
  ...initState()
})

export const getters = {
  SHOW_BUY_MODAL: state => state.showBuyModal,
  COUNT: state => state.tickets.count,
  ONLY_FREE: state => state.schemes.results
    .filter(scheme => {
      const price = scheme.tickets_price.amount ? parseFloat(scheme.tickets_price.amount): 0.0
      return price > 0.0
    }
  ).length === 0,
  SCHEMES: state => {
    return state.schemes.results
      .map(scheme => {
        const { session, tickets_price, tickets_left, tickets_total } = scheme
        const { start_date, start_time, timezone } = session

        let dateTime  = null
        let date = null
        let time = null
        if (start_date && start_time){
          dateTime = moment.tz(`${start_date} ${start_time}`, timezone)
          date = dateTime.format('DD.MM.YYYY')
          time = dateTime.format('HH:mm')
        } else if (start_date){
          dateTime = moment.tz(`${start_date} 00:00`, timezone)
          date = dateTime.format('DD.MM.YYYY')
        } else if (start_time){
          dateTime = moment.tz(`${moment().format()} ${start_time}`, timezone)
          time = dateTime.format('HH:mm')
        }

        return {
          ...scheme,
          view: {
            sessionId: session.id,
            price: parseFloat(tickets_price.amount),
            currency: tickets_price.currency.symbol,
            tickets_left,
            tickets_total,
            date: date,
            time: time,
            dateTime,
            sortingDateTime: dateTime ? dateTime.toDate(): null,
          },
        }
      })
      .sort((a, b) => a.view.sortingDateTime - b.view.sortingDateTime)
  },
  HAS_MORE_SCHEMES: state => Boolean(state.schemes.next),
  SCHEMES_LOADING: state => Boolean(state.schemes.loading),

  AVAILABLE_TICKETS_COUNT(_, { SCHEMES }) {
    return SCHEMES.reduce((sum, scheme) => sum + scheme.view.tickets_left, 0)
  },
  PURCHASE: state => state.purchase,
  PURCHASE_SUMMARY: (_, getters) => {
    const { PURCHASE_TICKETS, SCHEMES } = getters

    return PURCHASE_TICKETS.reduce(
      (result, ticket) => {
        const schemeId = ticket.scheme
        const scheme = SCHEMES.find(s => s.id === schemeId)

        result.ticketsCount += 1
        result.totalPrice += scheme.view.price
        result.currency = scheme.view.currency

        const hasScheme = Object.prototype.hasOwnProperty.call(
          result.schemes,
          schemeId,
        )

        if (!hasScheme) {
          result.schemes[schemeId] = {
            data: scheme,
            count: 1,
          }
        } else {
          result.schemes[schemeId].count += 1
        }

        return result
      },
      {
        ticketsCount: 0,
        totalPrice: 0,
        schemes: {},
        currency: '',
      },
    )
  },
  IS_FREE_PURCHASE: (_, getters) => {
    const { ticketsCount, totalPrice } = getters.PURCHASE_SUMMARY

    return Boolean(ticketsCount && !totalPrice)
  },
  PURCHASE_TICKETS: (_, getters) => {
    return Object.keys(getters.TICKETS_FORM).reduce((result, sessionId) => {
      const sessionSchemes = getters.TICKETS_FORM[sessionId]

      const schemes = Object.keys(sessionSchemes).reduce((result, schemeId) => {
        result.push(...sessionSchemes[schemeId])

        return result
      }, [])
      result.push(...schemes)

      return result
    }, [])
  },
  PURCHASE_SESSION_ID: state => state.purchase.selectedSessionId,

  TICKETS: state => state.tickets.results,
  HAS_MORE_TICKETS: state => Boolean(state.tickets.next),
  TICKETS_LOADING: state => Boolean(state.tickets.loading),

  IS_SEAT_PURCHASED_BY_ID: (_, getters) => id => {
    if (id !== undefined) {
      return getters.PURCHASE_TICKETS.some(({ seat }) => seat !== undefined && seat === id)
    }
    return false
  },
  SCHEME_BY_ID: (_, getters) => id => {
    return getters.SCHEMES.find(s => s.id === id)
  },
  TICKETS_FORM: state => state.ticketForm,
}

export const actions = {
  RESET_STORE({ commit }){
    commit('RESET_STORE')
  },

  TOGGLE_BUY_MODAL({commit}, show= false){
    commit('TOGGLE_BUY_MODAL', show)
  },
  SAVE_PURCHASE({ commit}, data = {}) {
    commit('SAVE_PURCHASE', data)
  },
  async FETCH_SCHEMES({ commit, getters, dispatch }, params = {}) {
    if (getters.SCHEMES_LOADING) return

    const expand = uniq(
      'session'
        .split(',')
        .concat((params.expand || '').split(','))
        .filter(Boolean),
    ).join(',')

    try {
      commit('LOADING_SCHEMES', true)

      const { data } = await this.$axios.get(`/tickets/schemes/`, {
        params: { actual: true, ...params, expand },
      })
      commit('SET_SCHEMES', data)
      dispatch('INITIALIZE_TICKETS_FORM')
    } catch (e) {
      console.log(e)
    } finally {
      commit('LOADING_SCHEMES', false)
    }
  },
  async FETCH_SCHEMES_MORE({ state, commit, getters }) {
    if (getters.SCHEMES_LOADING || !getters.HAS_MORE_SCHEMES) return

    try {
      commit('LOADING_SCHEMES', true)

      const { data } = await this.$axios.get(state.schemes.next)

      commit('ADD_SCHEMES', data)
    } finally {
      commit('LOADING_SCHEMES', false)
    }
  },

  async BUY_TICKETS({ getters }, data = {}) {
    const { selectedSessionId, user } = getters.PURCHASE
    const { PURCHASE_TICKETS } = getters

    const postData = {
      ...user,
      session: selectedSessionId,
      tickets: PURCHASE_TICKETS,
    }
    const res = await this.$axios.post('/tickets/orders/', postData)

    const { payment_url, is_free, redirect_url } = res.data

    if (is_free) {
      location.href = redirect_url
    } else if (payment_url) {
      location.href = payment_url
    }
  },

  async FETCH_TICKETS({ getters, commit }, params) {
    if (getters.TICKETS_LOADING) return

    try {
      commit('LOADING_TICKETS', true)

      const { data } = await this.$axios.get('/tickets/', {
        params: {
          expand:
            'order,scheme.session,scheme.event,scheme.event.cover,scheme.event.type,scheme.event.category,scheme.session.location,scheme.organizer',
          ...params,
        },
      })
      commit('SET_TICKETS', data)
    } catch (e) {
      console.log(e)
    } finally {
      commit('LOADING_TICKETS', false)
    }
  },
  async FETCH_TICKETS_MORE({ state, commit, getters }) {
    if (getters.TICKETS_LOADING || !getters.HAS_MORE_TICKETS) return

    try {
      commit('LOADING_TICKETS', true)

      const { data } = await this.$axios.get(state.tickets.next)

      commit('ADD_TICKETS', data)
    } finally {
      commit('LOADING_TICKETS', false)
    }
  },
  TOGGLE_PURCHASE_TICKET({ commit, getters }, v) {
    const hasTicket = getters.TICKETS_FORM[v.session][v.scheme].some(
      ({ seat }) => seat === v.seat,
    )
    if (hasTicket) {
      commit('REMOVE_PURCHASE_TICKET', v)
    } else {
      commit('ADD_PURCHASE_TICKET', v)
    }
  },
  INITIALIZE_TICKETS_FORM({ commit, getters }) {
    const { SCHEMES } = getters
    const ticketForm = SCHEMES.reduce((result, scheme) => {
      const { sessionId } = scheme.view
      const hasSession = Object.prototype.hasOwnProperty.call(result, sessionId)

      if (hasSession) {
        result[sessionId][scheme.id] = []
      } else {
        result[sessionId] = {}
        result[sessionId][scheme.id] = []
      }

      return result
    }, {})
    commit('SET_TICKETS_FORM', ticketForm)
  },
  SET_PURCHASE_TICKETS_BY_SESSION({ commit, getters }, v) {
    const currency = path(['view', 'currency'], getters.SCHEME_BY_ID(v.scheme))
    const sessionSchemes = getters.TICKETS_FORM[v.session]

    const checkCurrency = Object.keys(sessionSchemes).some(schemeId => {
      const hasTickets = sessionSchemes[schemeId].length

      return (
        hasTickets &&
        currency ===
          path(['view', 'currency'], getters.SCHEME_BY_ID(Number(schemeId)))
      )
    })

    if (!checkCurrency) {
      commit('RESET_PURCHASE_TICKETS_BY_SESSION', { session: v.session })
    }

    commit('SET_PURCHASE_TICKETS_BY_SESSION', v)
  },
  RESET_PURCHASE({ commit, dispatch }, v) {
    dispatch('INITIALIZE_TICKETS_FORM')
    commit('RESET_PURCHASE')
  },
}

export const mutations = {
  RESET_STORE(state){
    state = Object.assign(state, initState())
  },
  TOGGLE_BUY_MODAL(state, { show }) {
    state.showBuyModal = show
  },
  ADD_PURCHASE_TICKET(state, { session, scheme, seat }) {
    state.ticketForm[session][scheme].push({ scheme, seat })
  },
  REMOVE_PURCHASE_TICKET(state, { session, scheme, seat }) {
    const tickets = state.ticketForm[session][scheme].filter(
      s => s.seat !== seat,
    )

    state.ticketForm[session][scheme] = tickets
  },
  SET_PURCHASE_TICKETS_BY_SESSION(state, { tickets, session, scheme }) {
    state.ticketForm[session][scheme] = tickets
  },
  RESET_PURCHASE_TICKETS_BY_SESSION(state, { session }) {
    Object.keys(state.ticketForm[session]).forEach(schemeId => {
      state.ticketForm[session][schemeId] = []
    })
  },
  LOADING_SCHEMES(state, loading) {
    state.schemes.loading =
      typeof loading !== 'undefined' ? Boolean(loading) : !state.schemes.loading
  },
  SET_SCHEMES(state, data = {}) {
    state.schemes = mergeDeepRight(state.schemes, data)
  },
  ADD_SCHEMES(state, data = {}) {
    state.schemes = {
      ...state.schemes,
      ...data,
      results: state.schemes.results.concat(data.results),
    }
  },
  RESET_SCHEMES(s) {
    s.schemes = state().schemes
  },
  SAVE_PURCHASE(state, data = {}) {
    state.purchase = mergeDeepRight(state.purchase, data)
  },
  RESET_PURCHASE(state) {
    state.purchase = purchase()
  },

  LOADING_TICKETS(state, loading) {
    state.tickets.loading =
      typeof loading !== 'undefined' ? Boolean(loading) : !state.schemes.loading
  },
  SET_TICKETS(state, data = {}) {
    state.tickets = mergeDeepRight(state.tickets, data)
  },
  ADD_TICKETS(state, data = {}) {
    state.tickets = {
      ...state.tickets,
      ...data,
      results: state.tickets.results.concat(data.results),
    }
  },
  SET_TICKETS_FORM(state, v) {
    state.ticketForm = v
  },
}
