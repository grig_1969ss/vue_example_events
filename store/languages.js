export const state = () => ({
  count: 0,
  next: null,
  num_pages: 0,
  page_number: 1,
  page_size: 4,
  previous: null,
  results: [],
})

export const getters = {
  RESULTS: state => state.results,
  MORE: state => Boolean(state.next),
}

export const actions = {
  async FETCH({ commit, getters }, params) {
    const { data } = await this.$axios.get('/languages/', { params })
    commit('SET', data)
    return data
  },
  async FETCH_NEXT({ commit, getters, state }, params) {
    const { data } = await this.$axios.get(state.next, { params })
    data.results = getters.RESULTS.concat(data.results)
    commit('SET', data)
  },
}

export const mutations = {
  SET(state, data) {
    Object.assign(state, data)
  },
}

export const namespaced = true
