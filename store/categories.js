import { path, uniqBy } from 'ramda'

export const state = () => ({
  categories: null,
})

export const getters = {
  categories: state => state.categories || [],
  filtered: (state, getters) =>
    [...getters.categories]
      .sort((a, b) => (a.total_events > b.total_events ? -1 : 1))
      .slice(0, 15),
  selected: (state, getters) =>
    [...getters.categories]
      .filter(value => value.selected && value.cover && value.icon)
      .slice(0, 8),
}

export const actions = {
  async FETCH({ commit }, params) {
    try {
      const results = await Promise.all([
        this.$axios.get('/categories/', { params }).catch(e => {
          console.log(e.message)
          return { data: { results: [] } }
        }),
        this.$axios.get('/genres/', { only_parents: true, params }).catch(e => {
          console.log(e.message)
          return { data: { results: [] } }
        }),
        this.$axios.get('/event_types/', { params }).catch(e => {
          console.log(e.message)
          return { data: { results: [] } }
        }),
      ])

      const result = uniqBy(item => item.slug, [
        ...(path(['data', 'results'], results[0]) || []),
        ...(path(['data', 'results'], results[1]) || []),
        ...(path(['data', 'results'], results[2]) || []),
      ])
      commit('SET', result)
      return result
    } catch (err) {
      console.log(err)
    }
  },
}

export const mutations = {
  SET(state, data) {
    state.categories = data
  },
}

export const namespaced = true
