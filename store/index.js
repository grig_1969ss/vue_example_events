import cookieConsts from '~/utils/cookieConsts'
import cityByIP from '~/utils/geoLite2'
import { RU_LANG_COUNTRIES } from '~/constants/ru_countries'

const requestIp = process.server ? require('request-ip') : undefined
const crypto = process.server ? require('crypto') : undefined


export const actions = {
  async nuxtServerInit({ getters, commit, dispatch }, { app, req, query }) {
    async function get_lang(){
      /* set default language */

      if (query.lang && dispatch('lang/CHECK_LANG', {lang: query.lang}))
        return query.lang
      else if (parsed.django_language && dispatch('lang/CHECK_LANG', {lang: parsed.django_language}))
        return parsed.django_language

      if (ip) {
        const { country } = await cityByIP(ip)
        if (country){
          return RU_LANG_COUNTRIES.includes(country.geonameId) ? 'ru' : 'en'
        }
      }
      if (typeof req.headers['accept-language'] !== 'undefined') {
        return getHeaderLang(req)
      }
      return 'en'
    }

    if (process.env.NUXT_GENERATE === 'TRUE') {
      commit('lang/SET_LANG', 'en')
      const locale = getters['lang/LANG']
      app.i18n.locale = locale
      app.moment.locale(locale)
      return
    }

    const ip = requestIp.getClientIp(req)
    app.$axios.proxyHeaders(req, 'user-agent')
    app.$axios.setClientIp(ip)

    const parsed = app.$cookies.getAll()
    const viewsHash = parsed.VIEWS_HASH
    app.$cookies.setIfAccepted('VIEWS_HASH', generateViewsKey(viewsHash), {
      maxAge: 60 * 60 * 24 * 30,
      path: '/',
    })

    if (parsed[cookieConsts.isCookieAccepted] && parsed.auth) {
      await dispatch('auth/SET_UP_TOKEN', parsed.auth.token)
    } else {
      const token = getters['auth/TOKEN']
      if (token){
        await dispatch('auth/SET_UP_TOKEN', token)
      }
    }
    const lang = await get_lang()
    commit('lang/SET_LANG', lang)

    app.$cookies.setIfAccepted('django_language', getters['lang/LANG'], {
      maxAge: 60 * 60 * 24 * 30,
      path: '/',
    })
    /* set lang – end */

    /* set country, city start */
    const isBot = getters['detect/IS_BOT']
    if (isBot) {
      commit('lang/SET_LANG', 'ru')
      /*
        if isBot user-agent, set country RUSSIA (2017370)
        and city MOSCOW (524901)
      */
      await dispatch('city/GET_ORIGIN_COUNTRY', {
        ip,
        id: 2017370,
      })
      await dispatch('city/INITIAL_CITY', { ip, id: 524901 })
    } else {
      await dispatch('city/GET_ORIGIN_COUNTRY', {
        ip,
        id: parsed.fromCountryId,
      })

      const cityId =
        parsed.userSelectedCity || parsed.geoCityId || parsed.cityId
      await dispatch('city/INITIAL_CITY', { ip, id: cityId })
    }
    /* set country, city end */

    const locale = getters['lang/LANG']
    app.i18n.locale = locale
    app.moment.locale(locale)
  },
}

function getHeaderLang(req) {
  return req.headers['accept-language']
    .split(',')[0]
    .toLocaleLowerCase()
    .substring(0, 2)
}

function generateViewsKey(viewsHash) {
  if (viewsHash) {
    return viewsHash
  } else {
    return crypto.randomBytes(8).toString('hex')
  }
}

export const strict = false
