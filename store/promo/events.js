import { getLatestModified } from '~/utils'

export const state = () => ({
  loading: false,
  count: 0,
  next: null,
  num_pages: 0,
  page_number: 1,
  page_size: 4,
  previous: null,
  results: [],
})

export const getters = {
  EVENTS: state => state.results,
  modified: state => getLatestModified(state.results),
  COUNT: state => state.count,
  MORE: state => Boolean(state.next),
  LOADING: state => state.loading,
}

export const actions = {
  async FETCH({ commit, getters }, params) {
    if (getters.LOADING) return

    commit('SET', {
      count: 0,
      next: null,
      previous: null,
      results: [],
    })

    try {
      commit('LOADING', true)
      const { data } = await this.$axios.get('/promo/events/', { params })
      commit('SET', data)
    } catch (err) {
      throw err
    } finally {
      commit('LOADING', false)
    }
  },
  async FETCH_MORE({ commit, getters, state }) {
    if (getters.LOADING) return
    try {
      commit('LOADING', true)

      const { data } = await this.$axios.get(state.next)
      data.results = state.results.concat(data.results)
      commit('SET', data)
    } finally {
      commit('LOADING', false)
    }
  },
}

export const mutations = {
  SET(state, data) {
    state = Object.assign(state, data)
  },
  LOADING(state, value) {
    state.loading = value
  },
}

export const namespaced = true
