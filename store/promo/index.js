// function scrollToTop(time) {
//   let scrollStep = -window.scrollY / (time / 15)
//   let scrollInterval = setInterval(() => {
//     if (window.scrollY !== 0) {
//       window.scrollBy(0, scrollStep)
//     } else clearInterval(scrollInterval)
//   }, 15)
// }

export const state = () => ({
  data: {
    tags: [],
    gender: 2,
    age_from: null,
    age_to: null,
    plan: null,
    extra: [],
    start_dt: '',
    email: null,
  },
  errors: false,
  loader: false,
  offers: [],
  disabled: false,
  valueTariff2: null,
})

export const getters = {
  DATA: state => state.data,
  PROMO_GENDER: state => state.data.gender,
  PROMO_EMAIL: state => state.data.email,
  PROMO_ERRORS: state => state.errors,
  PROMO_LOADER: state => state.loader,
  PROMO_OFFERS: state => state.offers,
  DISABLED: state => state.disabled,
  VALUE_TARIFF2: state => state.valueTariff2,
}

export const actions = {
  BTN_DISABLED({ commit }, value) {
    commit('BTN_DISABLED', value)
  },
  VALUE_TARIFF2({ commit }, value) {
    commit('VALUE_TARIFF2', value)
  },
  async SEND_PROMO_DATA({ commit, state }, id) {
    try {
      state.loader = true
      const res = await this.$axios.post(`/events/${id}/promote/`, state.data)
      if (res.data.payment_url) {
        window.location.assign(res.data.payment_url)
      }
    } catch (err) {
      commit('SAVE_PROMO_ERRORS', err.response.data)
      // scrollToTop(500)
      state.loader = false
    }
  },
  async GET_OFFER({ commit }) {
    try {
      const res = await this.$axios.get(`/promo/plans/`, {
        params: { expand: 'rule,service' },
      })
      const data = res.data.results
      commit('SAVE_PROMO_OFFER', data[data.length - 1].id)
      commit('SAVE_PROMO_OFFERS', data)
      return data
    } catch (err) {
      console.log(err)
    }
  },
  SET_GENDER({ commit }, gen) {
    commit('SAVE_PROMO_GENDER', gen)
  },
  SET_AGE({ commit }, age) {
    commit('SAVE_PROMO_AGE', age)
  },
  SET_OFFER({ commit }, id) {
    commit('SAVE_PROMO_OFFER', id)
  },
  SET_DATE({ commit }, date) {
    commit('SAVE_PROMO_DATE', date)
  },
  SET_EMAIL({ commit }, email) {
    commit('SAVE_PROMO_EMAIL', email)
  },
}

export const mutations = {
  BTN_DISABLED(state, value) {
    state.disabled = value
  },
  VALUE_TARIFF2(state, value) {
    state.valueTariff2 = value
  },
  SAVE_PROMO_ERRORS(state, err) {
    state.errors = err
  },
  SAVE_PROMO_GENDER(state, gen) {
    state.data.gender = gen
  },
  SAVE_PROMO_AGE(state, age) {
    state.data.age_from = age[0]
    state.data.age_to = age[1]
  },
  SAVE_PROMO_OFFER(state, id) {
    state.data.plan = id
  },
  SAVE_PROMO_OFFERS(state, offers) {
    state.offers = offers
  },
  SAVE_PROMO_DATE(state, date) {
    state.data.start_dt = date
  },
  SAVE_PROMO_EMAIL(state, email) {
    state.data.email = email
  },
}

export const namespaced = true
