import { getLatestModified } from '~/utils'

const initState = () => ({
  count: 0,
  next: null,
  num_pages: 0,
  page_number: 1,
  page_size: 4,
  previous: null,
  results: [],
})

export const state = () => ({
  ...initState()
})

export const getters = {
  JOURNALS: state => state.results,
  modified: state => getLatestModified(state.results),
  COUNT: state => state.count,
  MORE: state => Boolean(state.next),
}

export const actions = {
  RESET_STORE({ commit }){
    commit('RESET_STORE')
  },
  async FETCH({ commit, getters }, params) {
    commit('SET', {
      count: 0,
      next: null,
      previous: null,
      results: [],
    })
    const { data } = await this.$axios.get('/journals/', { params })
    commit('SET', data)
    return data
  },
  async FETCH_MORE({ commit, getters, state }) {
    const { data } = await this.$axios.get(state.next)
    data.results = state.results.concat(data.results)
    commit('SET', data)
  },
}

export const mutations = {
  RESET_STORE(state){
    state = Object.assign(state, initState())
  },
  SET(state, data) {
    state = Object.assign(state, data)
  },
}

export const namespaced = true
