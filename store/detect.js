export const state = () => ({
  isBot: null,
})

export const getters = {
  IS_BOT: state => state.isBot,
}

export const actions = {
  SET_IS_BOT({ commit }, v) {
    commit('SET_IS_BOT', v)
  },
}

export const mutations = {
  SET_IS_BOT(state, v) {
    state.isBot = v
  },
}

export const namespaced = true
