import { prop } from 'ramda'

export const state = () => ({
  data: {},
})

export const getters = {
  data: state => state.data,
  socialLinks: state => {
    const links = prop('links', state.data)

    return (
      links &&
      links.reduce((res, l) => {
        if (l.type !== 'other') {
          res[l.type] = l.link
        }

        return res
      }, {})
    )
  },
}

export const actions = {
  async fetch({ dispatch, getters }, { id }) {
    try {
      const project = id
      await Promise.all([
        dispatch('fetchProject', { project }),
        dispatch('fetchComments', { project }),
        dispatch('project/sessions/fetch', { project }, { root: true }),
        dispatch('project/videos/fetch', { project }, { root: true }),
        dispatch('project/images/fetch', { project }, { root: true }),
        dispatch('project/users/fetch', { project }, { root: true }),
      ])
      return getters.data
    } catch (err) {}
  },
  async fetchProject({ commit, getters }, { project }) {
    try {
      const { data } = await this.$axios.get(`/projects/${project}/`, {
        params: { expand: 'logo,cover' },
      })
      commit('set', data)
    } catch (err) {
    }
  },
  async fetchComments({ dispatch }, { project }) {
    const { data } = await this.$axios.get(`/projects/${project}/comments/`, {
      params: { comments_expand: 'created_by.avatar' },
    })
    dispatch('comments/SET', data, { root: true })
  },
  async sendComment({ dispatch, getters, rootGetters }, payload) {
    const check = await dispatch('user/CHECK_PERSONAL_DATA_AGREEMENT', null, {
      root: true,
    })
    if (!check) return

    const id = getters.data.id
    const { data } = await this.$axios.post(`/projects/${id}/comment/`, payload)
    dispatch(
      'comments/ADD',
      { ...data, created_by: rootGetters['user/GET_USER'] },
      { root: true },
    )
  },
  async sendForm({ dispatch }, payload) {
    await this.$axios.post(`/projects/registrations/`, payload)
  },
}

export const mutations = {
  set(state, data) {
    state.data = data
  },
}

export const namespaced = true
