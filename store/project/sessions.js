import { getLatestModified } from '~/utils'
import { mapDatesData, deSerializeObjectDates } from '~/utils/event'

export const state = () => ({
  count: 0,
  results: [],
})

export const getters = {
  results: state => {
    return state.results.map(session => {
      let event = session.event
      if (typeof(event) === 'object' && event !== null){
        event = deSerializeObjectDates(event)
      }
      return deSerializeObjectDates(Object.assign(session, {event: event}))
    })
  },
  count: state => state.count,
  modified: state => getLatestModified(state.results),
  more: state => Boolean(state.next),
  leftToGet: state => state.count - state.results.length,
}

export const actions = {
  async fetch({ commit, getters }, params) {
    try {
      const innerParams = {
        expand: [
          'event',
          'event.cover',
          'event.category',
          'event.type',
          'location',
        ].join(),
        period: 'month',
        page_size: 50,
      }
      const { data } = await this.$axios.get('/events/sessions/', {
        params: { ...innerParams, ...params },
      })
      commit('set', data)
      return data
    } catch (err) {}
  },
  async fetchMore({ commit, getters }, params) {
    try {
      const { data } = await this.$axios.get(getters.next, { params })
      data.results = getters.results.concat(data.results)
      commit('set', data)
      return data
    } catch (err) {}
  },
}

export const mutations = {
  set(state, data) {
    const results = data.results || []
    results.map(session => {
      if (typeof(session.event) === 'object' && session.event !== null){
        session.event = mapDatesData(session.event)
      }
      return mapDatesData(session)
    })
    data.results = results
    Object.assign(state, data)
  },
}

export const namespaced = true
