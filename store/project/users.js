export const state = () => ({
  results: [],
})

export const getters = {
  results: state => state.results || [],
  next: state => state.next,
  pageSize: state => state.page_size,
  leftToGet: state => state.count - state.results.length,
}

export const actions = {
  async fetch({ commit, getters }, params) {
    try {
      const innerParams = { expand: 'avatar' }
      const { data } = await this.$axios.get('/users/', {
        params: { ...innerParams, ...params },
      })
      commit('set', data)
      return data
    } catch (err) {}
  },
  async fetchMore({ commit, getters }, params) {
    try {
      const { data } = await this.$axios.get(getters.next, { params })
      data.results = getters.results.concat(data.results)
      commit('set', data)
      return data
    } catch (err) {}
  },
}

export const mutations = {
  set(state, data) {
    Object.assign(state, data)
  },
}

export const namespaced = true
