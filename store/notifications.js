import { path } from 'ramda'

export const state = () => ({
  messages: [],
  delay: 5000,
})

export const getters = {
  messages: state => state.messages,
}

export const actions = {
  nullify({ commit, state }) {
    if (state.messages.length > 0) {
      setTimeout(() => commit('nullify'), state.delay)
    }
  },
  error({ dispatch, state }, message) {
    dispatch('message', { message, type: 'error' })
  },
  success({ dispatch, state }, message) {
    dispatch('message', { message, type: 'success' })
  },
  message({ commit, state }, { type, message }) {
    const parseText = message => {
      switch (typeof message) {
        case 'string':
          return [message]
        case 'object':
          if (Array.isArray(message)) {
            return message.reduce((acc, msg) => [...acc, ...parseText(msg)], [])
          } else {
            return parseText(Object.values(message))
          }
        default:
          return []
      }
    }

    let requestResponse = path(['request', 'response'], message)
    try {
      requestResponse = JSON.parse(requestResponse)
    } catch (e) {}
    const messages = [...new Set(parseText(requestResponse || message))]

    messages.map(text => {
      const id = (~~(Math.random() * 1e8)).toString(16)
      commit('add', { id, type, text })
      setTimeout(() => {
        commit('check', id)
      }, state.delay)
    })
  },
  check({ commit }, index) {
    commit('check', index)
  },
}

export const mutations = {
  add(state, message) {
    state.messages.push(message)
  },
  check(state, id) {
    state.messages = state.messages.filter(msg => msg.id !== id)
  },
  nullify(state) {
    state.messages = []
  },
}

export const namespaced = true
