export const state = () => ({
  data: null,
})

export const getters = {
  results: state => state.data || [],
  filtered: (state, getters) =>
    [...getters.results]
      .sort((a, b) => (a.total_events > b.total_events ? -1 : 1))
      .slice(0, 15),
  selected: (state, getters) =>
    [...getters.results]
      .filter(value => value.selected && value.cover && value.icon)
      .slice(0, 8),
}

export const actions = {
  async FETCH({ commit, dispatch }, params) {
    const result = await dispatch('categories/FETCH', params, { root: true })
    commit('save', result)
  },
}

export const mutations = {
  save(state, data) {
    state.data = data
  },
}

export const namespaced = true
