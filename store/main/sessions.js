import { prop, groupBy, sort } from 'ramda'
import { getLatestModified } from '~/utils'
import { mapDatesData, deSerializeObjectDates } from '~/utils/event'

export const state = () => ({
  data: null,
})

export const getters = {
  results: state => {
    const results = prop('results', state.data) || []
    return results.map(group => {
      return group.map(session => {
        if (typeof(session.event) === 'object' && session.event !== null){
          session.event = deSerializeObjectDates(session.event)
        }
        return deSerializeObjectDates(session)
      })
    })
  },
  isMore: state => prop('next', state.data),
  count: state => prop('count', state.data),
  modified: (state, getters) => {
    let sessions = []
    getters.results.forEach(group => {
      sessions = sessions.concat(group)
    })
    return getLatestModified(sessions)
  },
}

export const actions = {
  async FETCH({ commit }, params) {
    const innerParams = {
      expand: [
        'event',
        'event.cover',
        'event.category',
        'event.type',
        'location',
      ].join(),
      period: 'month',
      page_size: 50,
    }
    const { data } = await this.$axios.get('/events/sessions/', {
      params: { ...innerParams, ...params },
    })

    const results = data.results || []
    results.map(session => {
      if (typeof(session.event) === 'object' && session.event !== null){
        session.event = mapDatesData(session.event)
      }
      return mapDatesData(session)
    })

    const groupSessions = Object.values(data.results)
    const groupSessions2 = groupSessions.map(function(item, idx) {
      const pair = { idx: idx }
      return { ...item, ...pair }
    })
    const groupSessions3 = Object.values(
      groupBy(item => item.event.id, groupSessions2),
    )

    const diff = function(a, b) {
      return a[0].idx - b[0].idx
    }

    const groupSessions4 = sort(diff, groupSessions3)

    commit('save', {
      ...data,
      results: groupSessions4.slice(0, 7),
      next: groupSessions4.length > 7,
    })
  },
}

export const mutations = {
  save(state, data) {
    state.data = data
  },
}

export const namespaced = true
