import { sortWith, prop, ascend, path } from 'ramda'
import popoverConsts from '~/utils/popoverConsts'

export const state = () => ({
  results: [],
})

export const getters = {
  // RESULTS: state => state.results,
  RESULTS: state => {
    const plans = state.results.map(plan => ({
      ...plan,
      durationDays: +plan.duration.split(' ')[0],
    }))

    return sortWith([
      ascend(prop('price')),
      ascend(prop('price_original')),
      ascend(prop('durationDays')),
      ascend(prop('audience')),
    ])(plans)
  },
  SERVICE_NAME: state => path([0, 'rule', 'service', 'name'], state.results),
}

export const actions = {
  async FETCH({ commit, getters }, params) {
    const { data } = await this.$axios.get('/promo/plans/', {
      params: { ...params, expand: 'rule,service' },
    })
    commit('SET', data)
    return data
  },

  async BUY_PLAN({ commit, dispatch }, payload) {
    const { plan } = payload
    payload.plan = plan.id

    const { data } = await this.$axios.post('/promo/events/', payload)
    const { payment_url } = data

    if (plan.price === 0) {
      commit('popovers/SET_POPOVER_DATA', { status: 'request' }, { root: true })
      dispatch('popovers/TOGGLE_POPOVER', popoverConsts.paymentSuccess, {
        root: true,
      })
    } else if (payment_url) {
      window.location = payment_url
    }
  },
}

export const mutations = {
  SET(state, data) {
    Object.assign(state, data)
  },
}

export const namespaced = true
