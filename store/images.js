import { compressImage } from '~/utils'

export const state = () => ({
  loading: false,
  page_number: 1,
  page_size: 6,
  count: 0,
  results: [],
})

export const getters = {
  IMAGES: state => state.results,
  HAS_IMAGES: state => state.results.length,
  COUNT: state => state.count,
  LEFT_TO_GET: state => state.count - state.results.length,
  LOADING: state => state.loading,
}

export const actions = {
  async FETCH({ commit, getters }, params) {
    if (getters.LOADING) return
    try {
      commit('LOADING', true)
      const { data } = await this.$axios.get('/images/', { params })
      commit('SET', data)
    } finally {
      commit('LOADING', false)
    }
  },
  async FETCH_MORE({ commit, getters, state }, params) {
    if (getters.LOADING) return
    try {
      commit('LOADING', true)
      Object.assign(params, { page: state.page_number + 1 })
      const { data } = await this.$axios.get('/images/', { params })
      data.results = state.results.concat(data.results)
      commit('SET', data)
    } finally {
      commit('LOADING', false)
    }
  },
  async UPLOAD(_, payload) {
    if (payload.file.type === 'image/gif') {
      throw Error(this.app.i18n.t('error.unsupportedFormat'))
    }
    const file = await compressImage(payload.file)
    const { find_gradient, find_main_color } = payload
    const formData = new FormData()
    formData.append('image', file)
    if (find_gradient) formData.append('find_gradient', true)
    if (find_main_color) formData.append('find_main_color', true)

    try {
      const { data } = await this.$axios.post('/images/', formData)

      return data
    } catch {
      throw Error(this.app.i18n.t('error.failedLoadImage'))
    }
  },
}

export const mutations = {
  SET(state, data) {
    Object.assign(state, data)
  },
  LOADING(state, value) {
    state.loading = value
  },
}

export const namespaced = true
