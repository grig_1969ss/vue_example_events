export const state = () => ({})

export const actions = {
  async SUBSCRIBE_CITY({ state, rootGetters }, email) {
    let data = {
      email: email,
      subscription: 'cities',
      subscription_id: rootGetters['city/GET_CITY_ID'],
    }
    try {
      let response = await this.$axios.post('/subscribe/', data)
      if (response.data) {
        return response
      }
    } catch (e) {
      console.log(e)
      return e.response.data
    }
  },
  async SUBSCRIBE_ALL({ state, getters }, email) {
    let data = {
      email: email,
      subscription: null,
      subscription_id: null,
    }
    try {
      let response = await this.$axios.post('/subscribe/', data)
      if (response.data) {
        return response
      }
    } catch (e) {
      console.log(e)
      return e.response.data
    }
  },
}

export const namespaced = true
