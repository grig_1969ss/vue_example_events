export const state = () => ({
  events: [],
})

export const getters = {
  EVENTS: state => state.events,
  EVENTS_IDS: state => state.events.map(({ id }) => id),
  EVENTS_COUNT: state => state.events.length,
  HAS_EVENT_ID: state => eventId =>
    state.events.some(({ id }) => id === eventId),
}

export const actions = {
  RESET_EVENTS({ commit }) {
    commit('SET_EVENTS', [])
  },
  ADD_EVENT({ commit, getters }, event) {
    const hasEvent = getters.EVENTS.some(({ id }) => id === event.id)
    if (hasEvent) return

    commit('SET_EVENTS', [...getters.EVENTS, event])
  },
  REMOVE_EVENT({ commit, getters }, event) {
    const events = getters.EVENTS.filter(({ id }) => id !== event.id)

    commit('SET_EVENTS', events)
  },
}

export const mutations = {
  SET_EVENTS(state, events) {
    state.events = events
  },
}

export const namespaced = true
