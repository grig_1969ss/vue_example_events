import { path } from 'ramda'
import { mapDatesData, deSerializeObjectDates} from '~/utils/event'

const withToken = s =>
  path(['auth', 'token'], s)
    ? { Authorization: `JWT ${path(['auth', 'token'], s)}` }
    : {}

export const state = () => ({
  event: {},
  eventEditServerErrors: {},
})

export const getters = {
  EVENT: state => deSerializeObjectDates(state.event),
  modified: state => path(['modified'], deSerializeObjectDates(state.event)),
  IS_PREMIUM: state => path(['is_promo'], state.event),
  URL_ID: state => path(['url_id'], state.event),
  TICKETS_PROVIDER: state => path(['tickets', 'provider'], state.event),
  isUserVisitor: state => path(['is_user_visitor'], state.event),
  visitorsCount: state => path(['visitors_count'], state.event),
  SEATS_SCHEME: state => path(['tickets', 'data', 'seats_scheme'], state.event),
  EVENT_EDIT_SERVER_ERRORS: state => state.eventEditServerErrors,
}

export const actions = {
  async FETCH({ commit, rootState }, { id, params }) {
    const innerParams = {
      expand: [
        'cover',
        'genres',
        'category',
        'tags',
        'type',
        'organizer.logo',
        'poster_1080x1080',
        'poster_1200x630',
        'poster_800x1080',
        'images',
      ].join(),
    }
    try {
      const { data } = await this.$axios.get(`/events/${id}/`, {
        headers: withToken(rootState),
        params: { ...innerParams, ...params },
      })
      commit('SET', data)
      commit('comments/SET', data.comments, { root: true })
      return data
    } catch (e){
      console.error(e)
      this.app.router.push('/')
    }
  },
  async UPDATE({ commit, rootState }, payload) {
    try {
      const res = await this.$axios({
        method: 'patch',
        url: `/events/${payload.id}/`,
        data: payload.data,
        headers: withToken(rootState),
      })
      return res.data
    } catch (e) {
      const responseError = e.response.data

      const parsedErrors = {}

      for (const item in responseError) {
        const isArray = Array.isArray(responseError[item])

        parsedErrors[item] = isArray
            ? responseError[item][0]
            : responseError[item]
      }
      commit('EVENT_EDIT_SERVER_ERRORS',  parsedErrors)
      throw e
    }
  },
  async DELETE(ctx, { id }) {
    try {
      return await this.$axios.delete(`/events/${id}/`)
    } catch (err) {
      console.log('error during removal')
      throw err
    }
  },
  async POSTERS({ commit }, payload) {
    const res = await this.$axios.post(`/events/${payload.id}/posters/`)
    return res.data
  },
  async FETCH_COMMENTS({ dispatch }, { id }) {
    const { data } = await this.$axios.get(`/events/${id}/comments/`, {
      params: { comments_expand: 'created_by.avatar' },
    })
    dispatch('comments/SET', data, { root: true })
  },
  async SEND_COMMENT({ dispatch, getters, rootGetters, rootState }, payload) {
    const check = await dispatch('user/CHECK_PERSONAL_DATA_AGREEMENT', null, {
      root: true,
    })
    if (!check) return

    const id = getters.EVENT.id
    try {
      const { data } = await this.$axios({
        method: 'post',
        url: `/events/${id}/comment/`,
        params: { comments_expand: 'created_by.avatar' },
        data: payload,
        headers: withToken(rootState),
      })
      dispatch(
        'comments/ADD',
        { ...data, created_by: rootGetters['user/GET_USER'] },
        { root: true },
      )
    } catch (e) {
      console.log(e)
    }
  },
  async MAKE_POSTERS({ commit, rootState }, payload) {
    const res = await this.$axios('/events/third-step/', {
      method: 'post',
      data: payload.data,
      headers: { 'accept-language': rootState.lang.locale },
    })
    return res.data
  },
  async PROMOTE({ commit }, payload) {
    const { data } = await this.$axios.post('/promo/events/', payload)
    return data
  },
  async GO_EVENT({ commit, getters, dispatch }) {
    const id = path(['id'], getters.EVENT)
    const isUserVisitor = getters.isUserVisitor
    const url = isUserVisitor
      ? `/events/${id}/attend/decline/`
      : `/events/${id}/attend/`
    try {
      const { data } = await this.$axios.post(url)
      dispatch('notifications/success', data.message, { root: true })
    } catch (e) {}
    const visitors_count = getters.visitorsCount + 1 - 2 * isUserVisitor
    commit('UPDATE_EVENT', {
      is_user_visitor: !isUserVisitor,
      visitors_count,
    })
  },
  restoreEventEditServerErrorrs({ commit }) {
    commit('EVENT_EDIT_SERVER_ERRORS', {})
  },
}

export const mutations = {
  SET(state, data) {
    state.event = mapDatesData(data)
  },
  UPDATE_EVENT(state, data) {
    Object.assign(state.event, data)
  },
  EVENT_EDIT_SERVER_ERRORS(state, v) {
    state.eventEditServerErrors = v
  },
}

export const namespaced = true
