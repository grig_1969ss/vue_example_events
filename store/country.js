import { prop } from 'ramda'

export const state = () => ({
  result: null,
})

export const getters = {
  RESULT: state => state.result,
  TOP_CITIES: state => prop('cities', state.result) || [],
}

export const actions = {
  async FETCH({ commit, dispatch }, id) {
    if ( !id ) {
      return
    }
    const { data } = await this.$axios.get(`/geo/countries/${id}/`, {
      params: { expand: 'cover' },
    })
    commit('SET', data)
    return data
  },
}

export const mutations = {
  SET(state, data) {
    state.result = data
  },
}

export const namespaced = true
