export const state = () => ({
  loading: false,
  count: 0,
  next: null,
  num_pages: 0,
  page_number: 1,
  page_size: 4,
  previous: null,
  results: [],
})

export const getters = {
  GET_VIDEOS: state => state.results,
  HAS_VIDEOS: state => state.results.length,
  HAS_MORE_VIDEOS: state => Boolean(state.next),
  LOADING_VIDEOS: state => state.loading,
  LEFT_TO_GET: state => state.count - state.results.length,
  PAGE_SIZE: state => state.page_size,
}

export const actions = {
  async FETCH_MORE({ commit, getters, state }, params) {
    if (getters.LOADING_VIDEOS) return
    try {
      commit('LOADING_VIDEOS', true)
      const { data } = await this.$axios.get(state.next, { params })
      data.results = getters.GET_VIDEOS.concat(data.results)
      commit('SET', data)
    } finally {
      commit('LOADING_VIDEOS', false)
    }
  },
}

export const mutations = {
  LOADING_VIDEOS(state, value) {
    state.loading = value
  },
  SET(state, data) {
    state = Object.assign(state, data)
  },
  RESTORE(state) {
    state.results = []
  },
}

export const namespaced = true
