export const state = () => ({
  locale: 'en',
  locales: ['ru', 'en'],
})

export const getters = {
  LANG: state => state.locale,
  LOCALES: state => state.locales,
}

export const actions = {
  SWITCH({ commit, getters }, lang) {
    if (this.$cookies.get('django_language') === lang) return null
    else {
      this.$cookies.setIfAccepted('django_language', lang, {
        maxAge: 60 * 60 * 24 * 30,
        path: '/',
      })

      commit('SET_LANG', lang)
      return lang
    }
  },
  TOGGLE_LOCALE({ getters, dispatch }) {
    const locale = getters.LANG === 'ru' ? 'en' : 'ru'
    dispatch('SWITCH', locale)
  },
  CHECK_LANG({ getters }, lang) {
    return getters.LOCALES.includes(lang)
  },
}

export const mutations = {
  SET_LANG(state, lang) {
    if (state.locales.includes(lang)) {
      state.locale = lang
    } else {
      state.locale = 'en'
    }

    // very very smelly
    this.$axios.setAcceptLanguageHeader(state.locale)
  },
}

export const namespaced = true
