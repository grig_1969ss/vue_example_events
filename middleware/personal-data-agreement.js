import bannerConsts from '~/utils/bannerConsts'

export default async function({ store, redirect, from, route, query }) {
  /*
    don't need to check pda for not auth user
    enable auth middleware before this middleware, if you need to
  */
  if (!store.getters['auth/IS_AUTH']) return true

  if (!store.getters['user/HAS_USER']) {
    await store.dispatch('user/GET_USER_DATA')
  }

  const check = await store.dispatch('user/CHECK_PERSONAL_DATA_AGREEMENT')
  if (check) return true

  let redirectPath = ''

  if (from) {
    redirectPath = from.fullPath
  } else {
    /* custom redirects by route-name with default redirect to root */
    switch (route.name) {
      case 'events-id-promo':
        redirectPath = {
          name: 'events-id',
          params: route.params,
          query: { banner: bannerConsts.agreementBanner, ...query },
        }
        break

      default:
        redirectPath = {
          path: '/',
          query: { banner: bannerConsts.agreementBanner, ...query },
        }
        break
    }
  }

  return redirect(redirectPath)
}
