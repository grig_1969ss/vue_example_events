import { prop } from 'ramda'

export default function({ store, redirect, app, query }) {
  if (process.server) {
    const token = prop('token', app.$cookies.get('auth'))
    if (token) return redirect('/', query)
  }
  if (process.client) {
    if (store.getters['auth/IS_AUTH']) return redirect('/', query)
  }
}
