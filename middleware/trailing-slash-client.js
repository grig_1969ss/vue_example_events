export default function({ route, redirect, query }) {
  if (process.server) return
  if (
    !(
      route.fullPath.slice(-1) === '/' ||
      route.path.slice(-1) === '/'
    )
  ) {
    return redirect(
      route.fullPath.replace(route.path, route.path +'/'),
      query
    )
  }
}
