export default (req, res, next) => {
  if (req.url.slice(-1) === '/' || req.url.includes('?')) {
    next()
  } else {
    res.writeHead(301, { Location: req.url + '/' })
    res.end()
  }
}
