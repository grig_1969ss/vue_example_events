const logger = require('pino-http')()

export default (req, res, next) => {
  logger(req, res)
  next()
}
