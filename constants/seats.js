import ryumochanayaSeats from '~/constants/ryumochanayaSeats'
import ryumochanayaVipSeats from '~/constants/ryumochanayaVipSeats'

export const seatsColors = [
  '#96c5d5',
  '#f37779',
  '#6200fb',
  '#778ff3',
  '#77c1f3',
  '#95c4d4',
  '#94de94',
  '#ffba7f',
  '#c554d1',
]

export const seatsSchemes = {
  ryumochnaya: 'ryumochnaya',
  ryumochnaya_vip: 'ryumochnaya_vip',
}

export const seatsSchemeComponents = {
  [seatsSchemes.ryumochnaya]: () =>
      import('~/components/events/seats-schemes/Ryumochnaya'),

  [seatsSchemes.ryumochnaya_vip]: () =>
      import('~/components/events/seats-schemes/Ryumochnaya'),
}

export const seatsSchemeMap = {
  [seatsSchemes.ryumochnaya]: ryumochanayaSeats,
  [seatsSchemes.ryumochnaya_vip]: ryumochanayaVipSeats,
}

export const seatsTypes = {
  withoutSeat: 0,
  withSeat: 1,
}
