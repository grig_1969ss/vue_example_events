import { i18n } from '~/plugins/i18n'

export default () => ({
  statuses:  [
    {
      value: 0,
      display: i18n.t('marketplace.replyCanceled'),
    },
    {
      value: 1,
      display: i18n.t('marketplace.replyRejected'),
    },
    {
      value: 2,
      display: i18n.t('marketplace.replyPending'),
    },
    {
      value: 3,
      display: i18n.t('marketplace.replySelected'),
    },
    {
      value: 4,
      display: i18n.t('marketplace.replyDone'),
    },
  ]
})