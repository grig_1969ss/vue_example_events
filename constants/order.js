import { i18n } from '~/plugins/i18n'

export default () => ({
  statuses:  [
    {
      value: 0,
      display: i18n.t('marketplace.orderOpened'),
    },
    {
      value: 1,
      display: i18n.t('marketplace.orderInWork'),
    },
    {
      value: 2,
      display: i18n.t('marketplace.orderWaiting'),
    },
    {
      value: 3,
      display: i18n.t('marketplace.orderDone'),
    },
    {
      value: 4,
      display: i18n.t('marketplace.orderCanceled'),
    },
  ],
  priceTypes:  [
    {
      value: 'simple',
      display: i18n.t('marketplace.orderPriceSimple'),
    },
    {
      value: 'interval',
      display: i18n.t('marketplace.orderPriceInterval'),
    },
    {
      value: 'cheapest',
      display: i18n.t('marketplace.orderPriceCheapest'),
    },
  ]
})