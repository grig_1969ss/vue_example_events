export default [
  // table 1
  {
    cx: 122.9,
    cy: 37.6,
    key: 'ryumochnaya-stol-1-mesto-1',
  },
  {
    cx: 129.9,
    cy: 54.6,
    key: 'ryumochnaya-stol-1-mesto-2',
  },
  {
    cx: 146.9,
    cy: 60.6,
    key: 'ryumochnaya-stol-1-mesto-3',
  },
  // table 2
  {
    cx: 146.9,
    cy: 84.6,
    key: 'ryumochnaya-stol-2-mesto-1',
  },
  {
    cx: 153.9,
    cy: 101.6,
    key: 'ryumochnaya-stol-2-mesto-2',
  },
  {
    cx: 170.9,
    cy: 107.6,
    key: 'ryumochnaya-stol-2-mesto-3',
  },
  // table 3
  {
    cx: 122.9,
    cy: 132.6,
    key: 'ryumochnaya-stol-3-mesto-1',
  },
  {
    cx: 129.9,
    cy: 149.6,
    key: 'ryumochnaya-stol-3-mesto-2',
  },
  {
    cx: 146.9,
    cy: 155.6,
    key: 'ryumochnaya-stol-3-mesto-3',
  },
  // table 4
  {
    cx: 146.9,
    cy: 179.6,
    key: 'ryumochnaya-stol-4-mesto-1',
  },
  {
    cx: 153.9,
    cy: 196.6,
    key: 'ryumochnaya-stol-4-mesto-2',
  },
  {
    cx: 170.9,
    cy: 202.6,
    key: 'ryumochnaya-stol-4-mesto-3',
  },
  // table 5
  {
    cx: 431.9,
    cy: 37.6,
    key: 'ryumochnaya-stol-5-mesto-1',
  },
  {
    cx: 424.9,
    cy: 54.6,
    key: 'ryumochnaya-stol-5-mesto-2',
  },
  {
    cx: 407.9,
    cy: 60.6,
    key: 'ryumochnaya-stol-5-mesto-3',
  },
  // table 6
  {
    cx: 407.9,
    cy: 85.6,
    key: 'ryumochnaya-stol-6-mesto-1',
  },
  {
    cx: 400.9,
    cy: 102.6,
    key: 'ryumochnaya-stol-6-mesto-2',
  },
  {
    cx: 383.9,
    cy: 108.6,
    key: 'ryumochnaya-stol-6-mesto-3',
  },
  // table 7
  {
    cx: 431.9,
    cy: 132.6,
    key: 'ryumochnaya-stol-7-mesto-1',
  },
  {
    cx: 424.9,
    cy: 149.6,
    key: 'ryumochnaya-stol-7-mesto-2',
  },
  {
    cx: 407.9,
    cy: 155.6,
    key: 'ryumochnaya-stol-7-mesto-3',
  },
  // table 8
  {
    cx: 407.9,
    cy: 179.6,
    key: 'ryumochnaya-stol-8-mesto-1',
  },
  {
    cx: 400.9,
    cy: 196.6,
    key: 'ryumochnaya-stol-8-mesto-2',
  },
  {
    cx: 383.9,
    cy: 202.6,
    key: 'ryumochnaya-stol-8-mesto-3',
  },
  // table 9
  {
    cx: 236.9,
    cy: 66.6,
    key: 'ryumochnaya-stol-9-mesto-1',
  },
  {
    cx: 236.9,
    cy: 82.6,
    key: 'ryumochnaya-stol-9-mesto-2',
  },
  {
    cx: 236.9,
    cy: 98.6,
    key: 'ryumochnaya-stol-9-mesto-3',
  },
  {
    cx: 236.9,
    cy: 114.6,
    key: 'ryumochnaya-stol-9-mesto-4',
  },
  {
    cx: 236.9,
    cy: 130.6,
    key: 'ryumochnaya-stol-9-mesto-5',
  },
  {
    cx: 236.9,
    cy: 146.6,
    key: 'ryumochnaya-stol-9-mesto-6',
  },
  {
    cx: 316.9,
    cy: 66.6,
    key: 'ryumochnaya-stol-9-mesto-7',
  },
  {
    cx: 316.9,
    cy: 82.6,
    key: 'ryumochnaya-stol-9-mesto-8',
  },
  {
    cx: 316.9,
    cy: 98.6,
    key: 'ryumochnaya-stol-9-mesto-9',
  },
  {
    cx: 316.9,
    cy: 114.6,
    key: 'ryumochnaya-stol-9-mesto-10',
  },
  {
    cx: 316.9,
    cy: 130.6,
    key: 'ryumochnaya-stol-9-mesto-11',
  },
  {
    cx: 316.9,
    cy: 146.6,
    key: 'ryumochnaya-stol-9-mesto-12',
  },
  {
    cx: 260.9,
    cy: 164.6,
    key: 'ryumochnaya-stol-9-mesto-13',
  },
  {
    cx: 276.9,
    cy: 164.6,
    key: 'ryumochnaya-stol-9-mesto-14',
  },
  {
    cx: 292.9,
    cy: 164.6,
    key: 'ryumochnaya-stol-9-mesto-15',
  },
  // table 10
  {
    cx: 236.9,
    cy: 204.6,
    key: 'ryumochnaya-stol-10-mesto-1',
  },
  {
    cx: 236.9,
    cy: 220.6,
    key: 'ryumochnaya-stol-10-mesto-2',
  },
  {
    cx: 236.9,
    cy: 236.6,
    key: 'ryumochnaya-stol-10-mesto-3',
  },
  {
    cx: 236.9,
    cy: 252.6,
    key: 'ryumochnaya-stol-10-mesto-4',
  },
  {
    cx: 236.9,
    cy: 268.6,
    key: 'ryumochnaya-stol-10-mesto-5',
  },
  {
    cx: 236.9,
    cy: 284.6,
    key: 'ryumochnaya-stol-10-mesto-6',
  },
  {
    cx: 316.9,
    cy: 204.6,
    key: 'ryumochnaya-stol-10-mesto-7',
  },
  {
    cx: 316.9,
    cy: 220.6,
    key: 'ryumochnaya-stol-10-mesto-8',
  },
  {
    cx: 316.9,
    cy: 236.6,
    key: 'ryumochnaya-stol-10-mesto-9',
  },
  {
    cx: 316.9,
    cy: 252.6,
    key: 'ryumochnaya-stol-10-mesto-10',
  },
  {
    cx: 316.9,
    cy: 268.6,
    key: 'ryumochnaya-stol-10-mesto-11',
  },
  {
    cx: 316.9,
    cy: 284.6,
    key: 'ryumochnaya-stol-10-mesto-12',
  },
  {
    cx: 260.9,
    cy: 302.6,
    key: 'ryumochnaya-stol-10-mesto-13',
  },
  {
    cx: 276.9,
    cy: 302.6,
    key: 'ryumochnaya-stol-10-mesto-14',
  },
  {
    cx: 292.9,
    cy: 302.6,
    key: 'ryumochnaya-stol-10-mesto-15',
  },
  // table 11
  {
    cx: 431.9,
    cy: 227.6,
    key: 'ryumochnaya-stol-11-mesto-1',
  },
  {
    cx: 424.9,
    cy: 244.6,
    key: 'ryumochnaya-stol-11-mesto-2',
  },
  {
    cx: 407.9,
    cy: 250.6,
    key: 'ryumochnaya-stol-11-mesto-3',
  },
  // table 12
  {
    cx: 407.9,
    cy: 274.6,
    key: 'ryumochnaya-stol-12-mesto-1',
  },
  {
    cx: 400.9,
    cy: 291.6,
    key: 'ryumochnaya-stol-12-mesto-2',
  },
  {
    cx: 383.9,
    cy: 297.6,
    key: 'ryumochnaya-stol-12-mesto-3',
  },
  // table 13
  {
    cx: 122.9,
    cy: 313.6,
    key: 'ryumochnaya-stol-13-mesto-1',
  },
  {
    cx: 129.9,
    cy: 330.6,
    key: 'ryumochnaya-stol-13-mesto-2',
  },
  // table 14
  {
    cx: 206.9,
    cy: 334.6,
    key: 'ryumochnaya-stol-14-mesto-1',
  },
  {
    cx: 222.9,
    cy: 334.6,
    key: 'ryumochnaya-stol-14-mesto-2',
  },
  {
    cx: 206.9,
    cy: 402.6,
    key: 'ryumochnaya-stol-14-mesto-3',
  },
  {
    cx: 222.9,
    cy: 402.6,
    key: 'ryumochnaya-stol-14-mesto-4',
  },
  {
    cx: 188.9,
    cy: 352.6,
    key: 'ryumochnaya-stol-14-mesto-5',
  },
  {
    cx: 188.9,
    cy: 368.6,
    key: 'ryumochnaya-stol-14-mesto-6',
  },
  {
    cx: 188.9,
    cy: 384.6,
    key: 'ryumochnaya-stol-14-mesto-7',
  },
  {
    cx: 240.9,
    cy: 352.6,
    key: 'ryumochnaya-stol-14-mesto-8',
  },
  {
    cx: 240.9,
    cy: 368.6,
    key: 'ryumochnaya-stol-14-mesto-9',
  },
  {
    cx: 240.9,
    cy: 384.6,
    key: 'ryumochnaya-stol-14-mesto-10',
  },
  // table 15
  {
    cx: 330.9,
    cy: 334.6,
    key: 'ryumochnaya-stol-15-mesto-1',
  },
  {
    cx: 346.9,
    cy: 334.6,
    key: 'ryumochnaya-stol-15-mesto-2',
  },
  {
    cx: 330.9,
    cy: 402.6,
    key: 'ryumochnaya-stol-15-mesto-3',
  },
  {
    cx: 346.9,
    cy: 402.6,
    key: 'ryumochnaya-stol-15-mesto-4',
  },
  {
    cx: 312.9,
    cy: 352.6,
    key: 'ryumochnaya-stol-15-mesto-5',
  },
  {
    cx: 312.9,
    cy: 368.6,
    key: 'ryumochnaya-stol-15-mesto-6',
  },
  {
    cx: 312.9,
    cy: 384.6,
    key: 'ryumochnaya-stol-15-mesto-7',
  },
  {
    cx: 364.9,
    cy: 352.6,
    key: 'ryumochnaya-stol-15-mesto-8',
  },
  {
    cx: 364.9,
    cy: 368.6,
    key: 'ryumochnaya-stol-15-mesto-9',
  },
  {
    cx: 364.9,
    cy: 384.6,
    key: 'ryumochnaya-stol-15-mesto-10',
  },
]
