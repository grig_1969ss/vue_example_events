export default {
  defaultSeats: [
    // table 1
    {
      cx: 139,
      cy: 50,
      key: 'ryumochnaya-stol-1-mesto-1',
    },
    {
      cx: 146,
      cy: 67,
      key: 'ryumochnaya-stol-1-mesto-2',
    },
    {
      cx: 163,
      cy: 73,
      key: 'ryumochnaya-stol-1-mesto-3',
    },
    // table 2
    {
      cx: 153,
      cy: 117,
      key: 'ryumochnaya-stol-2-mesto-1',
    },
    {
      cx: 160,
      cy: 134,
      key: 'ryumochnaya-stol-2-mesto-2',
    },
    {
      cx: 177,
      cy: 140,
      key: 'ryumochnaya-stol-2-mesto-3',
    },
    // table 3
    {
      cx: 129,
      cy: 165,
      key: 'ryumochnaya-stol-3-mesto-1',
    },
    {
      cx: 136,
      cy: 182,
      key: 'ryumochnaya-stol-3-mesto-2',
    },
    {
      cx: 153,
      cy: 188,
      key: 'ryumochnaya-stol-3-mesto-3',
    },
    // table 4
    {
      cx: 153,
      cy: 212,
      key: 'ryumochnaya-stol-4-mesto-1',
    },
    {
      cx: 160,
      cy: 229,
      key: 'ryumochnaya-stol-4-mesto-2',
    },
    {
      cx: 177,
      cy: 235,
      key: 'ryumochnaya-stol-4-mesto-3',
    },
    // table 5
    {
      cx: 448,
      cy: 50,
      key: 'ryumochnaya-stol-5-mesto-1',
    },
    {
      cx: 441,
      cy: 67,
      key: 'ryumochnaya-stol-5-mesto-2',
    },
    {
      cx: 424,
      cy: 73,
      key: 'ryumochnaya-stol-5-mesto-3',
    },
    // table 6
    {
      cx: 434,
      cy: 118,
      key: 'ryumochnaya-stol-6-mesto-1',
    },
    {
      cx: 427,
      cy: 135,
      key: 'ryumochnaya-stol-6-mesto-2',
    },
    {
      cx: 410,
      cy: 141,
      key: 'ryumochnaya-stol-6-mesto-3',
    },
    // table 7
    {
      cx: 458,
      cy: 165,
      key: 'ryumochnaya-stol-7-mesto-1',
    },
    {
      cx: 451,
      cy: 182,
      key: 'ryumochnaya-stol-7-mesto-2',
    },
    {
      cx: 432,
      cy: 188,
      key: 'ryumochnaya-stol-7-mesto-3',
    },
    // table 8
    {
      cx: 434,
      cy: 212,
      key: 'ryumochnaya-stol-8-mesto-1',
    },
    {
      cx: 427,
      cy: 229,
      key: 'ryumochnaya-stol-8-mesto-2',
    },
    {
      cx: 410,
      cy: 235,
      key: 'ryumochnaya-stol-8-mesto-3',
    },
    // table 9
    {
      cx: 256,
      cy: 120,
      key: 'ryumochnaya-stol-9-mesto-1',
    },
    {
      cx: 256,
      cy: 136,
      key: 'ryumochnaya-stol-9-mesto-2',
    },
    {
      cx: 256,
      cy: 152,
      key: 'ryumochnaya-stol-9-mesto-3',
    },
    {
      cx: 256,
      cy: 168,
      key: 'ryumochnaya-stol-9-mesto-4',
    },
    {
      cx: 256,
      cy: 184,
      key: 'ryumochnaya-stol-9-mesto-5',
    },
    {
      cx: 256,
      cy: 200,
      key: 'ryumochnaya-stol-9-mesto-6',
    },
    {
      cx: 338,
      cy: 120,
      key: 'ryumochnaya-stol-9-mesto-7',
    },
    {
      cx: 338,
      cy: 136,
      key: 'ryumochnaya-stol-9-mesto-8',
    },
    {
      cx: 338,
      cy: 152,
      key: 'ryumochnaya-stol-9-mesto-9',
    },
    {
      cx: 338,
      cy: 168,
      key: 'ryumochnaya-stol-9-mesto-10',
    },
    {
      cx: 338,
      cy: 184,
      key: 'ryumochnaya-stol-9-mesto-11',
    },
    {
      cx: 338,
      cy: 200,
      key: 'ryumochnaya-stol-9-mesto-12',
    },
    {
      cx: 280,
      cy: 219,
      key: 'ryumochnaya-stol-9-mesto-13',
    },
    {
      cx: 296,
      cy: 219,
      key: 'ryumochnaya-stol-9-mesto-14',
    },
    {
      cx: 312,
      cy: 219,
      key: 'ryumochnaya-stol-9-mesto-15',
    },
    // table 10
    {
      cx: 256,
      cy: 241,
      key: 'ryumochnaya-stol-10-mesto-1',
    },
    {
      cx: 256,
      cy: 257,
      key: 'ryumochnaya-stol-10-mesto-2',
    },
    {
      cx: 256,
      cy: 273,
      key: 'ryumochnaya-stol-10-mesto-3',
    },
    {
      cx: 256,
      cy: 288,
      key: 'ryumochnaya-stol-10-mesto-4',
    },
    {
      cx: 256,
      cy: 305,
      key: 'ryumochnaya-stol-10-mesto-5',
    },
    {
      cx: 256,
      cy: 321,
      key: 'ryumochnaya-stol-10-mesto-6',
    },
    {
      cx: 338,
      cy: 241,
      key: 'ryumochnaya-stol-10-mesto-7',
    },
    {
      cx: 338,
      cy: 257,
      key: 'ryumochnaya-stol-10-mesto-8',
    },
    {
      cx: 338,
      cy: 273,
      key: 'ryumochnaya-stol-10-mesto-9',
    },
    {
      cx: 338,
      cy: 288,
      key: 'ryumochnaya-stol-10-mesto-10',
    },
    {
      cx: 338,
      cy: 305,
      key: 'ryumochnaya-stol-10-mesto-11',
    },
    {
      cx: 338,
      cy: 321,
      key: 'ryumochnaya-stol-10-mesto-12',
    },
    {
      cx: 280,
      cy: 339,
      key: 'ryumochnaya-stol-10-mesto-13',
    },
    {
      cx: 296,
      cy: 339,
      key: 'ryumochnaya-stol-10-mesto-14',
    },
    {
      cx: 312,
      cy: 339,
      key: 'ryumochnaya-stol-10-mesto-15',
    },
    // table 11
    {
      cx: 458,
      cy: 260,
      key: 'ryumochnaya-stol-11-mesto-1',
    },
    {
      cx: 451,
      cy: 277,
      key: 'ryumochnaya-stol-11-mesto-2',
    },
    {
      cx: 434,
      cy: 283,
      key: 'ryumochnaya-stol-11-mesto-3',
    },
    // table 12
    {
      cx: 434,
      cy: 307,
      key: 'ryumochnaya-stol-12-mesto-1',
    },
    {
      cx: 427,
      cy: 324,
      key: 'ryumochnaya-stol-12-mesto-2',
    },
    {
      cx: 410,
      cy: 330,
      key: 'ryumochnaya-stol-12-mesto-3',
    },
    // table 13
    {
      cx: 139,
      cy: 336,
      key: 'ryumochnaya-stol-13-mesto-1',
    },
    {
      cx: 146,
      cy: 353,
      key: 'ryumochnaya-stol-13-mesto-2',
    },
    // table 14
    {
      cx: 228,
      cy: 362,
      key: 'ryumochnaya-stol-14-mesto-1',
    },
    {
      cx: 244,
      cy: 362,
      key: 'ryumochnaya-stol-14-mesto-2',
    },
    {
      cx: 228,
      cy: 428,
      key: 'ryumochnaya-stol-14-mesto-3',
    },
    {
      cx: 244,
      cy: 428,
      key: 'ryumochnaya-stol-14-mesto-4',
    },
    {
      cx: 210,
      cy: 379,
      key: 'ryumochnaya-stol-14-mesto-5',
    },
    {
      cx: 210,
      cy: 395,
      key: 'ryumochnaya-stol-14-mesto-6',
    },
    {
      cx: 210,
      cy: 411,
      key: 'ryumochnaya-stol-14-mesto-7',
    },
    {
      cx: 260,
      cy: 379,
      key: 'ryumochnaya-stol-14-mesto-8',
    },
    {
      cx: 260,
      cy: 395,
      key: 'ryumochnaya-stol-14-mesto-9',
    },
    {
      cx: 260,
      cy: 411,
      key: 'ryumochnaya-stol-14-mesto-10',
    },
    // table 15
    {
      cx: 351,
      cy: 362,
      key: 'ryumochnaya-stol-15-mesto-1',
    },
    {
      cx: 367,
      cy: 362,
      key: 'ryumochnaya-stol-15-mesto-2',
    },
    {
      cx: 352,
      cy: 428,
      key: 'ryumochnaya-stol-15-mesto-3',
    },
    {
      cx: 368,
      cy: 428,
      key: 'ryumochnaya-stol-15-mesto-4',
    },
    {
      cx: 334,
      cy: 379,
      key: 'ryumochnaya-stol-15-mesto-5',
    },
    {
      cx: 334,
      cy: 395,
      key: 'ryumochnaya-stol-15-mesto-6',
    },
    {
      cx: 334,
      cy: 411,
      key: 'ryumochnaya-stol-15-mesto-7',
    },
    {
      cx: 384,
      cy: 379,
      key: 'ryumochnaya-stol-15-mesto-8',
    },
    {
      cx: 384,
      cy: 395,
      key: 'ryumochnaya-stol-15-mesto-9',
    },
    {
      cx: 384,
      cy: 411,
      key: 'ryumochnaya-stol-15-mesto-10',
    },
  ],

  vipTables: [
    {
      key: 'ryumochnaya-v1',
      vipTableText: {
        d: 'M104.313 64.5H103.1l-.78 1.91a7.458 7.458 0 00-.33 1.04 6.093 6.093 0 00-.34-1.02l-.83-1.93h-1.37l2.073 4.52h.85zm.332-4.588l.976 1.52 1.379-1.04V69h1.84V57.768h-1.331z',
        transform: 'translate(109, 21)',
      },
      vipLabel: {
        figureTransform: 'translate(900, -297) scale(-1, 1)',
        textTransform: 'translate(633, -577) rotate(93)',
      },
      table: {
        cx: 215,
        cy: 85,
      },
      seats: [
        {
          cx: 192,
          cy: 84,
        },
        {
          cx: 197,
          cy: 100,
        },
        {
          cx: 211,
          cy: 108,
        },
      ],
    },
    {
      key: 'ryumochnaya-v2',
      vipTableText: {
        d: 'M682.41 382.665h-1.21l-.78 1.91a7.458 7.458 0 00-.33 1.04 6.093 6.093 0 00-.34-1.02l-.83-1.93h-1.37l2.07 4.52h.85zm.567 4.5h7.04v-1.712h-4.651c.88-.72 4.224-3.552 4.224-6.688a2.905 2.905 0 00-3.168-2.928 3.2 3.2 0 00-3.36 2.656l1.536.688a1.708 1.708 0 011.616-1.5 1.314 1.314 0 011.424 1.452c0 2.176-2.832 4.768-4.656 6.56z',
        transform: 'translate(-313, -297)',
      },
      vipLabel: {
        figureTransform: 'translate(-311, -297)',
        textTransform: 'translate(-311, -297)',
      },
      table: {
        cx: 372,
        cy: 85,
      },
      seats: [
        {
          cx: 395,
          cy: 84,
        },
        {
          cx: 390,
          cy: 100,
        },
        {
          cx: 377,
          cy: 108,
        },
      ],
    },
  ],
}
