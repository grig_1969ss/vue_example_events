export const popovers = {
  errMsg: 'Desafortunadamente, no hay resultados para tu búsqueda',
  menu: {
    profile: 'Mi perfil',
    ww: 'Los mejores eventos cerca de ti y alrededor del mundo',
    about: '¿Qué es WhatWhere.World?',
    help: 'Ayuda',
    terms: 'Términos de Uso',
    pp: 'Política de privacidad',
    descr:
      'Agregamos eventos en todo el mundo, sin embargo, prestamos mucha atención a la calidad del contenido del evento en cada ciudad. Puedes ayudarnos a mejorar WhatWhere.World.',
    partner: 'Conviértete en un socio',
    changeLanguage: 'Cambiar a español',
  },
  place: {
    selCity: 'Selecciona una ciudad:',
  },
  addEvent: {
    title: 'Encontrar eventos',
    subtitle: 'Puedes encontrar eventos por palabras clave',
    subtitle2: 'o seleccionar por filtros',
  },
  addPlace: {
    title: 'Añadir lugar',
    name: 'Nombre del lugar',
    placeholder: 'Introduce un nombre de lugar',
    subtitle2: '¿Añadir todos los eventos en este lugar al blog?',
    selectLeft: 'No mostrar eventos de blog',
    selectRight: 'Mostrar eventos de este lugar en el blog',
  },
  share: {
    copied: 'Copiado',
    title: 'Cuéntale al mundo:',
    share: 'Compartir',
  },
  paymentError: {
    tryAgain: 'Intentar de nuevo',
    errorTitle: 'Algo salió mal',
    errorDesc:
      'El pago no ha sido procesado, por favor intente nuevamente <br> o contacte a soporte :',
  },
  paymentSuccess: {
    thankYou: '¡Gracias!',
    desc: '¡Gracias! El pago ha sido procesado correctamente',
    descStatusRequest: 'Hemos recibido tu solicitud',
    descStatusPending: 'El pago se está procesando',
    descStatusFree: 'Registration completed successfully',
    hooray: '¡Hurra!',
  },
  searchWidget: {
    copyCode: 'Copiar el código',
    selectWidget: 'Elije el tipo de widget',
    copied: 'Copiado',
    vertical: {
      title: 'En posición vertical',
      text: 'Tres cartas de evento desplazadas',
    },
    horizontal: {
      title: 'En posición horizontal',
      text: 'Tres cartas de evento desplazadas',
    },
    map: {
      title: 'Mapa',
      text: 'Mapa de la ciudad con los pins de los eventos marcados',
    },
    search: {
      title: 'Búsqueda',
      text: 'Widget con la barra de búsqueda, selección de fecha y ciudad.',
    },
  },
  agreementBanner: {
    send: 'Enviar',
    cancel: 'Cancelar',
    cookieText:
      'Nuestro sitio web almacena cookies en tu ordenador. Las cookies se utilizan para recopilar información sobre cómo interactúa con nuestro sitio web y le permite brindar una experiencia de usuario personalizada. Además, al utilizar estos archivos, recopilamos análisis sobre la interacción del usuario con el sitio para optimizarlo y simplificar la navegación. Puede encontrar más información sobre el uso de cookies en',
    privacyPolicy: 'Política de privacidad',
  },
  cookieBanner: {
    allow: 'Doy mi consentimiento',
    description:
      'Almacenamos cookies en tu ordenador. Las cookies se utilizan para recopilar información sobre cómo interactúa con nuestro sitio web y le permite brindar una experiencia de usuario personalizada. Además, al utilizar estos archivos, recopilamos análisis sobre la interacción del usuario con el sitio para optimizarlo y simplificar la navegación. Puede encontrar más información sobre el uso de cookies en',
    privacyPolicy: 'Política de privacidad',
    declineDescription:
      'Si se niega a usar cookies, tu información no se guardará, excepto el archivo que se usará en tu navegador para recordar que no se debe realizar un seguimiento de tus datos.',
  },
  search: {
    showCategories: 'Mostrar más categorías',
    hideCategories: 'Cerrar categorías',
  },
  close: 'Cerrar la ventana pop-up',
  copyShareLink: 'Copiar el enlace',
}
