export const organizers = {
  organizers: 'Organizadores',
  topTitle: '¿Por qué publicar un anuncio de tu evento en WhatWhere.World?',
  topText:
    'Tu evento, más bien es una obra de teatro o una exhibición, un concierto o un torneo,un taller o una degustación aparecerá en la misma línea con los mejores eventos en tu área. ',
  ourFocus: 'Мы делаем акцент на авторских журналах экспертов',
  createJournal: 'Создать свой экспертный журнал',
  musicFromBg: 'Музыка от БГ',
  subscribers: 'seguidores',
  newArtists: 'Новые художники',
  journalsAbout:
    'О рок-музыке составит афишу БГ, о выставках современного искусства напишет Марат Гельман, о рэп-концертах — Децл. Если вы оказались на этой странице, значит вы знаете толк в культурной жизни.',
}
