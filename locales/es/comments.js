export const comments = {
  "comments": "Comentarios",
  "writeComment": "ESCRIBIR UN COMENTARIO",
  "showEarlierComments": "Mostrar comentarios anteriores ({n})",
  "reply": "Responder",
  "send": "Publicar tu comentario",
  "showResponses": " Sin respuestas | Mostrar {n} respuesta | Mostrar {n} respuestas | Mostrar {n} respuestas",
  "showMoreResponses": " Sin respuestas | Mostrar {n} respuesta más | Mostrar {n} respuestas más | Mostrar {n} respuesta más",
  "yourComment": "Tu comentario",
  "hiddenUser": "Usuario oculto",
  "hidden": "El comentario ha sido escondido",
  "banned": "El comentario ha sido prohibido por el moderador"
}