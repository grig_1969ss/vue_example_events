export const search = {
  breadcrumbs: {
    mainPage: 'Main',
    searchPage: 'Búsqueda',
  },
  tabs: {
    events: 'Eventos',
    journals: 'Revistas',
    organizers: 'Organizers',
  },
  searchResults:
    'no hay resultados | {n} resultado de búsqueda para | {n} resultado de búsqueda para | {n} resultados de búsqueda para',
  notFound: {
    look: 'Mira',
    example: 'conciertos de rock',
    exampleQ: 'rock',
    notFound: 'No hemos encontrado nada para tu solicitud',
  },
  categories: {
    filters: 'Filtros',
    tags: 'Etiquetas',
    all: 'Todos',
  },
  getWidget: 'Obtener código de widget con resultados de búsqueda',
}
