export const about = {
  title: `WhatWhere.World - es la colección de los eventos interesantes en las
  ciudades del mundo`,
  description:
    'Agregamos eventos de muchas fuentes diferentes y los mostramos de manera conveniente. Nuestro proyecto ayuda a aprender sobre lo que está pasando en el lugar donde te encuentras o adonde quieres venir: conciertos, exposiciones, seminarios, capacitación empresarial y mucho más.',
  searchButton: 'Encontrar un evento cerca',
  joinAndHelp:
    '¡Únete a WhatWhere.World y ayuda a otras personas a aprender sobre los eventos interesantes que pasan cerca de ti y en todo el mundo!',
  signUp: 'Зарегистрироваться',
  mostInteresting: 'Mas interesante',
  eventTitle: 'Fantástico evento en tu ciudad',
  eventAddress: 'Plaza central',
  eventDate: '31 de diciembre, 00:00',
  personTitle: 'Marat Gelman',
  personSubTitle:
    'Coleccionista ruso, propietario de la galería, pгblicista y director de arte, ideólogo de WhatWhere.World',
  personText:
    'Gracias a la fusión de sus muchos años de experiencia en el campo de la cultura, la organización de eventos masivos y nuestra plataforma única de agregación, creamos un servicio que satisfará mejor las necesidades de las instituciones culturales, los organizadores de eventos y los representantes activos de la comunidad urbana.',
}
