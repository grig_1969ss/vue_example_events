export const place = {
  opened: 'Abierto:',
  nearPlaces: 'Lugares cerca',
  about: 'Sobre nosotros',
  repr: 'Representantes oficiales',
  eventFilters: [
    {
      value: '',
      name: 'Todos los eventos',
    },
    {
      value: 'today',
      name: 'Hoy',
    },
    {
      value: 'tomorrow',
      name: 'Mañana',
    },
    {
      value: 'week',
      name: 'Semana',
    },
    {
      value: 'month',
      name: 'Mes',
    },
  ],
}
