export const profile = {
  profile: 'Perfíl',
  user: 'Usuario',
  organizer: 'Organizador',
  journals: 'Blogs',
  events: 'Eventos',
  allEvents: 'Todos los eventos',
  hey: 'Hola',
  avatarEditor: {
    ok: 'Hecho',
    changePhoto: 'Elige otra foto',
    successChange: 'Foto de perfil cambiada',
    errorChange: 'No se pudo cambiar la foto del perfil',
    successDelete: 'Foto de perfil eliminada',
    errorDelete: 'No se pudo borrar la foto del perfil',
  },
  event: {
    removeQuestion: '¿Estás seguro de que quieres eliminar el evento?',
  },
}
