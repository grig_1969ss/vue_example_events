export const special = {
  connectWithUs: 'Nosotros en redes sociales',
  photos: 'Fotoreportaje',
  viewMore: 'Ver más',
  showMore: 'Mostrar más',
  officialRepresentatives: 'Representantes oficiales',
  events: 'Eventos',
  connectOrganizer: 'Conectate con el organizador',
  video: 'Video',
  gallery: 'Galería',
  applyNow: 'Regístrese ahora',
  successMessage:
    'Tu solicitud ha sido enviada con éxito. En breve recibirás un correo electrónico de confirmación en la bandeja de entrada.',
}
