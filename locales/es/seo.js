export const seo = {
  description:
    'Agregamos eventos de muchas fuentes diferentes y los ' +
    'mostramos de manera conveniente. Nuestro proyecto ayuda a aprender ' +
    'sobre lo que está pasando en el lugar donde te encuentras o adonde ' +
    'quieres venir: conciertos, exposiciones, seminarios, ' +
    'capacitación empresarial y mucho más.',
  title:
    'WhatWhere.World – los mejores eventos cerca de ti y alrededor del mundo',
  in: 'en el',
  on: 'en',
  allEvents: 'Todos los eventos en',
  whereToGo: 'Where to go',
  inWorld: 'in World',
  search: 'Búsqueda',
  onSite: 'en WhatWhere.World',
  searchPageMeta: {
    title: '- eventos, conciertos, actuaciones en WhatWhere.World',
    description: ': fotos, descripción, precios, contactos',
  },
  citySearchPageMeta: {
    title:
      'Where to go in {city} - {count} events: exhibitions, concerts, performances, holidays',
    description:
      'Where to go in {city} today or this weekend: all city events of {month}',
  },
}
