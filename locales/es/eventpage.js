export const eventpage = {
  contact: 'Contactar',
  promote: 'promover el evento',
  promoteShort: 'promover',
  links: 'Enlaces:',
  popular: 'Evento popular',
  thisIsMyEvent: 'Este es mi evento',
  rating: 'Tu valoración:',
  similarEvents: 'Eventos similares',
  similarButton: 'Todos los eventos en la ciudad',
  tickets: 'Comprar entradas',
  site: 'Detalles en el sitio:',
  location: 'Lugar de celebración',
  posters: 'Generar y descargar los pósteres',
  organizers: 'Organizadores',
  statistic: {
    blog: 'revista | de la revista | revistas',
  },
  moreButton: 'Leer completo',
  hideButton: 'Ocultar texto',
  from: 'de',
  upTo: 'hasta',
  ticket: 'Boletos',
}
