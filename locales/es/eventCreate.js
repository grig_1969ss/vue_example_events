export const eventCreate = {
  ta: 'Tu público objetivo',
  men: 'HOMBRES',
  women: 'MUJERES',
  mixed: 'MIXTO',
  addLink: 'Añadir el enlace',
  downloadPoster: 'Descargar cartel',
  edit: 'Editar el evento',
}
