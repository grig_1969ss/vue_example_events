export const error = {
  failedLoadImage: 'La foto no pudo ser cargada',
  unsupportedFormat: 'Este formato de imagen no es compatible',
  chosePhotoSmaller: 'Elige una foto más pequeña',
  notFound:
    'No encontramos la página que necesitas, <nobr>, lo sentimos mucho: (</ nobr> ',
  mainReturnQuestion:
    '¿Deseas volver a la <a href="/"> página principal </a> o encontrar <a href="/"> algo interesante </a>?',
  accessDenied: 'Acceso denegado',
  posterErr: 'El cartel no pudo ser cargado',
  serverError: 'No se pudo cargar la página, error del servidor',
  serverErrorChild: '¿Quieres volver a la <a href="/"> página principal </a>?',
  incorrectLink: 'Enlace incorrecto',
  coverFailed: 'No ha sido posible subir el foto de la portada',
}
