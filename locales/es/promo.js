export const promo = {
  title: 'Campaña promocional de tu evento',
  description:
    'Ofrecemos la manera más fácil de promocionar tu evento en nuestra página web y en las redes sociales para tu público objetivo. Solamente tienes que elegir un paquete de servicios y nos haremos el resto del trabajo.',
  ta: 'El público objetivo:',
  place: 'Lugar +',
  dur: 'La duración de la campaña',
  startPromo: 'Empezar la campaña :',
  selectLanguage: 'Elija las idiomas de la campaña :',
  footer: '',
  btn: 'Promover el evento',
  btnWithout: 'Crear el evento sin promoción',
  btnCancel: 'Cancelar',
  socialAds: 'Publicidad en redes sociales',
  interestTA: 'Mostrar público objetivo por interés',
  emailAd: 'E-mail correo',
  analytics: 'Informes y análisis',
  showRadius: 'Mostrar en el radio seleccionado',
  promoPage: 'Página de promoción',
  attachPromo: 'Закрепление в выдаче',
  free: 'Gratis',
  people: 'personas',
  km: 'km',
  day: 'el dia',
  days: '0 días | 1 dia | {n} del dia | {n} días',
  from: 'de',
  to: 'a',
  age: 'años',
  category: 'Categoría',
  summary:
    'Durante {duration} desde {date} tu evento será conocido ' +
    'por aproximadamente {audience} personas en el radio de {area} km alrededor del lugar del evento',
  site: {
    onlyEvent: 'ANUNCIAR SOLO EL EVENTO',
    eventAndSite: 'ANUNCIAR EL EVENTO + TU PAGINA WEB',
    error: 'Introduzca la URL de su sitio para promocionar.',
    placeholder: 'Su sitio: www...',
  },
  coupon: {
    title: '¿Tienes un código promocional?',
    activate: 'Insertar código promocional',
    error:
      'No se encontró el cupón, verifique que el código se haya ingresado correctamente.',
    discount: 'Descuento {discount}% activado',
  },
  why: '¿Por qué es rentable?',
  hints: [
    {
      title: 'Si faltas conicimientos y habilidades',
      text:
        'No tienes que entender las herramientas de publicidad de las redes sociales, haremos todo por ti.',
    },
    {
      title:
        'Si las agencias de publicidad te quitan la mayor parte del presupuesto',
      text:
        'Con nosotros puedes lanzar la campaña de promoción con el mínimo presupuesto, sin necesidad de pagar por la oficina y empleados de otra persona.',
    },
    {
      title: 'Si no tienes tiempo para controlar el  proceso de promoción',
      text:
        'Hemos automatizado todas las acciones rutinarias, solo necesitas crear un evento hacer un par de clicks.',
    },
    {
      title: 'Si los freelancers fallan',
      text:
        'Nuestros algoritmos funcionan como un reloj. Especifique la fecha de la promoción y la publicidad comenzará exactamente a tiempo.',
    },
  ],
}
