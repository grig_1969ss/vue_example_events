export const auth = {
  login: 'Iniciar sesión',
  logout: 'Salir',
  register: 'Registrarse',
  passwordsDoNotMatch: 'Las contraseñas no coinciden',
  auth: {
    email: 'Correo electronico',
    forgot: 'No recuerdo la contraseña, ayuda!',
    login: 'Iniciar sesión',
    password: 'Contraseña',
    register: 'Registrarse',
  },
  restore: {
    reset: 'Resetear la contraseña',
    resetTitle: 'Restaurar la contraseña',
    resetText:
      'Recibirá el enlace para restaurar su contraseña en su correo electrónico en 15 minutos',
    resetRemember: '¡Hurra! ¡Recuerdo la contraseña!',
  },
  signUp: {
    register: 'Registrarse',
    returnToAuth: '¿Ya estas registrado? Conéctate',
    personalDataProcessing: 'Acepto el tratamiento de mis datos personales',
  },
  registration: 'Registrarse',
  name: 'Nombre',
  email: 'Correo electronico',
  password: 'Contraseña',
  vk: 'Iniciar sesión con VK',
  fb: 'Inicia sesión con Facebook',
  google: 'Inicia sesión con Google',
  newPassword: 'Establecer una nueva contraseña: al menos 6 caracteres latinos',
  changePassword: 'Cambiar contraseña',
  validation: {
    notEmptyField: 'Este campo no puede estar vacío',
  },
  registerUser: 'Inscripción completa',
  haveAcc: '¿Ya estás registrado?',
  searchSomething: 'Interesado en eventos en {city}?',
  regHint: 'Regístrate y encuentra aún más eventos',
  loginGoogle: 'Iniciar sesión con Google',
  loginFb: 'Iniciar sesión con Facebook',
  or: 'o',
  regThanks: 'Gracias por registrarse',
  regCheckMail:
    'Debes confirmar tu dirección de correo electrónico. Para hacerlo, diríjase a tu correo electrónico, abra nuestra carta y haga clic en el enlace que se encuentra dentro. Si no has recibido este correo electrónico, revise tu carpeta de Spam.',
  regAlsoSub:
    'También puedes seguirnos en las redes sociales para conocer todas las novedades',
}
