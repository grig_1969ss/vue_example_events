export const validation = {
  required: 'Campo obligatorio',
  requiredAll: 'Rellene todos los campos',
  lenMin: 'La longitud debe ser mayor',
  lenMax: 'La longitud debe ser menor',
  err: 'Error',
  noRes: 'No se encontraron resultados',
}
