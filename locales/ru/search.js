export const search = {
  breadcrumbs: {
    mainPage: 'Главная',
    searchPage: 'Поиск',
  },
  tabs: {
    events: 'События',
    journals: 'Журналы',
    organizers: 'Организаторы',
  },
  searchResults:
    'нет результатов | {n} результат по запросу | {n} результата по запросу | {n} результатов по запросу',
  notFound: {
    look: 'Посмотрите',
    example: 'рок-концерты',
    exampleQ: 'рок',
    notFound: 'Мы ничего не нашли по вашему запросу',
  },
  categories: {
    filters: 'Фильтры',
    tags: 'Теги',
    all: 'Все',
  },
  getWidget: 'Получить код виджета с результатами поиска',
}
