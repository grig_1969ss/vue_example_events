export const profile = {
  profile: 'Профиль',
  user: 'Пользователь',
  organizer: 'Организатор',
  customer: 'Заказчик',
  supplier: 'Подрядчик',
  journals: 'Журналы',
  events: 'События',
  marketplace: 'Маркетплейс',
  allEvents: 'Все события',
  hey: 'Привет',
  avatarEditor: {
    ok: 'Готово',
    changePhoto: 'Выбрать другое фото',
    successChange: 'Аватар изменен',
    errorChange: 'Не получилось изменить аватар',
    successDelete: 'Аватар удален',
    errorDelete: 'Не получилось удалить аватар',
  },
  event: {
    removeQuestion: 'Вы уверены, что хотите удалить событие?',
  },
}
