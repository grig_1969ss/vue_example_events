export const place = {
  opened: 'Открыто:',
  nearPlaces: 'Места рядом',
  about: 'О нас',
  repr: 'Официальные представители',
  eventFilters: [
    {
      value: '',
      name: 'все события',
    },
    {
      value: 'today',
      name: 'сегодня',
    },
    {
      value: 'tomorrow',
      name: 'завтра',
    },
    {
      value: 'week',
      name: 'Неделя',
    },
    {
      value: 'month',
      name: 'Месяц',
    },
  ],
}
