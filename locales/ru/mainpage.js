export const mainpage = {
  hero: {
    banner: {
      imageNotFound: 'Мы еще не нашли красивое фото',
    },
    text: {
      title: 'Чем заняться',
      in: 'в',
      thisPlanet: 'на этой планете',
    },
  },
  searchBar: {
    placeholder: 'Например, выставка',
    allWorld: 'Весь мир',
    anyDate: 'Любая дата',
    week: 'На этой неделе',
    month: 'В этом месяце',
    weekend: 'В выходные',
    selectDates: 'Выбрать даты',
  },
  placeholder: 'Например, выставка',
  events: 'События',
  categories: 'Категории событий',
  allEvents: 'Все события',
  createEventAsEventCard:
    'Заведи и продвигай свое событие, чтобы о нем все узнали',
  cityPlaceholder: 'Начните вводить название города',
  createEvent: 'Создать событие',
  createBlog: 'Создать свой журнал',
  filters: [
    { value: 'today', name: 'Сегодня' },
    { value: 'tomorrow', name: 'Завтра' },
    { value: 'week', name: 'Неделя' },
    { value: 'month', name: 'Месяц' },
    { value: '', name: 'Все события' },
  ],
  allEventsCity: 'Все события города',
  subscribe:
    'Не пропусти самое интересное, подпишись на рассылку' + ' событий в',
  subscribeMail: 'Мояэлектронная@почта.ру',
  journals: 'Журналы',
  allJournals: 'Все журналы',
  places: 'Популярные места',
  allPlaces: 'Все места',
  imageShare: 'Хотите поделиться своим?',
  havePhoto: 'У меня есть классное фото',
  alwaysNear: 'Подпишись и будь в курсе событий в',
  info: {
    earth: 'Земля',
    footer: {
      allCities: 'Все города',
      allCountry: 'Все страны',
    },
  },
  myCity: 'Это мой город',
  notGuess: 'Не угадали',
  year: 'год',
}
