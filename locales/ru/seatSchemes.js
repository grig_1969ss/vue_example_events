export const schemes = {
  ryumochnayaTemplate: {
    scene: 'сцена',
    entrance: 'вход',
    toilet: 'туалет',
    buffet: 'буфет',
  },
  legend: {
    yourChoice: 'Выбрано вами',
    taken: 'Занято',
  },
};
