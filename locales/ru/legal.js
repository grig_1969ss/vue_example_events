export const legal = {
  address: 'Адресс',
  inn: 'ИНН',
  ogrn: 'ОГРН',
  kpp: 'КПП',
  okpo: 'ОКПО',
  bank_account_number: 'Номер Счета',
  bik: 'БИК Банка'
}