export const special = {
  connectWithUs: 'Мы в соцсетях:',
  photos: 'Фотоотчет',
  viewMore: 'Смотреть ещё',
  showMore: 'Показать ещё',
  officialRepresentatives: 'Официальные представители',
  events: 'События',
  connectOrganizer: 'Связаться с организатором',
  video: 'Видео',
  gallery: 'Галерея',
  applyNow: 'Apply Now',
  successMessage:
    'Your application has been successfully submitted. Shortly you will receive a confirmation email in your inbox.',
}
