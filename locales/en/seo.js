export const seo = {
  description:
    'Exploring and checking hundreds of disparate sources we' +
    ' aggregate events and present them in a most convenient form, ' +
    'specially for you. Our project exists to help you to get more ' +
    'information about what goes on in your location and other places ' +
    'you are going to visit, such as concerts, exhibitions, workshops, ' +
    'business trainings and much more.',
  title: 'WhatWhere.World – The best events near and around the world',
  in: 'in',
  on: 'on',
  atIn: 'at',
  allEvents: 'All events in',
  whereToGo: 'Where to go',
  inWorld: 'in World',
  search: 'Search',
  onSite: 'on WhatWhere.World',
  searchPageMeta: {
    title: '- events, concerts, performances on WhatWhere.World',
    description: ': photos, description, prices, contacts',
  },
  citySearchPageMeta: {
    title:
      '{when} - {count} events: exhibitions, concerts,' +
      ' performances, holidays',
    description:
      'Where to go in {city} today or this weekend: all city events of {month}',
  },
  cityPageMeta: {
    title: 'Where to go in {city} - {eventsCount} events: exhibitions, concerts, performances, holidays',
    description: 'Where to go in {city} today or this weekend: all city events of {mothCapitalized}'
  }
}
