export const validation = {
  required: 'Required field',
  requiredAll: 'Fill in all fields',
  lenMin: 'Length must be more than',
  lenMax: 'Length must be lower than',
  err: 'Error',
  noRes: 'No results',
  dtEnd: 'End time can not be earlier start time',
  minValue: 'Value cannot be less than',
  maxValue: 'Value cannot be greater than',
  email: 'Enter correct email address',
  invalid: 'Invalid value'
}
