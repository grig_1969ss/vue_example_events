export const organizers = {
  organizers: 'Organizers',
  topTitle: 'Why should you publish your event on WhatWhere.World?',
  topText:
    'Your event, rather it is a theatrical performance or an exhibition, a concert or a tournament, workshop or a tasting will appear on the same line up with the top events in your area. WhatWhere.World will make sure to make it known to your target audience.',
  ourFocus: 'We introduce you to blogs of well-known Experts',
  createJournal: 'Start your own blog',
  musicFromBg: 'Music from BG',
  subscribers: 'subscribers',
  newArtists: 'New artists',
  journalsAbout:
    'BG will create rock concert billboard, Marat Gelman will write about contemporary art exhibitions, Decl – about rap concerts. If you are on this page, you know a lot about cultural life.',
}
