export const popovers = {
  errMsg: 'Sorry, nothing has found by your request',
  menu: {
    profile: 'My profile',
    ww: 'The best events near you and around the world',
    about: 'What is WhatWhere.World?',
    help: 'Help',
    terms: 'Terms & Conditions',
    pp: 'Privacy Policy',
    ticketPurchaseAgreement: 'Ticket Purchase Agreement',
    ticketingAgencyAgreement: 'Ticketing Agency Agreement',
    descr:
      'We aggregate events around the world, yet we pay close attention to the quality of event content in each city. You can help us make WhatWhere.World better.',
    partner: 'Become a Partner',
    changeLanguage: 'Перейти на русский',
  },
  place: {
    selCity: 'Select city:',
  },
  addEvent: {
    title: 'Search for events',
    subtitle: 'You can search event by keywords',
    subtitle2: 'or use filters',
  },
  addPlace: {
    title: 'Add place',
    name: 'Venue name',
    placeholder: 'Enter venue name',
    subtitle2: 'Add all events from this place to journal?',
    selectLeft: "Don't show events in the blog",
    selectRight: 'Show events in the blog',
  },
  share: {
    copied: 'Copied',
    title: 'Share with the world:',
    share: 'Share',
  },
  paymentError: {
    tryAgain: 'Try again',
    errorTitle: 'Something went wrong',
    errorDesc:
      'Payment was not made, please try again<br> or contact customer service:',
  },
  paymentSuccess: {
    thankYou: 'Thank you for your order!',
    desc: 'Thank you! We have successfully received your payment',
    descStatusRequest: 'We will contact you shortly',
    descStatusPending: 'Payment in process',
    descStatusFree: 'Registration completed successfully',
    hooray: 'Hooray!',
  },
  searchWidget: {
    copyCode: 'Copy code',
    selectWidget: 'Select widget type',
    copied: 'Copied',
    vertical: {
      title: 'Vertical',
      text: 'Three events in a carousel slider',
    },
    horizontal: {
      title: 'Horizontal',
      text: 'Three events in a carousel slider',
    },
    map: {
      title: 'Map',
      text: "City map with pinned events' locations on it",
    },
    search: {
      title: 'Search',
      text: 'Widget with a search bar, date and city selection',
    },
  },
  agreementBanner: {
    send: 'Submit',
    cancel: 'Cancel',
    cookieText:
      'This website stores cookies on your computer. These cookies are used to collect information about how you interact with our website and allow us to remember you. We use this information in order to improve and customize your browsing experience and for analytics and metrics about our visitors both on this website and other media. To find out more about the cookies we use, see our',
    privacyPolicy: 'Privacy Policy',
  },
  cookieBanner: {
    allow: 'Allow',
    description:
      'This website stores cookies on your computer. These cookies are used to collect information about how you interact with our website and allow us to remember you. We use this information in order to improve and customize your browsing experience and for analytics and metrics about our visitors both on this website and other media. To find out more about the cookies we use, see our',
    privacyPolicy: 'Privacy Policy',
    declineDescription:
      'If you decline, your information won’t be tracked when you visit this website. A single cookie will be used in your browser to remember your preference not to be tracked.',
  },
  search: {
    showCategories: 'Show more categories',
    hideCategories: 'Hide categories',
  },
  close: 'Close the pop-up window',
  copyShareLink: 'Copy link',
}
