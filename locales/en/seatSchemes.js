export const schemes = {
  ryumochnayaTemplate: {
    scene: 'scene',
    entrance: 'entrance',
    toilet: 'wc',
    buffet: 'buffet',
  },
  legend: {
    yourChoice: 'Your choice',
    taken: 'Taken',
  },
};
