export const place = {
  opened: 'Opened:',
  nearPlaces: 'Places nearby',
  about: 'About us',
  repr: 'Official Representatives',
  eventFilters: [
    {
      value: '',
      name: 'All events',
    },
    {
      value: 'today',
      name: 'Today',
    },
    {
      value: 'tomorrow',
      name: 'Tomorrow',
    },
    {
      value: 'week',
      name: 'Week',
    },
    {
      value: 'month',
      name: 'Month',
    },
  ],
}
