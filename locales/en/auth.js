export const auth = {
  login: 'Log in',
  logout: 'Sign out',
  register: 'Sign Up',
  passwordsDoNotMatch: 'Passwords do not match',
  auth: {
    email: 'Email',
    forgot: 'Help! I forgot my password!',
    login: 'Log in',
    password: 'Password',
    register: 'Sign Up',
  },
  restore: {
    reset: 'Reset password',
    resetTitle: 'Password recovery',
    resetText:
      'A link to password reset will be sent to your email within 15 minutes',
    resetRemember: 'Hooray! I remember my password!',
  },
  signUp: {
    register: 'Sign Up',
    returnToAuth: 'I already have an account, log in',
    personalDataProcessing: 'I allow using my personal data',
  },
  registration: 'Registration',
  name: 'Name',
  email: 'Email',
  password: 'Password',
  vk: 'Login with VK',
  fb: 'Login with Facebook',
  google: 'Login with Google',
  newPassword:
    'Create new password – it must be at least 6 chacarters long and contain only latin characters',
  changePassword: 'Change password',
  validation: {
    notEmptyField: 'This field cannot be empty',
  },
  registerUser: 'Complete registration',
  haveAcc: 'Already signed up?',
  searchSomething: "Check out what's going on around you!",
  searchSomething2: "Check out what's going on around you!",
  regHint: 'Sign up now and learn out about even more events',
  loginGoogle: 'Google',
  loginFb: 'Facebook',
  or: 'or',
  regThanks: 'Thank you for signing up!',
  regCheckMail:
    'You need to confirm your email address. ' +
    'Please check your inbox for an email from us and click the ' +
    'link in that email.\n If you do not receive a confirmation email, ' +
    'please check your spam folder. ',
  regAlsoSub: 'Stay tuned with our news on social media:',
  loginWith: 'Login with',
  hasAccount: 'I already have an account,',
  social: {
    google: 'Google',
    fb: 'Facebook',
  },
  userProfile: 'User profile',
  companyProfile: 'Organization profile',
  fullName: 'Full Name',
  companyTitle: 'Company Title',
  agreement: 'by clicking Sign Up you agree to',
  privacy: 'the site\'s use policy',
  profileType: 'Profile type',
  customer: 'I\'m the customer',
  supplier: 'I\'m a supplier',
}
