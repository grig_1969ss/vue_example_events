export const about = {
  title:
    'WhatWhere.World is a collection of interesting events in all cities of the world',
  description:
    'We aggregate events around the world, yet we pay close attention to the quality of event content in each city. You can help us make WhatWhere.World better.',
  searchButton: 'Search for events nearby',
  joinAndHelp:
    'Join WhatWhere.World and help other people learn about ' +
    'interesting events happening near you and around the world!',
  signUp: 'Sign up',
  mostInteresting: 'The most interesting',
  eventTitle: 'Fantastic event in your city',
  eventAddress: 'Сentral square',
  eventDate: 'December 31, 00:00',
  personTitle: 'Marat Gelman',
  personSubTitle:
    'Russian collector, gallery owner, publicist and art manager, ideologist of WhatWhere.World',
  personText:
    'Thanks to the fusion of his many years of experience in the field of culture, organizing mass events and our unique aggregation platform, we created a service that will best meet the needs of cultural institutions, event organizers and active representatives of the urban community.',
}
