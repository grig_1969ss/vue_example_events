export const profile = {
  profile: 'Profile',
  user: 'User',
  organizer: 'Organizer',
  customer: 'Customer',
  supplier: 'Supplier',
  journals: 'Blogs',
  events: 'Events',
  marketplace: 'Marketplace',
  allEvents: 'All events',
  hey: 'Hey',
  avatarEditor: {
    ok: 'Ok',
    changePhoto: 'Choose another photo',
    successChange: 'Avatar changed',
    errorChange: 'Could not change avatar',
    successDelete: 'Avatar removed',
    errorDelete: 'Could not delete avatar',
  },
  event: {
    removeQuestion: 'Are you sure you want to delete an event?',
  },
}
