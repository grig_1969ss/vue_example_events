export const error = {
  failedLoadImage: 'Failed to load image',
  unsupportedFormat: 'This image format is not supported',
  chosePhotoSmaller: 'Choose a smaller photo',
  notFound: 'We are sorry, <nobr>but requested page was not found :(</nobr>',
  mainReturnQuestion:
    'Do you want to return to the <a href="/">main page</a> or look for <a href="/">anything interesting</a>?',
  accessDenied: 'The action cannot be performed: access denied. Refresh the page and try again.',
  invalidToken: 'Token is invalid, sign in again',
  posterErr: "Couldn't get posters",
  serverError: 'Internal Server Error',
  serverErrorChild: 'Do you want to return to the <a href="/">main page</a>?',
  incorrectLink: 'Incorrect link',
  coverFailed: "Couldn't upload a cover",
}
