export const special = {
  connectWithUs: 'Connect with us:',
  photos: 'Photos',
  viewMore: 'View more',
  showMore: 'Show more',
  officialRepresentatives: 'Official representatives',
  events: 'Events',
  connectOrganizer: 'Contact the organizer',
  video: 'Video',
  gallery: 'Gallery',
  applyNow: 'Apply Now',
  successMessage:
    'Your application has been successfully submitted. Shortly you will receive a confirmation email in your inbox.',
}
