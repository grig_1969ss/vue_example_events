export const comments = {
  "comments": "Comments",
  "writeComment": "Write a comment",
  "showEarlierComments": "Show earlier comments ({n})",
  "reply": "Reply",
  "send": "Post your comment",
  "showResponses": "Show {n} responses",
  "showMoreResponses": "Show {n} more responses",
  "yourComment": "Your comment",
  "hiddenUser": "Hidden user",
  "hidden": "The comment is hidden",
  "banned": "The comment was banned by the moderator"
}