export const search = {
  breadcrumbs: {
    mainPage: 'Main',
    searchPage: 'Search',
  },
  tabs: {
    events: 'Events',
    journals: 'Blogs',
    organizers: 'Organizers',
  },
  searchResults: '{n} results matching your search ',
  notFound: {
    look: 'Look',
    example: 'rock concerts',
    exampleQ: 'rock',
    notFound: "We couldn't found anything by your request",
  },
  categories: {
    filters: 'Filters',
    tags: 'Tags',
    all: 'All',
  },
  getWidget: 'Get search results widget',
}
