export const legal = {
  address: 'Address',
  inn: 'ИНН',
  ogrn: 'ОГРН',
  kpp: 'КПП',
  okpo: 'ОКПО',
  bank_account_number: 'Bank Account Number',
  bik: 'Bank Identification Code'
}