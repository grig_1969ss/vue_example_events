export const mainpage = {
  hero: {
    banner: {
      imageNotFound: "We haven't found a beautiful photo",
    },
    text: {
      title: 'What to do',
      in: 'in',
      thisPlanet: 'on this planet',
    },
  },
  searchBar: {
    placeholder: 'E.g. rock-n-roll party',
    allWorld: 'All world',
    anyDate: 'Any date',
    week: 'Week',
    month: 'Month',
    weekend: 'Weekend',
    selectDates: 'Select dates',
  },
  placeholder: 'E.g. rock-n-roll party',
  events: 'Events',
  categories: 'Event categories',
  allEvents: 'All events',
  createEventAsEventCard:
    'Start and promote your event so that everyone will know about it',
  cityPlaceholder: 'Start type city name',
  createEvent: 'Create event',
  createBlog: 'Create your blog',
  filters: [
    { value: 'today', name: 'Today' },
    { value: 'tomorrow', name: 'Tomorrow' },
    { value: 'week', name: 'Week' },
    { value: 'month', name: 'Month' },
    { value: '', name: 'All events' },
  ],
  allEventsCity: 'All city events',
  subscribe:
    'Do not miss the most interesting events, subscribe to the' +
    ' newsletter in',
  subscribeMail: 'myemailaddress@mailbox.com',
  journals: 'Blogs',
  allJournals: 'All blogs',
  places: 'Popular Locations',
  allPlaces: 'All Locations',
  imageShare: 'Want to share yours?',
  havePhoto: 'I have great photo',
  alwaysNear: "Follow us to know what's going on in",
  info: {
    earth: 'Earth',
    footer: {
      allCities: 'All cities',
      allCountry: 'All country',
    },
  },
  myCity: "That's my city",
  notGuess: "Didn't guess",
  year: 'year',
}
