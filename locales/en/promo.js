export const promo = {
  title: 'Event promo',
  description:
    'We offer a simple way to promote your event on our website ' +
    'and in social media. Choose a Promo Package that best suits your ' +
    'needs and we do the rest of work to launch it.',
  ta: 'Target audience:',
  place: 'Location +',
  dur: 'Promo period',
  startPromo: 'Start promo:',
  selectLanguage: 'Select languages for your promo:',
  footer: '',
  btn: 'Promote event',
  btnWithout: 'Create the event without promotion',
  btnCancel: 'Cancel',
  socialAds: 'Social media ads',
  interestTA: 'Interests based targeting',
  emailAd: 'Mailing list',
  analytics: 'Reports and analytics',
  showRadius: 'Show ads within selected area',
  promoPage: 'Promo page',
  attachPromo: 'Premium position in search results',
  free: 'Free',
  people: 'people',
  km: 'km',
  day: 'day',
  days: '{n} days',
  from: 'From',
  to: 'to',
  age: 'year old',
  category: 'Category',
  summary:
    'Starting from {date} your event will be seen by at least {audience}' +
    ' people within a {area} km radius around the event location',
  site: {
    onlyEvent: 'ADVERTISE ONLY EVENT',
    eventAndSite: 'ADVERTISE EVENT + YOUR SITE',
    error: 'Enter the URL of your site to promote',
    placeholder: 'Your website: www ...',
  },
  coupon: {
    title: 'Have a promo code?',
    activate: 'Enter the promo code',
    error:
      'The coupon was not found, check that the code is entered correctly.',
    discount: 'Discount {discount}% activated',
  },
  why: 'Why does it profitable?',
  hints: [
    {
      title: "If you don't have enough knowledge and skills",
      text:
        'No need to understand every single promotional tool, ' +
        'our algorithms do it for you',
    },
    {
      title: 'If the agency is claiming the biggest part of your budget',
      text:
        'You can launch your advertising campaign with us with ' +
        'a minimum budget and without paying the price ' +
        "for someone else's office and employees",
    },
    {
      title:
        "If you don't have time to spend it on " +
        'launching your promo campaigns',
      text:
        'We have automated all the routine actions, ' +
        'you just need to create an event and ' +
        'do couple of clicks on our website.',
    },
    {
      title: "If you've been let down by freelancers",
      text:
        'Our algorithms run like clockwork. You just have to ' +
        'specify the dates of promotion and the ' +
        'advertisement will start right on time.',
    },
  ],
  target: "Campaign's target:",
  period: "Campaign's period:",
  location: "Campaign's location:",
  attractTarget: 'To attract target audience to the event',
  online: 'Online',
  budget: "Campaign's budget:",
  reach: "Campaign's expected reach:",
  social: 'For {price} within {duration} approximately {audience} people will know' +
      ' about your event through social networks and search engines',
  packageTitle: 'What does the package include',
  adSocial: 'Advertisement in social networks and search engines*',
  choose: '*You can choose how to allocate the budget',
  creatives: 'Making of creatives',
  audience: 'Selection of a narrow target audience',
  campaign: 'Creating and launching a promo campaign',
  optimization: 'Daily optimization',
  statistics: 'Real time statistics',
  support: 'Personal support manager',
  report: 'Report with recommendations',
  paymentTitle: 'What will happen after payment',
  workingDay: 'Within working day:',
  details: "We'll contact you for further details",
  hours: 'Within 24 hours:',
  prepare: "We'll prepare the promo campaign, select target audiences and make creatives",
  agreed: 'On the agreed date:',
  launch: "We'll  launch the promo campaign",
  daily: 'Daily:',
  send: "We'll send statistics and optimize the campaign to achieve the best result",
  after: 'After completion: ',
  sendFinal: "We'll send you a final report with recommendations",
  resultsTitle: 'What do we do to achieve results',
  resultDescription: 'We build multiple micro-segmented audiences and create an individual advert for each audience. This allows us to:',
  description1: 'target the ad based on specific characteristics of each micro-audience',
  description2: 'quicky identify audiences that are less effective and eliminate them',
  description3: 'reallocate budget to the best performing audiences',
  description4: 'make decisions on changing the creatives or texts for certain audiences',
  result1: 'Our experts daily monitor the advertising campaign, analyze statistics and optimize it to achieve the best result',
  result2: 'Unlike other advertising systems and agenices, we do not charge any fee, which means that the entire budget you pay will be spent directly on advertising',
  result3: 'When the promotion campaign is finished you will receive a detailed report with all the statistics and also our recommendations for the future campaigns',
  pay: 'Pay',
  payPal: 'Your payment is protected by PayPall',
  yandex: 'Your payment is protected by Yandex Checkout',
  question: 'Ask a question',
  supportLink: "https://support.whatwhere.world/en/",
  back: 'Back',
}
