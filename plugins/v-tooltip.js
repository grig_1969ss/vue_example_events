import Vue from 'vue'
import { VTooltip } from 'v-tooltip'

VTooltip.options.defaultTemplate =
  '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
VTooltip.options.defaultArrowSelector = '.tooltip-arrow, .tooltip__arrow'
VTooltip.options.defaultInnerSelector = '.tooltip-inner, .tooltip__inner'

Vue.directive('tooltip', VTooltip)
