import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

export default ({ store }) => {
  if (store.getters['detect/IS_BOT']) {
    Vue.use(VueGoogleMaps)
  } else {
    Vue.use(VueGoogleMaps, {
      load: {
        key: 'AIzaSyAhZJmPLMnOHbIw2R_csAjCSi-E7gTvlzA',
        libraries: 'places',
      },
    })
  }
}
