import Vue from 'vue'
import { VueEditor, Quill } from 'vue2-editor'
import { VideoResponsive } from '~/utils/quill'

Vue.component('VueEditor', VueEditor)
Quill.register(VideoResponsive, true)
