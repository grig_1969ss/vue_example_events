import cookieUniversal from 'cookie-universal'

import cookieConsts from '~/utils/cookieConsts'

export default ({ req, res }, inject) => {
  const options = {
    alias: 'cookies',
    parseJSON: true,
  }

  const createdCookieUniversal = cookieUniversal(req, res, options.parseJSON)

  /* method for set cookie only if accepted with GDPR */
  createdCookieUniversal.setIfAccepted = (...p) => {
    const isCookieAccepted = createdCookieUniversal.get(
      cookieConsts.isCookieAccepted,
    )

    if (!isCookieAccepted) return

    createdCookieUniversal.set(...p)
  }

  inject(options.alias, createdCookieUniversal)
}
