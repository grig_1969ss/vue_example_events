// https://ru.vuejs.org/v2/cookbook/creating-custom-scroll-directives.html

import Vue from 'vue'
import popConsts from '~/utils/popoverConsts.js'

Vue.directive('scroll', {
  inserted: function(el, binding) {
    let f = function(evt) {
      if (binding.value(evt, el)) {
        window.removeEventListener('scroll', f)
      }
    }
    window.addEventListener('scroll', f)
  },
})

Vue.directive('focus', {
  inserted: (el, binding) => {
    if (binding.value !== false) {
      el.focus()
    }
  },
})

// если пользователь авторизован, то выполнить переданную функцию,
// иначе открыть окно авторизации. Например:
// <button v-only-auth:click="createPost"></button>
Vue.directive('only-auth', {
  inserted: (el, binding, vnode) => {
    if (binding.value !== false && binding.arg) {
      el.addEventListener(binding.arg, () => {
        let Store = vnode.context.$store
        if (Store.getters['auth/IS_AUTH']) {
          binding.value()
        } else {
          Store.dispatch('popovers/TOGGLE_POPOVER', popConsts.auth)
        }
      })
    }
  },
})
