import createPersistedState from 'vuex-persistedstate'

export default ({ store, isHMR }) => {
  if (isHMR) return
  if (process.client) {
    createPersistedState({ paths: ['newEvent', 'createEvent.event'] })(store)
  }
}
