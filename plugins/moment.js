import Vue from 'vue'
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'

Vue.use(VueMoment, {
  moment,
})

export default ({ app, store }, inject) => {
  moment.locale(store.state.lang.locale)

  app.moment = moment
  // inject('moment', moment)
}
