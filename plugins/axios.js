import { i18n } from '~/plugins/i18n'
import { path, prop } from 'ramda'
import popoverConsts from '~/utils/popoverConsts'

const AXIOS_CONFIG = {
  baseURL: process.env.apiUrl + '/api/2.0',
  timeout: 60000,
}

const regexPageNumber = new RegExp('(page=)([0-9]*)')

function sendAnalyticsAndMetric(pageNumber) {
  if (typeof window === 'undefined') return

  let url = window.location.pathname
  url += url.includes('?') ? '&' : '?'
  url += 'page=' + pageNumber

  if (typeof window.yaCounter43939494 !== 'undefined') {
    window.yaCounter43939494.hit(url, {
      title: document.title,
      referer: 'https://whatwhere.world',
    })
  }
  if (typeof window.ga !== 'undefined') {
    window.ga('set', 'page', url)
    window.ga('send', 'pageview')
  }
}

/**
 * @param {String} [token]
 */
function setAuthHeader(token) {
  this.setToken(token, 'JWT')
  // this.setHeader('Authorization', `JWT ${token}`)
}

/**
 * @param {String} [lang]
 */
function setAcceptLanguageHeader(lang) {
  this.setHeader('accept-language', lang)
}

/**
 * @param {String} [ip]
 */
function setClientIp(ip) {
  ip = ip && ip !== '127.0.0.1' && ip !== '::1' ? ip : null
  this.setHeader('x-forwarded-for', ip)
}

/**
 * @param req
 * @param {String|String[]} [headers]
 */
function proxyHeaders(req, headers = []) {
  if (!req || !req.headers) return
  ;[].concat(headers).forEach(header => {
    this.setHeader(header, req.headers[header])
  })
}

export default ({ store, app, redirect, $axios, query }) => {
  Object.assign($axios.defaults, AXIOS_CONFIG)

  $axios.setAuthHeader = setAuthHeader.bind($axios)
  $axios.setAcceptLanguageHeader = setAcceptLanguageHeader.bind($axios)
  $axios.setClientIp = setClientIp.bind($axios)
  $axios.proxyHeaders = proxyHeaders.bind($axios)

  $axios.setAuthHeader(store.getters['auth/TOKEN'])
  $axios.setAcceptLanguageHeader(store.getters['lang/LANG'])

  $axios.onRequest(request => {
    const result = regexPageNumber.exec(request.url)
    if (result) {
      const pageNumber = result[2]
      sendAnalyticsAndMetric(pageNumber)
    }
    if (request.params && request.params.page) {
      sendAnalyticsAndMetric(request.params.page)
    }
    return request
  })

  $axios.onRequestError(error => {
    if (error.response.status === 408 || error.code === 'ECONNABORTED') {
      console.log(`A timeout happend on url ${error.config.url}`)
    }
    return Promise.reject(error)
  })

  $axios.onResponseError(async function(error) {
    const silent = path(['config', 'silent'], error)

    switch (prop('status', error.response)) {
      case 401:
        // console.log('===Interceptor 401===')

        await store.dispatch('auth/CLEAR_USER_DATA')

        if (!silent) return redirect('/', { params: 'notAuth', ...query })

        break
      case 403:
        // console.log('===Interceptor 403===')

        try {
          /* check if user token is valid */
          await store.dispatch('auth/VERIFY_TOKEN')
          if (!silent) {
            /* valid token */
            store.dispatch(
              'notifications/error',
              app.i18n.tc('error.accessDenied'),
            )
          }
        } catch (e) {
          try {
            if (error.config.retry === 1) throw new Error()

            /* not valid token – try to refresh */
            await store.dispatch('auth/REFRESH')

            const token = store.getters['auth/TOKEN']
            if (!token) throw new Error()

            error.config.headers['Authorization'] = `JWT ${token}`
            error.config.retry = 1

            /* successful token refresh */
            return $axios.request(error.config)
          } catch (e) {
            /* not successful token refresh */
            store.dispatch('auth/CLEAR_USER_DATA')

            if (
              !silent &&
              store.getters['popovers/OPENED_POPOVER'] !== popoverConsts.auth
            ) {
              store.dispatch('popovers/TOGGLE_POPOVER', popoverConsts.auth)
            }
          }
        }

        return Promise.reject(error)
    }

    if (process.client && !silent) {
      const sendErrMessage = msg => store.dispatch('notifications/error', msg)

      try {
        const errorArray = JSON.parse(path(['request', 'response'], error))
        sendErrMessage(errorArray)
      } catch (e) {
        if (error.code === 'ECONNABORTED') {
          sendErrMessage(i18n.t('common.axiosConnErr'))
        } else {
          sendErrMessage(i18n.t('common.axiosError'))
        }
      }
    }

    return Promise.reject(error)
  })
}
