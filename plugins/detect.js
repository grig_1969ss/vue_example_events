import isBot from 'isbot'
/*
  If needed browser detect in future, can be also placed here
  https://www.npmjs.com/package/bowser
  https://www.npmjs.com/package/detect-browser

  Maybe perfomance improvement , isbot-fast instead of is-bot
  https://www.npmjs.com/package/isbot-fast
*/

/*
  ! Doesn't detect bot or not bot on GENERATED pages now
*/

export default ({ store, req }) => {
  const botCheck = req ? isBot(req.headers['user-agent']) : false

  store.dispatch('detect/SET_IS_BOT', botCheck)
}
