FROM keymetrics/pm2:10-alpine

ARG BASE_URL
ARG API_URL
ARG THUMBOR_KEY
ARG THUMBOR_URL
ARG SENTRY_URL
ARG AWS_ACCESS_KEY_ID
ARG AWS_SECRET_ACCESS_KEY
ARG TC_AWS_REGION
ARG TC_AWS_STORAGE_BUCKET
ARG COMMIT_SHORT_SHA
ARG WIDGET_PATH
ARG ENVIRONMENT
ARG YA_METRIKA_ID
ARG GA_ID
ARG GTM_ID
ARG VK_ID
ARG FB_ID
ARG GOOGLE_ID
ARG ENABLE_SUPPORT_CHAT

ENV BASE_URL=${BASE_URL}
ENV API_URL=${API_URL}
ENV THUMBOR_KEY=${THUMBOR_KEY}
ENV THUMBOR_URL=${THUMBOR_URL}
ENV SENTRY_URL=${SENTRY_URL}
ENV AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
ENV AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
ENV TC_AWS_REGION=${TC_AWS_REGION}
ENV TC_AWS_STORAGE_BUCKET=${TC_AWS_STORAGE_BUCKET}
ENV COMMIT_SHORT_SHA=${COMMIT_SHORT_SHA}
ENV WIDGET_PATH=${WIDGET_PATH}
ENV ENVIRONMENT=${ENVIRONMENT}
ENV YA_METRIKA_ID=${YA_METRIKA_ID}
ENV GA_ID=${GA_ID}
ENV GTM_ID=${GTM_ID}
ENV VK_ID=${VK_ID}
ENV FB_ID=${FB_ID}
ENV GOOGLE_ID=${GOOGLE_ID}
ENV ENABLE_SUPPORT_CHAT=${ENABLE_SUPPORT_CHAT}

RUN apk add --no-cache git

WORKDIR /app
COPY package*.json ./
COPY ecosystem-*.config.js ./
RUN npm ci

COPY . .
RUN NODE_ENV=production npm run build
RUN NODE_ENV=production npm run generate
#RUN npm run build-storybook

EXPOSE 3000
CMD [ 'npm run start' ]
