/* 
  Add to head as ...metaCommon.call(this)
*/
export function metaCommon() {
  const href = `${process.env.domainUrl}${this.$route.path}`

  return [{ rel: 'alternate', hreflang: 'x-default', href }]
}
