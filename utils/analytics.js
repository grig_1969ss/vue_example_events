const eventPage = {
  eventCategory: 'event',
  eventAction: 'view',
  eventLabel: 'public',
  eventValue: 1,
}

const searchPage = {
  eventCategory: 'search',
  eventAction: 'query',
  eventLabel: 'public',
  eventValue: 1,
}

const contactBtn = {
  eventCategory: 'event',
  eventAction: 'click',
  eventLabel: 'contact-button',
  eventValue: 1,
}

const shareBtn = {
  eventCategory: 'event',
  eventAction: 'click',
  eventLabel: 'share',
  eventValue: 1,
}

const expandText = {
  eventCategory: 'event',
  eventAction: 'click',
  eventLabel: 'expand-text',
  eventValue: 1,
}

const eventUrl = {
  eventCategory: 'event',
  eventAction: 'click',
  eventLabel: 'event-url',
  eventValue: 1,
}

const buyTicketsBtn = {
  eventCategory: 'event',
  eventAction: 'click',
  eventLabel: 'buyticket',
  eventValue: 1,
}

const buyTicketsBtnInEventCard = {
  eventCategory: 'results',
  eventAction: 'click',
  eventLabel: 'buyticket',
  eventValue: 1,
}

const buyTicketsBtnOnEventPage = {
  eventCategory: 'event',
  eventAction: 'click',
  eventLabel: 'ticket-button',
  eventValue: 1,
}

const userReg = {
  eventCategory: 'user',
  eventAction: 'reg',
  eventLabel: 'public',
  eventValue: 1,
}

const userFastReg = {
  eventCategory: 'user',
  eventAction: 'fast-reg',
  eventLabel: 'public',
  eventValue: 1,
}

export const trackEvent = opts => ctx => {
  if (process.server){
    return
  }
  ctx.$gtm.push({
    event: opts.eventCategory,
    eventAction: opts.eventAction,
    eventLabel: opts.eventLabel,
    value: opts.eventValue
  })
}

export const trackSearchPage = trackEvent(searchPage)
export const trackEventPage = trackEvent(eventPage)
export const trackContactBtn = trackEvent(contactBtn)
export const trackShareBtn = trackEvent(shareBtn)
export const trackExpandText = trackEvent(expandText)
export const trackEventUrl = trackEvent(eventUrl)
export const trackBuyTicketsBtn = trackEvent(buyTicketsBtn)
export const trackBuyTicketsBtnInEventCard = trackEvent(
  buyTicketsBtnInEventCard,
)
export const trackBuyTicketsBtnOnEventPage = trackEvent(
  buyTicketsBtnOnEventPage,
)
export const trackUserRegistration = trackEvent(userReg)
export const trackUserFastRegistration = trackEvent(userFastReg)
