import { path, prop } from 'ramda'
import lodashDebounce from 'lodash.debounce'
import cyrillicToTranslit from 'cyrillic-to-translit-js'
import pathToRegexp from 'path-to-regexp'

import thumborize from './thumborize'
import { i18n } from '~/plugins/i18n'

export const compilePath = (url, params) => pathToRegexp.compile(url)(params)

export const getIfExists = (stringPath, src) => {
  const path = stringPath.split ? stringPath.split('.') : null
  if (path) {
    return path.reduce((res, cur) => {
      return res ? res[cur] : null
    }, src)
  } else {
    return false
  }
}

export const splitNumberBy3 = str =>
  str && str.toString().length > 3
    ? str.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')
    : str

const thumborized = (img, ifVk) => {
  if (ifVk) {
    return img ? thumborize(img, 'other', 540, 240) : false
  } else return img ? thumborize(img, 'other', 1200, 600) : false
}

const whatImage = (event, alt, ifVk) => {
  if (
    path(['poster_1200x630', 'image'], event) ||
    path(['poster_1080x1080', 'image'], event)
  ) {
    return (
      thumborized(path(['poster_1200x630', 'image'], event), ifVk) ||
      thumborized(path(['poster_1080x1080', 'image'], event))
    )
  } else if (path(['cover', 'image'], event)) {
    return thumborized(event.cover.image, ifVk)
  } else if (!path(['cover', 'image'], event) && alt) {
    return thumborized(alt, ifVk)
  } else return null
}

export const hasCover = (ev, altImg) => {
  if (path(['cover', 'image'], ev)) {
    return [
      {
        hid: 'img',
        property: 'og:image',
        content: `${whatImage(ev, altImg)}`,
      },
      {
        hid: 'imgurl',
        property: 'og:image:url',
        content: `${whatImage(ev, altImg)}`,
      },
      {
        hid: 'vkImg',
        property: 'vk:image',
        content: `${whatImage(ev, altImg, true)}`,
      },
    ]
  } else return []
}

export const fmtDescription = obj => {
  const description = prop('description_text', obj) || ''
  const newDescription = description.substring(0, 300)
  return description.length > newDescription.length
    ? newDescription + '...'
    : newDescription
}

export const sha256 = async text => {
  const msgBuffer = new TextEncoder('utf-8').encode(text)
  const hashBuffer = await crypto.subtle.digest('SHA-256', msgBuffer)
  const hashArray = Array.from(new Uint8Array(hashBuffer))
  return hashArray.map(b => ('00' + b.toString(16)).slice(-2)).join('')
}

export const debounce = lodashDebounce

export const makeTranslit = ({ input, spaceReplacement }) =>
  cyrillicToTranslit().transform(input, spaceReplacement)

export const debounceEvent = (callback, time = 250, interval) =>
  // eslint-disable-next-line
  (...args) =>
    clearTimeout(
      interval,
      // eslint-disable-next-line standard/no-callback-literal
      (interval = setTimeout(() => callback(...args), time)),
    )

export const scrollToElement = ({
  element,
  align = 'center',
  topIndent = 0,
}) => {
  if (!element) return

  let alignTop = null

  const el = document.scrollingElement || document.documentElement

  switch (align) {
    case 'center':
      alignTop = el.scrollTop - el.clientHeight / 2
      break

    case 'top':
      alignTop = el.scrollTop
      break
  }

  const domRect = element.getBoundingClientRect()

  window.scrollTo({
    top: domRect.top + alignTop + topIndent,
    behavior: 'smooth',
  })
}

export function makeSearchQuery(routeQuery) {
  const query = {}
  const { period, dates, ...restQuery } = routeQuery

  if (period) {
    query.period = period
  } else if (dates) {
    query.dates = dates
  } else {
    query.actual = true
  }

  return { ...query, ...restQuery }
}

export function makePrefixParams({ prefix, params }) {
  return Object.keys(params).reduce((res, item) => {
    const excludeParams = ['page_size', 'page']
    const isExclude = excludeParams.some(p => p === item)

    if (isExclude) {
      res[item] = params[item]
    } else {
      res[`${prefix}-${item}`] = params[item]
    }

    return res
  }, {})
}

export function calcBrightness(color) {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color)

  return result
    ? (parseInt(result[1], 16) * 299 +
        parseInt(result[2], 16) * 587 +
        parseInt(result[3], 16) * 114) /
        1000 >
        123
    : null
}

export function formatPrice({ max: initMax, min: initMin, currency, lang }) {
  lang = !lang ? i18n.locale : lang
  currency = !currency ? '' : currency
  const max = Number(initMax)
  const min = Number(initMin)

  if (isNaN(max) && isNaN(min)){
    return null
  }

  if (max === 0.0 || min === 0.0){
    return i18n.t('eventpage.free')
  }

  const isRu = lang === 'ru'
  const currPref = isRu ? '' : ' ' + currency
  const currSuff = isRu ? currency + ' ' : ''

  if (!min)
    return `${i18n.t('common.upTo')} ${currPref}${max}${currSuff}`
  else return `${i18n.t('common.from')} ${currPref}${min}${currSuff}`
}

export function formatPriceText({ max: initMax, min: initMin}) {
  const max = Number(initMax)
  const min = Number(initMin)

  if (isNaN(max) && isNaN(min)){
    return i18n.t('eventpage.tickets')
  }

  if (max === 0.0 || min === 0.0){
    return i18n.t('eventpage.freeRegistration')
  }
  return i18n.t('eventpage.tickets')
}

/** @param { File } file **/
export const compressImage = async file =>
  new Promise((resolve, reject) => {
    const MAX_DIMENSION = 4000
    const MAX_SIZE = 1e7

    const reader = new FileReader()
    reader.onload = evt => {
      const img = new Image()
      img.onload = evt => {
        let height = img.height
        let width = img.width
        if (
          height > MAX_DIMENSION ||
          width > MAX_DIMENSION ||
          file.size > MAX_SIZE
        ) {
          const scale =
            height > width ? MAX_DIMENSION / height : MAX_DIMENSION / width
          height *= scale
          width *= scale
          const elem = document.createElement('canvas')
          elem.width = width
          elem.height = height
          const ctx = elem.getContext('2d')
          ctx.drawImage(img, 0, 0, width, height)
          ctx.canvas.toBlob(
            blob => {
              const compressedFile = new File([blob], file.name, {
                type: 'image/png',
                lastModified: Date.now(),
              })
              resolve(compressedFile)
            },
            'image/jpeg',
            0.7,
          )
        } else {
          resolve(file)
        }
      }
      img.src = evt.target.result
    }
    reader.readAsDataURL(file)
  })

/**
 *  @param { Array } results
 */
export const getLatestModified = results => {
  if (!prop('length', results)) return
  const arr =
    results.map(obj => obj.modified || path(['event', 'modified'], obj)) || []
  return arr.reduce((a, b) => (a > b ? a : b))
}

/**
 *  @param { Object } context
 *  @param { Array } dates
 */
export const setLatestModifiedAnd304 = ({ res, req, store }, dates = []) => {
  const maxDate = dates
    .filter(v => v)
    .reduce((accum, value) => (accum > value ? accum : value), null)
  const lastModified = maxDate ? new Date(maxDate) : new Date()
  lastModified.setMilliseconds(0)

  res.setHeader('Last-Modified', lastModified.toGMTString())
  if (
    store.getters['detect/IS_BOT'] &&
    new Date(req.headers['if-modified-since']) >= lastModified
  ) {
    res.statusCode = 304
  }
}

/**
 * @param input
 * @returns {boolean}
 */
export const isPlainObject = input =>
  Object.prototype.toString.call(input) === '[object Object]'

export function getTextFromNode(domElement) {
  const root = domElement
  const text = []

  function traverseTree(root) {
    Array.prototype.forEach.call(root.childNodes, function(child) {
      if (child.nodeType === 3) {
        const str = child.nodeValue.trim()
        if (str.length > 0) {
          text.push(str)
        }
      } else {
        traverseTree(child)
      }
    })
  }
  traverseTree(root)
  return text.join(' ')
}

/**
 * @param {string} link
 * @returns {string}
 */
export const normalizeLink = link => {
  link = link || ''
  return (link && !/https?:\/\//.test(link) ? `http://${link}` : link).trim()
}

/**
 * @param {string} phone
 * @returns {string}
 */
export const normalizePhone = phone => {
  phone = phone || ''
  let retVal = phone.trim()
  const plus = retVal[0] === '+' ? '+' : ''
  retVal = phone.replace(/[^\d]/g, '')

  return retVal ? `${plus}${retVal}` : retVal
}

/**
 * @param {Function} fn
 * @param {Object} [opts]
 * @param {Number} [opts.timeout=3000]
 * @param {Number} [opts.interval=50]
 * @returns {Promise<unknown>}
 */
export const poll = async (fn, opts = {}) => {
  const { timeout = 3000, interval = 50 } = opts

  const tstart = Date.now()

  return new Promise((resolve, reject) => {
    const intervalId = setInterval(async () => {
      try {
        let result = fn()
        result =
          result && typeof result.then === 'function' ? await result : result

        if (typeof result !== 'undefined') {
          clearInterval(intervalId)
          resolve(result)
        } else if (Date.now() - tstart > timeout) {
          clearInterval(intervalId)
          reject(new Error('Poll timed out'))
        }
      } catch (err) {
        reject(err)
      }
    }, interval)
  })
}

// /**
//  * @param {Array} arr
//  * @returns {Array}
//  */
// export const normalizeArray = arr => {
//   return arr
//     .filter(Boolean)
//     .filter((item, idx, arr) => arr.indexOf(item) === idx)
// }

export function makeGeoString({ lat, lng }) {
  return `${lng},${lat}`
}

export const paramsConst = {
  thisWeek: 'this-week',
  thisMonth: 'this-month',
}
export function getFileNameFromContentDisposition({ contentDisposition }) {
  const regex = /filename\*?=['"]?(?:UTF-\d['"]*)?([^;\r\n"']*)['"]?;?/gi
  const matches = regex.exec(contentDisposition)

  return matches ? matches[1] : ''
}

export function truncateString(str, ln){
  if (str.length <= ln) {
    return str
  }
  return str.slice(0, ln) + '...'

}

export function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
