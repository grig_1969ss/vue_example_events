import { isNil, isEmpty, prop, path } from 'ramda'
import moment from "moment";

export function prepareLocation({ location }) {
  return {
    title: location.name,
    geometry: {
      lat: location.geometry.location.lat,
      lng: location.geometry.location.lng,
    },
    address: {
        format: 'google',
        data: {
          place_id: location.place_id,
          address_components: location.address_components,
        }
    },
  }
}

function getCountryIdFromCity(city) {
  return typeof city.country === 'number' ? city.country : city.country.id
}

export function preFormatEvent({
  event,
  sessions,
  allowEmptyArray = false,
  isCreate = false,
}) {
  const hasKeyValue = key => {
    if (isNil(event[key])) return false

    if (allowEmptyArray) {
      return true
    } else {
      return !isEmpty(event[key])
    }
  }

  const data = {
    // о событии
    ...(hasKeyValue('title') && { title: event.title }),
    ...(hasKeyValue('cover') && { cover: prop('id', event.cover) }),
    ...(hasKeyValue('description') && { description: event.description }),
    ...(hasKeyValue('language') && { language: prop('iso', event.language) }),
    ...(hasKeyValue('links') && { links: event.links }),
    ...(hasKeyValue('language') && { language: prop('iso', event.language) }),
    // организатор
    ...(hasKeyValue('phones') && { phones: event.phones.filter(Boolean) }),
    ...(hasKeyValue('emails') && { emails: event.emails.filter(Boolean) }),
    // продвижение
    ...(hasKeyValue('eventType') && { type: event.eventType.slug }),
    ...(hasKeyValue('category') && { category: event.category.slug }),
    ...(hasKeyValue('genres') && {
      genres: event.genres.map(({ slug }) => slug),
    }),
    ...(hasKeyValue('tags') && { tags: event.tags.map(({ slug }) => slug) }),
    // ...(hasKeyValue('newTags') && { new_tags: event.newTags }),
  }

  return !isCreate
    ? data
    : {
        ...data,
        ...(hasKeyValue('isOnline') && { is_online: event.isOnline }),
        ...(event.isOnline
          ? { sessions }
          : {
              locations: [
                {
                  city: event.city.id,
                  country: getCountryIdFromCity(event.city),
                  ...(event.location &&
                    prepareLocation({ location: event.location })),
                  sessions,
                },
              ],
            }),
        ...(prop('id', event.promoPlan) && {
          promo: {
            plan: prop('id', event.promoPlan),
            start_dt: event.promoStartDate,
            gender: event.gender,
            email: event.promoEmail,
            age_from: event.age[0],
            age_to: event.age[1],
            languages: event.promoLanguages,
            customer_site: event.promoCustomerSite,
          },
        }),
      }
}

function hasRequiredParams({ params }) {
  const paramsKeys = Object.keys(params)
  const requiredParamsKeys = ['period', 'dates', 'actual']

  return paramsKeys.some(v => requiredParamsKeys.indexOf(v) !== -1)
}

export function getParams({ params = {}, defaultParams = {} }) {
  const hasParams = hasRequiredParams({
    params: { ...params, ...defaultParams },
  })

  return {
    ...defaultParams,
    ...params,
    ...(!hasParams && { actual: true }),
  }
}

export function getGenreFullTitle({ genre }) {
  const parentTitle = path(['parent', 'title'], genre)

  return parentTitle ? `${parentTitle}/${genre.title}` : genre.title
}

export function mapDatesData(data={}){
  const DAY = 1000 * 60 * 60 * 24
  const FOUR_WEEKS = DAY * 28

  const utcNow = moment.utc()
  const localNow = moment()
  const startDate = prop('start_date', data)
  const startTime = prop('start_time', data)
  const endDate = prop('end_date', data)
  const endTime = prop('end_time', data)
  const startDTUTC = prop('start_dt', data) ? moment.utc(data.start_dt): null
  const endDTUTC = prop('end_dt', data) ? moment.utc(data.end_dt): null

  const isStarted = startDTUTC ? utcNow >= startDTUTC: false
  const isFinished = endDTUTC ? utcNow > endDTUTC: false

  const startDTLocal = startDate && startTime
    ? moment.utc(`${startDate} ${startTime}`)
    : startDate
      ? moment.utc(`${startDate} 00:00`)
      : null

  const endDTLocal = endDate && endTime
    ? moment.utc(`${endDate} ${endTime}`)
    : endDate
      ? moment.utc(`${endDate} 23:59`)
      : null

  const isPeriod = startDTLocal && endDTLocal ? endDTLocal - startDTLocal >= DAY: false
  const isBigPeriod = isPeriod ? endDTLocal - startDTLocal >= FOUR_WEEKS: false

  let closesStart = startDTLocal
  if (isBigPeriod && isStarted && !isFinished) {
      const weekdays = path(['weekdays'], data) || []
      closesStart = getClosesStart(startDTLocal, endDTLocal, weekdays)
  }
  return Object.assign(data, {
    isStarted: isStarted,
    isFinished: isFinished,
    startDTLocal: startDTLocal ? startDTLocal.format(): null,
    endDTLocal: endDTLocal ? endDTLocal.format(): null,
    closesStartDT: closesStart ? closesStart.format(): null,
    hasStartTime: Boolean(startTime),
    hasEndTime: Boolean(endTime),
    isPeriod: isPeriod,
    isBigPeriod: isBigPeriod,
    displayAsPeriod: isPeriod && !isBigPeriod && isStarted && !isFinished,
    displayYear: startDTLocal ? startDTLocal.year() - localNow.year() !== 0: false,
    periodLocal: {
      start: startDTLocal ? startDTLocal.format(): null,
      end: endDTLocal ? endDTLocal.format(): null
    },
    periodUTC: {
      start: startDTUTC ? startDTUTC.format(): null,
      end: endDTUTC ? endDTUTC.format(): null
    }
  });
}

export function getClosesStart(start_dt, end_dt, weekdays) {
  const nextStart = moment.utc();
  const now = moment.utc()

  nextStart.hours(start_dt.hours())
    .minutes(start_dt.minutes())
    .seconds(start_dt.seconds())
    .milliseconds(start_dt.milliseconds())
  if (nextStart <= now){
    nextStart.add(1, 'days')
  }
  if (weekdays){
    if (!weekdays.includes(nextStart.day())){
      nextStart.day(weekdays.reduceRight((prev, curr) => {
        return (Math.abs(curr - nextStart.day()) < Math.abs(prev - nextStart.day()) ? curr : prev);
      }))
    }
  }
  return end_dt && nextStart > end_dt ? start_dt: nextStart
}

export function deSerializeObjectDates(obj){
  return Object.assign(obj, {
    startDate: prop('start_date', obj),
    startTime: prop('start_time', obj),
    endDate: prop('end_date', obj),
    endTime: prop('end_time', obj),
    startDTUTC: prop('start_dt', obj) ? moment.utc(obj.start_dt): null,
    endDTUTC: prop('end_dt', obj) ? moment.utc(obj.end_dt): null,
    created: prop('created', obj) ? moment.utc(obj.created): null,
    modified: prop('created', obj) ? moment.utc(obj.modified): null,
    startDTLocal: prop('startDTLocal', obj) ? moment.utc(obj.startDTLocal): null,
    endDTLocal: prop('endDTLocal', obj) ? moment.utc(obj.endDTLocal): null,
    closesStartDT: prop('closesStartDT', obj) ? moment.utc(obj.closesStartDT): null,
    periodLocal: {
      'start': path(['periodLocal', 'start'], obj) ? moment.utc(obj.periodLocal.start): null,
      'end': path(['periodLocal', 'end'], obj) ? moment.utc(obj.periodLocal.end): null
    },
    periodUTC: {
      'start': path(['periodUTC', 'start'], obj) ? moment.utc(obj.periodUTC.start): null,
      'end': path(['periodUTC', 'end'], obj) ? moment.utc(obj.periodUTC.end): null
    }
  })
}

export function getEventsData (sessions) {
    return sessions.map(session => {
      let event = session.event
      if (typeof(event) === 'object' && event !== null){
        event = deSerializeObjectDates(event)
      }
      return deSerializeObjectDates(Object.assign(session, {event: event}))
    })
  }
