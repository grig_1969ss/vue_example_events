import { isNil, isEmpty, prop } from 'ramda'

export function getAudience(){
  const max = 35000
  const min = 15000
  return (Math.random() * (max - min) + min).toFixed(0)
}


export function prepareAddress({ address }) {
    return {
        format: "whatwhere",
        data:
            {components:
                    [{type: "plain_address", value: address}]
            }

    }
}


export function preFormatEvent({
                                   event,
                                   sessions,
                                   allowEmptyArray = false,
                                   isCreate = false,
                               }) {
    const hasKeyValue = key => {
        if (isNil(event[key])) return false

        if (allowEmptyArray) {
            return true
        } else {
            return !isEmpty(event[key])
        }
    }

    const data = {
        ...(hasKeyValue('title') && { title: event.title }),
        ...(hasKeyValue('cover') && { cover: prop('id', event.cover) }),
        ...(hasKeyValue('links') && { links: event.links }),
        ...(hasKeyValue('tags') && { tags: event.tags.map(({ slug }) => slug) }),
        ...(hasKeyValue('emails') && { emails: [event.emails] }),
    }

    return !isCreate
        ? data
        : {
            ...data,
            ...(hasKeyValue('isOnline') && { is_online: event.isOnline }),
            ...(event.isOnline
                ? { sessions }
                : {
                    locations: [
                        {
                           address:  prepareAddress({ address: event.address }),
                            sessions,
                        },
                    ],
                }),
        }
}

export function periodRequired(value) {
    return value.some(i => i)
}