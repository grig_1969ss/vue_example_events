import moment from 'moment'

const formatDay = dayNumber =>
  moment()
    .days(dayNumber + 1)
    .format('dd')

export default businessHours => {
  if (!businessHours) return []

  let compressedHours = []
  businessHours.forEach(day => {
    const index = compressedHours.findIndex(
      elem =>
        elem.opening_time === day.opening_time &&
        elem.closing_time === day.closing_time,
    )

    if (~index) {
      compressedHours[index].weekdays.push(day.weekday)
      if (day.all_day) {
        compressedHours[index].allDays.push({
          wday: day.weekday,
          isAllDay: day.all_day,
        })
      }
    } else {
      compressedHours.push({
        opening_time: day.opening_time,
        closing_time: day.closing_time,
        weekdays: [day.weekday],
        allDays: [{ wday: day.weekday, isAllDay: day.all_day }],
      })
    }
  })

  return compressedHours.map(row => {
    if (row.allDays.length) {
      if (row.allDays.length === 7) {
        return 'Каждый день, весь день'
      }
      return 'Весь день'
    }

    const open = moment()
      .hours(row.opening_time.slice(0, 2))
      .minutes(row.opening_time.slice(3, 5))
      .seconds(row.opening_time.slice(6, 7))
      .format('HH:mm')
    const close = moment()
      .hours(row.closing_time.slice(0, 2))
      .minutes(row.closing_time.slice(3, 5))
      .seconds(row.closing_time.slice(6, 7))
      .format('HH:mm')

    // каждый день с 08:00 до 19:30
    if (row.weekdays.length === 7) {
      return ['каждый день с', open, 'до', close, 'без выходных'].join(' ')
    }

    // каждый рабочий день с 08:00 до 19:30
    if (
      row.weekdays.length === 5 &&
      row.weekdays.every(weekday => weekday < 5)
    ) {
      return ['каждый рабочий день с', open, 'до', close].join(' ')
    }

    // каждый рабочий день кроме пн с 08:00 до 19:30
    if (
      row.weekdays.length === 4 &&
      row.weekdays.every(weekday => weekday < 5)
    ) {
      let excludeDay = 0
      for (let i = 0; i < 5; i++) {
        if (!row.weekdays.includes(i)) {
          excludeDay = i
        }
      }
      return (
        'каждый рабочий день кроме ' +
        [formatDay(excludeDay), 'с', open, 'до', close].join(' ')
      )
    }

    // пн - ср с 08:00 до 19:30
    const checkIncrementDays = row.weekdays.reduce(
      (accumulator, currentValue) =>
        accumulator === currentValue - 1 ? currentValue : accumulator,
    )
    const lastDay = row.weekdays[row.weekdays.length - 1]
    const firstDay = formatDay[row.weekdays[0]]
    if (checkIncrementDays !== lastDay) {
      return [firstDay, '-', formatDay(lastDay), 'с', open, 'до', close].join(
        ' ',
      )
    }

    // пн с 08:00 до 19:30 || пн, ср, пт с 08:00 до 19:30
    const d = row.weekdays.map(weekday => formatDay(weekday)).join(', ')
    return [d, 'с', open, 'до', close].join(' ')
  })
}
