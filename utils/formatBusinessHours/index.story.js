import { storiesOf } from '@storybook/vue'
import formatBusinessHours from '.'
import {
  everyDay,
  everyWorkDay,
  workDayWithoutMonday,
  workDayWithoutFriday,
  workDayWithoutTuesday,
  threeDays,
  randomHours,
} from '~/.storybook/lib/businessHours'

storiesOf('utils/formatBusinessHours', module).add(
  'default',
  () => ({
    data: () => ({
      testCases: [
        everyDay,
        everyWorkDay,
        workDayWithoutMonday,
        workDayWithoutTuesday,
        workDayWithoutFriday,
        threeDays,
        randomHours,
      ],
    }),
    methods: {
      formatBusinessHours(businessHours) {
        return formatBusinessHours(businessHours)
      },
    },
    template: `<div>
                  <ul 
                      v-for="(hours, i) in testCases"
                      :key="i"
                      style="border-bottom: solid 1px gray; padding: 24px"
                  >
                    <li 
                      v-for="(item, j) in formatBusinessHours(hours)" 
                      :key="j"
                    >
                      {{ item }}
                    </li>
                  </ul>
              </div>`,
  }),
  { info: true },
)
