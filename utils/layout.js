import cookieConsts from '~/utils/cookieConsts'

export const layoutConsts = {
  default: 'default',
  empty: 'empty',
}

export function getLayoutName(v) {
  return layoutConsts[v] || layoutConsts.default
}

export function getEventCreatePageLayout({ query, app }) {
  const cookieLayout = app.$cookies.get(cookieConsts.eventLayoutName) || null
  const queryLayout = query.layout ? getLayoutName(query.layout) : null

  return queryLayout || cookieLayout || layoutConsts.default
}
