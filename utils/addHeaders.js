import { omit } from 'ramda'

const keysToOmit = ['host', 'referer', 'accept']

const omitKeys = xs => obj => omit(xs, obj)
const omitHeaders = omitKeys(keysToOmit)

// merge client headers to nodejs axios instance omitting some keys
export const addHeaders = (target, headers) =>
  Object.assign(target, omitHeaders(headers))
