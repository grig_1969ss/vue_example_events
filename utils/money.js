import { isNil, prop } from "ramda"


export function displayPrice(
  priceObj={},
  currencyKey='symbol',
  displayCurrency=true,
  digits=2
){
  if ( isNil(prop('amount', priceObj)) ){
    return ''
  }
  const amount = Number.parseFloat(priceObj.amount).toFixed(digits);
  const currencyObj = prop('currency', priceObj) || {}
  const currency = prop(currencyKey, currencyObj) || ''
  if ( !displayCurrency || !currency ){
    return amount
  }
  if (currencyObj.code === 'RUB'){
    return `${amount}${currency}`
  } else {
    return `${currency}${amount}`
  }
}