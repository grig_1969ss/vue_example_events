// horizontal mousewheel scroll directive

const wheelHandler = (el, binding) => e => {
  // if (!el.dataset.scrollable) return
  let mult = binding.value || 2
  let delta = (e.wheelDelta || -e.detail * 20) / mult
  let scrollingDown = delta < 0
  let scrollingUp = delta > 0
  let scrollAtEnd =
    el.firstChild.scrollWidth - el.firstChild.scrollLeft ===
    el.firstChild.offsetWidth
  let scrollAtStart = el.firstChild.scrollLeft === 0
  e.preventDefault()
  e.stopPropagation()

  if (el.clientWidth === el.scrollWidth && el.firstChild) {
    el.firstChild.scrollLeft -= delta
    if (scrollingDown && scrollAtEnd) {
      window.scrollBy(0, -delta)
    }
    if (scrollingUp && scrollAtStart) {
      window.scrollBy(0, -delta)
    }
  } else {
    el.scrollLeft -= delta
  }
}

// FF doesn't recognize mousewheel as of FF3.x
const mouseWheelEvent =
  process.client && /Firefox/i.test(navigator.userAgent)
    ? 'DOMMouseScroll'
    : 'mousewheel'

const directive = (el, binding) =>
  el.addEventListener(mouseWheelEvent, wheelHandler(el, binding), false)
const unbind = (el, binding) =>
  el.removeEventListener(mouseWheelEvent, wheelHandler(el, binding), false)

export default {
  inserted: directive,
  componentUpdated: directive,
  unbind,
}
