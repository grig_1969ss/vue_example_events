export default {
  isCookieAccepted: 'isCookieAccepted',
  /* 
    Set empty layout on event creation. Cookie is set on
    create event first page. Cookies is removed after accessing
    'default' layout or when time is expired.
  */
  eventLayoutName: 'eventLayoutName',
}
