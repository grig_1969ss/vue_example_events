import { helpers } from 'vuelidate/lib/validators'

const { req, withParams } = helpers

export const phone = withParams(
  { type: 'phone' },
  value => !req(value) || /^\+?\d+$/.test(value.replace(/^[+\d]$/g, '')),
)

export function periodRequired(value) {
  return value.every(i => i)
}
