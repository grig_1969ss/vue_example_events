import { isNil, isEmpty } from "ramda";

export default class ProfileRoutesService {
  static profileIndexPage = 'profile-id'

  static organizerIndexPage = 'organizers-id'

  static profileEventsPage = 'profile-id-events'

  static organizerEventsPage = 'organizers-id-events'

  static profileJournalsPage = 'profile-id-journals'

  static profileTicketsPage = 'profile-id-tickets'

  static profileMarketplacePage = 'profile-id-marketplace'

  constructor({ route, store, router }) {
    this.store = store
    this.router = router
    route && this.setRoute(route)
  }

  setRoute(route) {
    this.route = route
    this.params = route.params
  }

  get paramId() {
    return parseInt(this.params.id)
  }

  get isOrganizerView() {
    return this.route.name.startsWith(ProfileRoutesService.organizerIndexPage)
  }

  get isProfileView() {
    return !this.isOrganizerView
  }

  get isProfileIndexPage() {
    return this.route.name === ProfileRoutesService.profileIndexPage
  }

  get isOrganizerIndexPage() {
    return this.route.name === ProfileRoutesService.organizerIndexPage
  }

  get isIndexPage() {
    return this.isProfileIndexPage || this.isOrganizerIndexPage
  }

  get isProfileEventsPage() {
    return this.route.name === ProfileRoutesService.profileEventsPage
  }

  get isOrganizerEventsPage() {
    return this.route.name === ProfileRoutesService.organizerEventsPage
  }

  get isEventsPage() {
    return this.isProfileEventsPage || this.isOrganizerEventsPage
  }

  get isProfileJournalsPage() {
    return this.route.name === ProfileRoutesService.profileJournalsPage
  }

  get isJournalsPage() {
    return this.isProfileJournalsPage
  }

  get isProfileTicketsPage() {
    return this.route.name === ProfileRoutesService.profileTicketsPage
  }

  get isTicketsPage() {
    return this.isProfileTicketsPage
  }

  get isMarketplacePage(){
    return this.route.name === ProfileRoutesService.profileMarketplacePage
  }

  get profile() {
    const profile = this.store.getters['user/PROFILE']
    const organizer_as_profile = this.store.getters['user/ORGANIZER_AS_PROFILE']
    const user = this.store.getters['user/GET_USER']
    return !isNil(profile) && !isEmpty(profile)
      ? profile
      : !isNil(organizer_as_profile) && !isEmpty(organizer_as_profile)
        ? organizer_as_profile
        : user
  }

  get organizer() {
    return this.store.getters['user/ORGANIZER']
  }

  get supplier() {
    return this.store.getters['marketplace/profile/SUPPLIER']
  }

  get customer() {
    return this.store.getters['marketplace/profile/CUSTOMER']
  }

  get isOwnProfile() {
    return this.store.getters['user/IS_OWN_PROFILE']
  }

  get indexRoute() {
    if (this.isOrganizerView) {
      return {
        name: ProfileRoutesService.organizerIndexPage,
        params: { id: this.organizer.url_id },
      }
    }

    return {
      name: ProfileRoutesService.profileIndexPage,
      params: { id: this.profile.id },
    }
  }

  get eventsRoute() {
    if (this.isOrganizerView) {
      return {
        name: ProfileRoutesService.organizerEventsPage,
        params: { id: this.organizer.url_id },
      }
    }

    return {
      name: ProfileRoutesService.profileEventsPage,
      params: { id: this.profile.id },
    }
  }

  get journalsRoute() {
    return {
      name: ProfileRoutesService.profileJournalsPage,
      params: { id: this.profile.id },
    }
  }

  get marketplaceRoute() {
    return {
      name: ProfileRoutesService.profileMarketplacePage,
      params: { id: this.profile.id },
    }
  }

  get ticketsRoute() {
    return {
      name: ProfileRoutesService.profileTicketsPage,
      params: { id: this.profile.id },
    }
  }

  get fetchEventsParams() {
    const params = {
      expand: 'cover',
      page_size: 10,
    }

    if (this.isOrganizerView) {
      params.organizer = this.organizer.id
    } else {
      params.user = this.profile.id
    }

    return params
  }

  get fetchEventsPromoParams() {
    return {
      user: this.profile.id,
      expand: 'event,event.cover,reports,plan',
      page_size: 10,
    }
  }

  get fetchJournalsParams() {
    return {
      expand: 'cover,created_by,created_by.avatar',
      ordering: this.isOwnProfile ? '-modified' : void 0,
      page_size: 10,
      user: this.profile.id,
    }
  }

  get fetchSupplierOrdersParams() {
    return {
      supplier: this.supplier.id,
    }
  }

  get fetchCustomerOrdersParams(){
    return {
      customer: this.customer.id,
    }
  }

  get fetchSupplierRepliesParams() {
    return {
      supplier: this.supplier.id,
    }
  }

}
