const Reader = process.server
  ? require('@maxmind/geoip2-node').Reader
  : undefined

const readerPromise = process.server
  ? Reader.open('./geo/GeoLite2-City.mmdb', {
    cache: {
      max: 30000
    },
    watchForUpdates: true
  })
  : Promise.resolve()

export default async ip => {
  try{
    return (await readerPromise).city(ip)
  } catch (AddressNotFoundError) {
    return {}
  }
}
