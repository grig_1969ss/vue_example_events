export default {
  auth: 'popover_auth',
  signUp: 'popover_signUp',
  restore: 'popover_restore',
  share: 'popover_share',
  search: 'popover_search',
  place: 'popover_place',
  menu: 'popover_menu',
  addToJournal: 'addToJournal',
  video: 'popover_video',
  eventOwner: 'eventOwner',
  paymentSuccess: 'paymentSuccess',
  paymentError: 'paymentError',
  searchWidget: 'searchWidget',
  simpleReg: 'simpleReg',
  mailPicker: 'mailPicker',
  mailPickerSucc: 'mailPickerSucc',
  simpleRegSucc: 'simpleRegSucc',
  infoForm: 'infoForm',
  userMenuModal: 'userMenuModal',
  callMe: 'callMe',
  forOrganizer: 'forOrganizer',
  deleteUser: 'delete_user',
  editEventMainInfo: 'editEventMainInfo',
  editEventPlaceTime: 'editEventPlaceTime',
  deleteScheme: 'deleteScheme',
  login: 'popover_login',
  newSignUp: 'popover_newSignUp',
  orderFormModal: 'OrderFormModal',
  deleteOrder: 'delete_order',
  selectReply: 'select_reply',
  deleteReply: 'delete_reply',
  deleteFile: 'delete_file'
}

export const cities = {
  ru: [
    {
      id: 524901,
      name: 'Москва',
    },
    {
      id: 498817,
      name: 'Санкт-Петербург',
    },
    {
      id: 520555,
      name: 'Нижний Новгород',
    },
    {
      id: 551487,
      name: 'Казань',
    },
    {
      id: 1486209,
      name: 'Екатеринбург',
    },
    {
      id: 499099,
      name: 'Самара',
    },
    {
      id: 1496747,
      name: 'Новосибирск',
    },
    {
      id: 542420,
      name: 'Краснодар',
    },
    {
      id: 501175,
      name: 'Ростов-на-Дону',
    },
  ],
  en: [
    {
      id: 2643743,
      name: 'London',
    },
    {
      id: 2950159,
      name: 'Berlin',
    },
    {
      id: 3117735,
      name: 'Madrid',
    },
    {
      id: 3169070,
      name: 'Rome',
    },
    {
      id: 2988507,
      name: 'Paris',
    },
    {
      id: 2800866,
      name: 'Brussels',
    },
    {
      id: 6094817,
      name: 'Ottawa',
    },
    {
      id: 2759794,
      name: 'Amsterdam',
    },
    {
      id: 2172517,
      name: 'Canberra',
    },
    {
      id: 4717560,
      name: 'Paris',
    },
    {
      id: 5549222,
      name: 'Washington',
    },
    {
      id: 5128581,
      name: 'New York City',
    },
  ],
}
