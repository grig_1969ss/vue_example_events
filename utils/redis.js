const redis = process.server ? require('redis') : undefined

let clientPromise
function getClientPromise() {
  return new Promise((resolve, reject) => {
    const client = redis.createClient({
      host: process.env.redisHost,
      port: process.env.redisPort,
      db: process.env.redisDB,
      password: process.env.redisPassword,
    })

    client.on('error', err => {
      clientPromise = null
      client.quit()
      reject(err)
    })
    client.on('end', () => {
      clientPromise = null
      reject(new Error('Redis connection closed'))
    })
    client.on('ready', () => {
      resolve(client)
    })
  })
}

const get = async key => {
  if (!clientPromise) clientPromise = getClientPromise()

  const client = await clientPromise

  return new Promise((resolve, reject) => {
    client.get(key, (err, data) => {
      if (err) return reject(err)
      resolve(data)
    })
  })
}

export default { get }
