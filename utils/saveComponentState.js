export default {
  methods: {
    saveState(getter) {
      return this.$store.getters[getter]
    },
  },
}
