import { prop, startsWith, pickBy } from 'ramda'
import { i18n } from '~/plugins/i18n'

const variants = {
  horizontal: 'horizontal',
  vertical: 'vertical',
  map: 'map',
  search: 'search',
}

function makeWidgetCode({
  style,
  styleClass,
  variant,
  query,
  version = '1',
  lang = 'ru',
}) {
  const styleString = style
    ? `.whatwhereworld-container.${styleClass}{${style}}`
    : ''
  const text = i18n.t('widget.widgetText')

  const dataQuery = JSON.stringify(query)

  return `
  <style>
    ${styleString}
    .whatwhereworld-container {
      width: 100%;
    }

    .whatwhereworld-text {
      display: inline-block;
      margin-top: 18px;
      width: 100%;
      text-align: right;
      font-family: 'Circe', Tahoma, Arial;
      font-size: 12px;
      color: #000;
    }
    .whatwhereworld-text a {
      color: #0070ff;
      text-decoration: none;
    }
    .whatwhereworld-text a:hover {
      text-decoration: underline;
    }
  </style>

  <div class="whatwhereworld-container ${styleClass || ''}">
    <div
      data-variant="${variant}"
      data-v="${version}"
      data-query='${dataQuery}'
      data-lang="${lang}"
      class="whatwhere-world"
    ></div>
    <span class="whatwhereworld-text">
      ${text}
      <a href="${process.env.domainUrl}" target="_blank">
        WhatWhere.World
      </a>
    </span>
  </div>

  <script src="${process.env.WIDGET_PATH}/app.js"></script>
  `
}

export const iframeLinkTemplates = {
  map: args =>
    makeWidgetCode({
      ...args,
      variant: variants.map,
      styleClass: 'whatwhereworld-map',
    }),
  horizontal: args =>
    makeWidgetCode({
      ...args,
      variant: variants.horizontal,
      styleClass: 'whatwhereworld-horizontal',
    }),
  vertical: args =>
    makeWidgetCode({
      ...args,
      variant: variants.vertical,
      styleClass: 'whatwhereworld-vertical',
      style: 'display: inline-block; width: auto;',
    }),
  search: args =>
    makeWidgetCode({
      ...args,
      variant: variants.search,
      styleClass: 'whatwhereworld-search',
    }),
}

export function getCurrentWidgetVersion({ query }) {
  return prop('v', query) || '1'
}

function getUtmFromQuery({ query }) {
  const utmPrefix = 'utm_'
  const checkStartsWith = (_, key) => startsWith(utmPrefix, key)

  return pickBy(checkStartsWith, query)
}

export function makeWidgetUtm({
  utmContentType,
  utmContentPlace,
  utmTerm,
  query,
}) {
  return {
    utm_source: 'partner',
    utm_medium: 'referral',
    utm_content: `widget_${utmContentType}_${utmContentPlace}`,
    ...(utmTerm && { utm_term: utmTerm }),
    ...(query && getUtmFromQuery({ query })),
  }
}
