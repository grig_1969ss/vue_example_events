import moment from 'moment'
import {
  format,
  parseISO,
  addDays,
  isSameMonth as isSameMonthCheck,
} from 'date-fns'
import { ru, es } from 'date-fns/locale'
import { i18n } from '~/plugins/i18n'

export const formatStrings = {
  // Суббота, 17 мая в 20:00,
  get withWeekDay() {
    return `iiii, d MMMM '${i18n.tc('formatDate.at')}' p`
  },
  get withWeekDayNoTime() {
    return 'iiii, d MMMM'
  },
  get withWeekDayShort() {
    return `iiiiii, d MMMM '${i18n.tc('formatDate.at')}' p`
  },
  get withWeekDayNoTimeShort() {
    return 'iiiiii, d MMMM'
  },
  get untilDate() {
    return `'${i18n.tc('formatDate.until')}' d MMMM yyyy`
  },
  iso8601: `yyyy-MM-dd'T'HH:mm:ssXXX`,
  isoDate: 'yyyy-MM-dd',
  isoTime: 'HH:mm:ssXXX',
  dayMonth: 'd MMMM',
  dayMonthYear: 'd MMMM yyyy',
  monthYear: 'MMMM yyyy',
  // 9 октября, ср, 11:38
  ticketFormat: 'd MMMM, iiiiii, HH:mm',
  // 30.06.2019, 16:30
  fullDateTime: 'dd.MM.yyyy, HH:mm',
  fullDate: 'dd.MM.yyyy',
  fullTime: 'HH:mm',
  ticketFullDateTime: 'dd MMMM yyyy, HH:mm',
  ticketFullDate: 'dd MMMM yyyy',
  ticketFullTime: 'HH:mm',
  month: 'MMMM',
  year: 'YYYY',
}

const locales = { ru, es }

export function minutesOfDay(m) {
  return m.minutes() + m.hours() * 60
}

export function makePeriodString({ startDt, duration }) {
  const durationInDays = duration.match(/\d+/)[0]
  const endDt = addDays(startDt, durationInDays)

  const isSameMonth = isSameMonthCheck(startDt, endDt)

  if (isSameMonth) {
    const endDayOfMonth = format(endDt, 'd')
    const startDayOfMonth = format(startDt, 'd')
    const monthYear = formatDateTime({
      dateTime: startDt,
      formatString: formatStrings.monthYear,
    })

    return `${startDayOfMonth}-${endDayOfMonth} ${monthYear}`
  }

  const startString = formatDateTime({
    dateTime: startDt,
    formatString: formatStrings.dayMonthYear,
  })

  const endString = formatDateTime({
    dateTime: endDt,
    formatString: formatStrings.dayMonthYear,
  })

  return `${startString} – ${endString}`
}

export function formatMonth({ small = false, dateTime }) {
  const lang = moment.locale()

  if (small) {
    return lang === 'ru'
      ? dateTime
          .format('D MMM')
          .toLowerCase()
          .replace(/[^а-я.]/g, '')
      : dateTime.format('MMM').toLowerCase()
  } else {
    return lang === 'ru'
      ? dateTime.format('D MMMM').replace(/[^а-я.]/g, '')
      : dateTime.format('MMMM')
  }
}

export function makeEventDateMain({
  start: startInit,
  end: endInit,
  format,
  timeLT = false,
}) {
  /*
    if isStringFormat, function will return data string,
    else – object with { dateTime, message, period }
  */
  const isStringFormat = format === 'string'
  const lang = moment.locale()

  const now = moment()
  const start = moment.parseZone(startInit)
  const end = moment.parseZone(endInit)

  const isSameDay = start.isSame(end, 'day')
  const hoursDiff = end.diff(start, 'hours')
  const isStartSameCurrentYear = start.isSame(now, 'year')

  const formats = {
    dayMonthYear: dt => ({
      date: dt.format('DD'),
      day: dt.format('dd'),
      month: formatMonth({ dateTime: dt, small: true }),
      year: dt.format('YYYY'),
    }),
    dayMonthTime: dt => {
      const hasMeridiem = !timeLT && lang === 'en'

      return {
        date: dt.format('DD'),
        day: dt.format('dd'),
        month: formatMonth({ dateTime: dt, small: true }),
        time: (() => {
          if (timeLT) return dt.format('LT')

          if (lang === 'en') {
            return dt.format('h:mm')
          } else {
            return dt.format('H:mm')
          }
        })(),
        ...(hasMeridiem && { meridiem: dt.format('A') }),
      }
    },
  }

  const stringFormats = {
    dayMonth: 'DD MMMM',
    year: 'YYYY',
    time: 'LT',
    dayMonthYear: 'DD MMMM, YYYY',
    dayMonthTime: 'DD MMMM LT',
  }

  /*
    1) если дата начала события та же что и дата конца,
    либо между ними не более 12 часов, то формат:
    21 сентября 16:00 - только дача и время начала
  */
  if (isSameDay || hoursDiff <= 12) {
    /*
    2) дополнение этого же правила: если год этой даты
    отличается от текущего, то рисуем:
    21 сентября, 2018 - без времени
  */
    if (!isStartSameCurrentYear) {
      if (isStringFormat) {
        return start.format(stringFormats.dayMonthYear)
      }
      return {
        dateTime: formats.dayMonthYear(start),
      }
    }

    if (isStringFormat) {
      return start.format(stringFormats.dayMonthTime)
    }
    return {
      dateTime: formats.dayMonthTime(start),
    }
  }

  const monthDiff = end.diff(start, 'month')
  const isEndSameCurrentYear = end.isSame(now, 'year')

  /*
    3) Если даты отличаются не более чем на 3 месяца, то формат:
    21 сентября - 13 октября, 16:00 - дата конца и начала и время начала
  */
  if (monthDiff <= 3) {
    const periodDay = `${start.format(stringFormats.dayMonth)} - ${end.format(
      stringFormats.dayMonth,
    )}`
    /*
      4) Дополнение прошлого правила: если при этом год
      обоих дат отличается от текущего, то рисуем:
      21 сентября - 13 октября, 2018 - без времени
    */
    if (!isEndSameCurrentYear && !isStartSameCurrentYear) {
      if (isStringFormat) {
        return `${periodDay}, ${start.format(stringFormats.year)}`
      }
      return {
        period: [formats.dayMonthYear(start), formats.dayMonthYear(end)],
      }
    }

    if (isStringFormat) {
      return `${periodDay}, ${start.format(stringFormats.time)}`
    }
    return {
      period: [formats.dayMonthTime(start), formats.dayMonthTime(end)],
    }
  } else {
    /*
      5) Если между датами больше 3 месяца, то правила такие:
      если дата начала < сегодня < даты конца, то:
      21 сентября 16:00 - рисуем сегодняшнее число в качестве даты и время начала
    */
    if (now.isBetween(start, end)) {
      if (isStringFormat) {
        return `${now.format(stringFormats.dayMonth)} ${start.format(
          stringFormats.time,
        )}`
      }

      const dateTime = moment(now)
        .clone()
        .set({
          hour: start.get('hour'),
          minute: start.get('minute'),
          second: start.get('second'),
        })

      return {
        dateTime: formats.dayMonthTime(dateTime),
      }
    }

    /*
      если дата конца < сегодня, то:
      пишем "Завершено"
    */
    if (now.isAfter(end)) {
      if (isStringFormat) {
        return i18n.tc('common.dateOver')
      }
      return {
        message: i18n.tc('common.dateOver'),
      }
    }

    /*
      если дата начала > сегодня, то:
      21 сентября 16:00 - пишем дату и время начала
    */
    if (now.isBefore(start)) {
      /*
        6) если при этом год отличается от текущего, то время не указываем,
        а пишем только год, кроме случая, где мы написали "Завершено"
       */
      if (!isStartSameCurrentYear) {
        if (isStringFormat) {
          return start.format(stringFormats.dayMonthYear)
        }
        return {
          dateTime: formats.dayMonthYear(start),
        }
      }

      if (isStringFormat) {
        return start.format(stringFormats.dayMonthTime)
      }
      return {
        dateTime: formats.dayMonthTime(start),
      }
    }
  }
}

export function makeEventDate({ start: startInit, end: endInit }) {
  const now = moment()
  const start = moment.parseZone(startInit)
  const end = moment.parseZone(endInit)
  const monthDiff = end.diff(start, 'month')

  let dateTime = null

  /*
    5) Если между датами больше 3 месяца, то правила такие:
    если дата начала < сегодня < даты конца, то:
    21 сентября 16:00 - рисуем сегодняшнее число в качестве даты и время начала
  */
  if (monthDiff > 3 && now.isBetween(start, end)) {
    dateTime = moment(now)
      .clone()
      .set({
        hour: start.get('hour'),
        minute: start.get('minute'),
        second: start.get('second'),
      })
  } else {
    dateTime = start
  }

  return dateTime
}

export function formatDateToObj({ dateTime, small = false }) {
  return {
    date: dateTime.format('DD'),
    day: dateTime.format('dd'),
    month: formatMonth({ small, dateTime }),
    time: dateTime.format('LT'),
  }
}

export function makeDateTimeString({
  date,
  time,
  formatString = formatStrings.iso8601,
  dateFormatString = formatStrings.isoDate,
  timeFormatString = formatStrings.isoTime,
}) {
  let _str = `${date} ${time}`
  let _format = formatString

  if ( !date && !time ) {
    return ''
  } else if( !date ) {
    _str = time
    _format = timeFormatString
  } else if( !time ) {
    _str = date
    _format = dateFormatString
  }
  return format(parseISO(_str), _format, {
    locale: locales[i18n.locale],
  })
}

export function formatDateTime({
  dateTime,
  formatString = formatStrings.iso8601,
}) {
  return format(dateTime, formatString, {
    locale: locales[i18n.locale],
  })
}

export function makeDateTimeStringFromEvent({ event = {}, variant }) {
  const variants = {
    start: {
      date: 'start_date',
      time: 'start_time',
    },
    end: {
      date: 'end_date',
      time: 'end_time',
    },
  }

  const v = variants[variant]

  const date = event[v.date]
  const time = event[v.time]

  return makeDateTimeString({ date, time })
}

export const currentMonth = () => {
  return moment().format(formatStrings.month)
}

export const currentMonthCapitalized = () => {
  return currentMonth()[0].toUpperCase() + currentMonth().slice(1)
}

export const currentYear = () => {
  return moment().format(formatStrings.year)
}