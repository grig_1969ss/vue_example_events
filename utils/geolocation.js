const isAvailable = () => 'geolocation' in window.navigator

export const getLocation = options => {
  return new Promise((resolve, reject) => {
    if (!isAvailable()) {
      reject("Browser doesn't support")
    } else {
      window.navigator.geolocation.getCurrentPosition(
        p => {
          resolve({
            lat: p.coords.latitude,
            lng: p.coords.longitude,
            altitude: p.coords.altitude,
            altitudeAccuracy: p.coords.altitudeAccuracy,
            accuracy: p.coords.accuracy,
          })
        },
        () => {
          reject('Has no access to position')
        },
        options,
      )
    }
  })
}
