export const genderMixed = `<svg xmlns="http://www.w3.org/2000/svg" width="23.685" height="22.5" viewBox="0 0 23.685 22.5">
<defs>
    <style>
        .svg-icn-mixed-1,.svg-icn-mixed-3{fill:none}.svg-icn-mixed-1{stroke-width:2px}.svg-icn-mixed-2{stroke:none}
    </style>
</defs>
<g id="Group_2402" data-name="Group 2402" transform="translate(-93 -2559.5)">
    <g id="Group_2333" data-name="Group 2333" transform="translate(1)">
        <g id="Ellipse_149" class="svg-icn-mixed-1" data-name="Ellipse 149" transform="translate(92 2565.179)">
            <circle cx="5.91" cy="5.91" r="5.91" class="svg-icn-mixed-2"/>
            <circle cx="5.91" cy="5.91" r="4.91" class="svg-icn-mixed-3"/>
        </g>
        <g id="Group_2332" data-name="Group 2332" transform="translate(-387.833 1894)">
            <path id="Path_425" d="M15394 924.333V919" class="svg-icn-mixed-1" data-name="Path 425" transform="translate(-14908.5 -236.333)"/>
            <path id="Path_426" d="M15386 927h5.334" class="svg-icn-mixed-1" data-name="Path 426" transform="translate(-14903.167 -241.667)"/>
        </g>
    </g>
    <g id="Group_2334" data-name="Group 2334" transform="translate(-309 1900.313)">
        <g id="Ellipse_15" class="svg-icn-mixed-1" data-name="Ellipse 15" transform="translate(408 664.866)">
            <circle cx="5.91" cy="5.91" r="5.91" class="svg-icn-mixed-2"/>
            <circle cx="5.91" cy="5.91" r="4.91" class="svg-icn-mixed-3"/>
        </g>
        <g id="Group_393" data-name="Group 393" transform="rotate(-135 348.567 247.536)">
            <path id="Path_427" d="M0 5.91V0" class="svg-icn-mixed-1" data-name="Path 427" transform="translate(2.955)"/>
            <path id="Path_428" d="M0 0l2.955 2.955L5.91 0" class="svg-icn-mixed-1" data-name="Path 428" transform="translate(0 2.955)"/>
        </g>
    </g>
</g>
</svg>
`
