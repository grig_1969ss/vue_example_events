import { Quill } from 'vue2-editor'

const BlockEmbed = Quill.import('blots/block/embed')

/* reassignment of standart video embed */
export class VideoResponsive extends BlockEmbed {
  static blotName = 'video'
  static tagName = 'P'
  static className = 'video-responsive'

  static create(value) {
    const node = super.create(value)

    const child = document.createElement('iframe')
    child.setAttribute('frameborder', '0')
    child.setAttribute('allowfullscreen', true)
    child.setAttribute('src', value)
    child.classList.add('video-responsive__inner')

    node.appendChild(child)

    return node
  }

  static value(domNode) {
    const iframe = domNode.querySelector('iframe')
    return iframe.getAttribute('src')
  }
}
