import Thumbor from 'thumbor-js-url-builder'
const thumbor = new Thumbor(process.env.thumborKey, process.env.thumborUrl)

/**
 *  @param { String } url
 *  @param { String } formattingType
 *  @param { String, Number } width
 *  @param { String, Number } height
 */
const thumborize = (url, formattingType, width = 600, height = 400) => {
  let result = thumbor.setImagePath(url)
  if (formattingType) result = result.filter(`format(${formattingType})`)
  if (width || height) result = result.resize(width, height)
  return result.buildUrl()
}

// Use this to fit image with no known sizes to fit inside exactly ExF
export const thumborFit = (url, size, type) => {
  return (
    thumbor
      .setImagePath(url)
      // imaginary box of “E” width and “F” height
      .fitIn(size.width, size.height)
      .filter(`format(${type})`)
      .buildUrl()
  )
}

export default thumborize
