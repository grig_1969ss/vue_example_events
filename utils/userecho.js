import { i18n } from '~/plugins/i18n'

const userechoConf = {
  __dangerouslyDisableSanitizersByTagID: {
    'userecho-conf': ['innerHTML'],
  },
  script: [
    {
      hid: 'userecho-conf',
      body: true,
      innerHTML: `
      var _ues = {
        host: 'supportwhatwhereworld.userecho.com',
        forum: '3',
        lang: '${i18n.locale}',
        tab_show: false,
        tab_icon_show: false,
        tab_corner_radius: 0,
        tab_font_size: 25,
        tab_image_hash: '',
        tab_alignment: 'right',
        tab_text_color: '#ffffff',
        tab_text_shadow_color: '#00000055',
        tab_bg_color: '#0070ff',
        tab_hover_color: '#005acc',
        chat_tab_text_show: false,
        chat_tab_show: true,
        chat_tab_icon_url: 'https://supportwhatwhereworld.userecho.com/s/attachments/26391/0/1/1b8c14a37e831356c0f943e68a484e6d.png',
      
        custom_css: {
          ".ueLabel": {
            "width": "20px !important",
          },
          "#ueButton": {
            "width": "64px",
            "text-align": "center",
            "padding-top": "10px",
            "padding-bottom": "10px",
            "border": "0",
          },
          "#ueChatButton": {
            "width": "64px",
            "text-align": "center",
            "padding-top": "21px",
            "padding-bottom": "17px",
            "margin-bottom": "10px",
            "border": "0",
          },
        },
      };`,
    },
    {
      src: 'https://cdn.userecho.com/js/widget-1.4.gz.js',
      body: true,
      async: true,
    },
  ]
}

export default process.env.enableSupportChat ? userechoConf: {}
