export const social_vk = {
  url: 'https://oauth.vk.com/authorize',
  params(path, x) {
    return {
      client_id: process.env.vkId,
      redirect_uri: process.env.domainUrl + '/verify',
      scope: ['email'],
      response_type: 'code',
      state: `{st=vk-oauth2,h=${x},path=${path}}`,
    }
  },
}

export const social_fb = {
  url: 'https://www.facebook.com/v3.1/dialog/oauth',
  params(path, x) {
    return {
      client_id: process.env.fbId,
      redirect_uri: process.env.domainUrl + '/verify',
      scope: ['public_profile', 'email'],
      state: `{st=facebook,h=${x},path=${path}}`,
    }
  },
}

export const social_google = {
  url: 'https://accounts.google.com/o/oauth2/auth',
  params(path, x) {
    return {
      client_id: process.env.googleId,
      redirect_uri: process.env.domainUrl + '/verify',
      scope: 'profile email',
      response_type: 'code',
      state: `{st=google-oauth2,h=${x},path=${path}}`,
    }
  },
}

export function serializer(params) {
  return Object.entries(params).reduce((res, [current_key, current_value]) => {
    return `${res}${res ? '&' : ''}${current_key}=${current_value}`
  }, '')
}
