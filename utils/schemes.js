import { prop } from 'ramda'
import { makeDateTimeString, formatStrings } from '~/utils/formatDate'
import { seatsTypes } from '~/constants/seats'

export function formatTotalTicketsData({ schemes }) {
  if (!schemes) return null

  return schemes.results.reduce(
    (result, scheme) => {
      const { tickets_price } = scheme
      const total_income = prop('total_income', scheme) || 0
      const tickets_sold = prop('tickets_sold', scheme) || 0
      const tickets_total = prop('tickets_total', scheme) || 0
      const tickets_used = prop('tickets_used', scheme) || 0

      result.allTicketsSold += tickets_sold
      result.allTicketsTotal += tickets_total
      result.allTicketsUsed += tickets_used

      const { symbol } = tickets_price.currency
      const hasCurrency = Object.prototype.hasOwnProperty.call(
        result.priceTable,
        symbol,
      )

      if (hasCurrency) {
        result.priceTable[symbol] += total_income
      } else {
        result.priceTable[symbol] = total_income
      }

      return result
    },
    {
      allTicketsSold: 0,
      allTicketsTotal: 0,
      allTicketsUsed: 0,
      priceTable: {},
    },
  )
}

export function formatTicketTableRows({ schemes }) {
  if (!schemes) return null

  return schemes.map(
    ({
      title,
      tickets_total,
      tickets_sold,
      total_income,
      tickets_used,
      id,
      tickets_price,
    }) => {
      const { symbol } = tickets_price.currency

      return {
        title,
        tickets_total: tickets_total || 0,
        tickets_sold: tickets_sold || 0,
        tickets_used: tickets_used || 0,
        total_income: total_income ? `${total_income} ${symbol}` : 0,
        id,
      }
    },
  )
}

export function formatTicketsPrice({ amount, currency }) {
  return `${amount} ${currency.symbol}`
}

export function schemesByType({ schemes }) {
  return schemes.reduce(
    (res, scheme) => {
      if (scheme.seats_type === seatsTypes.withoutSeat) {
        res.withoutSeats.push(scheme)
      } else if (scheme.seats_type === seatsTypes.withSeat) {
        res.withSeats.push(scheme)
      }
      return res
    },
    {
      withoutSeats: [],
      withSeats: [],
    },
  )
}
