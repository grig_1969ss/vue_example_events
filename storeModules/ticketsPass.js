import { path } from 'ramda'
import { getTicketsSchemes, getTickets } from '~/api'

export const namespaced = true

export const state = () => ({
  schemes: null,
  tickets: null,
})

export const getters = {
  schemes: state => state.schemes,
  tickets: state => state.tickets,
  ticketsResults: state => path(['tickets', 'results'], state) || [],
}

export const actions = {
  async getSchemes({ dispatch, rootGetters }) {
    const session = rootGetters['events/session/data']

    try {
      const { data } = await getTicketsSchemes(this.$axios, {
        params: {
          session: session.id,
        },
      })

      dispatch('setSchemes', { schemes: data })
    } catch (e) {
      console.log(e)
    }
  },
  async getTickets({ dispatch, rootGetters }) {
    const session = rootGetters['events/session/data']

    try {
      const { data } = await getTickets(this.$axios, {
        params: {
          session: session.id,
          only_paid: 'true',
          expand: 'seat,order,scheme',
        },
      })

      dispatch('setTickets', { tickets: data })
    } catch (e) {
      console.log(e)
    }
  },
  setSchemes({ commit }, { schemes }) {
    commit('setSchemes', schemes)
  },
  setTickets({ commit }, { tickets }) {
    commit('setTickets', tickets)
  },
}

export const mutations = {
  setSchemes(state, v) {
    state.schemes = v
  },
  setTickets(state, v) {
    state.tickets = v
  },
}

export default {
  namespaced,
  state,
  getters,
  actions,
  mutations,
}
