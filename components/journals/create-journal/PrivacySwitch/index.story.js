import { storiesOf } from '@storybook/vue'
import PrivacySwitch from '.'

storiesOf('Journal/CreateJournal/PrivacySwitch', module).add(
  'default',
  () => ({
    components: { PrivacySwitch },
    data: () => ({
      defaultTrue: true,
      defaultFalse: false,
    }),
    template: `
      <div style="
          background-color: #0063e0; 
          padding: 24px; 
          display: grid; 
          grid-gap: 24px;
      ">
          <span style="color: white; text-transform: uppercase">
            Переключатель видимости журнала
          </span>
          <PrivacySwitch v-model="defaultTrue" />   
          <PrivacySwitch v-model="defaultFalse" />           
      </div>`,
  }),
  { info: true },
)
