import { storiesOf } from '@storybook/vue'
import CoverUploader from '.'

storiesOf('Journal/CreateJournal/CoverUploader', module)
  .add(
    'default',
    () => ({
      components: { CoverUploader },
      data: () => ({ image: null }),
      template: `<div style="width: 500px; background-color: #0063e0; padding: 24px">
            <CoverUploader v-model="image" />
        </div>`,
    }),
    { info: true },
  )
  .add(
    'withImage',
    () => ({
      components: { CoverUploader },
      data: () => ({
        image: {
          id: 568019,
          image: '8806ed599d764399af444ad23e43c577',
          main_color: '#01b9df',
          original_height: 690,
          original_width: 1102,
        },
      }),
      template: `<div style="width: 500px; background-color: #0063e0; padding: 24px">
            <CoverUploader v-model="image" />
        </div>`,
    }),
    { info: true },
  )
