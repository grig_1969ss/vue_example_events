import { storiesOf } from '@storybook/vue'
import EventListWidget from '~/components/widgets/EventListWidget'
import { event, dataEvents } from '~/.storybook/lib/dataEvents'

storiesOf('widgets/EventListWidget', module)
  .add(
    'default',
    () => ({
      components: { EventListWidget },
      data: () => ({
        events: [dataEvents.eventIsPromo, event, dataEvents.eventIsPromo],
      }),
      template: `<div>
        <EventListWidget :events="events" />
        <EventListWidget currentVersion="2" :events="events" />
      </div>`,
    }),
    { info: true },
  )
  .add(
    'isVertical',
    () => ({
      components: { EventListWidget },
      data: () => ({
        events: [dataEvents.eventIsPromo, event, dataEvents.eventIsPromo],
      }),
      template: `<div style="display: flex;">
        <EventListWidget isVertical @infinite="onInfinite" :events="events" />
        <EventListWidget currentVersion="2" isVertical :events="events" />
      </div>`,
    }),
    { info: true },
  )
