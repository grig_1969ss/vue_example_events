import { storiesOf } from '@storybook/vue'
import EventListWidgetSnob from '~/components/widgets/EventListWidgetSnob'
import { event, dataEvents } from '~/.storybook/lib/dataEvents'

storiesOf('widgets/EventListWidgetSnob', module).add(
  'default',
  () => ({
    components: { EventListWidgetSnob },
    data: () => ({
      events: [
        dataEvents.eventIsPromo,
        event,
        dataEvents.eventIsPromo,
        dataEvents.eventIsPromo,
        event,
        dataEvents.eventIsPromo,
        dataEvents.eventIsPromo,
        event,
      ],
    }),
    template: `<div>
        <EventListWidgetSnob :events="events" />
      </div>`,
  }),
  { info: true },
)
