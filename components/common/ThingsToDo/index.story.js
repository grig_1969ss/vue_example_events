import { storiesOf } from '@storybook/vue'
import data from '~/.storybook/lib/thingsToDo'

storiesOf('Common/ThingsToDo/', module).add(
  'default',
  () => ({
    components: {
      ThingsToDo: () => import('.'),
    },
    data: () => ({
      data: [...data, ...data, ...data, ...data, ...data, ...data],
    }),
    template: `
    <div style="padding: 24px; background-color: #f8f9fa">
      <ThingsToDo :city="{}" :data="data" />  
    </div>`,
  }),
  { info: true },
)
