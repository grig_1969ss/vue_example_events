import { storiesOf } from '@storybook/vue'

storiesOf('common/AdminPanel', module).add(
  'default',
  () => ({
    components: { AdminPanel: () => import('.') },
    data: () => ({
      model: {
        manual_moderation_status: 0,
        is_top: false,
      },
    }),
    template: `
    <div style="padding:64px 120px;">
      Full 
      <AdminPanel :value="model" @input="model = { ...model, ...$event }" /> 
      <br><br>
      {{ model }}
    </div>`,
  }),
  { info: true },
)
