import { storiesOf } from '@storybook/vue'

import Logo from '.'

import images from '~/.storybook/lib/images'

storiesOf('common/Logo', module).add(
  'default',
  () => ({
    components: { Logo },
    data: () => ({ images }),
    template: `<div style="padding: 24px; background: #f3f3f3;">
      <Logo :image="images[0].image" />
      <Logo :image="images[1].image" alt="Mohn Wick" border />
      <Logo alt="w w" size="64"/>
      <Logo alt="i i" />
      <Logo image="404image" />
    </div>`,
  }),
  { info: true },
)
