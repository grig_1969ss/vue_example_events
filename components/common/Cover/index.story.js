import { storiesOf } from '@storybook/vue'
import Cover from '.'
import Badge from '~/components/common/Badge'
import Icon from '~/components/common/Icon'
import { dataEvents } from '~/.storybook/lib/dataEvents.js'

storiesOf('Common/Cover', module).add(
  'default',
  () => ({
    components: { Cover, Icon, Badge },
    data: () => ({
      cover404: { image: 'https://google.com', main_color: '#DDD' },
      eventIsPromo: dataEvents.eventIsPromo,
      eventIsPromoNoColors: dataEvents.eventIsPromoNoColors,
      eventImageVertical: dataEvents.eventImageVertical,
      eventImageNoSizes: dataEvents.eventImageNoSizes,
      badgeStyle: {
        'background-color': '#00ca0c',
        color: 'white',
      },
    }),
    template: `
    <div style="padding: 24px; display: grid; grid-template-columns: 3; justify-content: start; grid-gap: 8px">
      <Cover :cover="{ main_color: '#DDD' }" style="height: 300px; width: 300px;" />
      <Cover :cover="{ image: 'https://google.com' }" style="height: 300px; width: 300px;" />
      <Cover :cover="eventIsPromoNoColors.cover" style="height: 200px; width: 200px" />
      <Cover :cover="eventIsPromo.cover" style="height: 300px; width: 300px;" />
      <Cover :cover="eventImageVertical.cover" style="height: 300px; width: 300px;" />
      <Cover :cover="eventImageNoSizes.cover" style="height: 300px; width: 300px;">
      </Cover>
    </div>
    `,
  }),
  { info: true },
)
