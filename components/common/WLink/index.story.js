import { storiesOf } from '@storybook/vue'

import WLink from '~/components/common/WLink'

storiesOf('Common/WLink', module)
  .add(
    'default',
    () => ({
      components: { WLink },
      template: `
      <div>
        <WLink to="/">link :to</WLink><br>
        <WLink href="https://www.google.com/">link :href</WLink><br>
        <WLink href="https://www.google.com/" target="_blank">link :href _blank</WLink><br>
      </div>`,
    }),
    { info: {} },
  )
  .add(
    'color',
    () => ({
      components: { WLink },
      template: `
      <div>
        <WLink>default</WLink><br>
        <WLink color="light-blue">light-blue</WLink><br>
        <WLink color="blue">blue</WLink><br>
        <WLink color="black-blue">black-blue</WLink><br>
        <WLink color="white">white</WLink><br>
        <WLink color="black">black</WLink><br>
      </div>`,
    }),
    { info: {} },
  )
  .add(
    'display',
    () => ({
      components: { WLink },
      template: `
      <div>
        <WLink inlineBlock>inlineBlock</WLink>
        <WLink inlineBlock>inlineBlock</WLink>
        <WLink block>block</WLink>
        <WLink block>block</WLink>
      </div>`,
    }),
    { info: {} },
  )
