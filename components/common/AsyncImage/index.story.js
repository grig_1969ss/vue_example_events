import { storiesOf } from '@storybook/vue'
import AsyncImage from '.'
import images from '~/.storybook/lib/images'

storiesOf('Common/AsyncImage/', module)
  .add(
    'default',
    () => ({
      components: { AsyncImage },
      data: () => ({ image: images[2], images }),
      template: `
    <div style="
    display: grid; 
    grid-template-columns: repeat(4, 300px);
    grid-template-rows: repeat(4, 100px);
    grid-gap: 16px;
    padding: 24px; 
    background-color: #f8f9fa;
    color: white;
    font-weight: 800;">
      <AsyncImage 
      :image="image.image" 
      height="200" 
      width="300" 
      contain>
      300x200 contain
      </AsyncImage>
      <AsyncImage :image="image.image" width="200">w=200 cover</AsyncImage>
      <AsyncImage height="200" contain>contain no image</AsyncImage>
      <AsyncImage height="200">cover no image</AsyncImage>
    </div>
    `,
    }),
    { info: true },
  )
  .add(
    'contain / cover',
    () => ({
      components: { AsyncImage },
      data: () => ({ image: images[2] }),
      template: `
    <div style="
    display: grid; 
    grid-template-columns: 250px 1fr 1fr;
    justify-items: center;
    align-items: center;
    grid-gap: 24px;
    padding: 24px; 
    background-color: #f8f9fa">
      <h2>contain</h2>
      <div style="width: 250px; height: 100px; border: 1px;">
         <AsyncImage :image="image.image" height="400" contain />
      </div>
      <div style="width: 250px; height: 100px">
         <AsyncImage :image="image.image" width="400" contain/>
      </div>
      
      <h2>cover</h2>
      <div style="width: 250px; height: 100px">
         <AsyncImage :image="image.image" height="400"  />
      </div>
      <div style="width: 250px; height: 100px">
         <AsyncImage :image="image.image" width="400" />
      </div>
    </div>
    `,
    }),
    { info: true },
  )
