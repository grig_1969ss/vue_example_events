import { storiesOf } from '@storybook/vue'
import FormRow from '~/components/common/FormRowNew'
import FormInput from '~/components/common/input/FormInput'

storiesOf('Common/FormRowNew', module).add(
  'default',
  () => ({
    components: { FormRow, FormInput },
    data: () => ({
      model: '',
    }),
    template: `
    <div style="padding: 16px; background: #f8f9fa; display: grid; grid-gap: 16px;">
      <FormRow id="customId">
        <template #label>Название события</template>
        <template slot-scope="{ id, hasError }">
          <FormInput :id="id" :error="hasError" :value="model" placeholder="placeholder" />
        </template>
      </FormRow>
      <FormRow>
        <template #label>Название события</template>
        <template slot-scope="{ id, hasError }">
          <FormInput :id="id" :error="hasError" :value="model" placeholder="placeholder" />
        </template>
      </FormRow>
      <FormRow error="{}">
        <template #label>Название события</template>
        <template slot-scope="{ id, hasError }">
          <FormInput :id="id" :error="hasError" :value="model" placeholder="placeholder" />
        </template>
      </FormRow>
    </div>`,
  }),
  { info: true },
)
