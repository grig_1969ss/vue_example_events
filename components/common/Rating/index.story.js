import { storiesOf } from '@storybook/vue'
import Rating from '~/components/common/Rating'

storiesOf('Common/Rating', module).add(
  'default',
  () => ({
    components: { Rating },
    template: `<div style="display: grid;">
      <Rating value="4"/>
      <Rating value="3.5"/>
      <Rating value="4.9"/>
    </div>`,
  }),
  { info: true },
)
