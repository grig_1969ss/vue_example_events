import { storiesOf } from '@storybook/vue'
import { dateTimes } from '~/.storybook/lib/dateTimes'
import { event, dataEvents } from '~/.storybook/lib/dataEvents'

import SelectableEventCard from '~/components/common/SelectableEventCard'

storiesOf('Common/SelectableEventCard', module)
  .add(
    'default',
    () => ({
      components: { SelectableEventCard },
      data: () => dataEvents,
      template: `
      <div style="
        padding: 24px; 
        display: grid; 
        grid-gap: 40px;      
        grid-template-columns: repeat(auto-fill, 400px);
      ">
        <SelectableEventCard :event="event" />
        <SelectableEventCard :event="eventOverflowTest" />
      </div>`,
    }),
    { info: true },
  )
  .add(
    'datetimes test',
    () => ({
      components: { SelectableEventCard },
      data: () => ({
        events: dateTimes.map(({ start, end, desc }) => ({
          ...event,
          title: desc,
          start_dt: start,
          end_dt: end,
        })),
      }),
      template: `
      <div style="
        padding: 24px; 
        display: grid; 
        grid-gap: 40px;      
        grid-template-columns: repeat(auto-fill, 400px);
      ">
        <SelectableEventCard 
          v-for="event in events" 
          :event="event"
          :key="event.title"
        />
      </div>`,
    }),
    { info: true },
  )
