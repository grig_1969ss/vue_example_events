import { storiesOf } from '@storybook/vue'

import UserCard from '~/components/common/UserCard'
import users from '~/.storybook/lib/users'

storiesOf('Common/UserCard', module).add(
  'default',
  () => ({
    components: { UserCard },
    data() {
      return { users: users }
    },
    template: `
      <div style="
          background-color: #f8f9fa;
          padding: 24px; 
          display: grid;
          grid-gap: 16px;      
          grid-template-columns: repeat(auto-fill, 336px);
        ">
        <UserCard :user="users[0]" />
        <UserCard 
          v-for="user in users" 
          :user="user" 
          :key="user.id"
          style="background-color: white;"
        />
      </div>
`,
  }),
  { info: true },
)
