import { storiesOf } from '@storybook/vue'

import Badge from '~/components/common/Badge'
import Icon from '~/components/common/Icon'

storiesOf('Common/Badge/', module).add(
  'default',
  () => ({
    components: {
      Badge,
      Icon,
    },
    data: () => ({}),
    template: `
    <div style="display: flex; flex-direction: column;     align-items: flex-start;">
        <Badge 
          :title="$t('common.moderation.approved')" 
        >
          <Icon slot="badge" src="icons/check-round.svg" size="s" />
          {{ $t('common.moderation.approved') }}
        </Badge>
        <Badge 
          :title="$t('common.moderation.rejected')" 
        >
          <Icon slot="badge" src="icons/uncheck-round.svg" size="s" />
          {{ $t('common.moderation.rejected') }}
        </Badge>
        <Badge 
          :title="$t('common.moderation.approved')" 
        >
          <Icon slot="badge" src="icons/check-round.svg" size="s" />
        </Badge>
        <Badge 
          :title="$t('common.moderation.rejected')" 
        >
          <Icon slot="badge" src="icons/uncheck-round.svg" size="s" />
        </Badge>
    </div>`,
  }),
  { info: true },
)
