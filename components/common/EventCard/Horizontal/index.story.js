import { storiesOf } from '@storybook/vue'
import { event } from '~/.storybook/lib/dataEvents'
import EventCardHorizontal from '.'

storiesOf('Common/EventCard/Horizontal', module)
  .add(
    'default',
    () => ({
      components: { EventCardHorizontal },
      data: () => ({ event }),
      template: `
      <div style="background-color: #f8f9fa; padding: 16px;">
        <EventCardHorizontal :event="event" />
      </div>`,
    }),
    { info: true },
  )
  .add(
    'menu',
    () => ({
      components: { EventCardHorizontal },
      data: () => ({ event }),
      template: `
      <div style="background-color: #f8f9fa; padding: 16px;">
        <EventCardHorizontal :event="event" is-own />
      </div>`,
    }),
    { info: true },
  )
