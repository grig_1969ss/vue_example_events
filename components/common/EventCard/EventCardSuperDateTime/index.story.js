import { storiesOf } from '@storybook/vue'
import sessions from '~/.storybook/lib/sessions'
import EventCardDateTime from '.'

storiesOf('Common/EventCard/EventCardSuperDateTime', module).add(
  'default',
  () => ({
    components: { EventCardDateTime },
    data: () => ({ sessions }),
    template: `
      <div style="padding: 24px; max-width: 264px;">
         <EventCardDateTime :sessions="sessions.slice(0,1)" />
         <hr/>
         <EventCardDateTime :sessions="sessions" />
      </div>`,
  }),
  { info: {} },
)
