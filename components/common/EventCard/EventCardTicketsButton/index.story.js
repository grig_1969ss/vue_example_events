import { storiesOf } from '@storybook/vue'

import { dataEvents, event } from '~/.storybook/lib/dataEvents'
import EventCardTicketsButton from '.'

storiesOf('Common/EventCard/EventCardTicketsButton', module).add(
  'default',
  () => ({
    components: { EventCardTicketsButton },
    data: () => dataEvents,
    template: `
      <div style="padding: 24px 168px;">
        <EventCardTicketsButton :session="event">from $1000</EventCardTicketsButton>
      </div>`,
  }),
  { info: true },
)
