import { storiesOf } from '@storybook/vue'

import { dataEvents, event } from '~/.storybook/lib/dataEvents'
import EventCardStub from '.'

storiesOf('Common/EventCard/EventCardStub', module).add(
  'default',
  () => ({
    components: { EventCardStub },
    template: `
      <div style="
        background-color: #f8f9fa;
        padding: 24px; 
        display: grid; 
        grid-gap: 16px;
        grid-template-columns: repeat(3, minmax(248px, 296px));  
      ">
        <EventCardStub />
        <EventCardStub to="/events/create/a/1/" />
        <EventCardStub to="/events/create/a/1/">
          <span style="position: absolute; top: 50%; left: 35%;">Some body</span>
        </EventCardStub>
      </div>`,
  }),
  { info: true },
)
