import { storiesOf } from '@storybook/vue'

import { dataEvents } from '~/.storybook/lib/dataEvents'
import EventCardV2 from '~/components/common/EventCard/V2'

// <EventCard :event="eventNoMainColor"/>
// <EventCard :event="eventLightColor"/>
// <EventCard :event="eventIsPromo"/>
// <EventCard :event="eventIsPromoNoColors"/>
// <EventCard :event="eventWithRating"/>
// <EventCard :event="eventWithPrice"/>
// <EventCard :event="eventOverflowTest"/>

storiesOf('Common/EventCard/V2', module).add(
  'default',
  () => ({
    components: { EventCardV2 },
    data: () => dataEvents,
    template: `
      <div style="
        padding: 24px 24px 72px; 
        display: grid; 
        grid-gap: 40px;      
        grid-template-columns: repeat(auto-fill, 296px);
        background: #f3f3f3;
      ">
        <EventCardV2 :event="event"/>
        <EventCardV2 :event="eventOverflowTest"/>
        <EventCardV2 :event="eventIsPromo"/>
        <EventCardV2 :event="eventLightColor"/>
        <EventCardV2 :event="eventImageNoDim"/>
        <EventCardV2 :event="eventImageVertical"/>
        <EventCardV2 :event="eventImageHorizontal"/>
        <EventCardV2 :event="eventNoInstitution"/>
      </div>`,
  }),
  { info: true },
)
