import { storiesOf } from '@storybook/vue'

import { dataEvents } from '~/.storybook/lib/dataEvents'
import EventCardSnob from '~/components/common/EventCard/Snob'

storiesOf('Common/EventCard/Snob', module).add(
  'default',
  () => ({
    components: { EventCardSnob },
    data: () => dataEvents,
    template: `
      <div style="
        padding: 24px 24px 72px; 
        display: grid; 
        grid-gap: 40px;      
        grid-template-columns: repeat(auto-fill, 320px);
        background: #f3f3f3;
      ">
        <EventCardSnob :event="event"/>
        <EventCardSnob :event="eventOverflowTest"/>
        <EventCardSnob :event="eventIsPromo"/>
        <EventCardSnob :event="eventLightColor"/>
        <EventCardSnob :event="eventImageNoDim"/>
        <EventCardSnob :event="eventImageVertical"/>
        <EventCardSnob :event="eventImageHorizontal"/>
        <EventCardSnob :event="eventNoInstitution"/>
      </div>`,
  }),
  { info: true },
)
