import { storiesOf } from '@storybook/vue'
import EventCardDateTime from '.'

const dateTimes = [
  {
    title: 'one day',
    start: new Date().setDate(new Date().getDate() + 1),
    end: new Date().setDate(new Date().getDate() + 2),
  },
  {
    title: 'period',
    start: new Date().setDate(new Date().getDate() + 1),
    end: new Date().setDate(new Date().getDate() + 5),
  },
  {
    title: 'event is over:',
    start: new Date().setDate(new Date().getDate() - 2),
    end: new Date().setDate(new Date().getDate() - 1),
  },
  {
    title: 'next year:',
    start: new Date().setDate(new Date().getDate() + 365),
    end: new Date().setDate(new Date().getDate() + 366),
  },
]

storiesOf('Common/EventCard/EventCardDateTime', module).add(
  'default',
  () => ({
    components: { EventCardDateTime },
    data: () => ({
      dateTimes,
    }),
    template: `
      <div style="padding: 24px;">
        <div v-for="(dt, index) in dateTimes" :key="index" style="display: grid; grid-template-columns: 150px auto; grid-gap: 24px;">
         {{ dt.title }}
         <EventCardDateTime :start="dt.start" :end="dt.end" />
        </div>
      </div>`,
  }),
  { info: {} },
)
