import { storiesOf } from '@storybook/vue'

import { dateTimes } from '~/.storybook/lib/dateTimes'
import { dataEvents, event } from '~/.storybook/lib/dataEvents'
import EventCard from '.'
import Icon from '~/components/common/buttons/BtnWithIcon'

storiesOf('Common/EventCard/old', module)
  .add(
    'default',
    () => ({
      components: { EventCard },
      data: () => dataEvents,
      template: `
      <div style="
        padding: 24px; 
        display: grid; 
        grid-gap: 40px;      
        grid-template-columns: repeat(auto-fill, 296px);
      ">
        <EventCard :event="event"/>
        <EventCard :event="eventNoMainColor"/>
        <EventCard :event="eventLightColor"/>
        <EventCard :event="eventIsPromo"/>
        <EventCard :event="eventIsPromoNoColors"/>
        <EventCard :event="eventWithRating"/>
        <EventCard :event="eventWithPrice"/>
        <EventCard :event="eventOverflowTest"/>
        <EventCard :event="{ ...event, cover: { image: '404' }, title: 'image404' }"/>
      </div>`,
    }),
    { info: true },
  )
  .add(
    'cover ratio',
    () => ({
      components: { EventCard },
      data: () => dataEvents,
      template: `
      <div>
        <div style="
          padding: 24px; 
          display: grid; 
          grid-gap: 40px;      
          grid-template-columns: repeat(auto-fill, 296px);
        ">
          <EventCard :event="eventImageNoDim"/>
          <EventCard :event="eventImageVertical"/>
          <EventCard :event="eventImageHorizontal"/>
        </div>
        EventCard smaller
        <div style="
          padding: 24px; 
          display: grid; 
          grid-gap: 40px;      
          grid-template-columns: repeat(auto-fill, 296px);
        ">
          <EventCard smaller :event="eventImageNoDim"/>
          <EventCard smaller :event="eventImageVertical"/>
          <EventCard smaller :event="eventImageHorizontal"/>
        </div>
      </div>
      `,
    }),
    { info: true },
  )
  .add(
    'smaller',
    () => ({
      components: { EventCard },
      data: () => dataEvents,
      template: `
      <div style="
        padding: 24px; 
        display: grid; 
        grid-gap: 40px;      
        grid-template-columns: repeat(auto-fill, 296px);
      ">
        <EventCard smaller :event="event"/>
        <EventCard smaller :event="eventNoMainColor"/>
        <EventCard smaller :event="eventIsPromo"/>
        <EventCard smaller :event="eventIsPromoNoColors"/>
        <EventCard smaller :event="eventWithRating"/>
        <EventCard smaller :event="eventWithPrice"/>
        <EventCard smaller :event="eventOverflowTest"/>
      </div>`,
    }),
    { info: true },
  )
  .add(
    'isOwn',
    () => ({
      components: { EventCard },
      data: () => ({
        ...dataEvents,
      }),
      template: `
      <div style="
        padding: 24px; 
        display: grid; 
        grid-gap: 40px;      
        grid-template-columns: repeat(auto-fill, 296px);
      ">
        <EventCard isOwn :event="event"/>
        <EventCard isOwn canEdit :event="event"/>
        <EventCard isOwn canEdit :event="eventWithPrice" />
        <EventCard isOwn canEdit :event="eventIsPromo" />
      </div>`,
    }),
    { info: true },
  )
  .add(
    'isExternalLink',
    () => ({
      components: { EventCard },
      data: () => ({
        event,
      }),
      template: `
      <div style="
        padding: 24px; 
        display: grid; 
        grid-gap: 40px;      
        grid-template-columns: repeat(auto-fill, 296px);
      ">
        <EventCard isExternalLink :event="event"/>
      </div>`,
    }),
    { info: true },
  )
  .add(
    'hoverColor',
    () => ({
      components: {
        EventCard,
      },
      data: () => dataEvents,
      template: `
      <div style="
        padding: 24px; 
        display: grid; 
        grid-gap: 40px;      
        grid-template-columns: repeat(auto-fill, 296px);
      ">
        <EventCard hoverColor="green" :event="event"/>
        <EventCard hoverColor="green" :event="eventIsPromo"/>
      </div>`,
    }),
    { info: true },
  )
  .add(
    'datetimes test',
    () => ({
      components: {
        EventCard,
      },
      data: () => ({
        events: dateTimes.map(({ start, end, desc }) => ({
          ...event,
          title: desc,
          start_dt: start,
          end_dt: end,
        })),
      }),
      template: `
      <div>
        <div style="
          padding: 24px; 
          display: grid; 
          grid-gap: 40px;      
          grid-template-columns: repeat(auto-fill, 296px);
        ">
          <EventCard 
            v-for="event in events" 
            :key="event.title"
            :event="event" 
          />
        </div>
        <div style="
          padding: 24px; 
          display: grid; 
          grid-gap: 40px;      
          grid-template-columns: repeat(auto-fill, 296px);
        ">
          <EventCard 
            v-for="event in events" 
            smaller
            :key="event.title"
            :event="event" 
          />
        </div>
      </div>`,
    }),
    { info: true },
  )
  .add(
    'price test',
    () => ({
      components: {
        EventCard,
      },
      data: () => ({
        noPrice: {
          ...event,
          title: 'min_price null, max_price null',
          min_price: null,
          max_price: null,
          currency: 'руб',
        },
        nanStringPrice: {
          ...event,
          title: "min_price 'String' max_price 'String'",
          min_price: 'String',
          max_price: 'String',
          currency: 'руб',
        },
        zeroStringPrice: {
          ...event,
          title: "min_price '0.00' max_price '0.00'",
          min_price: '0.00',
          max_price: '0.00',
          currency: 'руб',
        },
        fromPrice: {
          ...event,
          title: 'min_price 1, max_price null',
          min_price: 1,
          max_price: null,
          currency: 'руб',
        },
        upToPrice: {
          ...event,
          title: 'min_price null, max_price 2',
          min_price: null,
          max_price: 2,
          currency: 'руб',
        },
        gapPrice: {
          ...event,
          title: 'min_price 1, max_price 2',
          min_price: 1,
          max_price: 2,
          currency: 'руб',
        },
        gapEqualPrice: {
          ...event,
          title: 'min_price 1, max_price 1',
          min_price: 1,
          max_price: 1,
          currency: 'руб',
        },
      }),
      template: `
      <div style="
        padding: 24px; 
        display: grid; 
        grid-gap: 40px;
        grid-template-columns: repeat(auto-fill, 296px);
      ">
        <EventCard showOverlay :event="noPrice"/>
        <EventCard showOverlay :event="zeroStringPrice"/>
        <EventCard showOverlay :event="nanStringPrice"/>
        <EventCard showOverlay :event="fromPrice"/>
        <EventCard showOverlay :event="upToPrice"/>
        <EventCard showOverlay :event="gapPrice"/>
        <EventCard showOverlay :event="gapEqualPrice"/>
      </div>`,
    }),
    { info: true },
  )
  .add(
    'showOverlay',
    () => ({
      components: {
        EventCard,
      },
      data: () => dataEvents,
      template: `
      <div style="
        padding: 24px; 
        display: grid; 
        grid-gap: 40px;
        grid-template-columns: repeat(auto-fill, 296px);
      ">
        <EventCard showOverlay :event="event"/>
        <EventCard showOverlay :event="eventIsPromo"/>
      </div>`,
    }),
    { info: true },
  )
  .add(
    'hideButtons',
    () => ({
      components: {
        EventCard,
      },
      data: () => dataEvents,
      template: `
      <div style="
        padding: 24px; 
        display: grid; 
        grid-gap: 40px;
        grid-template-columns: repeat(auto-fill, 296px);
      ">
        <EventCard showOverlay hideButtons :event="event"/>
        <EventCard showOverlay hideButtons :event="eventIsPromo"/>
        <EventCard isOwn showOverlay hideButtons :event="eventIsPromo"/>
      </div>`,
    }),
    { info: true },
  )
  .add(
    'slot bottomButtons',
    () => ({
      components: {
        EventCard,
        Icon,
      },
      data: () => ({
        event,
      }),
      template: `
        <div style="padding: 24px;">
          <EventCard :event="event">
            <template #bottomButtons>
              <Icon
                icon="pencil"
              />
            </template>
          </EventCard>
        </div>`,
    }),
    { info: true },
  )
  .add(
    'slot topButtons',
    () => ({
      components: {
        EventCard,
        Icon,
      },
      data: () => ({
        event,
      }),
      template: `
        <div style="padding: 24px;">
          <EventCard :event="event">
            <template #topButtons>
              <Icon
                icon="pencil"
              />
            </template>
          </EventCard>
        </div>`,
    }),
    { info: true },
  )
  .add(
    'moderated',
    () => ({
      components: {
        EventCard,
        Icon,
      },
      data: () => ({
        event,
      }),
      template: `
        <div style="display: grid; grid-auto-flow: column; grid-gap: 24px; justify-content: start; padding: 24px;">
          <EventCard :event="{ ...event, manual_moderation_status: 0 }"/>
          <EventCard :event="{ ...event, manual_moderation_status: 1 }"/>
          <EventCard :event="{ ...event, manual_moderation_status: 2 }"/>
        </div>`,
    }),
    { info: true },
  )
