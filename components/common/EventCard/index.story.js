import { storiesOf } from '@storybook/vue'
import sessions from '~/.storybook/lib/sessions'
import { dataEvents, event } from '~/.storybook/lib/dataEvents'
import EventCard from '.'

storiesOf('Common/EventCard', module)
  .add(
    'default',
    () => ({
      components: { EventCard },
      data: () => ({ sessions }),
      template: `
      <div style="
        background-color: #f8f9fa;
        padding: 16px; 
        display: grid; 
        grid-gap: 8px;
        grid-template-columns: repeat(2, minmax(160px, 296px));  
      ">
        <EventCard v-for="session in sessions" :value="session" is-own />
      </div>`,
    }),
    { info: true },
  )
  .add(
    'super',
    () => ({
      components: { EventCard },
      data: () => ({ sessions }),
      template: `
      <div style="
        background-color: #f8f9fa;
        padding: 16px; 
        display: grid; 
        grid-gap: 8px;
        grid-template-columns: repeat(2, minmax(160px, 296px));  
      ">
        <EventCard :value="sessions.slice(1)" is-own />
      </div>`,
    }),
    { info: true },
  )
// .add(
//   'dates',
//   () => ({
//     components: { EventCard },
//     data: () => ({
//       dataEvents,
//       dateTimes: [
//         {
//           title: 'one day',
//           start_dt: new Date().setDate(new Date().getDate() + 1),
//           end_dt: new Date().setDate(new Date().getDate() + 2),
//         },
//         {
//           title: 'period',
//           start_dt: new Date().setDate(new Date().getDate() + 1),
//           end_dt: new Date().setDate(new Date().getDate() + 5),
//         },
//         {
//           title: 'event is over:',
//           start_dt: new Date().setDate(new Date().getDate() - 2),
//           end_dt: new Date().setDate(new Date().getDate() - 1),
//         },
//         {
//           title: 'next year:',
//           start_dt: new Date().setDate(new Date().getDate() + 365),
//           end_dt: new Date().setDate(new Date().getDate() + 366),
//         },
//         {
//           title: '24 hour',
//           start_dt: new Date().setDate(new Date().getDate() - 1),
//           end_dt: '2019-09-17T23:58:59-03:00',
//         },
//       ],
//     }),
//     template: `
//     <div style="
//       background-color: #f8f9fa;
//       padding: 24px;
//       display: grid;
//       grid-gap: 16px;
//       grid-template-columns: repeat(3, minmax(248px, 296px));
//     ">
//       <EventCard
//         v-for="(d, index) in dateTimes"
//         :key="index"
//         :value="{ ...dataEvents.eventWithPrice, ...d, manual_moderation_status: 2}"
//       />
//     </div>`,
//   }),
//   { info: true },
// )
// .add(
//   'mobile',
//   () => ({
//     components: { EventCard },
//     data: () => dataEvents,
//     template: `
//     <div style="
//       background-color: #f8f9fa;
//       padding: 24px;
//       display: grid;
//       grid-gap: 16px;
//       grid-template-columns: repeat(3, 160px);
//     ">
//       <EventCard :value="{ ...event, manual_moderation_status: 2 }" />
//       <EventCard :value="eventImageVertical"/>
//       <EventCard :value="{ ...eventNoMainColor, manual_moderation_status: 0 }" />
//       <EventCard :value="eventLightColor"/>
//       <EventCard :value="{ ...eventIsPromo, manual_moderation_status: 2 }"/>
//       <EventCard :value="eventIsPromoNoColors"/>
//       <EventCard :value="eventWithRating"/>
//       <EventCard :value="eventWithPrice"/>
//       <EventCard :value="eventOverflowTest"/>
//       <EventCard :value="{ ...event, cover: { image: '404' }, title: 'image404' }"/>
//     </div>`,
//   }),
//   { info: true },
// )
