import { storiesOf } from '@storybook/vue'

import Loader from '.'

storiesOf('Common/Loader', module).add(
  'default',
  () => ({
    components: { Loader },
    template: `
      <div style="
          background-color: #f8f9fa;
          padding: 24px; 
          display: flex;
          flex-direction: row;
        ">
        <Loader />
        <Loader color="#f50052"/>
      </div>
`,
  }),
  { info: true },
)
