import { storiesOf } from '@storybook/vue'

import OrganizerCard from '.'
import organizers from '~/.storybook/lib/organizers'

storiesOf('Common/OrganizerCard', module).add(
  'default',
  () => ({
    components: { OrganizerCard },
    data: () => ({ organizers }),
    template: `
      <div style="
          background-color: #f8f9fa;
          padding: 24px; 
          display: grid;
          grid-gap: 16px;      
          grid-template-columns: repeat(auto-fill, 336px);
        ">
        <OrganizerCard 
          v-for="organizer in organizers" 
          :organizer="organizer" 
          :key="organizer.id"
        />
      </div>
`,
  }),
  { info: true },
)
