import { storiesOf } from '@storybook/vue'
import Avatar from '.'
import users from '~/.storybook/lib/users'

const sizes = [32, 40, 48, 64, 120, 240]

storiesOf('Common/Avatar/', module)
  .add(
    'default',
    () => ({
      components: { Avatar },
      data: () => ({
        user: users[0],
        sizes,
      }),
      template: `
    <div style="display: flex; align-items: baseline; padding: 24px; background-color: #f8f9fa">
      <div v-for="size in sizes" :key="size">
        <Avatar :user="user" :size="size" :title="'size=' + size"/>
      </div>
    </div>
    `,
    }),
    { info: true },
  )
  .add(
    'empty',
    () => ({
      components: { Avatar },
      data: () => ({
        user: users[0],
        sizes,
      }),
      template: `
    <div style="display: flex; align-items: baseline; padding: 24px; background-color: #f8f9fa">
      <div v-for="size in sizes" :key="size">
        <Avatar :size="size" :title="'size=' + size"/>
      </div>
    </div>
    `,
    }),
    { info: true },
  )
  .add(
    'hidden',
    () => ({
      components: { Avatar },
      data: () => ({
        hiddenUser: {
          id: null,
        },
        sizes,
      }),
      template: `
    <div style="display: flex; align-items: baseline; padding: 24px; background-color: #f8f9fa">
      <div v-for="size in sizes" :key="size">
        <Avatar :user="hiddenUser" :size="size" :title="'size=' + size"/>
      </div>
    </div>
    `,
    }),
    { info: true },
  )
