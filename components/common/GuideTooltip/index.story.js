import { storiesOf } from '@storybook/vue'
import GuideTooltip from '~/components/common/GuideTooltip'
import BtnWithIcon from '~/components/common/buttons/BtnWithIcon'

storiesOf('Common/GuideTooltip', module).add(
  'default',
  () => ({
    components: { GuideTooltip, BtnWithIcon },
    template: `<div style="padding: 24px">
      <GuideTooltip>
        <BtnWithIcon icon="add"/>
        <template #tooltip>My tooltip</template>
      </GuideTooltip>
    </div>`,
  }),
  { info: true },
)
