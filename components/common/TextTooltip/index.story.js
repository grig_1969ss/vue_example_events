import { storiesOf } from '@storybook/vue'
import TextTooltip from '~/components/common/TextTooltip'
import BtnWithIcon from '~/components/common/buttons/BtnWithIcon'

storiesOf('Common/TextTooltip', module).add(
  'default',
  () => ({
    components: { TextTooltip, BtnWithIcon },
    template: `<div style="padding: 24px">
      <TextTooltip>
        <BtnWithIcon icon="add"/>
        <template #tooltip>My tooltip</template>
      </TextTooltip>
    </div>`,
  }),
  { info: true },
)
