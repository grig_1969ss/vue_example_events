import { storiesOf } from '@storybook/vue'
import SearchBar from '.'
import SelectCity from '~/components/common/input/SelectCity'
import SelectPeriod from './SelectPeriod'
import * as cities from '~/store/cities'
import * as lang from '~/store/lang'
import Vuex from 'vuex'

const store = new Vuex.Store({
  modules: { cities, lang },
})

storiesOf('common/SearchBar', module)
  .add(
    'default',
    () => ({
      store,
      components: { SearchBar },
      data: () => ({
        search: 'выставки',
        city: { id: 524901, name: 'Нижний Новгород' },
        period: '',
        dates: [],
      }),
      template: `
    <div style="background-color: #f8f9fa; padding: 24px;">
      <SearchBar 
        :search.sync="search"
        :city.sync="city"
        :period.sync="period" 
        :dates.sync="dates"
        @search="this.console.log('click search event')"
      />
      <br>
      <ul>
        <li> search: {{ search }} </li>
        <li> city: {{ city }} </li>
        <li> period: {{ period }} </li>
        <li> dates: {{ dates }} </li>
      </ul>
    </div>
    `,
    }),
    { info: true },
  )
  .add(
    'dates',
    () => ({
      store,
      components: { SearchBar },
      data: () => ({
        search: 'выставки',
        city: { id: 524901, name: 'Нижний Новгород' },
        period: '',
        dates: [new Date()],
      }),
      template: `
    <div style="
      padding: 24px 16px; 
      background-color: #f8f9fa">
      <SearchBar 
        :search.sync="search"
        :city.sync="city"
        :period.sync="period" 
        :dates.sync="dates"
        @search="this.console.log('user click button!')"
      />
      <br>
      <ul>
        <li> search: {{ search }} </li>
        <li> city: {{ city }} </li>
        <li> period: {{ period }} </li>
        <li> dates: {{ dates }} </li>
      </ul>
    </div>
    `,
    }),
    { info: true },
  )
  .add(
    'without period',
    () => ({
      store,
      components: { SearchBar },
      data: () => ({
        search: 'выставки',
        city: { id: 524901, name: 'Нижний Новгород' },
      }),
      template: `
    <div style="
      padding: 24px 16px; 
      background-color: #f8f9fa">
      <SearchBar 
        :search.sync="search"
        :city.sync="city"
        withoutPeriod
        @search="this.console.log('user click button!')"
      />
      <br>
      <ul>
        <li> search: {{ search }} </li>
        <li> city: {{ city }} </li>
      </ul>
    </div>
    `,
    }),
    { info: true },
  )

storiesOf('common/SearchBar/', module).add(
  'SelectCity',
  () => ({
    store,
    components: { SelectCity },
    data: () => ({ city: { id: 524901, name: 'Москва' } }),
    template: `
    <div style="
      padding: 24px; 
      background-color: #f8f9fa">
      <SelectCity v-model="city" style="height: 64px;" />
      <br>
      city: {{ city }}
    </div>
    `,
  }),
  { info: true },
)

storiesOf('common/SearchBar/', module).add(
  'SelectPeriod',
  () => ({
    store,
    components: { SelectPeriod },
    data: () => ({
      period: {},
      options: [
        { value: '', text: 'anyDate' },
        { value: 'this-week', text: 'week' },
        { value: 'this-month', text: 'month' },
        { value: 'weekends', text: 'weekend' },
        { value: 'dates', text: 'selectDates' },
      ],
    }),
    template: `
    <div style="
      padding: 24px; 
      background-color: #f8f9fa">
      <SelectPeriod :value="period" :options="options" style="height: 64px;" @input="v => period = v.value" />
      <br/>
      period: {{ period }}
    </div>
    `,
  }),
  { info: true },
)
