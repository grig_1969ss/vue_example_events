import { storiesOf } from '@storybook/vue'
import HorizontalEventCardVariant2 from '~/components/common/HorizontalEventCard'
import { event } from '~/.storybook/lib/dataEvents'
import BtnWithIcon from '~/components/common/buttons/BtnWithIcon'

storiesOf('Common/HorizontalEventCard', module).add(
  'default',
  () => ({
    components: { HorizontalEventCardVariant2, BtnWithIcon },
    data: () => ({
      event,
    }),
    template: `<div>
      <HorizontalEventCardVariant2 :event="event"/>
      <HorizontalEventCardVariant2 :event="event">
        <template #topButtons>
          <BtnWithIcon icon="add" />
        </template>
      </HorizontalEventCardVariant2>
    </div>`,
  }),
  { info: true },
)
