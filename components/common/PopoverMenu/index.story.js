import { storiesOf } from '@storybook/vue'
import PopoverMenu from './index.vue'

storiesOf('Common/PopoverMenu', module).add('PopoverMenu', () => ({
  components: { PopoverMenu },
  template: `
    <div style="position: relative; width: 256px; padding: 1rem;">
      <PopoverMenu
        name="popoverStory"
        :pos="{ left: 0 }"
        style="background-color: #aaa"
      >
        <div slot="face">
          Custom Label or image or whatever (click here)
        </div>
        <div slot="content" style="background-color: #333; padding: 1rem; color: #fff;">
          <ul>
            <li><a href="#">kek</a></li>
            <li><a href="#">123313</a></li>
          </ul>
        </div>
      </PopoverMenu>
    </div>
    `,
}))
