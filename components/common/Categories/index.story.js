import { storiesOf } from '@storybook/vue'
import { filtered } from '~/.storybook/lib/categories'

storiesOf('Common/Categories/Checkbox', module)
  .add(
    'default',
    () => ({
      components: { Checkbox: () => import('./Checkbox') },
      data: () => ({ selected: [], options: filtered }),
      template: `
      <div style="display: flex; flex-direction: row; align-items: flex-start;">
        <ul>
          <li v-for="option in options.slice(0, 6)" :key="option.id">
            <Checkbox 
              v-model="selected"
              trackBy="id"
              label="title"
              :option="option"
            />
          </li>
        </ul>
        {{selected}}
      </div>
      `,
    }),
    { info: true },
  )
  .add(
    'slot',
    () => ({
      components: { Checkbox: () => import('./Checkbox') },
      data: () => ({ selected: [], options }),
      template: `
      <div style="display: flex; flex-direction: row; align-items: flex-start;">
        <ul>
          <li v-for="option in options.slice(0, 6)" :key="option.id">
            <Checkbox 
              v-model="selected"
              trackBy="id"
              :option="option"
            >
              <span>{{option.title}}</span>
              <template #checked>
                <span>{{option.title}}+</span>
              </template>
            </Checkbox>
          </li>
        </ul>
      </div>
      `,
    }),
    { info: true },
  )

storiesOf('Common/Categories/List', module).add(
  'default',
  () => ({
    components: { List: () => import('./List') },
    data: () => ({ selected: [options[0]], options }),
    template: `
      <div style="background-color: #0070ff; padding: 24px">
        <List v-model="selected" :options="options.slice(0, 6)" />
        {{selected}}
      </div>
      `,
  }),
  { info: true },
)
