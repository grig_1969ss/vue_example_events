import { storiesOf } from '@storybook/vue'

storiesOf('common/ReadMore', module).add(
  'default',
  () => ({
    components: { ReadMore: () => import('.') },
    template: `
      <div style="
        padding: 24px;
        display: grid;
        grid-auto-flow: column;
        grid-gap: 24px;
        background-color: #f8f9fa;
        max-width: 856px"
      >
        <ReadMore maxHeight="250" color="#FAA422">
          With TheNextWomen Female Founder Force Program women invest in women. Our program is designed for early-stage startups to gain exposure, valuable feedback and meaningful connections. Over 200 women-led startups apply, pitch, and train to become this year’s number 1. The last finalists Printr, Vastari, Plugify, Endeer and MT Art aised between €500K and €1M funding within 9 months after winning the competition. With this 7th edition of our sourcing program, we're supporting female entrepreneurship worldwide, in collaboration with a variety of Embassies of the Kingdom of the Netherlands, EY and StartupAmsterdam. #FemaleFoundersForce2019 calls for all ambitious women-led startups ready to grow their business to the next level. Who can apply? 1. At least one of the company's founders is female 2. Your business(plan) made >€20.000 last year 3. Your startup is between 1 and 3 years old 4. Your business has at least 2 FTE 5. You are looking for funding
          read more
          With TheNextWomen Female Founder Force Program women invest in women. Our program is designed for early-stage startups to gain exposure, valuable feedback and meaningful connections. Over 200 women-led startups
        </ReadMore>
        <ReadMore>
          With TheNextWomen Female Founder Force Program women invest in women. 
        </ReadMore>
      </div>`,
  }),
  { info: true },
)
