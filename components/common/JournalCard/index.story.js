import { storiesOf } from '@storybook/vue'

import journals from '~/.storybook/lib/journals'
import JournalCard from '.'

storiesOf('Common/JournalCard', module)
  .add(
    'default',
    () => ({
      components: { JournalCard },
      data: () => ({ journals }),
      template: `
      <div style="
        background-color: #f8f9fa;
        display: grid;
        grid-template-columns: repeat(auto-fill, minmax(328px, 408px));
        grid-gap: 24px;
        padding: 16px;
      ">
        <JournalCard v-for="journal in journals.results" :key="journal.id" :journal="journal" />
      </div>`,
    }),
    { info: true },
  )
  .add(
    'menu',
    () => ({
      components: { JournalCard },
      data: () => ({ journals }),
      template: `
      <div style="
        background-color: #f8f9fa;
        display: grid;
        grid-template-columns: repeat(auto-fill, minmax(328px, 408px));
        grid-gap: 24px;
        padding: 16px;
      ">
        <JournalCard v-for="journal in journals.results" :key="journal.id" :journal="journal" is-own />
      </div>`,
    }),
    { info: true },
  )
