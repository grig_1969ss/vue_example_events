import { storiesOf } from '@storybook/vue'
import Checkbox from '~/components/common/Checkbox'

storiesOf('Common/Checkbox', module)
  .add(
    'default',
    () => ({
      components: { Checkbox },
      data: () => ({
        model1: false,
        model2: true,
      }),
      template: `
    <div style="display: flex; flex-direction: column; align-items: flex-start;">
      <Checkbox :value="model1" @input="model1 = !model1">
        Даю согласие на обработку своих персональных данных
      </Checkbox>
      <Checkbox :value="model2" @input="model2 = !model2">
        Даю согласие на обработку своих персональных данных
      </Checkbox>
    </div>
    `,
    }),
    { info: true },
  )
  .add(
    'color',
    () => ({
      components: { Checkbox },
      data: () => ({
        model1: true,
        model2: true,
        model3: true,
        model4: true,
      }),
      template: `
      <div style="display: grid; grid-gap: 16px;">
        <div style="display: flex; flex-direction: column; align-items: flex-start; background: black; padding: 16px">
          <Checkbox color="white" :value="model2" @input="model2 = !model2">
            Даю согласие на обработку своих персональных данных
          </Checkbox>
        </div>

        <div style="display: flex; flex-direction: column; align-items: flex-start; padding: 16px">
          <Checkbox border :value="model1" @input="model1 = !model1">
            Даю согласие на обработку своих персональных данных
          </Checkbox><br>
          <Checkbox color="red" :value="model3" @input="model3 = !model3">
            Даю согласие на обработку своих персональных данных
          </Checkbox>
          <Checkbox color="blue" :value="model4" @input="model4 = !model4">
            Даю согласие на обработку своих персональных данных
          </Checkbox>
        </div>
      </div>
      `,
    }),
    { info: true },
  )
  .add(
    'border',
    () => ({
      components: { Checkbox },
      data: () => ({
        model1: false,
        model2: true,
      }),
      template: `
      <div style="display: grid; grid-template-columns: auto 1fr; grid-gap: 24px; padding: 24px;">
        <Checkbox v-model="model1" border> text </Checkbox>
        &lt;Checkbox v-model="model1" border&gt; text &lt;Checkbox/&gt;
        <Checkbox v-model="model2" border="dark-gray"> text </Checkbox>
        &lt;Checkbox v-model="model2" border="dark-gray"&gt; text &lt;Checkbox/&gt;
        <Checkbox v-model="model2" border="red"> text </Checkbox>
        &lt;Checkbox v-model="model2" border="red"&gt; text &lt;Checkbox/&gt;
      </div>
      `,
    }),
    { info: true },
  )
