import { storiesOf } from '@storybook/vue'
import StatString from '~/components/common/StatString'
import Icon from '~/components/common/Icon'

storiesOf('Common/StatString', module).add(
  'default',
  () => ({
    components: { StatString, Icon },
    template: `
    <div>
      <StatString>
        <template #icon>
          <Icon src="icons/book.svg" size="xs" />
        </template>
        <template #text>Продано</template>
        <template #left>10</template>
        <template #right>20</template>
      </StatString>

      <StatString>
        <template #icon>
          <Icon src="icons/book.svg" size="xs" />
        </template>
        <template #text>Продано</template>
        <template #left>10</template>
      </StatString>

      <StatString variant="blue" hasHover>
        <template #icon>
          <Icon src="icons/book.svg" size="xs" />
        </template>
        <template #text>Продано</template>
        <template #left>10</template>
        <template #right>20</template>
      </StatString>
    </div>`,
  }),
  { info: true },
)
