import { storiesOf } from '@storybook/vue'
import Breadcrumbs from '.'
import BredItem from './Item'

storiesOf('Common/Breadcrumbs', module)
  .add(
    'default',
    () => ({
      components: { Breadcrumbs, BredItem },
      template: `
      <div style="padding: 24px">
        <Breadcrumbs>
          <BredItem href="/">Первая</BredItem>
          <BredItem href="#">Вторая</BredItem>
          <BredItem>Неактивная</BredItem>
          <BredItem href="#">Последняя</BredItem>
        </Breadcrumbs>
      </div>
      `,
    }),
    { info: true },
  )
  .add(
    'centered',
    () => ({
      components: { Breadcrumbs, BredItem },
      template: `
      <div style="padding: 24px">
        <Breadcrumbs centered>
          <BredItem href="/">Первая</BredItem>
          <BredItem href="#">Вторая</BredItem>
          <BredItem>Неактивная</BredItem>
          <BredItem href="#">Последняя</BredItem>
        </Breadcrumbs>
      </div>
      `,
    }),
    { info: true },
  )
