import { storiesOf } from '@storybook/vue'

import QrCode from '~/components/common/QrCode'

storiesOf('Common/QrCode', module).add(
  'default',
  () => ({
    components: { QrCode },
    template: `
<div style="padding: 24px;">
  <p><a href="https://www.npmjs.com/package/qrcode#qr-code-options" target="_blank">All options</a></p>
  <p>
    <QrCode string="http://whatwhere.world/organizers/321310-bolshoi-theatre/events/" />
  </p>
  <p>
    <QrCode string="http://whatwhere.world/organizers/321310-bolshoi-theatre/events/" :config="{ margin: 4 }" />
  </p>
  <p>
    <QrCode string="http://whatwhere.world/organizers/321310-bolshoi-theatre/events/" :config="{ width: 200 }" />
  </p>
  <p>
    <QrCode string="http://whatwhere.world/organizers/321310-bolshoi-theatre/events/" :config="{ color: { dark: '#ff0000', light: '#eee' } }" />
  </p>
</div>`,
  }),
  { info: true },
)
