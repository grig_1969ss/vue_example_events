import { storiesOf } from '@storybook/vue'
import { grayBackground } from '~/.storybook/decorators'
import DatePicker from 'vue2-datepicker'
import Vue from 'vue'

import DatepickerPeriod from '.'

Vue.use(DatePicker)

storiesOf('common/input/DatePickerPeriod', module)
  // .addDecorator(grayBackground)
  .add(
    'default',
    () => ({
      components: { DatepickerPeriod },
      data: () => ({
        from: '07.05.2019',
        to: '',
      }),
      template: `
      <div style="
      padding: 24px; 
      background-color: #f8f9fa">
        <DatepickerPeriod :from.sync="from" :to.sync="to" />
        from: {{ from }} to: {{ to }}
      </div>`,
    }),
    { info: {} },
  )
  .add(
    'limit',
    () => ({
      components: { DatepickerPeriod },
      data: () => ({
        from: '07.05.2019',
        to: '',
        fromLimit: '07.05.2019',
        toLimit: '14.06.2019',
      }),
      template: `
        <div style="
        padding: 24px; 
        background-color: #f8f9fa">
          <DatepickerPeriod 
            :from.sync="from" 
            :to.sync="to" 
            :fromLimit="fromLimit"
            :toLimit="toLimit"
            :monthDiff="0"
          />
          from: {{ from }} to: {{ to }}
        </div>`,
    }),
    { info: {} },
  )
