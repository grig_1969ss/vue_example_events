import { storiesOf } from '@storybook/vue'
import WSwitch from '~/components/common/input/WSwitch'

storiesOf('Common/WSwitch', module).add(
  'default',
  () => ({
    components: { WSwitch },
    data: () => ({
      model1: false,
      model2: true,
    }),
    template: `
    <div>
      <WSwitch :value="model1" @input="model1 = !model1" />
      <WSwitch :value="model2" @input="model2 = !model2"  />
    </div>
    `,
  }),
  { info: true },
)
