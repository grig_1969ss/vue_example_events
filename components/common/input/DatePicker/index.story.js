import { storiesOf } from '@storybook/vue'
import DatePicker from 'vue2-datepicker'
import Vue from 'vue'

import Datepicker from '.'

Vue.use(DatePicker)

storiesOf('common/input/DatePicker', module)
  // .addDecorator(grayBackground)
  .add(
    'default',
    () => ({
      components: { Datepicker },
      data: () => ({ date: '' }),
      template: `
      <div style="
      padding: 24px; 
      background-color: #f8f9fa">
        <Datepicker  v-model="date" />
        date: {{ date }}
      </div>`,
    }),
    { info: {} },
  )
