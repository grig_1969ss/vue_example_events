import { storiesOf } from '@storybook/vue'

import FormInput from '~/components/common/input/FormInput'

storiesOf('Common/FormInput', module).add(
  'default',
  () => ({
    components: { FormInput },
    data: () => ({
      model: 'Some text',
      model2: '',
      model3:
        'Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text Some text',
    }),
    template: `
    <div style="display: grid; grid-gap: 16px; padding: 16px; background: #f8f9fa;">
    input 
      <FormInput 
        v-model="model" 
        placeholder="Например, фестиваль енотов" 
      />
      long input
      <FormInput 
        v-model="model3" 
        placeholder="Например, фестиваль енотов" 
      />
      placeholder
      <FormInput 
        v-model="model2" 
        placeholder="Например, фестиваль енотов"
      />
      disabled
      <FormInput
        disabled
        v-model="model" 
        placeholder="Например, фестиваль енотов" 
      />
      error
      <FormInput
        error
        v-model="model" 
        placeholder="Например, фестиваль енотов" 
      />
    </div>`,
  }),
  { info: true },
)
