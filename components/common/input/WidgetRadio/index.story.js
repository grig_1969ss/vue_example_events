import { storiesOf } from '@storybook/vue'
import WidgetRadio from '~/components/common/input/WidgetRadio'

storiesOf('Common/WidgetRadio', module).add(
  'default',
  () => ({
    components: { WidgetRadio },
    data: () => ({
      items: [
        {
          image: '',
          title: 'Вертикальный',
          text: 'Три карточки событий с возможностью прокрутки',
          value: 'vertical',
        },
        {
          image: '',
          title: 'Горизонтальный',
          text: 'Три карточки событий с возможностью прокрутки',
          value: 'horizontal',
        },
        {
          image: '',
          title: 'Карта',
          text: 'Карта города с отмеченными пинами событиями',
          value: 'map',
        },
        {
          image: '',
          title: 'Поиск',
          text: 'Виджет с поисковой строкой, выбором даты и города',
          value: 'search',
        },
      ],
      value: 'vertical',
    }),
    template: `
    <div style="background: #0070ff; padding: 24px;">
      <WidgetRadio :items="items" v-model="value" />
    </div>`,
  }),
  { info: true },
)
