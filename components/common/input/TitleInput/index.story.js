import { storiesOf } from '@storybook/vue'
import Input from '.'

storiesOf('common/input/TitleInput', module).add(
  'default',
  () => ({
    components: { Input },
    data: () => ({
      emptyText: null,
      text: 'something text',
    }),
    template: `
      <div style="
          max-width: 400px;
          padding: 24px; 
          display: grid; 
          
          grid-gap: 24px;
      ">  
          <Input style="border: solid 1px black; height: 64px"
              v-model="emptyText"
              placeholder="placeholder"
          />    
          <Input 
              v-model="text"
              error="something error"
              placeholder="placeholder"
              style="height: 80px"
          >
            Label
          </Input>          
      </div>`,
  }),
  { info: true },
)
