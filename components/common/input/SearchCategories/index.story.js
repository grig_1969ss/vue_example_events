import { storiesOf } from '@storybook/vue'
import SearchCategories from './index'

storiesOf('search', module).add(
  'SearchCategories',
  () => ({
    components: { SearchCategories },
    data: () => ({
      model: [],
      options: [
        { id: 294, slug: 'exhibitions', title: 'Выставки', total_events: 177 },
        { id: 305, slug: 'children', title: 'Детские', total_events: 162 },
        { id: 306, slug: 'concerts', title: 'Концерты', total_events: 46 },
        { id: 313, slug: 'performances', title: 'Спектакли', total_events: 17 },
        { id: 317, slug: 'holidays', title: 'Праздники', total_events: 2 },
        {
          id: 321,
          slug: 'museums',
          title: 'Нечитаемая длинная запутывающая юзера категория',
          total_events: 11,
        },
      ],
    }),
    template: `
      <div style="padding: 24px; display: flex; flex-direction: column; background-color: #f8f9fa">
        <SearchCategories 
          v-model="model" 
          :options="options" 
          trackBy="slug" 
          label="title"
        />
        <br>
        model = {{ model }}
      </div>`,
  }),
  { info: true },
)
