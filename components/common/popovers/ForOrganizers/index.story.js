import { storiesOf } from '@storybook/vue'
import * as auth from '~/store/auth'
import * as popovers from '~/store/popovers'
import Vuex from 'vuex'
import { action } from '@storybook/addon-actions'

const store = new Vuex.Store({
  modules: { auth, popovers },
})

storiesOf('popovers/ForOrganizers', module).add(
  'Form',
  () => ({
    store,
    components: { ForOrganizers: () => import('./Form') },
    methods: {
      log(e) {
        action('emit event')(e)
      },
    },
    template: `
    <div style="padding: 24px; display: flex; justify-content: center;">
      <ForOrganizers @close="log('close')" @success="log('success')"/>
    </div>
    `,
  }),
  { info: true },
)
