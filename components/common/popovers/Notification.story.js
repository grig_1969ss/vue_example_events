import { storiesOf } from '@storybook/vue'
import Notification from './Notification.vue'
import * as notifications from '~/store/notifications'
import Vuex from 'vuex'

const store = new Vuex.Store({
  modules: { notifications },
})

storiesOf('popovers/Notification', module).add(
  'default',
  () => ({
    store,
    components: { Notification },
    data: () => ({
      message: 'Connection error. Please try again later',
    }),
    methods: {},
    template: `
    <div style="padding: 24px; background-color: #f8f9fa">
      <Notification />
      <input v-model="message" style="margin-bottom: 16px"/>
      <button 
        @click="$store.dispatch('notifications/error', message)" 
        style="background-color: red"
      >
        error
      </button>
      <button 
        @click="$store.dispatch('notifications/success', message)" 
        style="background-color: green; color: white"
      >
        success
      </button>
    </div>
    `,
  }),
  { info: true },
)
