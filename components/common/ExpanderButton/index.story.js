import { storiesOf } from '@storybook/vue'
import ExpanderButton from '~/components/common/ExpanderButton'

storiesOf('Common/ExpanderButton', module).add(
  'default',
  () => ({
    components: { ExpanderButton },
    data: () => ({
      isOpen: false,
    }),
    template: `
    <div style="padding: 40px;">
      <ExpanderButton :isOpen="isOpen" @toggle="isOpen = !isOpen" />
    </div>
    `,
  }),
  { info: true },
)
