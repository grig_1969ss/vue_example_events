import { storiesOf } from '@storybook/vue'
import EventCardStub from '~/components/common/EventCardStub'

storiesOf('common/EventCardStub', module).add(
  'default',
  () => ({
    components: { EventCardStub },
    template: `
    <div style="padding: 16px;">
      <EventCardStub to="/">hello</EventCardStub>
    </div>`,
  }),
  { info: true },
)
