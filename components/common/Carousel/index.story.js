import { storiesOf } from '@storybook/vue'
import Carousel from '.'
import images from '~/.storybook/lib/images'

storiesOf('Common/Carousel', module)
  .addDecorator(() => ({
    template: `
    <div style="
      padding: 24px 112px; 
      max-width: 1180px; 
      background-color: #7f828b"
    >
      <story/>
    </div>`,
  }))
  .add(
    'default',
    () => ({
      components: { Carousel },
      data: () => ({
        images: images,
      }),
      template: `
    <div>
      <Carousel :images="images" :width="0" :height="200"/>
    </div>
    `,
    }),
    { info: true },
  )
  .add(
    'editable',
    () => ({
      components: { Carousel },
      data: () => ({
        images: images,
      }),
      template: `
    <div>
      <Carousel :images.sync="images" :width="0" :height="200" edit drag/>
    </div>
    `,
    }),
    { info: true },
  )
