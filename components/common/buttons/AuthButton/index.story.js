import { storiesOf } from '@storybook/vue'
import AuthButton from '.'

storiesOf('Common/Buttons/', module).add(
  'AuthButton',
  () => ({
    components: { AuthButton },
    data: () => ({
      variants: ['fb', 'vk', 'google'],
    }),
    template: `
    <div style="padding: 24px; background-color: #e6eaed">
      64px border
      <ul style="display: grid; grid-template-columns: repeat(auto-fill, 64px); grid-gap: 16px;">
        <li v-for="variant in variants">
          <AuthButton :variant="variant" border />
        </li>
      </ul>
      40px border
      <ul style="display: grid; grid-template-columns: repeat(auto-fill, 40px); grid-gap: 16px;">
        <li v-for="variant in variants">
          <AuthButton :variant="variant" small border />
        </li>
      </ul>
      40px
      <ul style="display: grid; grid-template-columns: repeat(auto-fill, 40px); grid-gap: 16px;">
        <li v-for="variant in variants">
          <AuthButton :variant="variant" small />
        </li>
      </ul>
    </div>
    `,
  }),
  { info: true },
)
