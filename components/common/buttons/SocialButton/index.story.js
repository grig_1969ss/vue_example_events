import { storiesOf } from '@storybook/vue'
import SocialButton from '.'

storiesOf('Common/Buttons/', module).add(
  'SocialButton',
  () => ({
    components: { SocialButton },
    data: () => ({
      variants: [
        'fb',
        'vk',
        'tw',
        'tg',
        'email',
        'instagram',
        'linkedin',
      ]
    }),
    template: `
    <div style="padding: 24px;">
      Round mini
      <ul style="display: flex;">
        <li v-for="variant in variants">
          <SocialButton :variant="variant" href="https://google.com" round mini />
        </li>
      </ul>
      32px
      <ul style="display: flex;">
        <li v-for="variant in variants">
          <SocialButton :variant="variant" href="https://google.com" mini />
        </li>
      </ul>
      40px
      <ul style="display: flex;">
        <li v-for="variant in variants">
          <SocialButton :variant="variant" href="https://google.com" small />
        </li>
      </ul>
      48px
      <ul style="display: flex;">
        <li v-for="variant in variants">
          <SocialButton :variant="variant" href="https://google.com" />
        </li>
      </ul>
      72px
      <ul style="display: flex;">
        <li v-for="variant in variants">
          <SocialButton :variant="variant" href="https://google.com" large />
        </li>
      </ul>
    </div>
    `,
  }),
  { info: true },
)
