import { storiesOf } from '@storybook/vue'
import YetAnotherButton from './'

storiesOf('Common/Buttons/', module).add(
  'YetAnotherButton',
  () => ({
    components: { YetAnotherButton },
    data: () => ({
      colors: [
        null,
        '#f50052',
        '#f8f9fa',
        '#a067df',
        '#feb02e',
        '#000000',
        '#ffffff',
      ],
      variants: [
        {},
        { invert: true },
        { fullWidth: true },
        { invert: true, fullWidth: true },
        { disabled: true },
        { disabled: true, invert: true },
        { disabled: true, fullWidth: true },
      ],
    }),
    template: `
      <div style="max-width: 512px;">
        <div
          v-for="color in colors"
          :key="color"
          style="padding: 24px 24px 1px;"
          :style="{ background: color === '#ffffff' ? '#f8f9fa' : null }"
        >
          <h4 style="margin: 0 0 12px;">Color: {{ color || 'default' }}</h4>
          <div
            v-for="variant in variants"
            :key="Object.keys(variant).join(', ') || 'default'"
            style="margin: 0 0 24px 24px;"
          >
            <h5 style="margin: 0 0 12px">Variant: {{ Object.keys(variant).join(', ') || 'default' }}</h5>
            <YetAnotherButton :color="color" v-bind="variant">Button text</YetAnotherButton>
          </div>
        </div>
      </div>
`,
  }),
  { info: true },
)
