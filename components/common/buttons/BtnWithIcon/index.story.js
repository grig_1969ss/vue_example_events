import { storiesOf } from '@storybook/vue'
// import { action } from '@storybook/addon-actions'
import BtnWithIcon from '~/components/common/buttons/BtnWithIcon'
import Icon from '~/components/common/Icon'

storiesOf('Common/Buttons/BtnWithIcon', module)
  .add(
    'default',
    () => ({
      components: { BtnWithIcon },
      template: `
      <div style="display: flex">
        <BtnWithIcon @click="onClick" icon="pencil" />
        <BtnWithIcon @click="onClick" icon="eye" />
        <BtnWithIcon @click="onClick" icon="trash" />
        <BtnWithIcon @click="onClick" icon="add" />
        <BtnWithIcon @click="onClick" icon="check" />
      </div>`,
      methods: {
        onClick() {
          console.log('button-click')
          // action('button-click')
          // https://github.com/storybooks/storybook/issues/6153
        },
      },
    }),
    { info: true },
  )
  .add(
    'slot',
    () => ({
      components: { BtnWithIcon, Icon },
      template: `
      <div style="display: flex">
        <BtnWithIcon inlineColor="#F44336" invert>
          <Icon src="arrow-down-v2.svg" size="l" />
        </BtnWithIcon>
      </div>`,
    }),
    { info: true },
  )
  .add(
    'invert',
    () => ({
      components: { BtnWithIcon },
      template: `
        <div style="display: flex">
          <BtnWithIcon invert icon="pencil" />
          <BtnWithIcon invert icon="eye" />
          <BtnWithIcon invert icon="trash" />
          <BtnWithIcon invert icon="add" />
          <BtnWithIcon invert icon="check" />
        </div>`,
    }),
    { info: true },
  )
  .add(
    'color',
    () => ({
      components: { BtnWithIcon },
      template: `
        <div style="display: flex">
          <BtnWithIcon icon="pencil" />
          <BtnWithIcon invert icon="pencil" />
          <BtnWithIcon color="blue" icon="pencil" />
          <BtnWithIcon color="blue" invert icon="pencil" />
          <BtnWithIcon color="red" icon="pencil" />
          <BtnWithIcon color="red" invert icon="pencil" />
          <BtnWithIcon color="blue-transparent" icon="pencil" />
          <BtnWithIcon color="blue-transparent" invert icon="pencil" />
        </div>`,
    }),
    { info: true },
  )
  .add(
    'size',
    () => ({
      components: { BtnWithIcon },
      template: `
        <div style="display: grid; grid-gap: 8px;">
          <BtnWithIcon icon="pencil"/>
          <BtnWithIcon icon="pencil" size="medium"/>
          <BtnWithIcon icon="pencil" size="small"/>
          <BtnWithIcon icon="pencil" size="tiny"/>
        </div>`,
    }),
    { info: true },
  )
  .add(
    'inlineColor',
    () => ({
      components: { BtnWithIcon },
      template: `
        <div style="display: grid; grid-gap: 8px;">
          <BtnWithIcon icon="pencil" inlineColor="#008000"/>
          <BtnWithIcon icon="pencil" inlineColor="#008000" invert/>
        </div>`,
    }),
    { info: true },
  )
