import { storiesOf } from '@storybook/vue'
import FlatButton from '~/components/common/buttons/FlatButton'

storiesOf('Common/Buttons/FlatButton', module)
  .add(
    'default',
    () => ({
      components: { FlatButton },
      template: `<div>
      <FlatButton to="/">FlatButton</FlatButton>
      <FlatButton to="/" fullWidth>FlatButton</FlatButton>
      <FlatButton to="/" round>FlatButton</FlatButton>
      <FlatButton to="/" variant="dark-blue">FlatButton</FlatButton>
    </div>`,
    }),
    { info: true },
  )
  .add(
    'size',
    () => ({
      components: { FlatButton },
      template: `<div>
        <FlatButton to="/" size="l">FlatButton</FlatButton>
        <FlatButton to="/" size="m">FlatButton</FlatButton>
      </div>`,
    }),
    { info: true },
  )
  .add(
    'type',
    () => ({
      components: { FlatButton },
      template: `<div>
        <FlatButton to="/" variant="dark-blue">FlatButton</FlatButton>
        <FlatButton to="/" variant="widget">FlatButton</FlatButton>
      </div>`,
    }),
    { info: true },
  )
