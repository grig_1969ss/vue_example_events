import { storiesOf } from '@storybook/vue'
import moment from 'moment'

import { dateTimes, dateTimesCase3 } from '~/.storybook/lib/dateTimes'
import DateTime from '~/components/common/DateTime'

const start = moment()
  .subtract(3, 'month')
  .format()
const end = moment()
  .add(3, 'month')
  .format()

storiesOf('Common/DateTime', module)
  .add(
    'default',
    () => ({
      components: { DateTime },
      data: () => ({
        start,
        end,
      }),
      template: `
      <div>
        <DateTime :start="start" :end="end" />
      </div>`,
    }),
    { info: {} },
  )
  .add(
    'sizes',
    () => ({
      components: { DateTime },
      data: () => ({
        start,
        end,
      }),
      template: `
      <div>
        default
        <DateTime :start="start" :end="end" /><br>
        medium
        <DateTime medium :start="start" :end="end" /><br>
        small
        <DateTime small :start="start" :end="end" /><br>
        smaller (no breakpoints)
        <DateTime smaller :start="start" :end="end" /><br>
        tiny (no breakpoints)
        <DateTime tiny :start="start" :end="end" /><br>
      </div>`,
    }),
    { info: {} },
  )
  .add(
    'color',
    () => ({
      components: { DateTime },
      data: () => ({
        start,
        end,
      }),
      template: `
      <div style="background: black;">
        <DateTime color="white" :start="start" :end="end" />
        <DateTime color="grey" :start="start" :end="end" />
      </div>`,
    }),
    { info: {} },
  )
  .add(
    'notWrap',
    () => ({
      components: { DateTime },
      data: () => ({
        start,
        end,
      }),
      template: `
      <div>
        <DateTime notWrap :start="start" :end="end" />
      </div>`,
    }),
    { info: {} },
  )
  .add(
    'format main',
    () => ({
      components: { DateTime },
      data: () => ({
        dateTimes,
      }),
      template: `
      <div>
        On event card: 360 screen -> 160 event card -> 144 datetime width, 728px -> 248px -> 208px
        <br><br>
        <div v-for="dt in dateTimes" :key="dt.start+dt.end">
          <div v-if="dt.desc"> {{ dt.desc }}</div>
          <div>
            start: {{ dt.start }} / end: {{ dt.end }}
          </div>
          <div style="width: 208px; border: 1px solid black;">
            <DateTime 
              format="main" 
              small 
              hasMeridiem
              :start="dt.start" 
              :end="dt.end"
            />
          </div>
          <br>
        </div>
      </div>`,
    }),
    { info: {} },
  )
  .add(
    'format main hasPeriodTimeYear',
    () => ({
      components: { DateTime },
      data: () => ({
        dateTimesCase3,
      }),
      template: `
      <div>
        <DateTime 
          v-for="(dt, idx) in dateTimesCase3"  
          :key="idx" 
          hasPeriodTimeYear 
          format="main"
          :start="dt.start" 
          :end="dt.end" 
        />
        style="width: 300px"
        <DateTime 
          style="width: 300px"
          v-for="(dt, idx) in dateTimesCase3"  
          :key="idx + 'key'" 
          format="main"
          hasPeriodTimeYear
          :start="dt.start" 
          :end="dt.end" 
        />
      </div>`,
    }),
    { info: {} },
  )
