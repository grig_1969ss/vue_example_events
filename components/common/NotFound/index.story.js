import { storiesOf } from '@storybook/vue'

import NotFound from '~/components/common/NotFound'

storiesOf('Common/NotFound', module).add(
  'default',
  () => ({
    components: { NotFound },
    template: `<div style="padding: 24px; background-color: #f8f9fa">
        <NotFound />
        <NotFound event-page />
      </div>`,
  }),
  { info: true },
)
