import { storiesOf } from '@storybook/vue'

import Counter from '~/components/common/Counter'

storiesOf('Common/Counter', module).add(
  'default',
  () => ({
    components: { Counter },
    data: () => ({
      value1: 0,
      value2: 0,
      config: {
        min: 0,
        max: 10,
        step: 1,
      },
    }),
    template: `
      <div style="padding: 24px;">
        <div>
          <h4 style="margin: 0 0 12px;">Default (you can hold mouse click on buttons):</h4>
          <div style="margin: 0 0 24px;">
            <counter v-model="value1" />
          </div>
        </div>
        <div style="margin-top: 36px;">
          <h4 style="margin: 0 0 12px;">With limits:</h4>
          <div style="margin: 0 0 24px;"">
            <h5 style="margin: 0">setup min:</h5>
            <counter v-model="config.min" :max="config.max" />
          </div>
          <div style="margin: 0 0 24px;"">
            <h5 style="margin: 0">setup max:</h5>
            <counter v-model="config.max" :min="config.min" />
          </div>
          <div style="margin: 0 0 24px;"">
            <h5 style="margin: 0">setup step:</h5>
            <counter v-model="config.step" :min="1" :max="10" :step="1" />
          </div>
          <div style="margin: 0 0 24px;">
            <h5 style="margin: 0">value:</h5>
            <counter v-model="value2" :min="config.min" :max="config.max" :step="config.step" />
          </div>
        </div>
      </div>`,
  }),
  { info: true },
)
