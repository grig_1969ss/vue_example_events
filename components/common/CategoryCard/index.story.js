import { storiesOf } from '@storybook/vue'

import CategoryCard from '.'
import { selected } from '~/.storybook/lib/categories'

storiesOf('Common/CategoryCard', module).add(
  'default',
  () => ({
    components: { CategoryCard },
    data: () => ({ categories: selected }),
    template: `
    <div style="
      padding: 24px; 
      display: grid; 
      grid-template-columns: repeat(auto-fill, 400px);
      grid-gap: 16px; 
      background: #f3f3f3;
    ">
      <CategoryCard v-for="(category, i) in categories" :key="i" :category="category" />
    </div>`
  }),
  { info: true }
)
