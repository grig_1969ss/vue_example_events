import path from 'path'
import { storiesOf } from '@storybook/vue'

let icons = []

function importAll(r) {
  r.keys().forEach(key => {
    icons.push(path.join('icons/', key))
  })
}

importAll(require.context('~/static/icons', true, /\.svg$/))

storiesOf('Common/Icon/', module).add(
  'default',
  () => ({
    components: {
      Icon: () => import('.'),
    },
    data: () => ({
      icons,
      manySizeIcon: 'cross.svg',
      sizes: ['xs', 's', 'm', 'l'],
      size: 'm',
    }),
    template: `
    <div style="padding: 24px; background-color: #f8f9fa">
      <div style="display: grid; grid-auto-flow: column; grid-gap: 16px; justify-content: start; align-items: center">
        <div v-for="(s, i) in sizes" :key="i" style="cursor: pointer" @click="size = s">
          <Icon 
            :src="manySizeIcon" 
            :size="s" :title="'size ' + s" 
            :style="{color: size === s ? '#0070ff' : 'black'}" 
          />
        </div>
        &lt;Icon src="{{ manySizeIcon }}" size="{{size}}" /&gt;
      </div>
      <br><br>
      <div style="display: grid; grid-template-columns: repeat(16, auto); grid-gap: 8px; justify-content: start">
        <div v-for="icon in icons" :key="icon" style="cursor: pointer" @click="manySizeIcon = icon">
          <Icon :src="icon" :title="icon" />
        </div>
      </div>
    </div>`,
  }),
  { info: true },
)
