import { storiesOf, addDecorator } from '@storybook/vue'
import Answer from './Answer'
import Comment from './Comment'
import CommentTree from './CommentTree'

import comments from '~/.storybook/lib/comments'

// addDecorator(() => ({
//   template: `
//     <div style="padding: 16px; background-color: #f8f9fa; max-width: 856px">
//     <story/>
//     </div>`
// }))

storiesOf('common/Comments', module)
  .add(
    'CommentTree',
    () => ({
      components: { CommentTree },
      data: () => ({ comments: comments }),
      template: `
      <div style="padding: 24px; width: 600px">
        <CommentTree :comments="comments" />
      </div>`,
    }),
    { info: true },
  )
  .add(
    'Comment',
    () => ({
      components: { Comment },
      data: () => ({ comments: comments }),
      template: `
      <div style="padding: 24px; width: 600px">
        <Comment 
          v-for="comment in comments" 
          :key="comment.id" 
          :comment="comment" 
          style="margin-bottom: 8px;" 
        />
      </div>`,
    }),
    { info: true },
  )
  .add(
    'Answer',
    () => ({
      components: { Answer },
      template: `
      <div style="padding: 24px; width: 600px">
        <Answer />
      </div>`,
    }),
    { info: true },
  )
