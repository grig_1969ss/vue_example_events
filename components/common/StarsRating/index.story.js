import { storiesOf } from '@storybook/vue'

import StarsRating from '~/components/common/StarsRating'

storiesOf('Common/StarsRating', module).add(
  'default',
  () => ({
    components: { StarsRating },
    template: `
<div style="padding: 24px;">
  <div>
    <StarsRating :value="4.3" />
  </div>
  <div>
    <StarsRating :value="3.7" :total="5" :stars="10" />
  </div>
  <div>
    <StarsRating :value="5" :total="10" />
  </div>
  <div>
    <StarsRating :value="0" />
  </div>
</div>`,
  }),
  { info: true },
)
