import { storiesOf } from '@storybook/vue'
import { event } from '~/.storybook/lib/dataEvents'

storiesOf('events/main/StickyPanel', module).add(
  'default',
  () => ({
    components: { StickyPanel: () => import('./StickyPanel') },
    data: () => ({ event }),
    template: `
    <div style="padding:24px;">
      &lt;StickyPanel /&gt;
      <StickyPanel />
      <br>
      &lt;StickyPanel :promo="true" /&gt;
      <StickyPanel promo />
    </div>`,
  }),
  { info: true },
)
