import { storiesOf } from '@storybook/vue'
import GoButton from './GoButton'
import users from '~/.storybook/lib/users'

storiesOf('events/main/GoButton', module).add(
  'default',
  () => ({
    components: { GoButton },
    data: () => ({ isUserVisitor: false, users }),
    template: `
      <div style="padding: 24px; background-color: #f8f9fa; height: 100vh;">
        <GoButton 
          :value="isUserVisitor"
          :count="+isUserVisitor"
          @click="isUserVisitor = !isUserVisitor"
        />
        <GoButton 
          :value="isUserVisitor"
          :visitors="users"
          :count="3+isUserVisitor"
          @click="isUserVisitor = !isUserVisitor"
        />
      </div>
`,
  }),
  { info: true },
)
