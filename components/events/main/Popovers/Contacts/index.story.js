import { storiesOf } from '@storybook/vue'
import ContactsPopover from '.'
import {
  fullEvent,
  equalInstitutions,
  differentNoOrgs,
} from '~/.storybook/lib/dataEventsContact'

storiesOf('events/main/Popovers/Contacts', module)
  .add(
    'full',
    () => ({
      components: { ContactsPopover },
      data: () => ({ event: fullEvent }),
      template: '<ContactsPopover :event="event" />',
    }),
    { info: true },
  )
  .add(
    'institution === location.institution',
    () => ({
      components: { ContactsPopover },
      data: () => ({ event: equalInstitutions }),
      template: '<ContactsPopover :event="event" />',
    }),
    { info: true },
  )
  .add(
    'institution === location.institution && isNullOrganizers',
    () => ({
      components: { ContactsPopover },
      data: () => ({ event: differentNoOrgs }),
      template: '<ContactsPopover :event="event" />',
    }),
    { info: true },
  )
