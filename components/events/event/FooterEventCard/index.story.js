import { storiesOf } from '@storybook/vue'
import FooterEventCard from '.'
import { event, dataEvents } from '~/.storybook/lib/dataEvents'

storiesOf('events/event/FooterEventCard', module)
  .add(
    'default',
    () => ({
      components: { FooterEventCard },
      data: () => ({
        event,
        dataEvents,
      }),
      template: `
    <div style="padding:24px; max-width: 500px; background: #0a1118">
      <FooterEventCard :event="event" />
      <FooterEventCard :event="dataEvents.eventOverflowTest" />
    </div>`,
    }),
    { info: true },
  )
  .add(
    'isBright',
    () => ({
      components: { FooterEventCard },
      data: () => ({
        event,
        dataEvents,
      }),
      template: `
    <div style="padding:24px; max-width: 500px;">
      <FooterEventCard isBright :event="event" />
    </div>`,
    }),
    { info: true },
  )
