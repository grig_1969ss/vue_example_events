import { storiesOf } from '@storybook/vue'
import schedule from '../../../../.storybook/lib/schedule'
import Schedule from './index'

storiesOf('events/Schedule', module).add(
  'default',
  () => ({
    components: { Schedule },
    data: () => ({ schedule }),
    template: `
      <div style="max-width: 800px; padding: 24px;">  
       <Schedule :sessions="schedule.results"/>
      </div>`,
  }),
  { info: true },
)
