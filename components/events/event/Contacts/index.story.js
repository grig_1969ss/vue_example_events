import { storiesOf } from '@storybook/vue'
import Contacts from '.'
import {
  fullEvent,
  equalInstitutions,
  differentNoOrgs,
} from '~/.storybook/lib/dataEventsContact'

storiesOf('events/main/Contacts', module)
  .add(
    'full',
    () => ({
      components: { Contacts },
      data: () => ({ event: fullEvent }),
      template: `
      <Contacts 
        :event="event" 
        style="padding: 112px; background-color: #f8f9fa"
      />`,
    }),
    { info: true },
  )
  .add(
    'institution === location.institution',
    () => ({
      components: { Contacts },
      data: () => ({ event: equalInstitutions }),
      template: `
      <Contacts 
        :event="event" 
        style="padding: 112px; background-color: #f8f9fa"
      />`,
    }),
    { info: true },
  )
  .add(
    'different institutions and no orgs',
    () => ({
      components: { Contacts },
      data: () => ({ event: differentNoOrgs }),
      template: `
      <Contacts 
        :event="event" 
        style="padding: 112px; background-color: #f8f9fa"
      />`,
    }),
    { info: true },
  )
