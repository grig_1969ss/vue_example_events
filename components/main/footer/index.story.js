import { storiesOf } from '@storybook/vue'
import NavList from './NavList'

storiesOf('main/footer/NavList', module).add(
  'default',
  () => ({
    components: { NavList },
    data: () => ({
      top: Array(12).fill({
        id: 524901,
        name: 'Москва',
        url_id: '524901-moscow',
      }),
      other: Array(36).fill({
        id: 524901,
        name: 'Москва',
        url_id: '524901-moscow',
      }),
    }),
    template: `
      <div style=" padding: 24px; "
      >  
          <NavList
            :top="top"
            :other="other"
          ></NavList>          
      </div>`,
  }),
  { info: true },
)
