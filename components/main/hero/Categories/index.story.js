import { storiesOf } from '@storybook/vue'
import Categories from '.'
import { filtered } from '~/.storybook/lib/categories'

storiesOf('main/hero/Categories', module).add(
  'default',
  () => ({
    components: { Categories },
    data: () => ({
      categories: filtered,
    }),
    template: `
      <div style=" padding: 24px; max-width: 600px"> 
          <Categories :value="categories" />          
      </div>`,
  }),
  { info: true },
)
