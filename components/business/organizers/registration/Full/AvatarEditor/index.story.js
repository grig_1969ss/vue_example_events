import { storiesOf } from '@storybook/vue'
import AvatarEditor from '.'
import * as images from '~/store/images'
import Vuex from 'vuex'

const store = new Vuex.Store({
  modules: { images },
})

storiesOf('business/AvatarEditor', module).add(
  'default',
  () => ({
    components: { AvatarEditor },
    store,
    data: () => ({
      src: '',
    }),
    template: `
    <div style="padding: 24px; background-color: #f8f9fa; width: 100%; max-width: 108px;">
    <AvatarEditor alt="dfdf ppp sss" />
      <AvatarEditor v-model="src" />
      src = {{ src }}
    </div>
    `,
  }),
  { info: true },
)
