import { storiesOf } from '@storybook/vue'
import Form from '.'
import { action } from '@storybook/addon-actions'

storiesOf('business/Form', module)
  .add(
    'default',
    () => ({
      components: { Form },
      data: () => ({
        form: {},
      }),
      methods: {
        log(e) {
          action('link target')(e)
        },
      },
      template: `
    <div style="padding: 24px; background-color: #a7daf9;">
      <Form @submit="log($event)" style="width: 100%; max-width: 1008px; margin: auto;" />
    </div>
    `,
    }),
    { info: true },
  )
  .add(
    'MyInput',
    () => ({
      components: { MyInput: () => import('./MyInput') },
      data: () => ({
        model: '',
        show: true,
      }),
      template: `
    <div style="padding: 24px; width: 400px;">
      <MyInput v-model="model" :show.sync="show">Test</MyInput>
      <MyInput v-model="model" password>Password</MyInput>
      <MyInput v-model="model">Без значка</MyInput>
      model = {{ model }} 
      show = {{ show }}
    </div>
    `,
    }),
    { info: true },
  )
