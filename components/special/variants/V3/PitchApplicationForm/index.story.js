import { storiesOf } from '@storybook/vue'

storiesOf('special/V3', module)
  .add(
    'PitchApplicationForm',
    () => ({
      components: { PitchApplicationForm: () => import('.') },
      template: `
      <div style="
        padding: 24px;
        background-color: #f8f9fa;
        max-width: 856px"
      >
        <PitchApplicationForm color="#d29c21" />
      </div>`,
    }),
    { info: true },
  )
  .add(
    'FormInput',
    () => ({
      components: {
        FormInput: () => import('./FormInput'),
        Icon: () => import('~/components/common/Icon'),
      },
      data: () => ({
        text: 'example',
        link: `Link <a href="https://www.thenextwomen.com/fff-program/">https://www.thenextwomen.com/fff-program/</a>`,
      }),
      template: `
      <div style="
        display: grid;
        grid-gap: 16px;
        padding: 24px;
        background-color: #f8f9fa;
        max-width: 856px"
      >
        <FormInput v-model="text" :label="link">
          What is the email address where we can reach you?
        </FormInput>
        <FormInput v-model="text">
          What is the email address where we can reach you?
          <template #hint>
            Preferably not a generic email address.
          </template>
        </FormInput>
        <FormInput>
          What is the email address where we can reach you?
          <template #hint>
            Preferably not a generic email address.
          </template>
          <template #placeholder>
            <Icon src="icons/map-point.svg" />
          </template>
        </FormInput>
      </div>`,
    }),
    { info: true },
  )
  .add(
    'FormTextarea',
    () => ({
      components: { FormTextarea: () => import('./FormTextarea') },
      data: () => ({
        text: 'example',
        link: `Link <a href="https://www.thenextwomen.com/fff-program/">https://www.thenextwomen.com/fff-program/</a>`,
      }),
      template: `
      <div style="
        display: grid;
        grid-gap: 16px;
        padding: 24px;
        background-color: #f8f9fa;
        max-width: 856px"
      >
        <FormTextarea v-model="text" :label="link">
          What is the email address where we can reach you?
        </FormTextarea>
        <FormTextarea v-model="text">
          What is the email address where we can reach you?
          <template #hint>
            Preferably not a generic email address.
          </template>
        </FormTextarea>
      </div>`,
    }),
    { info: true },
  )
  .add(
    'FormCheckbox',
    () => ({
      components: { FormCheckbox: () => import('./FormCheckbox') },
      data: () => ({
        text: 'example',
        link: `Link <a href="https://www.thenextwomen.com/fff-program/">https://www.thenextwomen.com/fff-program/</a>`,
        options: ['Buenos Aires', 'Lisbon', 'Tel Aviv', 'Amsterdam'],
      }),
      template: `
      <div style="
        display: grid;
        grid-gap: 16px;
        padding: 24px;
        background-color: #f8f9fa;
        max-width: 856px"
      >
        <FormCheckbox v-model="text" :options="options">
          What is the email address where we can reach you?
        </FormCheckbox>
        <FormCheckbox v-model="text" :options="options" color="#d29c21">
          What is the email address where we can reach you?
          <template #hint>
            Preferably not a generic email address.
          </template>
        </FormCheckbox>
        <FormCheckbox v-model="text" :options="['yes', 'no']" :label="link" color="#d29c21">
          <template #hint>
            Preferably not a generic email address.
          </template>
        </FormCheckbox>
        <FormCheckbox v-model="text" :options="[{ title: 'agree', value: true }]" :label="link" color="#d29c21">
          <template #hint>
            Preferably not a generic email address.
          </template>
        </FormCheckbox>
      </div>`,
    }),
    { info: true },
  )
