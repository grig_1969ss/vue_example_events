import { storiesOf } from '@storybook/vue'
import images from '~/.storybook/lib/images'

storiesOf('special/V3', module).add(
  'Gallery',
  () => ({
    components: { Gallery: () => import('./Gallery') },
    data: () => ({ images, videos: [] }),
    template: `
      <div style="
        padding: 24px;
        background-color: #f8f9fa;
        max-width: 856px"
      >
        <Gallery :images="images" :videos="videos" color="#d29c21" />
      </div>`,
  }),
  { info: true },
)
