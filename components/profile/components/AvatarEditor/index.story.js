import { storiesOf } from '@storybook/vue'
import Cropper from './Cropper'

storiesOf('profile/edit/AvatarEditor', module).add(
  'Cropper',
  () => ({
    components: { Cropper },
    data: () => ({
      image:
        'https://graph.facebook.com/' +
        '1387819034852828/picture' +
        '?width=450&height=450',
      src: '',
    }),
    computed: {},
    template: `
      <div style="
        display: flex;
        flex-direction: column;
        align-items: center;"
      >
        <Cropper :value="image" @change="event => src = event" />
        <img :src="src" alt="" style="margin-top: 24px">
      </div>`,
  }),
  { info: true },
)
