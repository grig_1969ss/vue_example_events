import { storiesOf } from '@storybook/vue'
import Carousel from '.'

storiesOf('about/Carousel', module).add(
  'default',
  () => ({
    components: { Carousel },
    data: () => ({
      data: [
        {
          img: 'about/Guneri',
          name: 'Kate Guneri, CMO, SmartBrain',
          review:
            'WhatWhere.World helped to bring attention to our international events and launched successful promo campaigns without having me to even move a finger.',
        },
        {
          img: 'about/Terry-Wilson',
          name: 'Terry Wilson, Event organizer',
          review:
            'Organizing an event in a small town is hard. We were struggling with attendance at our events and WhatWhere.World came in very handy.',
        },
        {
          img: 'about/Diane-Volbeat',
          name: 'Diane Volbeat, Event organizer',
          review:
            'Great service even for a compact budget. Easy to launch and to monitor results. Thank you.',
        },
      ],
    }),
    template: `
    <div style="padding: 24px; background-color: #7f828b">
      <Carousel :data="data" />
    </div>
    `,
  }),
  { info: true },
)
