import moment from 'moment'
import { isEmpty, path, isNil, prop } from 'ramda'
import {
  required,
  helpers,
  decimal,
  numeric,
  email,
  maxLength,
  minLength,
  maxValue,
  minValue,
  requiredIf,
} from 'vuelidate/lib/validators'
import { i18n } from '~/plugins/i18n'


export function buildField(field) {
  const now = moment()
  const params = {
    key: field.key,
    label: field.label,
    placeholder: field.placeholder
  }
  if ( !isEmpty(field.options) ){
    Object.assign(params, {
      component: 'AutoSelect',
      componentProps: {
        options: field.options,
        trackBy: 'value',
        allowEmpty: !field.is_required
      }
    })
  } else {
    if (field.type === 'bool') {
      Object.assign(params, {
        component: 'AutoRadio',
        componentProps: {
          options: [
            { 'id': `${field.key}-true`, 'value': "1", label: i18n.t('common.yes') },
            { 'id': `${field.key}-false`, 'value': "0", label: i18n.t('common.no') }
          ]
        }
      })
    } else if (field.type === 'int' || field.type === 'float') {
      Object.assign(params, {
        component: 'AutoInput',
        componentProps: { type: 'number' }
      })
    } else if (field.type === 'text') {
      Object.assign(params, {
        component: 'AutoInput',
        componentProps: {
          component: 'textarea'
        }
      })
    } else if (field.type === 'date') {
      Object.assign(params, {
        component: 'AutoDatepicker',
        componentProps: {
          placeholder: `${now.format('DD MMM, ddd')}`
        }
      })
    } else if (field.type === 'time') {
      Object.assign(params, {
        component: 'AutoInput',
        componentProps: {
          component: 'TheMask',
          mask: '##:##',
          masked: true,
          placeholder: `${now.format('HH:mm')}`,
        }
      })
    } else if (field.type === 'email') {
      Object.assign(params, {
        component: 'AutoInput',
        componentProps: {
          type: 'email',
        }
      })
    } else if (field.type === 'phone') {
      Object.assign(params, {
        component: 'AutoInput',
        componentProps: {
          type: 'tel',
        }
      })
    } else {
      Object.assign(params, {
        component: 'AutoInput',
        componentProps: {}
      })
    }
  }

  if (field.is_interval) {
    Object.assign(params, {
      component: 'AutoInterval',
      start: buildField(path(['interval', 'start'], field)),
      end: buildField(path(['interval', 'end'], field)),
    })
    params.start.placeholder = params.start.placeholder || params.start.label
    params.start.label = null
    params.end.placeholder = params.end.placeholder || params.end.label
    params.end.label = null
  }
  return params
}

function build_validator(key, value){
  if (key === 'max_value') {
    return {maxValue: maxValue(value)}
  }
  if (key === 'min_value') {
    return {minValue: minValue(value)}
  }
  if (key === 'max_length') {
    return {maxLength: maxLength(value)}
  }
  if (key === 'min_length') {
    return {minLength: minLength(value)}
  }
  if (key === 'regex') {
    const regexValidator = helpers.regex('alpha', new RegExp(value))
    return {regex: regexValidator}
  }
}

export function buildFieldValidation(field) {
  let data = {}
  const validator = {}

  if (field.is_required)
    data.required = required
  if (field.type === 'int')
    data.numeric = numeric
  if (field.type === 'email')
    data.email = email
  else if (field.type === 'float')
    data.numeric = decimal
  for (const [key, value] of Object.entries(field.validators)) {
    if (isNil(value)) continue
    Object.assign(data, build_validator(key, value))
  }

  if ( field.is_interval ) {
    const start_validator = path(['interval', 'start'], field)
      ? buildFieldValidation(field.interval.start)
      : {start: {}}
    const end_validator = path(['interval', 'end'], field)
      ? buildFieldValidation(field.interval.end)
      : {end: {}}

    const start = Object.values(start_validator)[0] || {}
    const end = Object.values(end_validator)[0] || {}

    Object.assign(start, data)
    Object.assign(end, data)
    data = {start: start, end: end}
  }

  validator[field.key] = data
  return validator
}

function castValueType(formField, value){
  const val = {
    value,
    display: value
  }
  if (formField.type === 'number'){
      val.value = Number.parseInt(value)
      val.display = value
    } else if (formField.type === 'decimal') {
      val.value = Number.parseFloat(value)
      val.display = val.value.toFixed(2)
    } else if (formField.type === 'bool') {
      val.value = value
      val.display = val.value ? i18n.t('common.yes'): i18n.t('common.no')
    } else if (formField.type === 'date' && val.value) {
      try {
        val.value = moment(value, 'YYYY-MM-DD')
        val.display = val.value.format('D MMMM, YYYY')
      } catch (e) {
        console.error(e)
      }
    }
  return val
}

export function serializeValue(key, value, formField){

  if ( !formField ){
    return null
  }
  const valueObj = {
    value: value,
    label: formField.label || key,
    type: formField.type,
    isInterval: formField.is_interval
  }
  if (formField.is_interval) {
    const start = castValueType(formField, value.start)
    const end = castValueType(formField, value.end)
    valueObj.value = {
      start: start.value,
      end: end.value
    }
    if (!isNil(start.display) && !isNil(end.display)){
      valueObj.display = `${start.display} — ${end.display}`
    } else if (!isNil(start.display)) {
      valueObj.display = `${i18n.t('common.from')} ${start.display}`
    } else if (!isNil(end.display)) {
      valueObj.display = `${i18n.t('common.to')} ${end.display}`
    }
  } else {
    Object.assign(valueObj, castValueType(formField, valueObj.value))
  }
  if (formField.options.length){
    const option = formField.options.filter(item => item.value === value)[0]
    if ( option ){
      valueObj.display = option.display
    }
  }
  valueObj.display = isNil(valueObj.display) || isEmpty(valueObj.display)
    ? ' - '
    : valueObj.display
  return valueObj
}

export function getValidations(orderFormScheme) {
  const defaults = {
    title: { required, maxLength: maxLength(255) },
    city: { required },
    price_type: { required },
    price: {
      required: requiredIf(function (data) {
        return data.price_type === 'simple'
      })
    },
    price_interval_from: {
      required: requiredIf(function (data) {
        return data.price_type === 'interval'
      })
    },
    price_interval_to: {
      required: requiredIf(function (data) {
        return data.price_type === 'interval'
      })
    },
    description: { required },
  }
  const fields = prop(['fields'], orderFormScheme) || []
  const extraValidators = {}
  fields.forEach(
    field => Object.assign(extraValidators, buildFieldValidation(field))
  )
  return {
    form: {
      ...defaults,
      ...extraValidators
    }
  }
}
