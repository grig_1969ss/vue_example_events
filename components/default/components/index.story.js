import { storiesOf } from '@storybook/vue'

import Logo from './Logo'
import Menu from './Menu'
import Question from './Support'
import Search from './SearchSide'
import CreateEvent from './CreateEvent'
import Login from './Login'

import users from '~/.storybook/lib/users'

storiesOf('default/components', module)
  .add(
    'other',
    () => ({
      components: { CreateEvent, Menu, Question, Search },
      data: () => ({ user: users[0] }),
      template: `
      <div style="padding: 24px; display: grid; grid-gap: 16px;">
        <Menu />
        <Question />
        <Search />
      </div>`,
    }),
    { info: {} },
  )
  .add(
    'Logo',
    () => ({
      components: { Logo },
      data: () => ({ user: users[0] }),
      template: `
      <div style="padding: 24px;">
        mobile 64x64 <Logo style="height: 64px; width: 64px"/>
        <br>
        desktop 112x112 <Logo style="height: 112px; width: 112px"/>
      </div>`,
    }),
    { info: {} },
  )
  .add(
    'createEvent',
    () => ({
      components: { CreateEvent },
      template: `
      <div style="padding: 24px;">
        <CreateEvent/>
      </div>`,
    }),
    { info: {} },
  )
  .add(
    'Login',
    () => ({
      components: { Login },
      data: () => ({ user: users[0] }),
      template: `
      <div style="
        padding: 24px; 
        display: grid; 
        grid-template-columns: 100px 200px; 
        grid-gap: 16px; 
        justify-items: flex-end;"
      >
        mobile: 
        <Login :user="user" />
        mobile auth: 
        <Login :user="user" is-auth />
        desktop: 
        <Login :user="user" type="full">
          <template #login>{{ $t('auth.login') }}</template>
          <template #logout>{{ $t('auth.logout') }}</template>
        </Login>
        desktop auth:
        <Login :user="user" type="full" is-auth>
          <template #login>{{ $t('auth.login') }}</template>
          <template #logout>{{ $t('auth.logout') }}</template>
        </Login>
      </div>`,
    }),
    { info: {} },
  )
