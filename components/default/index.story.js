import { storiesOf } from '@storybook/vue'

import Side from './Side'
import Top from './Top'
import MobileTop from './MobileTop'

import LoremIpsum from '~/.storybook/lib/loremIpsum'

storiesOf('default', module)
  .add(
    'desktop',
    () => ({
      components: { Side, Top },
      data: () => ({ text: LoremIpsum }),
      template: `
      <div style="background-color: #f8f9fa; height: 100vh">
        <div style="position: fixed; left: 0; top: 0" >
          <Side />
        </div>
        
          <Top />
        <div class="wrapper" style="margin-top: 24px">
          {{text}}
        </div>
      </div>`,
    }),
    { info: {} },
  )
  .add(
    'mobile',
    () => ({
      components: { MobileTop },
      data: () => ({ text: LoremIpsum }),
      template: `
      <div style="background-color: #f8f9fa">
        <MobileTop />
        <div class="wrapper" style="margin-top: 24px">
          {{text}}
        </div>
      </div>`,
    }),
    { info: {} },
  )
