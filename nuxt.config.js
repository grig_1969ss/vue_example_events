import { format } from 'date-fns'
require('dotenv').config()

const path = require('path')
const CompressionPlugin = require('compression-webpack-plugin');
const S3Plugin = require('ww-webpack-s3-uploader')
const klawSync = require('klaw-sync')
const MomentLocalesPlugin = require('moment-locales-webpack-plugin')

let REVISION
let PUBLIC_PATH = '/static/'
let STATIC_PATH = process.env.STATIC_PATH || '/static/'
const ENVIRONMENT = process.env.ENVIRONMENT || 'local'

const IS_DEPLOY_TO_S3_NEEDED = Boolean(
  process.env.AWS_ACCESS_KEY_ID &&
    process.env.AWS_SECRET_ACCESS_KEY &&
    process.env.TC_AWS_REGION &&
    process.env.TC_AWS_STORAGE_BUCKET
)
if (IS_DEPLOY_TO_S3_NEEDED) {
  REVISION = `fe/${format(Date.now(), 'yyyyMMdd')}${process.env.COMMIT_SHORT_SHA}`
  PUBLIC_PATH = `/static/${REVISION}/`
  STATIC_PATH = PUBLIC_PATH
}

module.exports = {
  debug: process.env.ENVIRONMENT === 'local',
  rootDir: __dirname,
  env: {
    enableSupportChat: process.env.ENABLE_SUPPORT_CHAT === 'true',
    GTM_ID: process.env.GTM_ID,
    apiUrl: process.env.API_URL,
    domainUrl: process.env.BASE_URL,
    thumborUrl: process.env.THUMBOR_URL,
    thumborKey: process.env.THUMBOR_KEY,
    sentryUrl: process.env.SENTRY_URL,
    vkId: process.env.VK_ID,
    fbId: process.env.FB_ID,
    googleId: process.env.GOOGLE_ID,
    secretSalt: 'verysecretsalt',
    STATIC_PATH,
    WIDGET_PATH: process.env.WIDGET_PATH,
  },
  head: {
    htmlAttrs: {
      lang: 'ru',
    },
    __dangerouslyDisableSanitizersByTagID: {
      fbscript: ['innerHTML'],
      yandm: ['innerHTML'],
    },
    script: [
      {
        hid: 'fbscript',
        innerHTML:
          '(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;   js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/ru_RU/all.js";fjs.parentNode.insertBefore(js, fjs);}(document, "script", "facebook-jssdk"));',
      },
    ],
    meta: [
      { charset: 'utf-8' },
      { property: 'fb:app_id', content: process.env.FB_ID },
      { hid: 'ogtype', property: 'og:type', content: 'website' },
      { hid: 'ogUrl', property: 'og:url', content: `https://whatwhere.world` },
      {
        hid: 'ogTitle',
        property: 'og:title',
        content: 'WhatWhere.World',
      },
      {
        hid: 'img',
        property: 'og:image',
        content: STATIC_PATH + 'imgs/fb.png',
      },
      { hid: 'imgwidth', property: 'og:image:width', content: '1200' },
      { hid: 'imgheight', property: 'og:image:height', content: '600' },
      { property: 'og:site_name', content: 'WhatWhere.World' },
      {
        hid: 'vkImg',
        property: 'vk:image',
        content: STATIC_PATH + 'imgs/fb.png',
      },
      { name: 'twitter:card', content: 'summary' },
    ],
    link: [
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: STATIC_PATH + '180.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: STATIC_PATH + '32.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: STATIC_PATH + '16.png',
      },
      { rel: 'manifest', href: STATIC_PATH + 'manifest.json' },
    ],
  },
  mode: 'universal',
  loading: {
    color: '#0070ff',
    height: '2px',
  },
  css: [
    '@/assets/css/normalize.css',
    '@/assets/fonts/fonts.css',
    '@/assets/css/variables.css',
  ],
  plugins: [
    { src: '~/plugins/detect', mode: 'server' },
    { src: '~/plugins/moment' },
    { src: '~/plugins/vuelidate' },
    { src: '~/plugins/directives' },
    { src: '~/plugins/i18n' },
    { src: '~/plugins/axios' },
    { src: '~/plugins/v-tooltip', mode: 'client' },
    { src: '~/plugins/cookie-universal' },
    { src: '~/plugins/vue2-google-maps', mode: 'client' },
    { src: '~/plugins/vue-slider-component', mode: 'client' },
    { src: '~/plugins/vue-the-mask', mode: 'client' },
    { src: '~/plugins/vue2-datepicker', mode: 'client' },
    { src: '~/plugins/vue-carousel', mode: 'client' },
    { src: '~/plugins/vue-youtube', mode: 'client' },
    { src: '~/plugins/vuejs-paginate', mode: 'client' },
    { src: '~/plugins/drag-scroll', mode: 'client' },
    { src: '~/plugins/click-outside', mode: 'client' },
    { src: '~/plugins/vue2-editor', mode: 'client' },
    { src: '~/plugins/localStorage', mode: 'client' },
    { src: '~/plugins/vue-observe-visibility'},
  ],
  serverMiddleware: [
    // '~/middleware/server-debug',
    '~/middleware/trailing-slash.js',
  ],
  polyfill: {
    features: [
      {
        require: 'smoothscroll-polyfill',
        detect: () =>
          'scrollBehavior' in document.documentElement.style &&
          window.__forceSmoothScrollPolyfill__ !== true,
        install: smoothscroll => smoothscroll.polyfill(),
      },
      {
        require: 'intersection-observer',
        detect: () => 'IntersectionObserver' in window,
      },
    ],
  },
  buildModules: [
    '@nuxtjs/gtm',
  ],
  gtm: {
    id: process.env.GTM_ID,
    dev: ENVIRONMENT === 'local',
  },
  modules: [
    ['vue-scrollto/nuxt'],
    [
      '@nuxtjs/axios',
      {
        progress: false,
        proxyHeaders: false,
      },
    ],
    '@nuxtjs/sentry',
    'nuxt-polyfill',
  ],
  router: {
    middleware: ['trailing-slash-client'],
  },
  build: {
    parallel: true,
    publicPath: PUBLIC_PATH,
    cssSourceMap: process.env.DISABLE_CSS_SOURCEMAP !== 'true',
    extend(config, { isClient }) {
      // Extend only webpack config for client-bundle
      if (isClient) {
        config.devtool =
          process.env.NODE_ENV === 'production' ? false : '#eval-source-map'
      }

      config.module.rules.push({
        resourceQuery: /blockType=i18n/,
        type: 'javascript/auto',
        loader: '@kazupon/vue-i18n-loader',
      })

      config.plugins.push(
        new MomentLocalesPlugin({
          localesToKeep: ['ru', 'es'],
        }),
      )

      if (IS_DEPLOY_TO_S3_NEEDED && isClient) {
        const { options } = this.buildContext.nuxt
        const staticBasePath = path.join(__dirname, options.dir.static)
        const staticFiles = klawSync(staticBasePath, { nodir: true }).map(
          file => ({
            path: file.path,
            name: file.path.slice(staticBasePath.length),
          }),
        )

        config.output.futureEmitAssets = false // S3Plugin not working without this
        config.plugins.push(
          new CompressionPlugin({
            test: /\.(js|css|svg)$/,
            algorithm: 'gzip',
          }),
          new S3Plugin({
            basePath: REVISION,
            progress: false, // `progress` option break nuxt's progress bar
            s3Options: {
              accessKeyId: process.env.AWS_ACCESS_KEY_ID,
              secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
              region: process.env.TC_AWS_REGION,
            },
            s3UploadOptions: {
              Bucket: process.env.TC_AWS_STORAGE_BUCKET,
              CacheControl: 'max-age=15552000',
              // ContentEncoding(fileName) {
              //   if (/\.(gz)$/.test(fileName)) {
              //     return 'gzip'
              //   }
              // },
              // Here we set the Content-Type header for the gzipped files to their appropriate values, so the browser can interpret them properly
              ContentType(fileName) {
                if (/\.css/.test(fileName)) {
                  return 'text/css'
                }
                if (/\.svg/.test(fileName)) {
                  return 'image/svg+xml'
                }
                if (/\.js/.test(fileName)) {
                  return 'application/javascript'
                }
              }
            },
            additionalFiles: staticFiles,
          }),
        )
      }
    },
    transpile: [/^vue2-google-maps($|\/)/],
  },
  sentry: {
    dsn: process.env.SENTRY_URL,
    disabled: ENVIRONMENT === 'local',
    config: {},
    serverIntegrations: {
      ExtraErrorData: {},
      RewriteFrames: {},
      Transaction: {},
    },
    clientIntegrations: {
      ExtraErrorData: {},
      ReportingObserver: {},
      RewriteFrames: {},
      Vue: { attachProps: true },
    },
  },
  generate: {
    routes: [
      '/widget/horizontal/',
      '/widget/vertical/',
      '/widget/map/',
      '/widget/search/',
    ],
    exclude: [
      /^(?!.*\bwidget\b).*$/
    ]
  },
}
