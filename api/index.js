import { compilePath } from '~/utils'

/**
 * generic function that could be used to modify the request params
 * most important feature is `url` `urlParams` properties
 * e.g. use it like
 *
 * `request({ url: '/:id/:type/, urlParams: { id, type }})`
 *  will parse `:id` and `:type` to actual values contains in `urlParams`
 *
 * @typedef {Object} payload
 * @prop {string} url
 * @prop {'get' | 'post' | 'patch' | 'delete'} method
 * @prop {any} params
 * @prop {any} urlParams
 * @prop {any} body
 * @prop {any} headers
 */
export const request = (instance, { responseType, ...payload }) =>
  instance({
    url: compilePath(payload.url, payload.urlParams || {}),
    method: payload.method || 'GET',
    params: payload.params || {},
    data: payload.body || {},
    headers: payload.headers || {},
    baseURL: `${process.env.apiUrl}/api/2.0`,
    ...(responseType && { responseType }),
    ...payload,
  })
/**
 * @returns
 */
export const journals = instance => {
  const bUrl = '/journals/'
  const query = payload =>
    compilePath(payload.url || '', payload.urlParams || {})
  return {
    get: payload =>
      request(instance, {
        ...payload,
        method: 'GET',
        url: bUrl + query(payload),
      }),
    post: payload =>
      request(instance, {
        ...payload,
        method: 'POST',
        url: bUrl + query(payload),
      }),
    patch: payload =>
      request(instance, {
        ...payload,
        method: 'PATCH',
        url: bUrl + query(payload),
      }),
    delete: payload =>
      request(instance, {
        ...payload,
        method: 'DELETE',
        url: bUrl + query(payload),
      }),
  }
}

export const infoRequest = instance => {
  const url = '/requests/'

  return {
    post: payload => request(instance, { ...payload, method: 'POST', url }),
  }
}

export const getTimeZones = (instance, payload) => {
  const url = '/timezones/'

  return request(instance, {
    method: 'GET',
    url,
    ...payload,
  })
}

export const getCities = (instance, payload) => {
  const url = '/geo/cities/'

  return request(instance, {
    method: 'GET',
    url,
    ...payload,
  })
}

export const getLanguages = (instance, payload) => {
  const url = '/languages/'

  return request(instance, {
    method: 'GET',
    url,
    ...payload,
  })
}

export const getTags = (instance, payload) => {
  const url = '/tags/'

  return request(instance, {
    method: 'GET',
    url,
    ...payload,
  })
}

export const getCategories = (instance, payload) => {
  const url = '/categories/'

  return request(instance, {
    method: 'GET',
    url,
    ...payload,
  })
}

export const getEventTypes = (instance, payload) => {
  const url = '/event_types/'

  return request(instance, {
    method: 'GET',
    url,
    ...payload,
  })
}

export const getGenres = (instance, payload) => {
  const url = '/genres/'

  return request(instance, {
    method: 'GET',
    url,
    ...payload,
  })
}

export const getPromoEvents = (instance, payload) => {
  const url = '/promo/events/'

  return request(instance, {
    method: 'GET',
    url,
    ...payload,
  })
}

export const getEventById = (instance, { id, ...payload }) => {
  const url = `/events/${id}`

  return request(instance, {
    method: 'GET',
    url,
    ...payload,
  })
}

export const getTicketsSchemes = (instance, payload) => {
  const url = '/tickets/schemes/'

  return request(instance, {
    method: 'GET',
    url,
    ...payload,
  })
}

export const deleteTicketsSchemeById = (instance, { id, ...payload }) => {
  const url = `/tickets/schemes/${id}/`

  return request(instance, {
    method: 'DELETE',
    url,
    ...payload,
  })
}

export const getPaymentCurrencies = (instance, payload) => {
  const url = '/payments/currencies/'

  return request(instance, {
    method: 'GET',
    url,
    ...payload,
  })
}

export const getTicketByKey = (instance, { key, ...payload }) => {
  const url = `/tickets/${key}/`

  return request(instance, {
    method: 'GET',
    url,
    ...payload,
  })
}

export const getTickets = (instance, { key, ...payload }) => {
  const url = `/tickets/`

  return request(instance, {
    method: 'GET',
    url,
    ...payload,
  })
}

export const validateTicketByKey = (instance, { key, ...payload }) => {
  const url = `/tickets/${key}/take/`

  return request(instance, {
    method: 'POST',
    url,
    ...payload,
  })
}

export const generateTicketsListByEventId = (instance, { id, ...payload }) => {
  const url = `/events/${id}/tickets/generate_list/`

  return request(instance, {
    method: 'GET',
    responseType: 'arraybuffer',
    url,
    ...payload,
  })
}
