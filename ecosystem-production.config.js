const { join } = require('path')

// Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
module.exports = {
  apps: [
    {
      name: 'WhatWhere.World',
      script: './node_modules/nuxt-start/bin/nuxt-start.js',
      exec_mode: 'cluster',
      watch: false,
      instances: 'max',
      merge_logs: true,
      log_type: 'raw',
      cwd: './',
      args: `-c ${join(__dirname, 'nuxt.config.js')}`,
      env: {
        NODE_ENV: 'development',
      },
      env_production: {
        HOST: '0.0.0.0',
        NODE_ENV: 'production',
      },
    },
  ],
}
