# WhatWhereWorld

### Style

#### Visibility

Conditionally display an element based upon the current viewport. These classes can be applied using the following format `hidden-{breakpoint}-{condition}`

Breakpoints:

- `mobile` - from 0px to 759px _(same as desktopSD - 1px)_
- `desktopSD` - from 768px to (desktopHD - 1px)
- `desktopHD` - from 1024px to (sideShow - 1px)
- `sideShow` - from 1280px to (desktopHDp - 1px)
- `desktopHDp` - from 1600px to (desktopFullHD - 1px)
- `desktopFullHD` - from 1920px to ∞

Conditions:

- `only` - hide the element only on the specified breakpoint
- `and-down` - hide the element on the specified breakpoint and down
- `and-up` - hide the element on the specified breakpoint and up

### Подключение и использование статики

_`STATIC_PATH` - эта переменная окружения доступна в рантайме и всегда заканчивается слэшем._

1. Конкатенация `${STATIC_PATH}путь/к/файлу`:

   ```javascript
   export default {
     // ...
     head() {
       return {
         // ...
         meta: [
           {
             hid: 'img',
             property: 'og:image',
             content: process.env.STATIC_PATH + `imgs/bigevent_bg.jpg`
           }
         ]
       }
     }
   }
   ```

   ```vue
   <script :src="`${STATIC_PATH}js/iframeResizer.contentWindow.min.js`" />
   ```

   _В браузер попадёт просто полный путь к файлу, в исходном виде, с добавленным в начало доменом - такие файлы через вебпак не проходят, не сжимаются, урлы на хэши не заменяются._

2. Алиасы `~assets` и `~static` в html-шаблонах и в css:

   - `background: url('~assets/...')`
   - `background: url('~static/...');`
   - `<img src="~assets/..." >`
   - `<source srcset="~static/... 1x, ~static/... 2x" />`

   _Не `~/assets`, а `~assets` - без ведущего слэша. Такие файлы проходят через вебпак и его лоадеры - файлы жмутся, пути меняются на хэш, картинки заменяются на base64 - всё в соответствии с настройками вебпака._

3. Динамические пути к статическим файлам.

   ```vue
   <img :src="require(`~/static/promo/${icons[index]}.svg`)" />
   ```

   ```javascript
   // 1. лучше делать так:
   const icon = 'tools'
   require(`~/static/promo/${icon}.svg`)

   // 2. но не так:
   const icon = 'promo/tools.svg'
   require(`~/static/${icon}`)
   ```

   _Не `~assets`, а `~/assets` - с ведущим слэшем._

   Из-за вебпаковских контекстов в динамических путях, необходимо придерживаться следующего правила:
   чтобы в переменной содержалось только имя файла (или даже только часть имени), без директорий и по возможности без расширения. В первом варианте вебпак засунет в бандл только содержимое папки `static/promo` и только svg-файлы, а во втором - всю `static`.

   Но можно делать проще - без всяких `require`-ов:

   ```vue
   <template>
     <img :src="`${STATIC_PATH}promo/${icons[index]}.svg`" />
   </template>

   <script>
   export default {
     computed: {
       STATIC_PATH: () => process.env.STATIC_PATH
     }

     // или так
     // created() {
     //   this.STATIC_PATH = process.env.STATIC_PATH;
     // }
   }
   </script>
   ```
   
   
   
